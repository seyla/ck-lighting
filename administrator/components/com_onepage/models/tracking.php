<?php
/**
 * @version		$Id: cache.php 21518 2011-06-10 21:38:12Z chdemko $
 * @package		Joomla.Administrator
 * @subpackage	com_cache
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Cache Model
 *
 * @package		Joomla.Administrator
 * @subpackage	com_cache
 * @since		1.6
 */
class JModelTracking extends OPCModel
{

  function store()
  {
  
     require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'mini.php'); 
	 if (!OPCmini::tableExists('virtuemart_plg_opctracking'))
	 {
	   $db = JFactory::getDBO(); 
	   $q = '
	CREATE TABLE IF NOT EXISTS `#__virtuemart_plg_opctracking` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`virtuemart_order_id` int(11) NOT NULL,
	`hash` varchar(32) NOT NULL,
	`shown` text NOT NULL,
	`created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`created_by` int(11) NOT NULL,
	`modified` datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\',
	`modified_by` int(11) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `hash_2` (`hash`,`virtuemart_order_id`),
	KEY `virtuemart_order_id` (`virtuemart_order_id`),
	KEY `hash` (`hash`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;'; 
	$db->setQuery($q); 
	$db->query(); 
	$e = $db->getErrorMsg(); if (!empty($e)) return $e; 
	 }
    require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'models'.DS.'config.php'); 
	if (!OPCJ3)
	{
	 require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opcparameters.php'); 
	}
	else
	{
	   require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'jformrender.php'); 
	}
	 require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'config.php'); 
		 $config = new JModelConfig(); 
		 $config->loadVmConfig(); 
		 $files = $config->getPhpTrackingThemes();
		 $statuses = $config->getOrderStatuses();
		 $data = JRequest::get('post');
		  jimport('joomla.filesystem.file');
     foreach ($files as $file)
	 {
	  
	   $file = JFile::makeSafe($file);
	
	   $path = JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'trackers'.DS.'php'.DS.$file.'.xml'; 
	   $nd = new stdClass(); 
	   
	   //$params = new OPCparameters($nd, $file, $path, 'opctracking'); 
	   $config = OPCconfig::buildObject($data[$file]); 
	   
	   //var_dump($data[$file]); die(); 
	   
	   OPCconfig::store('tracking_config', $file, 0, $config); 
	   
	   if (false)
	   foreach ($data[$file] as $key=>$param)
	    {
		  echo $key.' '.$param; 
		}
	   
	   
	 }
	 
	 foreach ($statuses as $status)
	 {
	    $status2 = $status['order_status_code']; 
	    $config = new stdClass(); 
	    foreach ($files as $file)
		{
		  $enabled = JRequest::getVar($file.'_'.$status2, 0); 
		  $config->$file = $enabled; 
		}
		$config->code = JRequest::getVar('adwords_code_'.$status2, '', 'post', 'string', JREQUEST_ALLOWRAW); 
		$only_when = JRequest::getVar('opc_tracking_only_when_'.$status2, ''); 
		$config->only_when = $only_when; 
		OPCconfig::store('tracking', $status2, 0, $config); 
	 }
	 $enabled = JRequest::getVar('adwords_enabled_0', false); 
	 
	 if (!file_exists(JPATH_SITE.DS.'plugins'.DS.'system'.DS.'opctracking'))
	    {
		
		   jimport('joomla.filesystem.folder');
		   jimport('joomla.filesystem.file');
		   if (JFolder::create(JPATH_SITE.DS.'plugins'.DS.'system'.DS.'opctracking')!==false)
		   {
		   JFile::copy(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'install'.DS.'opctracking'.DS.'opctracking.php', JPATH_SITE.DS.'plugins'.DS.'system'.DS.'opctracking'.DS.'opctracking.php');  
		   JFile::copy(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'install'.DS.'opctracking'.DS.'opctracking.xml', JPATH_SITE.DS.'plugins'.DS.'system'.DS.'opctracking'.DS.'opctracking.xml'); 
		   JFile::copy(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'install'.DS.'opctracking'.DS.'index.html', JPATH_SITE.DS.'plugins'.DS.'system'.DS.'opctracking'.DS.'index.html'); 
		   }
		   else return JText::sprintf('COM_ONEPAGE_CANNOT_CREATE_DIRECTORY', JPATH_SITE.DS.'plugins'.DS.'system'.DS.'opctracking'); 
		}
	 
	 if (!empty($enabled))
	   {
	   
	      $db = JFactory::getDBO(); 
		  $q = "select * from #__extensions where element = 'opctracking' and type='plugin' and folder='system' limit 0,1"; 
		  $db->setQuery($q); 
		  $isInstalled = $db->loadAssoc(); 
		  
		  if (empty($isInstalled))
		   {
		      $q = ' INSERT INTO `#__extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ';
			  $q .= " (NULL, 'plg_system_opctracking', 'plugin', 'opctracking', 'system', 0, 1, 1, 0, '{\"legacy\":false,\"name\":\"plg_system_opctracking\",\"type\":\"plugin\",\"creationDate\":\"December 2013\",\"author\":\"RuposTel s.r.o.\",\"copyright\":\"RuposTel s.r.o.\",\"authorEmail\":\"admin@rupostel.com\",\"authorUrl\":\"www.rupostel.com\",\"version\":\"1.7.0\",\"description\":\"One Page Checkout Affiliate Tracking support for VirtueMart 2\",\"group\":\"\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0) "; 
		      $db->setQuery($q); 
		      $db->query(); 
		   }
		   else
		   {
		      if (empty($isInstalled['enabled']))
			  {
			  
		 
		    $q = " UPDATE `#__extensions` SET  enabled =  '1' WHERE  element = 'opctracking' and folder = 'system' "; 
			$db->setQuery($q); 
			$db->query(); 
			//echo 'Disabled Linelab One Page Checkout extension in Plugin Manager <br />'; 
		     

			  }
			  
			  
		   }
		  
	   }
	   else
	   {
	      
			  
			$db = JFactory::getDBO(); 
		  $q = "select * from #__extensions where element = 'opctracking' and type='plugin' and folder='system' limit 0,1"; 
		  $db->setQuery($q); 
		  $isInstalled = $db->loadAssoc(); 
		  if (!empty($isInstalled))
		  {
		    $db = JFactory::getDBO(); 
		    $q = " UPDATE `#__extensions` SET  enabled =  '0' WHERE  element = 'opctracking' and folder = 'system' "; 
			$db->setQuery($q); 
			$db->query(); 
		  }
			//echo 'Disabled Linelab One Page Checkout extension in Plugin Manager <br />'; 
		 

			  
	   }
	   
	   
	 return;
  }
  function isEnabled()
  {
    $db = JFactory::getDBO(); 
		  $q = "select * from #__extensions where element = 'opctracking' and type='plugin' and folder='system' limit 0,1"; 
		  $db->setQuery($q); 
		  $isInstalled = $db->loadAssoc(); 
		  
		  if (empty($isInstalled)) return false; 
		  if (!empty($isInstalled['enabled'])) return true; 
	return false; 
  }
  function getStatusConfig($statuses)
  {
     $arr = array(); 
     foreach ($statuses as $status)
	 {
	   if (!isset($status->order_status_code))
	   {
	     //var_dump($status); die(); 
	   }
	   $status2 = $status['order_status_code']; 
	   $default = new stdClass(); 
	   $default->code = ''; 
	   $default->only_when = ''; 
	   $arr[$status2] = OPCconfig::getValue('tracking', $status2, 0, $default); 
	 }
	 return $arr; 
  }
  
  function showOrderVars()
  {
    $array = array(); 
	$object = new stdClass(); 
	require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opctracking.php'); 
	OPCtrackingHelper::getOrderVars(0, $array, $object, true); 
    
  }
  function getOrderVars()
  {
    require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opctracking.php'); 
	 $array = array(); 
	$object = new stdClass(); 
	 OPCtrackingHelper::getOrderVars(0, $array, $object, false); 
	 
	 return $array; 
  }
  
  function getJforms($files)
    { 
	
	 
	  require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'config.php'); 
	  if (!OPCJ3)
	  {
	 require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opcparameters.php'); 
	  }
	  else
	  {
	  require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'jformrender.php'); 
	  }
	 $ret = array(); 
	 foreach ($files as $file)
	 {
	   $path = JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'trackers'.DS.'php'.DS.$file.'.xml'; 
	   if (!file_exists($path)) continue; 
	   
	   
	   //$data = new stdClass();  
	   //$data->adwords_id = 1; 
	   $data = OPCconfig::getValue('tracking_config', $file, 0);
	   
	   $title = $description = ''; 
	   
	   if (function_exists('simplexml_load_file'))
	   {
	   $fullxml = simplexml_load_file($path);
	   
	   $title = $fullxml->name; 
	   $description = $fullxml->description; 
	   
	   
	   }
	   
	   if (!OPCJ3)
	   {
	    $params = new OPCparameters($data, $file, $path, 'opctracking'); 
	    $test = $params->vmRender($file); 
	   }
	   else
	   {
	   
	   
	   
	   	   $xml = file_get_contents($path); 
		$xml = str_replace('extension', 'form', $xml); 
		$xml = str_replace('params', 'fieldset', $xml); 
		$xml = str_replace('<fieldset', '<fields name="'.$file.'"><fieldset name="test" label="'.$title.'" ', $xml); 
		$xml = str_replace('param', 'field', $xml); 
		$xml = str_replace('</fieldset>', '</fieldset></fields>', $xml); 
		//$fullxml = simplexml_load_string($xml);

		//echo $xml; 
	    $test = JForm::getInstance($file, $xml, array(),true);
		
		//$test->bind($data); 
		foreach ($data as $k=>$vl)
		{
		  $test->setValue($k, $file, $vl); 
		}
		//debug_zval_dump($test); 
		$fieldSets = $test->getFieldsets();
		//var_dump($fieldSets); die(); 
	    //$test->load($fullxml);
		
		$test = OPCparametersJForm::render($test); 
		//debug_zval_dump($testf); die(); 
		
		//var_dump($test); die(); 
		//$test->bind($payment);
	   }
	   
	   
	   
	   
	   
	   $ret[$file]['params'] = $test;
		if (empty($title))
	   $ret[$file]['title'] = $file.'.php'; 
	    else $ret[$file]['title'] = (string)$title; 

		
	   
	    $ret[$file]['description'] = (string)$description; 
		
		
	 }
	 return $ret; 
	}
}
