<?php
/*
*
* @copyright Copyright (C) 2007 - 2010 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/

	defined( '_JEXEC' ) or die( 'Restricted access' );
	jimport( 'joomla.application.component.model' );
	jimport( 'joomla.filesystem.file' );
	
	require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'assets'.DS.'export_helper.php' );
	class JModelOrder_details extends OPCModel
{
    function __construct()
		{
			parent::__construct();
		
		}

    
    function save()
    {
     
     JRequest::setVar( 'view', '[ order_details ]' );
     JRequest::setVar( 'layout', 'default'  );  
     $order_id = JRequest::getVar('order_id',0);
     $d = JRequest::get('post');
     $ps_order = new ps_order;
     if ($ps_order->order_status_update($d)) $msg = 'Order status updated'; else 'Error updating order status';
      //echo 'som tu';die();
     $link = 'index.php?option=com_onepage&view=order_details&order_id='.$order_id;
     $this->setRedirect($link, $msg);
    }
    
 	// zisti stav objednavky a zformatuje ho podla toho
	function getStatus($tid)
	{
	  $order_id = JRequest::getVar('order_id');
     $ehelper = new OnepageTemplateHelper;
     $status = $ehelper->getStatus($order_id, $tid);
     // $status moze byt: NONE, PROCESSING, DONE, ERROR
     
     /// ... ernest tu ... 
     
     // vracat by malo html s formatovanym statusom
     return $status;
	  
	}
    
    
    // mal by vracat bud cely <a href=".... pre konkretnu vygenerovanu template
    // alebo ikonku pre vygenerovanie a pod
    function getHref($tid)
    {
      $order_id = JRequest::getVar('order_id');
      $ehelper = new OnepageTemplateHelper;
      return $ehelper->getFileHref($tid, $order_id);
      
    }  
  
	// vracia zoznam templatov aj s nastavenim v numerickom array    
    function getTemplates()
    {
     $ehelper = new OnepageTemplateHelper;
     return $ehelper->getExportTemplates();
    }
    
    // vracia xml pre request['order_id'] a zadanu tid
	// tato funkcia sa bude pouzivat najma pre AJAX
	// v teste ju mozeme pouzit priamo aj v order_details 
	function getXml($tid)
	{
     $order_id = JRequest::getVar('order_id');
     if (empty($order_id)) return "";
     $ehelper = new OnepageTemplateHelper;
     return $ehelper->getXml($tid, $order_id);
	}
    
    // vracia pole so vsetkymi beznymi hodnotami objednavky
    function getOrderData()
    {
     $order_id = JRequest::getVar('order_id');
     if (empty($order_id)) return "";
     $ehelper = new OnepageTemplateHelper;
     return $ehelper->getOrderData($order_id);
    }
    
}