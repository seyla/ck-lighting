<?php
/**
 * @version		$Id: default.php 21837 2011-07-12 18:12:35Z dextercowley $
 * @package		RuposTel OnePage Utils
 * @subpackage	com_onepage
 * @copyright	Copyright (C) 2005 - 2013 RuposTel.com
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$document =& JFactory::getDocument();
JHTMLOPC::script('toggle_langs.js', 'administrator/components/com_onepage/views/config/tmpl/js/', false);
$document->setTitle(JText::_('COM_ONEPAGE_TRACKING_PANEL')); 
JHTMLOPC::script('toggle_langs.js', 'administrator/components/com_onepage/views/config/tmpl/js/', false);
JHTML::_('behavior.tooltip');
?>
<script type="text/javascript">
    window.addEvent('domready', function(){ 
       var JTooltips = new Tips($$('.hasTip'), 
       { maxTitleChars: 50, fixed: false}); 
    });
</script>
<?php
$default_config = array('vm_lang'=>0, 'vm_menu_en_gb'=>0, 'selected_menu'=>0, 'menu_'=>0, 'tojlanguage'=>'*'); 
$session = JFactory::getSession(); 
$config = $session->get('opc_utils', $default_config); 
include(JPATH_SITE.DS."components".DS."com_onepage".DS."config".DS."onepage.cfg.php");

JToolBarHelper::Title(JText::_('COM_ONEPAGE_TRACKING_PANEL') , 'generic.png');
//	JToolBarHelper::install();
JToolBarHelper::apply();

if (!version_compare(JVERSION,'2.5.0','ge'))
{
  $j15 = true; 
  
}
jimport('joomla.html.pane');
jimport('joomla.utilities.utility');
$pane = OPCPane::getInstance('tabs', array('startOffset'=>0, 'active'=>'paneladw'), 'toptabs');

if (!empty($j15)) echo '<div>'.JText::_('COM_ONEPAGE_ONLY_J25').'</div>'; 
if (!empty($j15)) echo '<div style="display: none;">'; 


?>
<form action="index.php" id="adminForm" method="post">
<?php

echo $pane->startPane('pane');
echo $pane->startPanel(JText::_('COM_ONEPAGE_TRACKING_PANEL'), 'paneladw');


?>
		<fieldset class="adminform">
        <legend><?php echo JText::_('COM_ONEPAGE_TRACKING'); ?></legend>
        <p><?php //echo JText::_('COM_ONEPAGE_TRACKING_DESC'); 
		?></p>
        <table class="admintable" id="comeshere" style="width: 100%;">
	    <tr>
	    <td >
		 <input id="adwords_enabled_0" type="checkbox" name="adwords_enabled_0" <?php if (!empty($this->isEnabled)) echo 'checked="checked" '; ?>/>
	     
	    </td>
	    <td>
	   <label for="adwords_enabled_0"><?php echo JText::_('COM_ONEPAGE_TRACKING_ADWORDS'); ?></label> 
	    </td>
		<td>
		</td>
		</tr>
	    <tr style="display: none;">
	    <td class="key">
	     <label for="adwords_name_0"><?php echo JText::_('COM_ONEPAGE_TRACKING_ADWORDS_NAME'); ?></label> 
	    </td>
	    <td>
	    <input id="adwords_name_0" type="text" name="adwords_timeout" value="<?php if (!empty($adwords_timeout)) echo (int)$adwords_timeout; else echo "4000"; ?>"/>
	    </td><td><?php echo JText::_('COM_ONEPAGE_TRACKING_ADWORDS_NAME_DESC'); ?></td>
		</tr>

	    <tr style="display: none;">
	    <td class="key">
	     <label for="adwords_amount_0"><?php echo JText::_('COM_ONEPAGE_TRACKING_ADWORDS_AMOUNT'); ?></label> 
	    </td>
	    <td>
	    <input id="adwords_amount_0" type="text" name="adwords_amount_0" value="<?php if (!empty($adwords_amount[0])) echo $adwords_amount[0]; ?>"/>
	    </td><td><?php echo JText::_('COM_ONEPAGE_TRACKING_ADWORDS_AMOUNT_NAME'); ?></td>
		</tr>
		
	    <tr>
	    <td colspan="2" >
		<?php
		//if (false) 
		{
		$pane2 = OPCPane::getInstance('tabs', array('startOffset'=>0, 'active'=>'pane2P'), 'tab2');
		echo $pane2->startPane('pane2a');
		
		jimport( 'joomla.filesystem.file' );
		foreach ($this->statuses as $k=>$s)
		{
		$label = JText::_($s['order_status_name']); 
		$adwords_code[$s['order_status_code']] = JFile::read(JPATH_ROOT.DS."components".DS."com_onepage".DS."trackers".DS."body.html"); 
		if (!empty($adwords_code[$s['order_status_code']]))
		$label .= '...'; 
		echo $pane2->startPanel($label, 'pane2'.$s['order_status_code']); 
		
		
		{
		echo '<label for=="">'.JText::_('COM_ONEPAGE_TRACKING_TRIGGER_WHEN').'</label>'; 
		echo '<select name="opc_tracking_only_when_'.$s['order_status_code'].'" id="opc_tracking_only_when'.$s['order_status_code'].'">'; 
		echo '<option value=""'; 
		if (empty($opc_tracking_only_when[$s['order_status_code']])) echo ' selected="selected" '; 
		echo ' >'.JText::_('COM_ONEPAGE_UTILS_ALL').'</option>'; 
		foreach ($this->statuses as $k2=>$s2)
		{
		     echo '<option '; 
		 
			//if ((!empty($opc_tracking_only_when[$s['order_status_code']])) && ($opc_tracking_only_when[$s2['order_status_code']] == $s2['order_status_code'])) echo ' selected="selected" '; 
			  if (!empty($this->config[$s['order_status_code']]->only_when))
			  {
			  if ($this->config[$s['order_status_code']]->only_when  == $s2['order_status_code'])
			  echo ' selected="selected" ';
			  }
			  echo ' value="'.$s2['order_status_code'].'">'.JText::_($s2['order_status_name']).'</option>'; 
		}
		echo '</select>'; 
		echo '<label>'.JText::_('COM_ONEPAGE_TRACKING_TRIGGER_WHEN_TO_THIS_STATUS').' <span style="color: red;">'.JText::_($s['order_status_name']).'</span></label>'; 
		echo '<br style="clear:both;"/>'; 
		
		echo '<br style="clear: both;"/>';
		?>
		<table>
		
		
		<?php
		//echo '<select name="opc_tracking_php_'.$s['order_status_code'].'" id="opc_tracking_php_'.$s['order_status_code'].'">'; 
		

		foreach ($this->trackingfiles as $k2=>$s2)
		{
		     echo '<tr><td><input type="checkbox" name="'.$s2.'_'.$s['order_status_code'].'" id="id'.$s2.'_'.$s['order_status_code'].'" '; 
		    
		    
			  if (!empty($this->config[$s['order_status_code']]->$s2)) echo ' checked="checked" ';
			  
			  echo ' value="'.$s2.'" /></td><td><label for="id'.$s2.'_'.$s['order_status_code'].'">'.$s2.'.php</label></td><td>';

	
			  echo '</td></tr>'; 
		}
		//echo '</select><br style="clear:both;"/>'; 
		
		
		?>
		</table>
	    <textarea placeholder="<?php echo JText::_('COM_ONEPAGE_TRACKING_ADWORDS_CODE'); ?>" id="adwords_code_<?php echo $s['order_status_code']; ?>" name="adwords_code_<?php echo $s['order_status_code']; ?>" cols="60" rows="20"><?php 
		if (!empty($this->config[$s['order_status_code']]->code))
		echo  $this->config[$s['order_status_code']]->code; 	
		?></textarea> <br style="clear:both;" />
		<?php 
		}
		echo $pane2->endPanel();
		}
		echo $pane2->endPane();
		
		
		
				?>
				<br style="clear: both;"/>
		<?php 		//echo JText::_('COM_ONEPAGE_TRACKING_ADWORDS_CODE_NOTE'); 
		
	    	if (!empty($use_ssl))
				$op_securl = JURI::base(true).basename($_SERVER['PHP_SELF']);
			else
				$op_securl = JURI::base(true).basename($_SERVER['PHP_SELF']);
			
			$op_securl = str_replace('/administrator', '/', $op_securl); 
		?>
	        <?php //echo JText::sprintf('COM_ONEPAGE_TRACKING_ADWORDS_CODE_DESC', $op_securl,$op_securl  ); 
			}
			?>
		
	    </td>
	    
		</tr>
		
		</table>
</fieldset>
<?php

?>


 

<?php
		echo $pane->endPanel(); 
		echo $pane->startPanel(JText::_('COM_ONEPAGE_OPC_ORDER_VARS'), 'vars2a'); 
		?>
		<table>
		<tr><th><?php echo JText::_('COM_ONEPAGE_OPC_ORDER_VARS_TITLE'); ?></th><th><?php echo JText::_('COM_ONEPAGE_OPC_ORDER_VARS_TITLE_VALUE'); ?></th></tr>
		<?php foreach ($this->orderVars as $key=>$val) { ?>
		<tr><td><?php echo '{'.$key.'}'; ?>
		</td><td> <?php echo $val; ?>
		</td></tr>
		<?php } ?>
		</table>
		<?php
		echo $pane->endPanel(); 
		echo $pane->startPanel(JText::_('COM_ONEPAGE_TRACKING_CURL_LABEL'), 'curl2a'); 
?>
<fieldset>
<table>
<tr>
		<td colspan="3"><?php echo JText::_('COM_ONEPAGE_TRACKING_CURL'); 
		if (!function_exists('curl_multi_exec'))
		 {
		   echo '<span style="color: red;">curl_multi_exec is not found, contact your web hosting provider. The below feature will not work for you.</span>'; 
		 }
		?>
		<br />
		<?php echo JText::_('COM_ONEPAGE_TRACKING_CURL_DESC'); ?>
		
		</td>
		</tr>
		
		<?php 
		if (empty($curl_url)) { $curl_url = array(); $curl_url[0] = ''; }
		foreach ($curl_url as $i=>$val)
		{
		?>
		<tr>
		  <td class="key"><?php echo JText::_('COM_ONEPAGE_TRACKING_CURL_LABEL'); ?>
		  </td>
		  <td colspan="2">
		   <input style="width: 80%;" type="text" value="<?php 
		   if (!empty($curl_url[$i]))
		   echo base64_decode($curl_url[$i]);  ?>" name="curl_url_<?php echo $i; ?>" />
		   <?php if ($i == (count($curl_url)-1)) { ?>
		   <a href="#" onclick="javascript: return add_curl();" ><?php echo JText::_('COM_ONEPAGE_ADD_MORE'); ?></a><br />
		   <div id="add_here">&nbsp;</div>
		   <?php $code_sg = ''; 
		   $last_num = 0; 
		   ?>
		   <script type="text/javascript">
//<![CDATA[		   
		    var next = <?php echo count($curl_url); ?>;
		    var toAdd0 = '<input style="width: 80%;" type="text" value="" name="curl_url_';
			var toAdd1 = '" /><br />'; 
			
			 
			 var opc_line = "<?php 
		  $code_sg = str_replace('"', '\\"', $code_sg);
		  $code_sg = str_replace("\r\n", " ", $code_sg);
		  $code_sg = str_replace("\n", " ", $code_sg);
		  echo $code_sg; 
		  ?>"; 
		var line_iter = <?php echo $last_num; ?>;
//]]>
		   </script>
		   
		   <?php } ?>
		  </td>
		</tr>
		<?php
		}
		?>
		<tr>
		</tr>
</table>
</fieldset>
<?php		
		echo $pane->endPanel(); 
		 
		foreach ($this->trackingfiles as $k3=>$s3)
		{
		
		if (!empty($this->forms[$s3]))
		{
		echo $pane->startPanel($this->forms[$s3]['title'], 'adw'.$s3);
		if (!empty($this->forms[$s3]['description']))
		{
		echo '<fieldset class="adminform">'; 
		echo '<legend>'.JText::_('JGLOBAL_DESCRIPTION').'</legend>';
		echo '<p>'.$this->forms[$s3]['description'].'</p>';
		echo '</fieldset>'; 
		}
		echo '<fieldset class="adminform">'; 
		echo '<legend>'.$this->forms[$s3]['title'].'</legend>'; 
		echo $this->forms[$s3]['params']; 
	    echo '</fieldset>'; 
	 

		echo $pane->endPanel();
		}
		}

		
				echo $pane->endPane(); 
				?>
<input type="hidden" name="option" value="com_onepage" />
<input type="hidden" name="task" value="apply" id="task" />
<input type="hidden" name="view" value="tracking" />
</form>
<?php
if (!empty($j15)) echo '</div>'; 