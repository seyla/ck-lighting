<?php
/*
*
* @copyright Copyright (C) 2007 - 2010 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/

	defined( '_JEXEC' ) or die( 'Restricted access' );
	if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

   $ajax = JRequest::getVar('ajax','0');

   if ($ajax === 'yes')
   {
    require(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_onepage'.DS.'views'.DS.'order_details'.DS.'tmpl'.DS.'ajax'.DS.'onepage_ajax.php');
    die();
   }

	
	//JToolBarHelper::Title('OPC Order Management' , 'generic.png');
//	JToolBarHelper::install();
	//JToolBarHelper::save();
/*	JToolBarHelper::apply(); */
	//JToolBarHelper::cancel();
	
	jimport('joomla.html.pane');
	
	//echo '<h2>* UNDER DEVELOPMENT, ONLY FOR EXPERIMENTAL USE NOW *</h2>';
	
	$pane =& JPane::getInstance('tabs', array('startOffset'=>0));
	$document =& JFactory::getDocument();
	$style = '
	div#toolbar-box {
	 display: none;
	}
	div#border-top {
	  display: none;
	}
	div#submenu-box {
	 display: none;
	}
	div.current {
	 float: left;
	 padding: 5 !important;
	 width: 98%;
	}
	div {
	 text-indent: 0;
	}
	dl {
	 margin-left: 0 !important;
	 padding: 0 !important;
	}
	dd {
	 margin-left: 0 !important;
	 padding: 0 !important;
	 width: 100%;
	}
	dd div {
	 margin-left: 0 !important;
	 padding-left: 0 !important;
	 text-indent: 0 !important;
	 width: 100%;
	 left: 0;
	}
	div.current dd {
	 
	 padding-left:1px;
     padding-right:1px;
     margin-left:1px;
     margin-right:1px;
     text-indent:1px;
     float: left;
	}
	td {
	 border: 1px none !important;
	 border-style: none !important;
	}
	table.adminlist, td, th {
	 border: 0px none !important;
	}
	tr.trclass {
	 border-style: none !important;
	 border: 0px none !important;
	}
	table.adminlist, th {
	 background-color: #7A7A7A !important;
	 color: white;
	 font-weight: bold;
	 font-size: 120%;
	 border-style: none !important;
	 margin: 0 !important;
	}
	table.adminlist, tr, td {
	 height: 20px !important;
	 border: 0px none !important;
	 padding: 1px;
	 border-spacing: 0px !important;
	 border-style: none !important;
	 font-size: 12px;
	}
	input.inputfield {
	 height: 20px;
	 font-size: 14px;
	}
	input.image {
	 border-style: none !important;
	 border-width: 0px !important;
	 border: none !important;
	}
	input.ipt {
		color: #D9EEF7;
	}
	input.ipt {
	display: inline-block;
	outline: none;
	cursor: pointer;
	text-align: center;
	text-decoration: none;
	font: 12px Arial, Helvetica, sans-serif;
	/*padding: .5em 2em .55em;*/
	text-shadow: 0 1px 1px rgba(0,0,0,.3);
	-webkit-border-radius: .5em; 
	-moz-border-radius: .5em;
	border-radius: .5em;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	box-shadow: 0 1px 2px rgba(0,0,0,.2);
    }
	input.ipt:hover {
	text-decoration: none;
	
	}
	.input.ipt:active {
	position: relative;
	top: 1px;
	color: #fcd3a5;
	}
/* extremely helpfull demo: http://www.webdesignerwall.com/demo/css-buttons.html */

.blue {
	color: #d9eef7;
	border: solid 1px #0076a3;
	background: #0095cd;
	background: -webkit-gradient(linear, left top, left bottom, from(#00adee), to(#0078a5));
	background: -moz-linear-gradient(top,  #00adee,  #0078a5);
	filter:  progid: DXImageTransform.Microsoft.gradient(startColorstr=\'#00adee\', endColorstr=\'#0078a5\');
}
.blue:hover {
	background: #007ead;
	background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
	background: -moz-linear-gradient(top,  #0095cc,  #00678e);
	filter:  progid: DXImageTransform.Microsoft.gradient(startColorstr=\'#0095cc\', endColorstr=\'#00678e\');
}
.blue:active {
	color: #80bed6;
	background: -webkit-gradient(linear, left top, left bottom, from(#0078a5), to(#00adee));
	background: -moz-linear-gradient(top,  #0078a5,  #00adee);
	filter:  progid: DXImageTransform.Microsoft.gradient(startColorstr=\'#0078a5\', endColorstr=\'#00adee\');
}	
	
	div.leftdiv {
	 float: left;
	 width: 30%;
	 line-height: 15px;
	 font-weight: bold;
	}
	div.rightdiv {
	 float: left;
	 width: 70%;
	 clear: right;
	 word-wrap: break-word;
	 line-height: 15px;
	 font-weight: normal;
	}
	div.divleft {
	 float: left;
	 width: 30%;
	 line-height: 25px;
	 font-weight: bold;
	}
	div.divright {
	 float: left;
	 width: 70%;
	 clear: right; 
	 word-wrap: break-word;
	 line-height: 25px;
	 font-weight: normal;
	}
	div.rightd {
	 float: left;
	 width: 9%;
	 line-height: 25px;
	 font-weight: bold;
	 margin-left: 1%;
	}
	div.leftd {
	 text-align: right;
	 float: left;
	 width: 90%;
	 clear: right;
	 word-wrap: break-word;
	 line-height: 25px;
	 font-weight: normal;
	}
	.adminForm label {

	white-space: nowrap;

}

.adminForm {

	table-layout: auto;

	text-align: left;

}

.adminForm td, .adminForm tr {

	vertical-align: middle;

}

.adminlist th {

	white-space: nowrap;

}

	
	';
	
	$document->addStyleDeclaration($style);

	
	
     
        jimport('joomla.html.pane');
	JHTML::script('toggle_langs.js', 'administrator/components/com_onepage/views/config/tmpl/js/', false);
  
    
// Load the virtuemart main parse code
	require_once( JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart_parser.php' );
	require_once( JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_virtuemart'.DS.'compat.joomla1.5.php' );
    require_once( CLASSPATH.DS.'ps_order_status.php' ); 
    require_once( CLASSPATH.DS.'ps_html.php' ); 
	
    global $mosConfig_absolute_path, $mosConfig_live_site,$mosConfig_offset, $sess, $VM_LANG, $perm, $CURRENCY_DISPLAY, $my;
  
// missing variables:
	$limitstart = JRequest::getVar('limitstart', 0);
	$limit = JRequest::getVar('limit', 50);
	$keyword = JRequest::getVar('keyword', '');
	$modulename = 'order';	
	$ps_vendor_id = 1;
	$ps_order_status = new ps_order_status;
	$ps_html = new ps_html;
	//$GLOBALS('ps_order_status') = $ps_order_status;
	$VM_LANG->load('order');
	$VM_LANG->load('admin');
	$db = new ps_DB;


   {
   
   
/**
* Rest of this file is a modified copy of order.order_list.php of virtuemart page file
*
*
* @version $Id: order.order_print.php 1481 2008-07-21 19:11:07Z soeren_nb $
* @package VirtueMart
* @subpackage html
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
mm_showMyFileName( __FILE__ );


require_once(CLASSPATH.'ps_product.php');
require_once(CLASSPATH.'ps_order_status.php');
require_once(CLASSPATH.'ps_checkout.php');
require_once(CLASSPATH.'ps_order_change.php');
require_once(CLASSPATH.'ps_order_change_html.php');
require_once(CLASSPATH.'ps_order.php');

JHTML::script('onepage_ajax.js', 'administrator/components/com_onepage/views/order_details/tmpl/ajax/', false);
 //JRequest::setVar( 'hidemainmenu', 1 );

$ps_order = new ps_order;
$ps_product = new ps_product;
$order_id = vmRequest::getInt('order_id');
$ps_order_change_html = new ps_order_change_html($order_id);
echo '<form name="adminForm" id="adminFormOPC" method="post" action="'.$_SERVER['PHP_SELF'].'">';
if (!is_numeric($order_id))
    echo "<h2>The Order ID $order_id is not valid.</h2>";
else {
	echo '<input type="hidden" id="scrolly" name="scrolly" value="'.JRequest::getVar('scrolly',0).'" />';
	echo '<input type="hidden" id="op_curtab" name="op_curtab" value="'.JRequest::getVar('op_curtab', '').'" />';
    $dbc = new ps_DB;
	$q = "SELECT * FROM #__vm_orders WHERE order_id='$order_id'";
	$db->query($q);
	if( $db->next_record() ) {

 					if( $db->f("ship_method_id") ) { 
                       $details = explode( "|", $db->f("ship_method_id"));
                       $ship_method_id = $db->f("ship_method_id");
                    }
                    
              		$dbs = new ps_DB;
              		$q = 'SELECT shipping_rate_id, shipping_rate_name, shipping_rate_weight_start, shipping_rate_weight_end, shipping_rate_value, shipping_rate_package_fee, tax_rate, currency_name 
              				FROM #__vm_shipping_rate, #__vm_currency, #__vm_tax_rate 
              				WHERE currency_id = shipping_rate_currency_id 
              					AND tax_rate_id = shipping_rate_vat_id 
              				ORDER BY shipping_rate_list_order';
              		$dbs->query($q);
              		/*$shipa = array();*/
              		$sh = '';
              		$rate_details = explode( "|", $db->f("ship_method_id") );
              		if (count($rate_details) >= 5)
              		{
              		while ($dbs->next_record()){
              		/* $ship['shipping_rate_id'] = $dbs->f('shipping_rate_id');
              		 $ship['shipping_rate_name'] = $dbs->f('shipping_rate_name');
              		 $ship['shipping_rate_value'] = $dbs->f('shipping_rate_value');
					 $ship['tax_rate'] = $dbs->f('tax_rate');
					 $ship['shipping_rate_package_fee'] = $dbs->f('shipping_rate_package_fee');
					 $ship['currency_name'] = $dbs->f('currency_name');
					 $shipa[] = $ship;
					  */             		 
              			$sh = '<option value="'.$dbs->f('shipping_rate_id').'"';
              			if($dbs->f('shipping_rate_id') == $rate_details[4]) 
              			{
              				$sh .= " selected='selected' ";
              			    $shv = $dbs->f('shipping_rate_name');
              				$shv .= " ---&gt; ";
              				$shv .= " ".(($dbs->f('shipping_rate_value') * (1+$dbs->f('tax_rate'))) + $dbs->f('shipping_rate_package_fee'));
              				$shv .= " ".$dbs->f('currency_name');
              			}
              			$sh .= '>';
              			$sh .= $dbs->f('shipping_rate_name');
              			
              			//print "; (".$dbs->f('shipping_rate_weight_start')." - ".$dbs->f('shipping_rate_weight_end')."); ";
              			$sh .= " ---&gt; ";
              			$sh .= " ".(($dbs->f('shipping_rate_value') * (1+$dbs->f('tax_rate'))) + $dbs->f('shipping_rate_package_fee'));
              			$sh .= " ".$dbs->f('currency_name');
              			$sh .= '</option>';
              		}
				  }


	   
  			    $dbpm = new ps_DB;
  				$q  = "SELECT * FROM #__vm_payment_method, #__vm_order_payment WHERE #__vm_order_payment.order_id='$order_id' ";
  				$q .= "AND #__vm_payment_method.payment_method_id=#__vm_order_payment.payment_method_id";
  				$dbpm->query($q);
  				$dbpm->next_record();
  			   
  				// DECODE Account Number
  				$dbaccount = new ps_DB;
  			    $q = "SELECT ".VM_DECRYPT_FUNCTION."(order_payment_number,'".ENCODE_KEY."')
  					AS account_number, order_payment_code FROM #__vm_order_payment  
  					WHERE order_id='".$order_id."'";
  				$dbaccount->query($q);
  				$dbaccount->next_record();
  		
	    		$payment_id = $dbpm->f("payment_method_id"); 
	    		
	    		$dbs = new ps_DB;
      		$q  = "SELECT payment_method_id, payment_method_name, payment_method_discount FROM #__vm_payment_method WHERE payment_enabled = 'Y' ORDER BY payment_method_name ASC"; 
  			$dbs->query($q);
  			$pmh = '';
  		
  			while ($dbs->next_record()){
  		  if (!is_null( $dbs->f('payment_method_id') )) {
    			$pmh .= '<option value="'.$dbs->f('payment_method_id').'"';
    			if($dbs->f('payment_method_id') == $payment_id) 
    			{
    			 $pmh .= " selected='selected' ";
    			 $pmhv = $dbs->f('payment_method_name');
    			 $pmhv .= "-> ". $CURRENCY_DISPLAY->getFullValue(($dbs->f('payment_method_discount') * -1));
				}
  			    $pmh .= '>';
    			$pmh .= $dbs->f('payment_method_name');
    			$pmh .= "-> ". $CURRENCY_DISPLAY->getFullValue(($dbs->f('payment_method_discount') * -1));
    			$pmh .= '</option>';
  			}
  		}
  		

	    
	    
	    	
	  ?><input type="hidden" id="order_number" name="order_number" value="<?php echo $db->f('order_number'); ?>" />
	  <input type="hidden" id="general_param" name="general_param" value="0" />
	  <input type="hidden" id="task" name="task" value="save" />
	  <input type="hidden" name="view" value="order_details" />
	  <input type="hidden" name="contoller" value="order_details" />
	  <input type="hidden" name="option" value="com_onepage" />
	  <input type="hidden" id="general_param1" name="general_param1" value="0" />
	  <input type="hidden" id="cmd" name="cmd" value="" />
	  <input type="hidden" id="localid" name="localid" value="<?php echo $db->f('order_id'); ?>" />
	  <input type="hidden" id="orderid" name="orderid" value="<?php echo $db->f('order_id'); ?>" />
	  <input type="hidden" id="fieldid" name="fieldid" value="<?php echo $db->f('order_id'); ?>" />
	  
	  <?php
	  $order_number = $db->f('order_number');
	  // Print View Icon
	  $print_url = $_SERVER['PHP_SELF']."?page=order.order_printdetails&amp;order_id=$order_id&amp;no_menu=1&pop=1";
	  if( vmIsJoomla( '1.5', '>=' ) ) {
	  	$print_url .= "&amp;tmpl=component";
	  }
	  
	  $print_url = $sess->url( $print_url );
	  $print_url = defined( '_VM_IS_BACKEND' ) ? str_replace( "index2.php", "index3.php", $print_url ) : str_replace( "index.php", "index2.php", $print_url );
?>
	  <div style="float: right;">
	  
	  
	  <a href="index.php?option=com_onepage&view=orders">Back to Orders</a>
	  	<span class="pagenav" style="font-weight: bold;">
	  		<a href="javascript:void window.open('<?php echo $print_url ?>', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');">
	  			<img src="<?php echo $mosConfig_live_site ?>/images/M_images/printButton.png" align="ABSMIDDLE" height="16" width="16" border="0"alt="" />
	  			<?php echo $VM_LANG->_('PHPSHOP_CHECK_OUT_THANK_YOU_PRINT_VIEW') ?>
	  		</a>
			<?php
				  $html = ps_order::order_print_navigation( $order_id );
	  $html = str_replace('page=.order_print', 'view=order_details', $html);
	  $html = str_replace('option=com_virtuemart', 'option=com_onepage', $html);
	  $html = str_replace('<strong>', '', $html);
	  $html = str_replace('</strong>', '', $html);
	  $html = str_replace('href=', ' onclick="javascript: return op_saveVars(this);" href=', $html);
	  echo $html;
	  
			?>
	  	</span>
	  </div>
	<h1 style="font-size: 200%; float: left; padding: 0;">
	<?php
	if ($db->f("order_status")=='C') echo '<span style="color: green;">';  
	else
	if ($db->f("order_status")=='X') echo '<span style="color: red;">';  
	else 
	echo '<span>';

	echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_NUMBER').': '.$order_id;  
	
	
	$pmvh2w = $pmhv; 
$array = explode(" ", $pmvh2w);
if (count($array)>2)
{
 array_splice($array, 2);
 $pmvh2w = implode(" ", $array);
 

}
    $pmvh2w	= trim($pmvh2w);
	echo ' '.$pmvh2w; 
	
	echo ', '.vmFormatDate( $db->f("cdate")+$mosConfig_offset); ?>, <?php 
	echo ps_order_status::getOrderStatusName($db->f("order_status"));  ?>
	</span></h1>
<?php

	  // Navigation
	  
	  
	  // "index.php?page=.order_print&amp;order_id=584&amp;option=com_virtuemart&amp;Itemid=64
	//$prev = $order_id - 1;
	//$next = $order_id + 1;
	/* echo '<div align="center">
	<a class="pagenav" href="index.php?option=com_onepage&view=order_details&order_id='.$prev.'">&lt; previous</a> | <span class="pagenav">next &gt;</span>
	
	</div>'; */
	  $q = "SELECT * FROM #__vm_order_history WHERE order_id='$order_id' ORDER BY order_status_history_id ASC";
	  $dbc->query( $q );
	  $order_events = $dbc->record;
	  ?>
	 
	  
	  <div style="width:100%; float: left;">
	  
	  <?php
			 echo $pane->startPane('order_general');
  		     echo $pane->startPanel($VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_LBL'), 'px');
			?>
			<div>
			 <div style="width: 49%; float: left;">
				  <div class="leftdiv"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_NUMBER') ?>:</div>
				  <div class="rightdiv"><?php printf("%08d", $db->f("order_id"));?></div>
				  <div class="leftdiv"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_DATE') ?>:</div>
				  <div class="rightdiv"><?php echo vmFormatDate( $db->f("cdate")+$mosConfig_offset);?></div>
				  <div class="leftdiv"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') ?>:</div>
				  <div class="rightdiv"><?php echo ps_order_status::getOrderStatusName($db->f("order_status")) ?></div>
		          <div class="leftdiv"><?php echo $VM_LANG->_('VM_ORDER_PRINT_PO_IPADDRESS') ?>:</div>
			      <div class="rightdiv"><?php $db->p("ip_address"); ?></div>
		  <?php 
		  if( PSHOP_COUPONS_ENABLE == '1') { ?>
		  	  <div class="leftdiv"><?php echo $VM_LANG->_('PHPSHOP_COUPON_COUPON_HEADER') ?>:</div>
			  <div class="rightdiv"><?php if( $db->f("coupon_code") ) $db->p("coupon_code"); else echo '-'; ?></div>

		  <?php 
			}
			if (!empty($ship_method_id))
			{
			 ?>
			
			      <div class="leftdiv"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_LBL') ?>:</div>
			      <div class="rightdiv"><?php 
			      if (!empty($shv))
			      echo $shv; 
			      else
			      {
			       if (!empty($details) && (!empty($details[2])))
			      echo $details[2];
			      }
			      ?></div>
			<?php } ?>
			      <div class="leftdiv"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYMENT_LBL') ?>:</div>
			      <div class="rightdiv"><?php echo $pmhv; ?></div>
				  
				  


			<?php
			 echo '&nbsp;';
			 $user_id = $db->f("user_id");
			 echo '';
			 ?>
			 </div>
			 <div style="width: 50%; float: left;"> 
			 
			 <?php
			  $note = $db->f("customer_note"); 
			  if (!empty($note))
			  {
			  ?>
			  <div class="leftdiv"><?php  echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_CUSTOMER_NOTE') ?>:</div>
			  
			  <div class="rightdiv"><textarea readonly="readonly" cols="40" rows="4" style="min-height: 80%; width: 100%; font-size: 16px; color: red; font-weight: bold; border: none; background: transparent;"><?php if( $db->f("customer_note") ) echo @urldecode($db->f("customer_note")); else echo '-'; ?>
			  </textarea></div>
			  <?php 
			  }
			  ?>
			  <div class="leftdiv" style="position: relative; bottom: 0px; margin-top: 30px; margin-right: 50px; float: right;"><input type="submit" class="button" name="Submit" id="resendconf_<?php echo $db->f("order_id"); ?>" value="<?php echo $VM_LANG->_('PHPSHOP_ORDER_RESEND_CONFIRMATION_MAIL') ?>" onclick="javascript:return op_runCmd('resendconfirm', this);" /></div>
			 </div>
			 </div>
			 <?php
			 echo $pane->endPanel();
			 if ($user_id > 0)
			 {
			 echo $pane->startPanel( $VM_LANG->_('VM_USER_FORM_TAB_GENERALINFO'), "userform-page");
			 echo '<div>';
			   		
		$_REQUEST['user_id'] = $user_id;
		global $ps_shopper_group, $ps_product;
		global $acl, $database;
		include_class( 'shopper' );
		include_class( 'product' );

		if( !isset($ps_shopper_group)) {
   		 $ps_shopper_group = new ps_shopper_group();
		}
	    $cid		= vmRequest::getVar( 'cid', array(0), '', 'array' );
	    
	    // Set up the CMS General User Information
$row = new mosUser( $database );
$row->load( (int) $user_id );

if ( $user_id ) {
	$query = "SELECT *"
	. "\n FROM #__contact_details"
	. "\n WHERE user_id = " . (int) $row->id
	;
	$database->setQuery( $query );
	$contact = $database->loadObjectList();

	$row->name = trim( $row->name );
	$row->email = trim( $row->email );
	$row->username = trim( $row->username );
	$row->password = trim( $row->password );

} else {
	$contact 	= NULL;
	$row->block = 0;
}

// check to ensure only super admins can edit super admin info
if ( ( $my->gid < 25 ) && ( $row->gid == 25 ) ) {
	vmRedirect( 'index2.php?option=com_users', _NOT_AUTH );
}

$my_group = strtolower( $acl->get_group_name( $row->gid, 'ARO' ) );
if ( $my_group == 'super administrator' && $my->gid != 25 ) {
	$lists['gid'] = '<input type="hidden" name="gid" value="'. $my->gid .'" /><strong>Super Administrator</strong>';
} else if ( $my->gid == 24 && $row->gid == 24 ) {
	$lists['gid'] = '<input type="hidden" name="gid" value="'. $my->gid .'" /><strong>Administrator</strong>';
} else {
	// ensure user can't add group higher than themselves
	$my_groups = $acl->get_object_groups( 'users', $my->id, 'ARO' );
	if (is_array( $my_groups ) && count( $my_groups ) > 0) {
		$ex_groups = $acl->get_group_children( $my_groups[0], 'ARO', 'RECURSE' );
		if (!$ex_groups) $ex_groups = array();
	} else {
		$ex_groups = array();
	}

	$gtree = $acl->get_group_children_tree( null, 'USERS', false );

	// remove users 'above' me
	$i = 0;
	while ($i < count( $gtree )) {
		if (in_array( $gtree[$i]->value, $ex_groups )) {
			array_splice( $gtree, $i, 1 );
		} else {
			$i++;
		}
	}

	$lists['gid'] 		= vmCommonHTML::selectList( $gtree, 'gid', 'size="10"', 'value', 'text', $row->gid, true );
}

// build the html select list
$lists['block'] 		= vmCommonHTML::yesnoRadioList( 'block', 'class="inputbox" size="1"', 'value', 'text', $row->block );
// build the html select list
$lists['sendEmail'] 	= vmCommonHTML::yesnoRadioList( 'sendEmail', 'class="inputbox" size="1"', 'value', 'text', $row->sendEmail );

$canBlockUser 	= $acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'user properties', 'block_user' );
$canEmailEvents = $acl->acl_check( 'workflow', 'email_events', 'users', $acl->get_group_name( $row->gid, 'ARO' ) );

// Get the user parameters
if( vmIsJoomla( '1.5' ) ) {
	require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_users'.DS.'users.class.php' );
	$user =& JUser::getInstance( $user_id );
	$params = $user->getParameters(true);
} elseif( file_exists( $mosConfig_absolute_path . '/administrator/components/com_users/users.class.php' ) ) {
	require_once( $mosConfig_absolute_path . '/administrator/components/com_users/users.class.php' );
	$file 	= $mainframe->getPath( 'com_xml', 'com_users' );
	$params = new mosUserParameters( $row->params, $file, 'component' );
}

// Set the last visit date
$lvisit = $row->lastvisitDate;
if ($lvisit == "0000-00-00 00:00:00") {
	$lvisit = '***' . $VM_LANG->_('VM_USER_FORM_LASTVISIT_NEVER');
}
?>
<?php
//Then Start the form
?>
<fieldset class="adminform">
<legend><?php echo $VM_LANG->_('PHPSHOP_USER_FORM_LBL'); ?></legend>
<div><img src="/components/com_virtuemart/themes/default/images/administration/header/icon-48-user.png" alt="User Icon" style="float: right;" /></div>
<br style="clear: both;"/><input id="updatejoomlabtn" type="button" value="Save" style="float: right;" onclick="javascript:return op_runCmd('updateJoomla', this);" />
	<table class="admintable" cellspacing="1">
		<tr>
			<td width="150" class="key">
				<label for="name">
					<?php echo $VM_LANG->_('VM_USER_FORM_NAME'); ?>
				</label>
			</td>
			<td>
				<input type="text" name="name" id="userinfo_name" class="inputbox" size="40" value="<?php echo $row->name; ?>" />
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="username">
					<?php echo $VM_LANG->_('VM_USER_FORM_USERNAME'); ?>
				</label>
			</td>
			<td>
				<input type="text" name="username" id="userinfo_username" class="inputbox" size="40" value="<?php echo $row->username; ?>"  />
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="email">
					<?php echo $VM_LANG->_('VM_USER_FORM_EMAIL'); ?>
				</label>
			</td>
			<td>
				<input class="inputbox" type="text" name="email" id="userinfo_email" size="40" value="<?php echo $row->email; ?>" />
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="password">
					<?php echo $VM_LANG->_('VM_USER_FORM_NEWPASSWORD'); ?>
				</label>
			</td>
			<td>
				<input class="inputbox" type="password" name="password" id="userinfo_password" size="40" value=""/>
			</td>
		</tr>
		<tr>
			<td class="key">
				<label for="password2">
					<?php echo $VM_LANG->_('VM_USER_FORM_VERIFYPASSWORD'); ?>
				</label>
			</td>
			<td>
				<input class="inputbox" type="password" name="password2" id="userinfo_password2" size="40" value=""/>
			</td>
		</tr>
		<tr>
			<td valign="top" class="key">
				<label for="gid">
					<?php echo $VM_LANG->_('VM_USER_FORM_GROUP'); ?>
				</label>
			</td>
			<td>
				<?php echo $lists['gid']; 
				?>
			</td>
		</tr>
		<?php if ( $canBlockUser ) : ?>
		<tr>
			<td class="key">
				<?php echo $VM_LANG->_('VM_USER_FORM_BLOCKUSER'); ?>
			</td>
			<td>
				<?php echo $lists['block']; 
				?>
			</td>
		</tr>
		<?php endif; ?>
		<?php if ( $canEmailEvents ) : ?>
		<tr>
			<td class="key">
				<?php echo $VM_LANG->_('VM_USER_FORM_RECEIVESYSTEMEMAILS'); ?>
			</td>
			<td>
				<?php echo $lists['sendEmail']; 
				?>
			</td>
		</tr>
		<?php endif; ?>
		<?php if( $user_id ) : ?>
		<tr>
			<td class="key">
				<?php echo $VM_LANG->_('VM_USER_FORM_REGISTERDATE'); ?>
			</td>
			<td>
				<?php echo $row->registerDate;?>
			</td>
		</tr>
		<tr>
			<td class="key">
				<?php echo $VM_LANG->_('VM_USER_FORM_LASTVISITDATE'); ?>
			</td>
			<td>
				<?php echo $lvisit; ?>
			</td>
		</tr>
		<?php endif; ?>
	</table>
</fieldset>


<?php
echo '</div>';
echo $pane->endPanel();
echo $pane->startPanel( $VM_LANG->_('PHPSHOP_SHOPPER_FORM_LBL'), "third-page");

?>
<div>
<div style="width: 50%; float: left;">
<fieldset><legend><?php echo $VM_LANG->_('PHPSHOP_SHOPPER_FORM_LBL') ?></legend>
<table class="adminform">  
    <tr> 
        <td style="text-align:right;"><?php echo $VM_LANG->_('PHPSHOP_PRODUCT_FORM_VENDOR') ?>:</td>
        <td><?php ps_vendor::list_vendor($db->f("vendor_id"));  ?></td>
    </tr>
	<tr> 
        <td nowrap="nowrap" style="text-align:right;" width="38%" ><?php echo $VM_LANG->_('PHPSHOP_USER_FORM_PERMS') ?>:</td> 
        <td width="62%" > 
                <?php
                if( !isset( $ps_perms)) { $ps_perms = new ps_perm(); }
                $ps_perms->list_perms("perms", $db->sf("perms"));
                ?> 
        </td> 
    </tr> 
      <tr> 
    	<td style="text-align:right;"><?php echo $VM_LANG->_('PHPSHOP_USER_FORM_CUSTOMER_NUMBER') ?>:</td>
        <td > 
      	<input type="text" class="inputbox" name="customer_number" size="40" value="<?php echo $ps_shopper_group->get_customer_num($db->f("user_id")) ?>" />
        </td>
     </tr>
     <tr> 
    	<td style="text-align:right;"> <?php echo $VM_LANG->_('PHPSHOP_SHOPPER_FORM_GROUP') ?>:</td>
        <td ><?php
            include_class('shopper');
            $sg_id = $ps_shopper_group->get_shoppergroup_by_id($db->f("user_id"));
            echo ps_shopper_group::list_shopper_groups("shopper_group_id",$sg_id["shopper_group_id"]);?>
        </td>
    </tr>
</table> 
</fieldset>
       
<?php 
if( $db->f("user_id") ) { 
?> 
	
    <fieldset><legend><?php echo $VM_LANG->_('PHPSHOP_USER_FORM_SHIPTO_LBL') ?></legend>
    
    <a class="vmicon vmicon-16-editadd" href="<?php $sess->purl($_SERVER['PHP_SELF'] . "?page=admin.user_address_form&amp;user_id=$user_id") ?>" >
	(<?php echo $VM_LANG->_('PHPSHOP_USER_FORM_ADD_SHIPTO_LBL') ?>)</a> 
	
	
	
				  <?php
			$qt = "SELECT * from #__vm_user_info WHERE user_id='$user_id' AND address_type='ST'"; 
			$dbt = new ps_DB;
			$dbt->query($qt);
			if (!$dbt->num_rows()) {
			  echo "No shipping addresses.";
			}
			else {
			  while ($dbt->next_record()) {
				$url = $sess->url( $_SERVER['PHP_SELF'] . "?page=admin.user_address_form&user_id=$user_id&user_info_id=" . $dbt->f("user_info_id"));
				echo '&raquo; <a href="' . $sess->url($url) . '">';
				echo $dbt->f("address_type_name") . "</a><br/>";
			  }
			} ?> 
			
	</fieldset>
	</div>
         <?php 
}
?>
<div style="width: 49%; margin-left: 1%; float: left;">
<fieldset><legend>User Extra Information</legend>
<?php
require_once( CLASSPATH.'ps_userfield.php');
// Get only those fields that are NOT system fields
$userFields = ps_userfield::getUserFields( 'account', false, '', true );
$skip2 = ps_userfield::getUserFields( 'registration' );
$skipFields = array( 'delimiter_userinfo', 'username', 'email', 'password', 'password2', 'agreed' );
foreach ($skip2 as $s)
{
 $skipFields[] = $s->name;
}

$dbus = new ps_DB;
if( !empty($user_id) ) {
    $q = "SELECT * FROM #__users AS u LEFT JOIN #__vm_user_info AS ui ON id=user_id ";
    $q .= "WHERE id=$user_id ";
    $q .= "AND (address_type='BT' OR address_type IS NULL ) ";
    $q .= "AND gid <= ".$my->gid;
    $dbus->query($q);
	$dbus->next_record();
}
ob_start();
ps_userfield::listUserFields( $userFields, $skipFields, $dbus, false );
$uh = ob_get_clean();
$uh = str_replace('changeStateList()', 'changeStateList4()', $uh);
echo $uh;

echo '</fieldset></div></div>';

echo $pane->endPanel();
}

require_once( CLASSPATH . "pageNavigation.class.php" );
require_once( CLASSPATH . "htmlTools.class.php" );
require_once(CLASSPATH.'ps_order_status.php');
$ps_order_status = new ps_order_status;

$q = "";
$list  = "SELECT * FROM #__vm_orders ";
$count = "SELECT count(*) as num_rows FROM #__vm_orders ";
$q .= "WHERE  #__vm_orders.vendor_id='".$_SESSION['ps_vendor_id']."' AND #__vm_orders.user_id=".$user_id." ";
$q .= "ORDER BY #__vm_orders.cdate DESC ";
$count .= $q;
$list .= $q;
$db_count = new ps_DB;
$db_count->query($count);
$db_count->next_record();
$num_rows = $db_count->f("num_rows");
if( $num_rows ) {
	echo $pane->startPanel( $VM_LANG->_('PHPSHOP_ORDER_LIST_LBL') . ' ('.$num_rows.')', "order-list");
	?>
	        <div>
	<h3><?php echo $VM_LANG->_('PHPSHOP_ORDER_LIST_LBL') ?> </h3>
	
	<?php
	
	// Create the Page Navigation
	$pageNav = new vmPageNav( $num_rows, $limitstart, $limit );
	
	// Create the List Object with page navigation
	$listObj = new listFactory( $pageNav );
	
	$listObj->startTable();
	
	// these are the columns in the table
	$columns = Array(  "#" => "width=\"20\"", 
	                                        $VM_LANG->_('PHPSHOP_ORDER_LIST_ID') => '',
	                                        $VM_LANG->_('PHPSHOP_CHECK_OUT_THANK_YOU_PRINT_VIEW') => '',
	                                        $VM_LANG->_('PHPSHOP_ORDER_LIST_CDATE') => '',
	                                        $VM_LANG->_('PHPSHOP_ORDER_LIST_MDATE') => '',
	                                        $VM_LANG->_('PHPSHOP_ORDER_LIST_STATUS') => '',
	                                        $VM_LANG->_('PHPSHOP_ORDER_LIST_TOTAL') => '',
	                                        $VM_LANG->_('E_REMOVE') => "width=\"5%\""
	                                );
	$listObj->writeTableHeader( $columns );
	
	$db_count->query($list);
	$i = 0;
	while ($db_count->next_record()) { 
	    
	    $listObj->newRow();
	    
	    // The row number
	    $listObj->addCell( $pageNav->rowNumber( $i ) );
	    
	    $url = $_SERVER['PHP_SELF']."?page=order.order_print&limitstart=$limitstart&keyword=".urlencode($keyword)."&order_id=". $db_count->f("order_id");
	    $tmp_cell = "<a href=\"" . $sess->url($url) . "\">".sprintf("%08d", $db_count->f("order_id"))."</a><br />";
	    $listObj->addCell( $tmp_cell );
	    
	    $details_url = $sess->url( $_SERVER['PHP_SELF']."?page=order.order_printdetails&amp;order_id=".$db->f("order_id")."&amp;no_menu=1");
	    $details_url = stristr( $_SERVER['PHP_SELF'], "index2.php" ) ? str_replace( "index2.php", "index3.php", $details_url ) : str_replace( "index.php", "index2.php", $details_url );
	        
	    $details_link = "&nbsp;<a href=\"javascript:void window.open('$details_url', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');\">";
	    $details_link .= "<img src=\"$mosConfig_live_site/images/M_images/printButton.png\" align=\"center\" height=\"16\" width=\"16\" border=\"0\" /></a>"; 
	    $listObj->addCell( $details_link );
	
	    //$listObj->addCell( strftime("%d-%b-%y %H:%M", $db->f("cdate")));  //Original
		$listObj->addCell( date("Y-m-d G:i:s", $db_count->f("cdate") + ($mosConfig_offset*60*60)));  //Custom
	    //$listObj->addCell( strftime("%d-%b-%y %H:%M", $db->f("mdate")));  //Original
		$listObj->addCell( date("Y-m-d G:i:s", $db_count->f("mdate") + ($mosConfig_offset*60*60)));  //Custom
	    
	    $listObj->addCell(  $ps_order_status->getOrderStatusName($db_count->f("order_status")));
	    
	    $listObj->addCell( $CURRENCY_DISPLAY->getFullValue($db_count->f("order_total")));
	
	    $listObj->addCell( $ps_html->deleteButton( "order_id", $db_count->f("order_id"), "orderDelete", $keyword, $limitstart. '&user_id='.$user_id ) );
	
	    $i++; 
	}
	
	$listObj->writeTable();
	
	$listObj->endTable();
	echo '</div>';
	echo $pane->endPanel();
}
 	$templates = $this->templates;
 	$uri = JURI::base();
 	
 	if (stripos($uri, 'localhost')!==false) $templates = '';
	if (!empty($templates))
	{
	echo $pane->startPanel( 'Order Export', "order-export");
	echo '<div>';
	// Ernest toto je tvoja cast, ak potrebuje upravit javascript, tak ho najdes v /ajax/onepage_ajax.js
	// k datam sa je mozne dostat dvoma sposobmi:
	// 1 - cez model a referenciu v view.html, kde su akurat order_data a templates
	// 2 - cez ehelper referenciu v view.html cize
	// $ehelper->funkcia($tid...   order_id ako parameter uz nemusi byt kedze je uz asociovany v view.html
	
	// extra: $sql = "ALTER TABLE `jos_onepage_exported` ADD `specials` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT \'\' AFTER `ai`";
	//$data = $this->order_data;
	$templates = $this->templates;
	$first = reset($templates);
	$runTimer = false;
	if (count($templates)>0)
	$max_specials = $first['max_specials'];
	else $max_specials = 0;
	?>
	<table>
	<tr><th>Template name</th><th style="width: 50px;">Command</th>
	<?php for ($i = 0; $i<$max_specials; $i++) {
	if ($i==0) echo '<th>AI Special Entry</th>';
	else { ?>
	<th>Manual Entry <?php echo $i; ?></th>
	<?php } } ?>
	</tr>
	<?php
	foreach ($templates as $t)
	{
	 if (empty($t['tid_enabled'])) continue;
	 ?>
	 <tr><td>
	 <?php echo $t['tid_name'];
	 ?>
	 </td>
	 <td>
	 <?php
	 $status = $this->ehelper->getStatus($t['tid'], $order_id);
	 if ($status == 'AUTOPROCESSING') $status = 'PROCESSING';
	 if ($status == 'PROCESSING') $runTimer = true;
	 $status_txt = $this->ehelper->getStatusTxt($t['tid'], $order_id);
	 $specials = $this->ehelper->getSpecials($t['tid'], $order_id);
	 //echo $this->ehelper->getXml($t['tid'], $order_id);
	 ?>
	 <div id="tid_<?php echo $t['tid'] ?>_div">
	 <?php
	  $lin = '<a href="'.$this->ehelper->getExportItemLink($t['tid'], $order_id).'" id="tid_'.$t['tid'].'" onclick="'."javascript:return op_runCmd('sendXml', this);".'" >';
	  $plin = '<a href="#" id="tid_'.$t['tid'].'" >';
	  // status: NONE
	  ?><div id="tiddiv_<?php echo $t['tid'] ?>_NONE" <?php if ($status != 'NONE') echo ' style="display: none;" '; ?>><?php
 	  echo '<a href="'.$this->ehelper->getExportItemLink($t['tid'], $order_id).'" id="tid_'.$t['tid'].'_none" onclick="'."javascript:return op_runCmd('sendXml', this);".'" >'.'<img src="'.$mosConfig_live_site.'/images/M_images/new.png" alt="'.$status_txt.'" title="'.$status_txt.'" /></a>';
	  ?></div><?php
	  $item = $this->ehelper->getExportItem($t['tid'], $order_id);
	  $link = $this->ehelper->getPdfLink($item);
	  if (empty($link)) $link = '#';
	  $created_html = '<a href="'.$link.'" id="tidpdf_'.$t['tid'].'" target="_blank"'." ><img src='".$mosConfig_live_site."/images/M_images/pdf_button.png' alt='".$status_txt."' title='".$status_txt."' />".'</a>';
	  $processing_html2 = '<a href="#" id="tid_'.$t['tid'].'_2" onclick="javascript:return op_runCmd('."'sendXml'".', this);"'." ><img src='".$mosConfig_live_site."/administrator/components/com_onepage/assets/images/process.png' alt='RECREATE' title='RECREATE' /></a>";
	  $email = '   <a href="#" id="tid_'.$t['tid'].'_email" onclick="javascript:return op_runCmd('."'sendEmail'".', this);"><img src="'.$mosConfig_live_site.'/images/M_images/emailButton.png" /></a>';
	  if (empty($t['tid_email'])) $email = '';
	  $created_html = $created_html.$processing_html2.$email;
   	  ?><div id="tiddiv_<?php echo $t['tid'] ?>_CREATED" <?php if ($status != 'CREATED') echo ' style="display: none;" '; ?>><?php
	  echo $created_html;
	  ?></div><?php
	  $processing_html = '<a href="'.$this->ehelper->getExportItemLink($t['tid'], $order_id).'" id="tid_'.$t['tid'].'_proc" onclick="'."javascript:return op_runCmd('sendXml', this);".'" >'."<img src='".$mosConfig_live_site."/media/system/images/mootree_loader.gif' alt='".$status_txt."' title='".$status_txt."' /></a>";
	  ?><div id="tiddiv_<?php echo $t['tid'] ?>_PROCESSING" <?php if ($status != 'PROCESSING') echo ' style="display: none;" '; ?>><?php
	  echo $processing_html;
	  ?></div><?php
      $error_html = '<a href="'.$this->ehelper->getExportItemLink($t['tid'], $order_id).'" id="tid_'.$t['tid'].'_err" onclick="'."javascript:return op_runCmd('sendXml', this);".'" >'."<img  src='".$mosConfig_live_site."/administrator/components/com_media/images/remove.png' alt='".$status_txt."' title='".$status_txt."' /></a>";
	  ?><div id="tiddiv_<?php echo $t['tid'] ?>_ERROR" <?php if ($status != 'ERROR') echo ' style="display: none;" '; ?>><?php
	  echo $error_html;
	  ?></div>
	  
	  
	  
	  <?php
	 ?>
	 
	 </div>
	 </td>
	 <?php for ($i = 0; $i<$max_specials; $i++) { ?>
	 <td>
	 <?php //echo $this->ehelper->getXml($t['tid']);
	 //var_dump($this->ehelper->getXml($t['tid']));
	 if (!empty($t['tid_special']))
	 {
	 ?>
	 <input type="text" style="background-color: #ffaacc;" id="specialentry_<?php echo $t['tid'].'_'.$i; ?>" name="specialentry_<?php echo $t['tid']; ?>" size="12" <?php 
	 	 if (!empty($t['tid_ai']) || (!empty($t['tid_special'])))
	 	 {
	 	  if (empty($specials[$i])) $specials[$i] = "";
	 	  echo ' value="'.$specials[$i].'" ';
	 	 }
	 	 
	 	?>/>
	 <?php 
	 }
	 ?>
	 </td>
	 <?php 
	 }
	 ?>	 
	 </tr>
	 <?php
	}
    ?>
    </table>
    <?php
	echo '</div>';
	echo $pane->endPanel();
	}
	echo $pane->startPanel($VM_LANG->_('PHPSHOP_ORDER_STATUS_CHANGE'), "order_change_page");

		//$tab->startPane( "order_change_pane" );
		//$tab->startTab(  $VM_LANG->_('PHPSHOP_ORDER_STATUS_CHANGE'), "order_change_page" );
		echo '<div style="min-height: 280px;">';
			?>
			
			<table>
			 <tr>
			  <th colspan="2"><?php echo $VM_LANG->_('PHPSHOP_ORDER_STATUS_CHANGE') ?></th>
			 </tr>
			 <tr>
			  <td class="labelcell"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') .":"; ?>
			  </td>
			  <td><?php 
			  ob_start();
			  $ps_order_status->list_order_status($db->f("order_status")); 
			  $html = ob_get_clean();
			  $html = str_replace('name="', ' id="order_status" name="', $html);
			  echo $html;
			  ?>
				  <input type="submit" onclick="javascript:op_runCmd('orderstatusset');" class="button" name="Submit" value="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
					<input type="hidden" name="view" value="order_details" />
					<input type="hidden" name="vmtoken" value="<?php echo vmSpoofValue($sess->getSessionId()) ?>" />
				
					<input type="hidden" id="current_order_status" name="current_order_status" value="<?php $db->p("order_status") ?>" />
					<input type="hidden" name="order_id" value="<?php echo $order_id ?>" />
			  </td>
			 </tr>
			 <tr>
			  <td class="labelcell" valign="top"><?php echo $VM_LANG->_('PHPSHOP_COMMENT') .":"; ?>
			  </td>
			  <td>
				<textarea id="order_comment_s" name="order_comment" rows="5" cols="25"></textarea>
			  </td>
			  </tr>
			  <tr>
			 
			  <td class="labelcell"><label for="notify_customer"><?php echo $VM_LANG->_('PHPSHOP_ORDER_LIST_NOTIFY') ?></label></td>
			  <td><input type="checkbox" name="notify_customer" id="notify_customer" checked="checked" value="Y" /></td> 
				  </tr>
				  <tr>
				<td class="labelcell"><label for="include_comment"><?php echo $VM_LANG->_('PHPSHOP_ORDER_HISTORY_INCLUDE_COMMENT') ?></label>
			  </td>
			  <td>
			  <input type="checkbox" name="include_comment" id="include_comment" checked="checked" value="Y" /> 
				  </td>
			 </tr>
			</table>
			</div>
			
				<?php
				echo $pane->endPanel();
			$orderr = getRefOrders($order_id, true);

			if (!empty($orderr))
			{

				echo $pane->startPanel('Referals', "ref_page" );

//			$tab->endTab();
//			$tab->startTab( 'Referals', "ref_page" );
			echo '<div style="min-height: 250px; padding-left: 10px; width: 90%;">';
			
			if (!empty($orderr))
			{
			$ref = urldecode(urldecode($orderr[$order_id]['ref'][0]['ref']));
			echo '<h2>'.$ref.'</h2>';
			//var_dump($orderr['ref']);
			
			 foreach ($orderr[$order_id]['ref'] as $ind=>$ref)
 			{
   			if (empty($ref['title'])) $ref['title'] = urldecode(urldecode($ref['url']));
   			echo '<span>'.$ref['time'].' <a href="'.urldecode(urldecode($ref['url'])).'" target="_blank">'.urldecode(urldecode($ref['title'])).'</a></span>';
   			echo '<br style="clear: both;"/>';
   			if (strpos($ref['ref'], 'http')!==false)
   			if ($ind>0)
   				{
				if ($ref['ref'] != $orderr[$order_id]['ref'][$ind-1]['url'])
				{
			echo '<div style="padding-left: 20px;">';
    		echo '<span>Ref: <a href="'.urldecode(urldecode($ref['ref'])).'" target="_blank">'.urldecode($ref['ref']).'</a></span>';
      		echo '<br style="clear: both;"/>';
      		echo '</div>';
    			}
   				}
   			else
 			{
   				echo '<span style="padding-left: 10%;">Ref: <a href="'.urldecode($ref['ref']).'" target="_blank">'.urldecode($ref['ref']).'</a></span>'; 
   				echo '<br style="clear: both;"/>';
 			}  
 
 			}
			}
			
			echo '</div>';
			echo $pane->endPanel();
			}
			echo $pane->endPane();
			echo '</div>';
//			$tab->endTab();
//			$tab->endPane();
	/* </table>
	*/
			?>
		  
	  
	 
	  &nbsp;
	  
	  <div style="width: 100%; float: left; clear: both; padding-bottom: 10px;">
      <?php
	  $pane2e =& JPane::getInstance('sliders', array('allowAllClose'=>true));
	  echo $pane2e->startPane('history_slider');
	  echo $pane2e->startPanel( $VM_LANG->_('PHPSHOP_ORDER_HISTORY'), 'historysid');
	  ?>
	  <div style="">
				
			<table class="adminlist">
			 <tr >
			  <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_HISTORY_DATE_ADDED') ?></th>
			  <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_HISTORY_CUSTOMER_NOTIFIED') ?></th>
			  <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_LIST_STATUS') ?></th>
			  <th><?php echo $VM_LANG->_('PHPSHOP_COMMENT') ?></th>
			 </tr>
			 <?php 
			 $order_events = array_reverse($order_events);
			 foreach( $order_events as $order_event ) {
			  echo "<tr>";
			  echo "<td>".$order_event->date_added."</td>\n";
			  echo "<td align=\"center\"><img alt=\"" . $VM_LANG->_('VM_ORDER_STATUS_ICON_ALT') ."\" src=\"$mosConfig_live_site/administrator/images/";
			  echo $order_event->customer_notified == 1 ? 'tick.png' : 'publish_x.png';

			  echo "\" border=\"0\" align=\"absmiddle\" /></td>\n";
			  echo "<td>".$order_event->order_status_code."</td>\n";
			  echo "<td>".$order_event->comments."</td>\n";
			  echo "</tr>\n";
			 }
			 ?>
			</table>
			</div>
	<?php
	echo $pane2e->endPanel();
	echo $pane2e->endPane();
	
	?>
	<div style="width: 48%; float: left; margin-right: 2%;">
      <?php
		  
		  $dbt = new ps_DB;
		  $qt = "SELECT * from #__vm_order_user_info WHERE user_id='$user_id' AND order_id='$order_id' ORDER BY address_type ASC"; 
		  $dbt->query($qt);
		  $dbt->next_record();
    	require_once( CLASSPATH . 'ps_userfield.php' );
    	$userfields = ps_userfield::getUserFields('registration', false, '', true, true );
    	$shippingfields = ps_userfield::getUserFields('shipping', false, '', true, true );
    	
    	echo $pane->startPane('billtoa');
    	echo $pane->startPanel($VM_LANG->_('PHPSHOP_ORDER_PRINT_BILL_TO_LBL'), 'billtopane');
    	echo '<div>';
	   ?> 
		<?php 
		//$onblur = ' onblur="javascript:op_blur(this);" ';
		$onblur = ' onmouseover="javascript:opLight(this);" ';
		$onblur .= ' onmouseout="javascript:opLightOut(this);" ';
		$onblur .= ' onfocus="javascript:op_focus(this);" ';
		$onblur .= ' onkeydown="if ((event.keyCode == 13) || (event.keyCode == 27)) return op_key(this, event); else return true;" ';
		
		
		foreach( $userfields as $field ) {
			if( $field->name == 'email') $field->name = 'user_email';
			if($field->type == 'captcha') continue;
			?>
			<div class="divleft"><?php echo $VM_LANG->_($field->title) ? $VM_LANG->_($field->title) : $field->title ?>:</div>
			<div class="divright"><?php
				// this part is copied and modified from ps_userfield 
				
				
				switch($field->name) {
		          	case 'country':
		          		require_once(CLASSPATH.'ps_country.php');
		          		$country = new ps_country();
		          		$default['country'] = $dbt->f($field->name);
		          		$dbc = $country->get_country_by_code($dbt->f($field->name));
	          			if( $dbc !== false ) 
	          			{
	          			echo '<input type="hidden" id="orig_bt_'.$field->name.'" value="'.urlencode($dbt->f($field->name)).'" />';
	          			?>
	          			
	          			<?php
	          			foreach ($userfields as $f2)
	          			{
	          			 if ($f2->name == 'state')
	   						$onchange = "onchange=\"changeStateList2();\"";
	   					}
	   					if (!isset($onchange))
	   					$onchange = "";
	          			$ps_html->list_country("bt_country", $dbt->sf($field->name, true, false), "id=\"bt_country\" class=\"inputfield\" $onchange $onblur ");
	          			}
	          			printWrapper('bt_country');
	          			
		          		break;
		          	case 'state':
		          		$val = $dbt->f($field->name);
		          		if ($val == '-') $val = ' - ';
		          		echo '<input type="hidden" id="orig_bt_'.$field->name.'" value="'.urlencode($val).'" />';
		          		$html = $ps_html->dynamic_state_lists( "bt_country", "bt_state", $dbt->sf('country', true, false), $dbt->sf('state', true, false) );
		          		$html = str_replace('id="state"', " class=\"inputfield\" $onblur id=\"bt_state\" ", $html);
		          		echo $html;
		          		printWrapper('bt_state');

				    	//echo $ps_html->list_states("state", $dbt->sf('state', true, false), $dbt->sf('country', true, false), "id=\"state_field\" ");
	   					break;
		          	default:
		          	echo '<input type="text" autocomplete="off" class="inputfield" value="';
		          	  $fieldvalue = $dbt->f($field->name);
		          		if ( is_null($fieldvalue) OR $fieldvalue == "" ) {
		          		  echo " ";
		          		} else {
                    		echo $fieldvalue;
                  		}
                  echo '" size="60" style="border-style: none;" id="bt_'.$field->name.'" '.$onblur.' />';
                  printWrapper('bt_'.$field->name);
                  
                  echo '<input type="hidden" id="orig_bt_'.$field->name.'"  value="';
		          	  
		          		if ( is_null($fieldvalue) OR $fieldvalue == "" ) {
		          		  echo urlencode(" ");
		          		} else {
                    		echo urlencode($fieldvalue);
                  		}
                  echo '" />';

                  
		          		break;
		          		
		          }
		          ?>
			</div><br style="clear: both"/>
		  
		  <?php
			}
		   if (false)
		   {
		   ?>
		   
    <div class="divleft"><?php echo $VM_LANG->_('PHPSHOP_ORDER_CHANGE_UPD_BILL') ?>:</div>
	<div class="divright">
    
  	<select name="bill_to">
  		<?php
  		$dbs = new ps_DB;
      $q  = "SELECT user_id, last_name, first_name FROM #__vm_user_info WHERE address_type = 'BT' ORDER BY last_name ASC"; 
  		$dbs->query($q);
  		while ($dbs->next_record()){
  		  if (!is_null( $dbs->f('last_name') )) {
    			print '<option value="'.$dbs->f('user_id').'"';
    			if($dbs->f('user_id') == $user_id) print " selected ";
  			  print '>';
    			print $dbs->f('last_name');
    			print ", ".$dbs->f('first_name');
    			print '</option>';
  			}
  		}
  		?>
  	</select>
  	<input type="image" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
  	<input type="hidden" value="1" name="change_bill_to" />
  	</div><br style="clear: both"/>
    &nbsp;
    

	  <?php
	  }
	  echo '</div>';
	  echo $pane->endPanel();
	  echo $pane->endPane();
	?> </div> <?php  
	  // Get Ship To Address
	  $dbt->next_record();
	  ?> 
	 <div style="width: 50%; float: left; ">
		<?php 
		echo $pane->startPane('shipp');
		echo $pane->startPanel($VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIP_TO_LBL'), 'shippaddresspanel');
		echo '<div>';
		foreach( $shippingfields as $field ) {
			if( $field->name == 'email') $field->name = 'user_email';
			?>
			<div class="divleft"><?php echo $VM_LANG->_($field->title) ? $VM_LANG->_($field->title) : $field->title ?>:</div>
			<div class="divright"><?php
				switch($field->name) {
		          	case 'country':
		          			          		require_once(CLASSPATH.'ps_country.php');
		          		$country = new ps_country();
		          		$default['country'] = $dbt->f($field->name);
		          		$dbc = $country->get_country_by_code($dbt->f($field->name));
	          			//if( $dbc !== false ) echo $dbc->f('country_name');
	          			if( $dbc !== false ) 
	          			{
	          			echo '<input type="hidden" id="orig_st_'.$field->name.'" value="'.$dbt->f($field->name).'" />';
	          			printWrapper('st_'.$field->name);
	          			foreach ($userfields as $f2)
	          			{
	          			 if ($f2->name == 'state')
	   						$onchange = "onchange=\"changeStateList3();\"";
	   					}
	   					if (!isset($onchange))
	   					$onchange = "";
	   					
	   					
	          			$ps_html->list_country("st_country", $dbt->sf($field->name, true, false), "id=\"st_country\" $onchange $onblur ");
	          			}
		          		break;
		          	
		          	
/*		          	
		          		$dbc = $country->get_country_by_code($dbt->f($field->name));
		          		if( $dbc !== false ) echo $dbc->f('country_name');
		          		break;
		  */
		  				case 'state':
		          		$val = $dbt->f($field->name);
		          		if ($val == '-') $val = ' - ';
		          		echo '<input type="hidden" id="orig_st_'.$field->name.'" value="'.$val.'" />';
		          		$html = $ps_html->dynamic_state_lists( "st_country", "st_state", $dbt->sf('country', true, false), $dbt->sf('state', true, false) );
		          		$html = str_replace('id="state"', " $onblur id=\"st_state\" ", $html);
		          		echo $html;
        				printWrapper('st_'.$field->name);
		          		echo '<script language="javascript" type="text/javascript">//<![CDATA[
		          		function changeStateList3() { 
						  var selected_country = null;
						  var country = document.getElementById("st_country_field");
							  for (var i=0; i<country.length; i++)
				 				if (country[i].selected)
					selected_country = country[i].value;
			  		changeDynaList("st_state",states, selected_country, originalPos, originalOrder);
			  
						}'."\n".'//]]></script>';
				    	//echo $ps_html->list_states("state", $dbt->sf('state', true, false), $dbt->sf('country', true, false), "id=\"state_field\"");
	   					break;
		          	
		          	default:
		          	echo '<input type="text" value="';
		          	  $fieldvalue = $dbt->f($field->name);
		          		if ( is_null($fieldvalue) OR $fieldvalue == "" ) {
		          		  echo " ";
		          		} else {
                    		echo $fieldvalue;
                  		}
                  echo '" size="60" style="border-style: none;" id="st_'.$field->name.'" '.$onblur.' />';
                  echo '<input type="hidden" id="orig_st_'.$field->name.'"  value="';
		          	  
		          		if ( is_null($fieldvalue) OR $fieldvalue == "" ) {
		          		  echo " ";
		          		} else {
                    		echo $fieldvalue;
                  		}
                  echo '" />'; printWrapper('st_'.$field->name);
		          	
		          	
		          	 
                  		break;
                  		}
		          ?>
			</div><br style="clear: both"/>
		  
		  <?php
			}
		   ?>
  		<div class="divleft"><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_CHANGE_UPD_SHIP') ?>:</strong></div>
  		<div class="divright">
    		
      		<select name="ship_to" id="ship_to_id">
        		<?php
        		$dbs = new ps_DB;
            $q  = "SELECT user_info_id, address_type_name FROM #__vm_user_info WHERE user_id = '" . $user_id . "' ORDER BY address_type_name ASC"; 
        		$dbs->query($q);
        		while ($dbs->next_record()){
        		  if (!is_null( $dbs->f('user_info_id') )) {
          			print '<option value="'.$dbs->f('user_info_id').'">';
          			print $dbs->f('address_type_name');
          			print '</option>';
        			}
        		}
        		?>
      		</select>
      		<input type="image" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"
      		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" onclick="javascript:return op_runCmd('shipchange', this);"/>
      		<input type="hidden" value="1" name="change_ship_to" />
      	</div><br style="clear: both"/>
     <?php 
     echo "</div>";
     echo $pane->endPanel();
     echo $pane->endPane();
     ?>
    </div>
    </div>
	&nbsp;
	<div style="width: 100%; clear: both;">
			<table  class="adminlist">
			  <tr > 
				<th class="title" width="5%" align="left"><?php echo $VM_LANG->_('PHPSHOP_ORDER_EDIT_ACTIONS') ?></th>
				<th class="title" width="50" align="left"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_QUANTITY') ?></th>
				<th class="title" width="*" align="left"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_NAME') ?></th>
				<th class="title" width="10%" align="left"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SKU') ?></th>
				<th class="title" width="10%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') ?></th>
				<th class="title" width="50"><?php echo $VM_LANG->_('PHPSHOP_PRODUCT_FORM_PRICE_NET') ?></th>
				<th class="title" width="50"><?php echo $VM_LANG->_('PHPSHOP_PRODUCT_FORM_PRICE_GROSS') ?></th>
				<th class="title" width="5%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL') ?></th>
			  </tr>
			  <?php
			  $dbt = new ps_DB;
			  $qt  = "SELECT order_item_id,product_quantity,order_item_name,order_item_sku,product_id,product_item_price,product_final_price, product_attribute, order_status
						FROM `#__vm_order_item`
						WHERE #__vm_order_item.order_id='$order_id' ";
			  $dbt->query($qt);
			  $i = 0;
			  $dbd = new ps_DB();
			  while ($dbt->next_record()){
				if ($i++ % 2) {
				   $bgcolor='row0';
				} else {
				  $bgcolor='row1';
				}
				$t = $dbt->f("product_quantity") * $dbt->f("product_final_price");
				// Check if it's a downloadable product
  			$downloadable = false;
  			$files = array();
  			$dbd->query('SELECT product_id, attribute_name
  							FROM `#__vm_product_attribute`
  							WHERE product_id='.$dbt->f('product_id').' AND attribute_name=\'download\'' );
  			if( $dbd->next_record() ) {
  				$downloadable = true;
  				$dbd->query('SELECT product_id, end_date, download_max, download_id, file_name
  							FROM `#__vm_product_download`
  							WHERE product_id='.$dbt->f('product_id').' AND order_id=\''.$order_id .'\'' );
  				while( $dbd->next_record() ) {
  					$files[] = $dbd->get_row();
  				}
  			}
			  ?>
			  <tr class="<?php echo $bgcolor; ?>" valign="top">
          <?php $order_item_id = $dbt->f("order_item_id"); ?>
          <td width="5%">
  		   
    		<input type="image" title="<?php echo $VM_LANG->_('PHPSHOP_DELETE') ?>"  
    		src="<?php echo IMAGEURL ?>ps_image/delete_f2.gif"   id="deleteitem_<?php echo $order_item_id ?>" onclick="javascript:op_runCmd('deleteItem', this);"  alt="<?php echo $VM_LANG->_('PHPSHOP_DELETE') ?>" />
        	<input type="hidden" value="1" name="change_delete_item" />
    		<input type="hidden" name="order_item_id_<?php echo $order_item_id ?>" value="<?php echo $order_item_id ?>" />  
    		<input type="hidden" name="order_id" value="<?php echo $order_id ?>" />
  		 
		 </td>
		
          <?php $product_quantity = $dbt->f("product_quantity"); ?>
          	<td>
      	
    	  <input type="text" value="<?php echo $product_quantity ?>" id="productquantity_<?php echo $order_item_id ?>" name="productquantity_<?php echo $order_item_id ?>" size="5" />
    		<input type="image" id="quantityupdate_<?php echo $order_item_id ?>" onclick="javascript:op_runCmd('quantityupdate', this);"    title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" 
    		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
    		<input type="hidden" value="1" name="change_item_quantity" />
    		<input type="hidden" name="order_item_id_<?php echo $order_item_id ?>" value="<?php echo $order_item_id ?>" />  

		 
    	</td>
        <td width="30%" align="left">
				<?php $dbt->p("order_item_name"); 
  			  echo "<br /><span style=\"font-size: smaller;\">" . ps_product::getDescriptionWithTax($dbt->f("product_attribute")) . "</span>"; 
  			  if( $downloadable ) {
  			  	echo '<br /><br />
  			  			<div style="font-weight:bold;">'.$VM_LANG->_('VM_DOWNLOAD_STATS') .'</div>';
  			  	if( empty( $files )) {
  			  		echo '<em>- '.$VM_LANG->_('VM_DOWNLOAD_NOTHING_LEFT') .' -</em>';
//  			  		
	    require_once(CLASSPATH.'ps_function.php');
	    $ps_function = new ps_function;
	    $enable_download_function = $ps_function->get_function('insertDownloadsForProduct');
  			  		if( $perm->check( $enable_download_function['perms'] ) ) {
  			  		echo '
  			  			<input type="hidden" name="product_id" value="'.$dbt->f('product_id').'" />
  			  			<input type="hidden" name="user_id" value="'.$db->f('user_id').'" />
  			  			<input type="hidden" name="func" value="insertDownloadsForProduct" />
  						<input type="hidden" name="vmtoken" value="'. vmSpoofValue($sess->getSessionId()) .'" />
  			  			
  			  			<input class="button" type="submit" name="submit" value="'.$VM_LANG->_('VM_DOWNLOAD_REENABLE').'" />
  			  			';
  			  		}
  			  	} else {
  			  		foreach( $files as $file ) {
  			  			echo '<em>'
  			  					.'<a href="'.$sess->url( $_SERVER['PHP_SELF'].'?page=product.file_form&amp;product_id='.$dbt->f('product_id').'&amp;file_id='.$db->f("file_id")).'&amp;no_menu='.@$_REQUEST['no_menu'].'" title="'.$VM_LANG->_('PHPSHOP_MANUFACTURER_LIST_ADMIN').'">'
  			  					.$file->file_name.'</a></em><br />';
  			  			echo '<ul>';
  			  			echo '<li>'.$VM_LANG->_('VM_DOWNLOAD_REMAINING_DOWNLOADS') .': '.$file->download_max.'</li>';
  			  			if( $file->end_date > 0 ) {
  			  				echo '<li>'.$VM_LANG->_('VM_EXPIRY').': '.vmFormatDate( $file->end_date + $mosConfig_offset ).'</li>';
  			  			}
  			  			echo '</ul>';
  			  			echo '		  		
  			  		
  			  			<input type="hidden" name="page" value="'.$page.'" />
  			  			<input type="hidden" name="func" value="mailDownloadId" />
  						  <input type="hidden" name="vmtoken" value="'. vmSpoofValue($sess->getSessionId()) .'" />
  			  			
  			  			<input class="button" type="submit" name="submit" value="'.$VM_LANG->_('VM_DOWNLOAD_RESEND_ID').'" />
  			  			';
  			  		}
  			  		
  			  	}
  			  }
				  ?>
				</td>
				<td width="10%" align="left"><?php  $dbt->p("order_item_sku") ?></td>
  			<td width="10%">
  				
  				<?php echo "<strong>".$VM_LANG->_('PHPSHOP_ORDER_PRINT_PO_STATUS') .": </strong>";
  				 ob_start();
  			 	 $ps_order_status->list_order_status($dbt->f("order_status")); 
  			 	 $html = ob_get_clean();
  			 	 $item_id = $dbt->f('order_item_id');
  			 	 $html = str_replace('name="order_status"', 'id="order_status_'.$item_id.'" name="order_status_item_'.$item_id.'"', $html);
  			 	 echo $html;
  			 	 ?>
  				<input type="button" id='itemid_<?php $dbt->p("order_item_id") ?>' class="button" onclick="javascript:op_runCmd('orderitemstatusset', this);"  name="Submit" value="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
  				
  				<input type="hidden" name="vmtoken" value="<?php echo vmSpoofValue($sess->getSessionId()) ?>" />
  			
  				<input type="hidden" id="itemcurrentstatus_<?php $dbt->p("order_item_id") ?>" name="current_order_status" value="<?php $dbt->p("order_status") ?>" />
  			
  				<input type="hidden" name="order_item_id" value="<?php $dbt->p("order_item_id") ?>" />
  				
  			</td>
				<td align="right">
          <?php $product_item_price =  $dbt->f("product_item_price"); ?>
          
    	  <input type="text" value="<?php echo $product_item_price ?>" id="productitemprice_<?php echo $order_item_id ?>" name="productitemprice_<?php echo $order_item_id ?>" size="15" />
    		<input type="image" onclick="javascript:op_runCmd('productItemPrice', this);" id="productitempriceupdate_<?php echo $order_item_id ?>" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" 
    		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
    		<input type="hidden" value="1" name="change_product_item_price" />
    		<input type="hidden" name="order_item_id" value="<?php echo $order_item_id ?>" />  
    		<input type="hidden" name="order_id" value="<?php echo $order_id ?>" />  
		 
        </td>				
				<td align="right">
          <?php $product_final_price = $dbt->f("product_final_price"); ?>
          
    	  <input type="text" value="<?php echo $product_final_price ?>" name="product_final_price_<?php echo $order_item_id ?>" size="5" />
    		<input  onclick="javascript:op_runCmd('productFinalItemPrice', this);" id="productfinalitempriceupdate_<?php echo $order_item_id ?>"  type="image" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" 
    		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
    		<input type="hidden" value="1" name="change_product_final_price" />
    		<input type="hidden" name="order_item_id" value="<?php echo $order_item_id ?>" />  
    	
		 
        </td>
				<td width="5%" align="right" style="padding-right: 5px;"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($t, '', $db->f('order_currency')); ?></td>
			  </tr>
			  <?php 
			  } 
			  ?> 
			  
			  </table>
			  </div>
			  <div style="width: 100%; float: left;">
				  <div class="leftd"><strong> <?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SUBTOTAL') ?>: </strong></div>
				  <div class="rightd"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_subtotal"), '', $db->f('order_currency')); ?>
					</div>
				  <br style="clear: both;"/>
			  
	  <?php
			  /* COUPON DISCOUNT */
			$coupon_discount = $db->f("coupon_discount");
			
	  
			if( PAYMENT_DISCOUNT_BEFORE == '1') {
			  if ($db->f("order_discount") != 0) {
	  ?>
				<div class="leftd">
				<strong><?php 
				  if( $db->f("order_discount") > 0)
					 echo $VM_LANG->_('PHPSHOP_PAYMENT_METHOD_LIST_DISCOUNT');
				  else
					 echo $VM_LANG->_('PHPSHOP_FEE');
					?>:</strong>
				</div>
				<div class="rightd"><?php
					  if ($db->f("order_discount") > 0 )
					 echo "-" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency'));
				elseif ($db->f("order_discount") < 0 )
					 echo "+" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency')); ?>
				  </div>
			  <br style="clear:both;"/>
			  
			  
			  <?php 
			  } 
			  if( $coupon_discount > 0 || $coupon_discount < 0) {
	  ?>
			
				<div class="leftd"><strong><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:</strong></div>
				<div class="rightd"><?php
				  echo "- ".$GLOBALS['CURRENCY_DISPLAY']->getFullValue( $coupon_discount, '', $db->f('order_currency') ); ?>
				</div><br style="clear: both;"/>
			
			  <?php
			  }
			}
	  ?>
			  
			  
  				<div class="leftd"><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_TOTAL_TAX') ?>:</strong></div>
  				<div class="rightd"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_tax"), '', $db->f('order_currency')) ?></div>
			  <br style="clear: both;"/>
			  
				<div class="leftd"><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING') ?>:</strong></div>
				<div class="rightd"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_shipping"), '', $db->f('order_currency')) ?></div>
			  <br style="clear: both;"/>
			  
				<div class="leftd"><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_TAX') ?>:</strong></div>
				<div class="rightd"><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_shipping_tax"), '', $db->f('order_currency')) ?></div>
			  <br style="clear: both;"/>
	  <?php
			if( PAYMENT_DISCOUNT_BEFORE != '1') {
			  if ($db->f("order_discount") != 0) {
	  ?>
			  
				<div class="leftd"><strong><?php 
				  if( $db->f("order_discount") > 0)
					echo $VM_LANG->_('PHPSHOP_PAYMENT_METHOD_LIST_DISCOUNT');
				  else
					echo $VM_LANG->_('PHPSHOP_FEE');
					?>:</strong></div>
				<div class="rightd"><?php
					  if ($db->f("order_discount") > 0 )
					 echo "-" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency'));
				elseif ($db->f("order_discount") < 0 )
					 echo "+" . $GLOBALS['CURRENCY_DISPLAY']->getFullValue(abs($db->f("order_discount")), '', $db->f('order_currency')); ?>
				  </div>
				  
			<br style="clear: both;"/>
			  
			  <?php 
			  } 
			  if( $coupon_discount > 0 || $coupon_discount < 0) {
	  ?>
			  
  				<div class="leftd"><strong><?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:</strong></div> 
  				<div class="rightd"><?php echo "- ".$GLOBALS['CURRENCY_DISPLAY']->getFullValue( $coupon_discount, '', $db->f('order_currency') ); ?></div>
			  	<br style="clear: both;"/>
			  <?php
			  }
			}
	  ?>
			  
				<div class="leftd"><strong><?php echo $VM_LANG->_('PHPSHOP_CART_TOTAL') ?>:</strong></div>
				<div class="rightd"><strong><?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($db->f("order_total"), '', $db->f('order_currency')); ?></strong>
				  </div>
			  	<br style="clear: both;"/>
			  <?php
				  // Get the tax details, if any
				  $tax_details = ps_checkout::show_tax_details( $db->f('order_tax_details'), $db->f('order_currency') );
			  ?>
			  <?php if( !empty( $tax_details ) ) : ?>
			    <div class="leftd">Tax details</div>
				<div class="rightd"><?php echo $tax_details; ?></div>
			  	<br style="clear: both;"/>
			  <?php endif; ?>
			 </div>
			  <?php
	if (false)
	{
       		require_once(CLASSPATH . 'ps_product_attribute.php');
		$ps_product_attribute = new ps_product_attribute;
		
		// Get product_id
    $product_id = vmGet( $_REQUEST, 'product_id' );
    $product_id_bysku = vmGet( $_REQUEST, 'product_id_bysku' );
    
    // If sku was selected it overwrites the product_id
    if ($product_id_bysku > 0) {
			$product_id = $product_id_bysku;
		}
    
    // Output to generate a "return to parant"-button
		$html_return_parent = '
		<input type="submit" value="' . $VM_LANG->_('PHPSHOP_ORDER_EDIT_RETURN_PARENTS') . '" />
		<input type="hidden" name="product_id" value="-1" />
		<input type="hidden" name="add_product_validate" value="0" />
		<input type="hidden" name="add_product_item" value="0" />
		<input type="hidden" name="add_product" value="1" />
		<input type="hidden" name="order_edit_page" value="1" />';
		
	
	
   
    // Page reseted = -1 or called first time = ""
    if ($product_id < 0 || $product_id == "") { 
    	// Generate product list
      ?>
  		
  		  <br /><br />
  		
        <table class="adminlist">
          <tr>
            <th> 
            <?php 
            
            echo $VM_LANG->_('PHPSHOP_ORDER_EDIT_ADD_PRODUCT'); 
            ?></th>
          </tr>
          <tr>
            <td align="left"><?php echo list_products($product_id, true); echo list_products($product_id); ?></td>
          </tr>
        </table>
    		<input type="hidden" name="add_product_validate" value="0" />
    		<input type="hidden" name="add_product_item" value="0" />
		    <input type="hidden" name="add_product" value="1" />
		    
    		
      
      <?php
    }
  	else {
      // Query child products
  		$db_prod = new ps_DB;
  		$q = "SELECT product_id FROM #__vm_product WHERE ";
  		$q .= "product_parent_id = '".$product_id."'";
  		$db_prod->query($q);

      // Are there childs?
  		if ( $db_prod->num_rows()) {
         // Yes! Drop down list to select the child
         ?>
    		
    		  <br /><br />
          <table class="adminlist">
            <tr>
              <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_EDIT_ADD_PRODUCT') ?></th>
            </tr>
          </table>
          <table class="adminlist">
            <tr>
              <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_NAME') ?></th> 
            </tr>
            <tr>
              <td>
                <input type="hidden" name="add_product" value="1" />
            		<input type="hidden" name="add_product_validate" value="0" />
                <input type="hidden" name="add_product_item" value="1" />
                <?php echo $this->list_attribute($product_id) ?>
              </td>
            </tr>
          </table>
      		
        
      <?php echo $html_return_parent;
  		}
  		else {
         // No Childs or selected child product! Form to add a product that has no childs
         ?>
    		
    		  <br /><br />
          <table class="adminlist">
            <tr>
              <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_EDIT_ADD_PRODUCT') ?></th>
            </tr>
          </table>
          <table class="adminlist">
            <tr>
              <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_NAME') ?></th> 
              <th><?php echo $VM_LANG->_('PHPSHOP_PRODUCT_FORM_CUSTOM_ATTRIBUTE_LIST') ?></th>
              <th><?php echo $VM_LANG->_('PHPSHOP_PRODUCT_FORM_ATTRIBUTE_LIST') ?></th>
              <th align="left"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_QUANTITY') ?></th>
              <th align="left">Action</th>
            </tr>
            <tr>
            <?php
		        if (vmGet( $_REQUEST, 'add_product_item' ) == 1) {
              echo '<td>' . $this->list_attribute($product_id,false) . '</td>';
              echo '<input type="hidden" name="add_product_item" value="1" />';
            }
		        else {
              echo '<td>' . $this->list_products($product_id, true) . $this->list_products($product_id) . '</td>';
            }?>
              <td><?php echo $ps_product_attribute->list_advanced_attribute($product_id) ?></td>
              <td><?php echo $ps_product_attribute->list_custom_attribute($product_id) ?></td>
              <td>
                <input type="text" value="1" name="product_quantity" size="5" />
            		<input type="hidden" name="add_product_validate" value="1" />
        		    <input type="hidden" name="add_product" value="1" />
              </td>
              <td><input type="submit" value="<?php echo $VM_LANG->_('PHPSHOP_ORDER_EDIT_ADD') ?>" /></td>
            </tr>
          </table>
      	
        
      <?php
		    if (vmGet( $_REQUEST, 'add_product_item' ) == 1) {
          echo $html_return_parent;
        }
      }
     }
  	
	?>
		 </div>
	<?php } ?>
	&nbsp;
	<div style="width: 100%; clear: both;">
	<?php 
	
	if (!empty($ship_method_id))
	{
	?>
	<div style="width: 30%; float: left;">
    				<table class="adminlist">
    				  <tr>
      					<th ><?php 
      							echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_LBL') ?>
      					</th>
    				  </tr>
    				  <tr> 
    					  <td align="left">
        					<?php
                    if( $db->f("ship_method_id") ) { 
                      $details = explode( "|", $db->f("ship_method_id"));
                    }
                  ?>
    					    <strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_CARRIER_LBL') ?>: </strong>
    						  <?php  echo $details[1]; ?>&nbsp;
                </td>
    	        </tr>
    				  <tr>
    					  <td align="left">
    					    <strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_MODE_LBL') ?>: </strong>
    					    <?php echo $details[2]; ?>
                </td>
    				  </tr>
    				  <tr>
    					  <td align="left">
    					    <strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_PRICE_LBL') ?>: </strong>
    					    <?php echo $GLOBALS['CURRENCY_DISPLAY']->getFullValue($details[3], '', $db->f('order_currency')); ?>
    					  </td>
    				  </tr>
    			
              <?php 
              		

    ?>
  			<?php
    		if($db->f('ship_method_id') == "" OR preg_match('/^standard_shipping/', $db->f('ship_method_id'))) {
    		?>
      		<tr>
        		<td>
            		<select name="shipping">
              		<?php
              		echo $sh;
              		?>
              	</select>
            		<input type="image" onclick="javascript:op_runCmd('change_standard_ship', this);"  title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"
            		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
            		<input type="hidden" value="1" name="change_standard_shipping" />
            		
          		
          	</td>
          </tr><?php
         } else {
    		?>
      		<tr>
        		<td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING') ?>: </strong>
          		
            		<input type="text" style="border-style: none;" value="<?php $db->p("order_shipping") ?>" size="5" name="order_shipping" />
            		<input type="image" onclick="javascript:op_runCmd('order_shipping_update', this);" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"
            		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"  alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
            		<input type="hidden" value="1" name="change_shipping" />
            		<input type="hidden" name="order_edit_page" value="1" />
            		
          		
        		</td>     
      		</tr>
      		<tr>
        		<td><strong><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_SHIPPING_TAX') ?>: </strong>
          		
            		<input type="text" style="border-style: none;" value="<?php $db->p("order_shipping_tax") ?>" name="order_shipping_tax" size="5" />
            		<input type="image" onclick="javascript:op_runCmd('order_shipping_tax_update', this);"  title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"
            		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
            		<input type="hidden" value="1" name="change_shipping_tax" />
            	 
        		  
            </td>    
      		</tr>
    		<?php
    		}   
              ?>  
    				</table>
  			  </div>
  			  <?php 
  			  }
  			  ?>
			  <div style="width: 69%; margin-left: 1%; float: left;">
  			  <!-- Payment Information -->
  			  
    				<table class="adminlist">
    				  <tr class="sectiontableheader"> 
      					<th width="13%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYMENT_LBL') ?></th>
      					<th width="40%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_ACCOUNT_NAME') ?></th>
      					<th width="30%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_ACCOUNT_NUMBER'); ?></th>
      					<th width="17%"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_EXPIRE_DATE') ?></th>
    				  </tr>
    				  <tr> 
      					<td width="13%">
                 
                      
  	<select name="new_payment_id">
  		<?php
  		echo $pmh;
  		?>
  	</select>
  	<input onclick="javascript:op_runCmd('change_payment', this);" type="image" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"
  	src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"  alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
  	<input type="hidden" value="1" name="change_payment" />
  	 
  

                </td>
      					<td width="40%"><?php $dbpm->p("order_payment_name");?></td>
      					<td width="30%"><?php 
        					echo ps_checkout::asterisk_pad( $dbaccount->f("account_number"), 4, true );
        					if( $dbaccount->f('order_payment_code')) {
        						echo '<br/>(' . $VM_LANG->_('VM_ORDER_PAYMENT_CCV_CODE') . ': '.$dbaccount->f('order_payment_code').') ';
        					}
        					?>
                </td>
      					<td width="17%"><?php echo vmFormatDate( $dbpm->f("order_payment_expire"), '%b-%Y'); ?></td>
    				  </tr> 
    				<tr class="sectiontableheader">
						<th></th>
						<th></th>
						<th><?php echo $VM_LANG->_('PHPSHOP_CUSTOMER_ISSUE') ?></th>
						<th><?php echo $VM_LANG->_('PHPSHOP_CUSTOMER_STARTDATE') ?></th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><?php echo $dbpm->f("order_payment_issue"); ?></td>
						<td><?php echo vmFormatDate( $dbpm->f("order_payment_start"), '%b-%Y'); ?></td>
					</tr>
    				  <tr class="sectiontableheader"> 
      					<th colspan="4"><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_PAYMENT_LOG_LBL') ?></th>
    				  </tr>
    				  <tr> 
      					<td colspan="4"><?php if($dbpm->f("order_payment_log")) echo $dbpm->f("order_payment_log"); else echo "./."; ?></td>
    				  </tr>
    				  <tr>
    				    <td colspan="2" align="center">

                 		<?php echo $VM_LANG->_('PHPSHOP_PAYMENT_METHOD_LIST_DISCOUNT') ?>:
    				<input type="text" value="<?php $db->p("order_discount") ?>" style="border-style: none;" size="5" name="order_discount" />
    				<input onclick="javascript:op_runCmd('change_discount', this);" type="image" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"
    				src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"   alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
    				<input type="hidden" value="1" name="change_discount" />
    			

                </td>
                <td colspan="2" align="center">
                  
                   <?php echo $VM_LANG->_('PHPSHOP_COUPON_DISCOUNT') ?>:
    		<input type="text" value="<?php $db->p("coupon_discount") ?>" style="border-style: none;" size="5" name="coupon_discount" />
    		<input onclick="javascript:op_runCmd('change_coupon_discount', this);"  type="image" title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"
    		src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"  alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
    		<input type="hidden" value="1" name="change_coupon_discount" />
    	
                </td>
              </tr>
    				</table>
  		  </div>
  		<div style="float: left; width: 100%; clear: both; margin-top: 10px;">
  	
    				<table class="adminlist">
    				  <tr>
    					  <th><?php echo $VM_LANG->_('PHPSHOP_ORDER_PRINT_CUSTOMER_NOTE') ?></th>
    				  </tr>
    				  <tr>
    				    <td valign="top" align="center" width="50%">
             		
  		<textarea name="customer_note" cols="80" rows="5"><?php echo $db->f("customer_note"); ?></textarea>
  		
  		<input type="image" onclick="javascript:op_runCmd('update_customer_note', this);"   title="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>"	src="<?php echo VM_THEMEURL ?>images/edit_f2.gif"  alt="<?php echo $VM_LANG->_('PHPSHOP_UPDATE') ?>" />
  		<input type="hidden" value="1" name="change_customer_note" />
  	
    					  </td>
    				  </tr>
    				</table>
  			  </div>
  		</div>
	<?php
	}
	else {
	  echo $VM_LANG->_('VM_ORDER_NOTFOUND');
	}
}
}
	// debug window
?> 
   <div id="debug_window" ondblclick="javascript:this.style.display='none';" style="position: fixed; bottom: 0px; right: 0px; width: 20%; overflow:scroll; overflow-x: none; height: 30%; display: none; color: black; font-size: 10px; text-align: right; background-color: grey; filter: alpha(opacity=40); opacity: 0.4; ">Hello, close me with double click<br />
   </div>
	</form>
<?php
//echo ob_get_clean();

function getRefOrders($order_id, $tree=false)
	{
	 if (defined('PARTNERS_INSTALLED') && (PARTNERS_INSTALLED=='0')) return "";
	 
	 global $mainframe;

	 $db = JFactory::getDBO();
		 
	 $q = "SHOW TABLES LIKE '".$db->getPrefix()."partners_orders'";
	 $db->setQuery($q);
	 $r = $db->loadResult();
	 if (empty($r)) 
	 {
	 define('PARTNERS_INSTALLED', '0');
	 return "";
	 }
 
	 $q = "SELECT * FROM #__partners_orders where order_id = ".$order_id." order by id desc LIMIT 0,100";
	 $db->setQuery($q);
	 $res = $db->loadAssocList();
	 $msg = $db->getErrorMsg();
	 if (!defined('PARTNERS_INSTALLED'))
	 {
	 if (!empty($msg)) 
	 {
	  define('PARTNERS_INSTALLED', '0');
	  return "";
	 }
	 else 
	  define('PARTNERS_INSTALLED', '1');
	 }
	 if (!isset($res)) return "";
	 
	 $orders = array();
	 $refs = 0;
	 foreach($res as $val)
	 {
	  
	  $order_id = $val['order_id'];
	  $orders[$order_id] = array();
	  
	  $q = "select first_name, last_name from #__vm_order_user_info where order_id = '".$order_id."' and address_type = 'BT' limit 0,1 ";
	  $db->setQuery($q);
	  $row = $db->loadAssoc();
	  $orders[$order_id]['first_name'] = $row['first_name'];
	  $orders[$order_id]['last_name'] = $row['last_name'];
	  $orders[$order_id]['order_total'] = $val['order_total'];
	  $q = "select * from #__partners_ref where order_ref_id = '".$val['id']."' order by start asc";
	  $db->setQuery($q); 
	  $data = $db->loadAssocList();
	  foreach ($data as $k)
	  {
	   $arr = array();
	   
	   $arr['title'] = $k['title'];
	   $arr['url'] = $k['url'];
	   $arr['ref'] = $k['ref'];
	   if (!$tree)
	   {
	    if (empty($arr['ref'])) return '';
	    else
	    {
	     $ref = urldecode(urldecode($arr['ref']));
	     $p1 = strpos($ref, '//');
	     $p2 = strpos($ref, '/', $p1+3);
	     if ($p1 !== false && $p2 !== false)
	     return substr($ref, $p1+2, $p2-$p1-2);
	     else return $arr['ref'];
	    }
	   }
	   if (!empty($arr['ref'])) $refs++;
	   
	   
	   $start = $k['start'];
	   $end = $k['end'];
	   if (!empty($start) && (!empty($end)))
	   {
	    $time = $end-$start;
	    $time = number_format($time, 0).' sec ';
	    $arr['time'] = $time;
	   }
	   else $arr['time'] = 'Unknown';
	   if (empty($orders[$order_id]['ref']))
	   {
	    $orders[$order_id]['ref'] = array();
	    $orders[$order_id]['ref'][] = $arr;
	   }
	   else
	     $orders[$order_id]['ref'][] = $arr;
	  }
	 }
	 if (!$tree) return "";
	 if (empty($refs)) return "";
	 return $orders;
	}
    /**************************************************************************
	 * name: list_products
	 * created by: nfischer
	 * description: Create a list of products
	 * parameters: product_id
	 * returns: html to display
	 **************************************************************************/
	function list_products($product_id, $skumode=false) {
		global $VM_LANG;
		$db = new ps_DB;
		
		// List all products by sku
		if ($skumode) {
		  $sortby = 'product_sku';
		  $select_name = 'product_id_bysku';
		  $reset_other_list = 'this.form.product_id.value=-1';
		  $first_item = $VM_LANG->_('PHPSHOP_ORDER_EDIT_CHOOSE_PRODUCT_BY_SKU');
		}
		// List all products by name
    else {
		  $sortby = 'product_name';
		  $select_name = 'product_id';
		  $reset_other_list = 'this.form.product_id_bysku.value=-1';
		  $first_item = $VM_LANG->_('PHPSHOP_ORDER_EDIT_CHOOSE_PRODUCT');
    }

		$query_list_products = "SELECT DISTINCT `product_name`,`products_per_row`,`category_browsepage`,`category_flypage`";
		$query_list_products .= ",`#__vm_product`.`product_id`,`#__vm_category`.`category_id`,`product_full_image`,`product_thumb_image`";
		$query_list_products .= ",`product_s_desc`,`product_parent_id`,`product_publish`,`product_in_stock`,`product_sku`";
		$query_list_products .= " FROM (`#__vm_product`, `#__vm_category`, `#__vm_product_category_xref`";
		$query_list_products .= ",`#__vm_shopper_group`) LEFT JOIN `#__vm_product_price` ON";
		$query_list_products .= " `#__vm_product`.`product_id` = `#__vm_product_price`.`product_id`";
		$query_list_products .= " WHERE `#__vm_product_category_xref`.`category_id`=`#__vm_category`.`category_id`";
		$query_list_products .= " AND `#__vm_product`.`product_id`=`#__vm_product_category_xref`.`product_id`";
		$query_list_products .= " AND `#__vm_product`.`product_parent_id`='0'";

		$query_list_products .= " AND (( `#__vm_shopper_group`.`shopper_group_id`=`#__vm_product_price`.`shopper_group_id` )";
		$query_list_products .= " OR (`#__vm_product_price`.`product_id` IS NULL))";
		$query_list_products .= " GROUP BY `#__vm_product`.`product_sku` ORDER BY `#__vm_product`.`" . $sortby . "`";
		$db->query($query_list_products);

		$display = '<select name="' . $select_name . '" onChange="this.form.add_product_validate.value=0;' . $reset_other_list . ';this.form.submit();">';
		$display .= '<option value="-1">' . $first_item . '</option>';
		while ($db->next_record()) {
			$display .= '<option value="' . $db->f("product_id") . '"';
			if ($product_id == $db->f("product_id")) {
				$display .= ' selected="yes"';
			}
			if ($skumode) {
        $display .= '>' . $db->f("product_sku") . '</option>';
      }
      else {
        $display .= '>' . $db->f("product_name") . '</option>';      
      }
		}
		$display .= '</select>';
		
		return $display;
	}
	
	function printWrapper($field, $start = false)
	{
	 if ($start == true)
	 {
	  
	 }
	 else
	 {
	 echo '<div id="buttons_'.$field.'" style="display: none; background-color: transparent;">'
	       .'<input type="button" id="update_'.$field.'" value="Update All" size="10" class="ipt blue" onclick="javascript:op_update(this);" />'
			.'<input type="button" id="updateall_'.$field.'" value="Update Order" size="10" class="ipt blue" onclick="javascript:op_update_order(this);" />'
       			 .'<input type="button" id="cancel_'.$field.'" value="Cancel" size="10" class="ipt blue" onclick="javascript:op_cancel(this);" />'
	          			.'</div>';
	 
	 }
	}
	
		 /**************************************************************************
	 * name: list_attribute
	 * created by: nfischer
	 * description: Lists all child/sister products of the given product
	 * parameters: $product_id, $fils
	 * returns: string HTML code with Items, attributes & price
	 **************************************************************************/
	function list_attribute($product_id, $fils=true) {
		global $VM_LANG, $CURRENCY_DISPLAY;

		$ps_product = new ps_product;
		$db = new ps_DB;
		$db_sku = new ps_DB;
		$db_item = new ps_DB;

		if ($fils) {
		  // Generate childlist
			$q = "SELECT product_id,product_name FROM #__vm_product WHERE product_parent_id='$product_id'";
		}
		else {
		  // Child is selected, list siblings
			$q = "SELECT product_parent_id FROM #__vm_product WHERE product_id='$product_id'";
			$db->setQuery($q);
			$db->query();
			$db->next_record();
			$product_parent_id = $db->f("product_parent_id");
			$q = "SELECT product_id,product_name FROM #__vm_product WHERE product_parent_id='$product_parent_id'";
		}
		
		$db->setQuery($q);
		$db->query();
		if( $db->num_rows() > 0 ) {
			$display = '<select name="product_id" onChange="this.form.add_product_validate.value=0;this.form.submit();">';
			$display .= '<option value="-1">' . $VM_LANG->_('PHPSHOP_SELECT') . '</option>';
			while ($db->next_record()) {
				$display .= '<option value="' . $db->f("product_id") . '"';
				if ($product_id == $db->f("product_id")) {
					$display .= ' selected="yes"';
				}
				$display .= '>' . $db->f("product_name");

				if ($fils) {
          $searched_id = $product_id;
        }
        else {
          $searched_id = $product_parent_id;
        }
        
        // For each child get attribute values by looping through attribute list
				$q = "SELECT product_id, attribute_name FROM #__vm_product_attribute_sku ";
				$q .= "WHERE product_id='$searched_id' ORDER BY attribute_list ASC";
				$db_sku->setQuery($q);  $db_sku->query();

				while ($db_sku->next_record()) {
					$q = "SELECT attribute_name, attribute_value, product_id ";
					$q .= "FROM #__vm_product_attribute WHERE ";
					$q .= "product_id='" . $db->f("product_id") . "' AND ";
					$q .= "attribute_name='" . $db_sku->f("attribute_name") . "'";
					$db_item->setQuery($q);  $db_item->query();
					while ($db_item->next_record()) {
						$display .= ' - ' . $db_item->f("attribute_name") . " ";
						$display .= "(" . $db_item->f("attribute_value") . ")";
						if( !$db_sku->is_last_record() )
						$display .= '; ';
					}
				}
				// Attributes for this item are done.
				// Now get item price
				$price = $ps_product->get_price($db->f("product_id"));
				if( $_SESSION["auth"]["show_price_including_tax"] == 1 ) {
					$tax_rate = 1 + $ps_product->get_product_taxrate($db->f("product_id"));
					$price['product_price'] *= $tax_rate;
				}
				$display .= ' - '.$CURRENCY_DISPLAY->getFullValue($price["product_price"]);
				$display .=  '</option>';
			}
	
			$display .= '</select>';			
		}
		else {
			$display= "<input type=\"hidden\" name=\"product_id\" value=\"$product_id\" />\n";
		}

		return $display;
	}	

echo '<script language="javascript" type="text/javascript">//<![CDATA[

		          		function gotocontact( id ) {
						var form = document.adminForm;
						form.target = "_parent";
						form.contact_id.value = id;
						form.option.value = "com_users";
						submitform( "contact" );
						}
						var sendXml = "sendXml";
		          		var opTimer = null;
		          		var opStop = false;
		          		var opTemplates = [];
						var focusedE = null;
						var timeOut = null;
						var tmpElement = null;
						var scrollY = 0;
						var lasttab = 0;
						var deb = document.getElementById("debug_window");	
						function submitbutton(task, formId)
						{
						 if (formId == null) formId = "adminForm";
	 					 var d = document.getElementById("task");
	 					 d.value = task;
	 					 formm = document.getElementById(formId);
	 					 if (formm != null)
	 					 {
	 					  formm.submit();
	 					 }
	 					 
	 				     return true;
						}

		          		function changeStateList2() { 
						  var selected_country = null;
						  var country = document.getElementById("bt_country");
							  for (var i=0; i<country.length; i++)
				 				if (country[i].selected)
					selected_country = country[i].value;
			  		changeDynaList("bt_state",states, selected_country, originalPos, originalOrder);
			  
							} /*var xmlhttp2 = null;*/ var op_url = "'.$mosConfig_live_site.'/administrator/index3.php";
							var op_params = "option=com_onepage&view=order_details&task=ajax&orderid='.$order_id.'&localid='.$order_id.'&ajax=yes&order_number='.$order_number.'"; '."\n".'
							var op_localid = "'.$order_id.'"; 
							var multiOrders = false; ';
							if (!empty($runTimer)) echo ' 
							 opStop = true;
 							 opTimer=setTimeout("op_timer()", 2000);
							';
						$scrollY = JRequest::getVar('scrolly', 0);
						
							echo '
						
							//]]></script>';
							
	if (empty($javascript)) $javascript = '';							
    $javascript .= 'if(window.addEventListener){ // Mozilla, Netscape, Firefox' . "\n";
    $javascript .= '    window.addEventListener("load", function(){ op_init(); }, false);' . "\n";
    $javascript .= '} else { // IE' . "\n";
    $javascript .= '    window.attachEvent("onload", function(){ op_init(); });' . "\n";
    $javascript .= '}';
 
	$doc =& JFactory::getDocument();
	$doc->addScriptDeclaration( $javascript);
							



?>