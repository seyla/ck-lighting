<?php
/*
*
* @copyright Copyright (C) 2007 - 2010 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/

	defined( '_JEXEC' ) or die( 'Restricted access' );
	jimport('joomla.application.component.view');
	if (!class_exists('OnepageTemplateHelper'))
	require ( JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_onepage'.DS.'assets'.DS.'export_helper.php');

	class JViewOrder_details extends OPCView
	{
		function display($tpl = null)
		{	
			global $option, $mainframe;
			
			$order_id = JRequest::getVar('order_id');
			
			//$limit = JRequest::getVar('limit', $mainframe->getCfg('list_limit'));
			//limitstart = JRequest::getVar('limitstart', 0);
			$model = &$this->getModel();
			
			$ehelper = new OnepageTemplateHelper();
			$templates = $ehelper->getExportTemplates('ALL');

			//$templates = $model->getTemplates();
			//$order_data = $model->getOrderData();
		    
		    //$ehelper = new OnepageTemplateHelper($order_id);
			
			$this->assignRef('ehelper', $ehelper);
			$this->assignRef('templates', $templates);
			$this->assignRef('order_data', $order_data);
			jimport('joomla.html.pagination');
			parent::display($tpl); 
		}
	}
?>