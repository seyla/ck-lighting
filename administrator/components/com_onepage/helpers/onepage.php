<?php
// No direct access to this file
defined('_JEXEC') or die;
 
/**
 * OnePage component helper.
 */
abstract class OnepageHelper
{
        /**
         * Configure the Linkbar.
         */
        public static function addSubmenu($submenu) 
        {
                JSubMenuHelper::addEntry(
			JText::_('COM_ONEPAGE_CONFIGURATION_TITLE'),
			'index.php?option=com_onepage&view=config',
			$submenu == 'config'
		);
		JSubMenuHelper::addEntry(
			JText::_('COM_ONEPAGE_OPC_THEME_EDITOR'),
			'index.php?option=com_onepage&view=edittheme',
			$submenu == 'edittheme'
		);
		
	JSubMenuHelper::addEntry(
			JText::_('COM_ONEPAGE_TRACKING_PANEL'),
			'index.php?option=com_onepage&view=tracking',
			$submenu == 'tracking'
		);
		
		JSubMenuHelper::addEntry(
			JText::_('COM_ONEPAGE_UTILS'),
			'index.php?option=com_onepage&view=utils',
			$submenu == 'utils'
		);
		
		 $db = JFactory::getDBO(); 
	 $q = "select shipment_params from #__virtuemart_shipmentmethods where shipment_element = 'pickup_or_free' and published = 1 limit 0,1"; 
	 $db->setQuery($q); 
	 $json = $db->loadResult(); 
	 if (!empty($json))
	  {
	     JSubMenuHelper::addEntry(
			JText::_('COM_ONEPAGE_ROUTE_CONFIG_LINK'),
			'index.php?option=com_onepage&view=pickup',
			$submenu == 'pickup'
		);
	  }
		
		
                // set some global property
				/*
                $document = JFactory::getDocument();
                $document->addStyleDeclaration('.icon-48-helloworld ' .
                                               '{background-image: url(../media/com_helloworld/images/tux-48x48.png);}');
                */
        }
}