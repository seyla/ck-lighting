<?php 
defined( '_JEXEC' ) or die( 'Restricted access' );
if ($data['order_status_0'] == 'C') 
{
echo '<?xml version="1.0" encoding="Windows-1250"?>'; 
//echo var_export($data, true); 
?>


<dat:dataPack id="2223" ico="36644251" application="StwTest" version = "2.0" note="Import FA"        
xmlns:dat="http://www.stormware.cz/schema/version_2/data.xsd"        
xmlns:inv="http://www.stormware.cz/schema/version_2/invoice.xsd"        
xmlns:typ="http://www.stormware.cz/schema/version_2/type.xsd" >

<dat:dataPackItem id="IT<?php echo $data['special_value_ai_0']; ?>" version="2.0">

	<!-- faktura bez polozky s adresou, ale bez vazby na adresar -->
		<inv:invoice version="2.0">
			<inv:invoiceHeader>
				<inv:invoiceType>issuedInvoice</inv:invoiceType>
				<inv:symVar><?php echo $data['special_value_ai_0']?></inv:symVar>
				<inv:date><?php echo date('Y-m-j', $data['cdate_0']); ?></inv:date>
				<inv:dateTax><?php echo date('Y-m-j', $data['cdate_0']); ?></inv:dateTax>
				<inv:number>
					
					<typ:ids>2012</typ:ids>
					<typ:numberRequested><?php 
					$num = $data['special_value_ai_0']; 
					if ((substr($num, 0,4)=='2012') && (strlen($num)>4))
					echo substr($num, 4); 
					else
					echo $data['special_value_ai_0']; ?></typ:numberRequested>
				</inv:number>

				
				<inv:dateAccounting><?php echo date('Y-m-j', $data['cdate_0']); ?></inv:dateAccounting>
				<inv:dateDue><?php echo date('Y-m-j', $data['cdate_0']); ?></inv:dateDue>
				<inv:accounting>
					<typ:ids>opc</typ:ids>
				</inv:accounting>
				
				<inv:classificationVAT>
					<typ:classificationVATType><?php 
					if ((strlen($data['bt_vm_vat_0'])>0) && ($data['bt_country_0']!= 'SVK'))
					echo 'UDzahrSl'; 
					else echo 'UD'; 
					?></typ:classificationVATType>

				</inv:classificationVAT>
				<inv:text>RuposTel services</inv:text>
				<inv:partnerIdentity>
					<typ:address>
						<typ:company><?php echo $data['bt_company_0']; ?></typ:company>
						<typ:division></typ:division>
						<typ:name><?php echo $data['bt_first_name_0'].' '.$data['bt_last_name_0']; ?></typ:name>
						<typ:city><?php echo $data['bt_city_0'] ?></typ:city>
						<typ:street><?php echo $data['bt_address_1_0']; ?></typ:street>
						<typ:zip><?php echo $data['bt_zip_0']; ?></typ:zip>
						<typ:ico></typ:ico>
						<typ:dic><?php echo $data['bt_vm_vat_0']; ?></typ:dic>
						<typ:icDph><?php echo $data['bt_vm_vat_0']; ?></typ:icDph>
					</typ:address>

					<typ:shipToAddress>
					</typ:shipToAddress>
				</inv:partnerIdentity>
				<inv:numberOrder><?php echo $data['order_id_0']; ?></inv:numberOrder>

				<inv:dateOrder><?php echo date('Y-m-j', $data['cdate_0']); ?></inv:dateOrder>
				<inv:paymentType>
					<typ:ids>draft</typ:ids>
				</inv:paymentType>
				
				<inv:note>XML OPC</inv:note>
				<inv:intNote>OPC XML</inv:intNote>
			
				
			</inv:invoiceHeader>			
			<inv:invoiceSummary>
			

				<inv:homeCurrency>
					<typ:priceNone><?php echo $data['order_total_0']; ?></typ:priceNone>
				
				</inv:homeCurrency>
			</inv:invoiceSummary>
		</inv:invoice>

	</dat:dataPackItem>


</dat:dataPack>
<?php
}