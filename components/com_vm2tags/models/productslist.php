<?php
 error_reporting('E_ALL');

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla modelitem library
jimport('joomla.application.component.modelitem');
class Vm2tagsModelproductslist extends JModelItem
{
	public function getPriceformat() 
	{
		$db = &JFactory::getDBO();
		$q ="SELECT curr.*  
		FROM `#__virtuemart_currencies` AS curr 
		LEFT JOIN `#__virtuemart_vendors` AS vend ON vend.`vendor_currency` = curr.`virtuemart_currency_id`  
		WHERE vend.`virtuemart_vendor_id` = '1' ";
		$db->setQuery($q);
		$price_format 	= $db->loadRow();
		$this->_price_format = $price_format;
		return $this->_price_format;
	}
	
	function applytaxes( $pricebefore, $catid ,  $vendor_id){
			$is_shopper = 1;
			
			$db = &JFactory::getDBO();
			$q ="SELECT vc.`virtuemart_calc_id` , vc.`calc_name` , vc.`calc_kind` , vc.`calc_value_mathop` , vc.`calc_value` , vc.`calc_currency` ,  vc.`ordering` 
				FROM `#__virtuemart_calcs` vc 
				LEFT JOIN `#__virtuemart_calc_categories` vcc ON vcc.`virtuemart_calc_id` = vc.`virtuemart_calc_id`
				WHERE vc.`published`='1' 
				AND (vc.`shared` ='1' OR vc.`virtuemart_vendor_id` = '".$vendor_id."' )" ;
			if($is_shopper)
				$q .= " AND vc.`calc_shopper_published` = '1' ";
	
			$q .= "AND (vc.`publish_up`='0000-00-00 00:00:00' OR vc.`publish_up` <= NOW() ) ";
			$q .= "AND (vc.`publish_down`='0000-00-00 00:00:00' OR vc.`publish_down` >= NOW() ) 
			AND vcc.`virtuemart_category_id` ='".$catid."' 
				ORDER BY vc.`ordering` ASC";
			$db->setQuery($q);
			$taxes = $db->loadObjectList();
			$price_withtax = $pricebefore;
			if(count($taxes)>0){
				foreach($taxes as $tax){
					$calc_value_mathop = $tax->calc_value_mathop;
					$calc_value = $tax->calc_value;
					switch ($calc_value_mathop){
						case '+':
							$price_withtax = $price_withtax + $calc_value;
						break;
						case '-':
							$price_withtax = $price_withtax - $calc_value;
						break;
						case '+%':
							$price_withtax = $price_withtax + ( ( $price_withtax * $calc_value ) / 100 );
						break;
						case '-%':
							$price_withtax = $price_withtax - ( ( $price_withtax * $calc_value ) / 100 );
						break;
					}	
				}
			}
			return $price_withtax;	
		}
	
	public function getProducts() 
	{

		$db = &JFactory::getDBO();
		if (!class_exists( 'VmConfig' ))
			require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
		VmConfig::loadConfig();
		$tag						= JRequest::getVar('tag');
		$app = JFactory::getApplication();
		// Get the pagination request variables
		$limit = $app->getUserStateFromRequest('com_vm2tags.limit', 'limit', $app->getCfg('list_limit'), 'int');
		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
		
		$json_tag = json_encode($tag);
		$json_encoded = str_replace('"', '', $json_tag);
		$json_encoded = urlencode($json_encoded);	
		$json_encoded = str_replace('%5C', '%5C%5C%5C%5C' , $json_encoded);
		$json_encoded = urldecode($json_encoded);
		$q = "SELECT DISTINCT(vpl.`virtuemart_product_id`) , vpl.`product_name` , vpl.`product_s_desc` , 
		vp.`virtuemart_vendor_id` ,  
		vcl.`category_name` ,  vcl.`virtuemart_category_id`  ,
		vm.`file_url_thumb` ,
		vpc.`custom_param` AS json_tags , 
		vpp.product_price,
		SUBSTRING( vpc.`custom_param` , 
					LOCATE('{\"product_tags\":\"', vpc.`custom_param` )+17 ,
 					CHAR_LENGTH( vpc.`custom_param` ) - ( LOCATE('}\"', REVERSE( vpc.`custom_param` ) ) +2 ) - 16 
				) AS accented_tags 
		FROM `#__virtuemart_products_".VMLANG."` vpl  
		JOIN `#__virtuemart_products` vp ON vpl.`virtuemart_product_id` = vp.`virtuemart_product_id` 
		JOIN `#__virtuemart_product_customfields` vpc ON vpl.`virtuemart_product_id` = vpc.`virtuemart_product_id` 
		JOIN `#__virtuemart_product_categories` vpcat ON vpcat.`virtuemart_product_id` = vp.`virtuemart_product_id` 
		JOIN `#__virtuemart_categories_".VMLANG."` vcl ON vcl.`virtuemart_category_id` = vpcat.`virtuemart_category_id` 
		LEFT JOIN `#__virtuemart_product_medias` vpm ON vpl.`virtuemart_product_id` = vpm.`virtuemart_product_id` 
		LEFT JOIN `#__virtuemart_medias` vm ON vm.`virtuemart_media_id` = vpm.`virtuemart_media_id`   
		LEFT JOIN #__virtuemart_product_prices vpp ON  vpl.virtuemart_product_id = vpp.virtuemart_product_id 
		WHERE vp.`published`='1'  
		AND vpc.`custom_value`='vm2tags' 
		AND (
			SUBSTRING( vpc.`custom_param` ,  LOCATE('{\"product_tags\":\"', vpc.`custom_param` )+17 ,  CHAR_LENGTH( vpc.`custom_param` ) - ( LOCATE('}\"', REVERSE( vpc.`custom_param` ) ) +2 ) - 16 )  LIKE '%,".$json_encoded.",%' 
			OR SUBSTRING( vpc.`custom_param` ,  LOCATE('{\"product_tags\":\"', vpc.`custom_param` )+17 ,  CHAR_LENGTH( vpc.`custom_param` ) - ( LOCATE('}\"', REVERSE( vpc.`custom_param` ) ) +2 ) - 16 )  LIKE '".$json_encoded.",%' 
			OR SUBSTRING( vpc.`custom_param` ,  LOCATE('{\"product_tags\":\"', vpc.`custom_param` )+17 ,  CHAR_LENGTH( vpc.`custom_param` ) - ( LOCATE('}\"', REVERSE( vpc.`custom_param` ) ) +2 ) - 16 )  LIKE '%,".$json_encoded."' 
			OR SUBSTRING( vpc.`custom_param` ,  LOCATE('{\"product_tags\":\"', vpc.`custom_param` )+17 ,  CHAR_LENGTH( vpc.`custom_param` ) - ( LOCATE('}\"', REVERSE( vpc.`custom_param` ) ) +2 ) - 16 )  LIKE '".$json_encoded."' 
		)
		GROUP BY vpl.`virtuemart_product_id` 
		ORDER BY vp.`virtuemart_product_id` DESC  ";
		$db->setQuery($q);
		

		$total = @$this->_getListCount($q);
		$products = $this->_getList($q, $limitstart, $limit);
		return array($products, $total, $limit, $limitstart);
	}
}
?>