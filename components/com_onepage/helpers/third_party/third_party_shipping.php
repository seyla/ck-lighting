<?php
/* 
*
* @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/
defined('_JEXEC') or die('Restricted access');
// support for USPS: 
	if (stripos($shipping_method, 'usps_')!==false)
	 {
	   $dataa = OPCTransform::getFT($shipping_method, 'input', $shipmentid, 'type', 'radio', '>', 'data-usps');
	   
	  
	    if (empty($usps_saved_semafor))
	 {
	  $usps_saved = $session->get('usps', null, 'vm');
	  $usps_saved_semafor = true; 
	 }
	 else
	 {
	   $session->set('usps', $usps_saved, 'vm'); 
	 }
	   
	   if (!empty($dataa))
	    {
		 
		   // example data-usps='{"service":"Parcel Post","rate":15.09}'
		  $data = @json_decode($dataa[0], true); 
		   
		  if (!empty($data))
		   {
			   
			   $uid = $cart->virtuemart_shipmentmethod_id; 
		     JRequest::setVar('usps_name', (string)$data['service']); 
			 JRequest::setVar('usps_rate', (float)$data['rate']);
			 JRequest::setVar('usps_rate-'.$uid, (float)$data['rate']); 
			 $service = base64_decode($data['service']); 
			 $service = html_entity_decode($service, ENT_COMPAT, 'UTF-8'); 
			 $service = base64_encode($service); 
			 JRequest::setVar('usps_service', (string)$service);
			 JRequest::setVar('usps_name-'.$uid, (string)$service);
			 $html .= '<input type="hidden" name="'.$idth.'_extrainfo" value="'.base64_encode($dataa[0]).'"/>';
			 
			 
			 
			 
		   }
		   else
		   {
			   
		   }
		}
		else
		{
			
		}
			
	 }
	 // end support USPS
	 
	 
	 
	 // support for UPS: 
	if (stripos($shipping_method, 'ups_')!==false)
	 {
	    if (empty($ups_saved_semafor))
	 {
	  $ups_saved = $session->get('ups_rates', null, 'vm');
	  $ups_saved_semafor = true; 
	 }
	 else
	 {
	   $session->set('ups_rates', $ups_saved, 'vm'); 
	 }
	 unset($_SESSION['load_fedex_prices_from_session']); 
	   $dataa = OPCTransform::getFT($shipping_method, 'input', $shipmentid, 'type', 'radio', '>', 'data-ups');
	  
	   if (!empty($dataa))
	    {
		 
		   // example data-usps='{"service":"Parcel Post","rate":15.09}'
		  $data = @json_decode($dataa[0], true); 
		
		  if (!empty($data))
		   {
		     //JRequest::setVar('usps_name', (string)$data['service']); 
			 JRequest::setVar('ups_rate', (string)$data['id']);
			 JRequest::setVar('ups_rate-'.$cart->virtuemart_shipmentmethod_id, $data['id']); 
			 $html .= '<input type="hidden" name="'.$idth.'_extrainfo" value="'.base64_encode($dataa[0]).'"/>';
			 
			 
		   }
		}
	 }
	 // end support UPS
	
	 // canada post: 
	 if (stripos($shipping_method, 'cpsol_')!==false)
	 {
	  $dataa = OPCTransform::getFT($shipping_method, 'input', $shipmentid, 'type', 'radio', '>', 'data*');
	  
	   if (!empty($dataa))
	    {
		 
		   // example data-usps='{"service":"Parcel Post","rate":15.09}'
		  $data = @json_decode($dataa[0], true); 
		   
		  if (!empty($data))
		   {
		     //JRequest::setVar('usps_name', (string)$data['service']); 
			 JRequest::setVar('cpsol_name', (string)$data['name']);
			 JRequest::setVar('cpsol_rate', (string)$data['rate']);
			 JRequest::setVar('cpsol_shippingDate', (string)$data['shippingDate']);
			 JRequest::setVar('cpsol_deliveryDate', (string)$data['deliveryDate']);
			 
			 $html .= '<input type="hidden" name="'.$idth.'_extrainfo" value="'.base64_encode($dataa[0]).'"/>';
			 
			 
		   }
		}
	}
	 // support for ACS
	if (stripos($shipping_method, 'acs_')!==false)
	 {
	    if (empty($acs_saved_semafor))
	 {
	  $acs_saved = $session->get('acs_rates', null, 'vm');
	  $acs_saved_semafor = true; 
	 }
	 else
	 {
	   $session->set('acs_rates', $acs_saved, 'vm'); 
	 }
	 
	 unset($_SESSION['load_fedex_prices_from_session']); 
	   $dataa = OPCTransform::getFT($shipping_method, 'input', $shipmentid, 'type', 'radio', '>', 'data-acs');
	  
	   if (!empty($dataa))
	    {
		 
		   // example data-usps='{"service":"Parcel Post","rate":15.09}'
		  $data = @json_decode($dataa[0], true); 
		   
		  if (!empty($data))
		   {
		     //JRequest::setVar('usps_name', (string)$data['service']); 
			 JRequest::setVar('acs_rate', (string)$data['id']);
			
			 $html .= '<input type="hidden" name="'.$idth.'_extrainfo" value="'.base64_encode($dataa[0]).'"/>';
			 
			 
		   }
		}
	 }
	 // end support ACS
	
	 // canada post: 
	 if (stripos($shipping_method, 'fedex_id')!==false)
	 {
	 $session = JFactory::getSession();
	 if (false)
	 if (empty($fedex_saved_semafor))
	 {
	  $fedex_saved = $session->get('fedex_rates', null, 'vm');
	  $fedex_saved_semafor = true; 
	 }
	 else
	 {
	    $session->set('fedex_rates', $fedex_saved , 'vm');
	 }
	 
	  $dataa = OPCTransform::getFT($shipping_method, 'input', $shipmentid, 'type', 'radio', '>', 'data*');
	  
	  
	   if (!empty($dataa))
	    {
		 
		   // example data-usps='{"service":"Parcel Post","rate":15.09}'
		  $data = @json_decode($dataa[0], true); 
		   
		  if (!empty($data))
		   {
		     //JRequest::setVar('usps_name', (string)$data['service']); 
			 JRequest::setVar('fedex_rate', (string)$data['id']);
			 //JRequest::setVar('cpsol_rate', (string)$data['rate']);
			 //JRequest::setVar('cpsol_shippingDate', (string)$data['shippingDate']);
			 //JRequest::setVar('cpsol_deliveryDate', (string)$data['deliveryDate']);
			 
			 $html .= '<input type="hidden" name="'.$idth.'_extrainfo" value="'.base64_encode($dataa[0]).'"/>';
			 
			 
		   }
		}
	}
	 
	 $fedex_multi = $session->get("shipping_services", ''); 
	 $dataa = OPCTransform::getFT($shipping_method, 'input', $shipmentid, 'type', 'radio', '>', 'value');
	 if (!empty($fedex_multi))
	 if (!empty($id) && (strpos($id, ':')!==false))
	 {
	
	 $fi = explode(':', $id); 
	 foreach ($fedex_multi as $key=>$fedex_rate)
	 {
		 $fedex_multi[$key]['baseRequest']['selected'] = $fi[1]; 
		 
	
	 }
	 $session->set('shipping_services', $fedex_multi); 
	
	 }