<?php
/**
 * 
 *
 * @package One Page Checkout for VirtueMart 2
 * @subpackage opc
 * @author stAn
 * @author RuposTel s.r.o.
 * @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * One Page checkout is free software released under GNU/GPL and uses some code from VirtueMart
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * 
 */
defined('_JEXEC') or die;
class OPCconfig {
 static $config; 
 public static function get($var, $default=false)
  {
    if (isset(OPCconfig::$config[$var])) return OPCconfig::$config[$var]; 
	
    include(JPATH_ROOT.DS.'components'.DS.'com_onepage'.DS.'config'.DS.'onepage.cfg.php'); 
	$arr = get_defined_vars(); 
	foreach ($arr as $key=>$val)
	 {
	   OPCconfig::$config[$key] = $val;   
	 }
	if (in_array($var, $arr)) 
	{
	
	 
	 return $arr[$var]; 
	}
	
	
	//include(JPATH_OPC.DS.'themes'.DS.$selected_template.DS.'theme.xml'); 
  }
  
  function getValue($config_name, $config_sub, $config_ref=0, $default='')
  {
    if (is_array($config_name)) {$l++; die('2');}
	if (is_array($config_sub)) {$l++; die('3');}

    $db = JFactory::getDBO(); 
	if (OPCJ3)
	{
	      $q = "select value from #__onepage_config where config_name = '".$db->escape($config_name)."' and config_subname = '".$db->escape($config_sub)."' and config_ref = ".(int)$config_ref." limit 0,1"; 

	}
	else
	{
    $q = "select value from #__onepage_config where config_name = '".$db->getEscaped($config_name)."' and config_subname = '".$db->getEscaped($config_sub)."' and config_ref = ".(int)$config_ref." limit 0,1"; 
	}
	
	$db->setQuery($q); 
	$res = $db->loadResult(); 
	$r = @json_decode($res); 
	if (empty($r) && (!empty($default))) return $default; 
	else
	if (empty($r)) $r = new stdClass(); 
	return $r; 
  }
  
  function store($config_name, $config_sub, $config_ref=0, $data)
  {
  
     $db = JFactory::getDBO(); 
	 //var_dump($data); die(); 
	 
	 if (OPCJ3)
	 {
	   $datains = $db->escape(json_encode($data)); 
	  	 $q = "insert into `#__onepage_config` (`id`, `config_name`, `config_subname`, `config_ref`, `value`) values (NULL, '".$db->escape($config_name)."', '".$db->escape($config_sub)."', '".(int)$config_ref."', '".$datains."')  ON DUPLICATE KEY UPDATE value = '".$datains."' ";  

	 }
	 else
	 {
	 $datains = $db->getEscaped(json_encode($data)); 
	 $q = "insert into `#__onepage_config` (`id`, `config_name`, `config_subname`, `config_ref`, `value`) values (NULL, '".$db->getEscaped($config_name)."', '".$db->getEscaped($config_sub)."', '".(int)$config_ref."', '".$datains."')  ON DUPLICATE KEY UPDATE value = '".$datains."' ";  
	 }
	 
     $db->setQuery($q); 
	 $db->query(); 
	 $e = $db->getErrorMsg(); if (!empty($e)) { echo $e; die();  }
	
  }
  
  function buildObject($post, $key='')
  {
    $ret = new stdClass(); 
	if (!empty($key))
	{
     foreach ($post[$key] as $key2->$val)
	 {
	   $ret->$key2 = $val; 
	 }
	  
	}
	else
    foreach ($post as $key=>$val)
	{
	  $ret->$key = $val; 
	}
	return $ret; 
  }
}