<?php 
/*
 * This file is here for broader compatibility with Joomla system
 *
 * @package One Page Checkout for VirtueMart 2
 * @subpackage opc
 * @author stAn
 * @author RuposTel s.r.o.
 * @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * One Page checkout is free software released under GNU/GPL and uses some code from VirtueMart
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * 
 *
*/
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

$task = JRequest::getVar('task', ''); 
if ($task == 'loadjs')
{
  require_once(JPATH_ROOT.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'mini.php'); 
  $file = JRequest::getVar('file', ''); 
  if (!empty($file))
  {
   OPCmini::loadJSfile($file); 
   $app  = JFactory::getApplication(); 
   $app->close(); 
   die(); 
  }
}
else
if ($task == 'ping')
{
  require_once(JPATH_ROOT.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opctracking.php'); 
  OPCtrackingHelper::ping(); 
   $app  = JFactory::getApplication(); 
   $app->close(); 
   die(); 
}
//index.php?option=com_onepage&task=loadjs&file=onepage.js
$memstart = memory_get_usage(true); 
define('OPCMEMSTART', $memstart); 

include(JPATH_ROOT.DS.'components'.DS.'com_onepage'.DS.'config'.DS.'onepage.cfg.php'); 


{
if (!isset($opc_memory)) $opc_memory = '128M'; 
ini_set('memory_limit',$opc_memory);
ini_set('error_reporting', 0);
// disable error reporting for ajax:
error_reporting(0); 
}



if (!empty($opc_calc_cache))
		   {
			 require_once(JPATH_ROOT.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'cache.php'); 
		     OPCcache::installCache(); 
		   }


// since 2.0.109 we need to load com_onepage instead of com_virtuemart becuase of captcha support 
JRequest::setVar('option', 'com_virtuemart'); 
if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
$task = JRequest::getVar('task', ''); 



// support for http://www.barg-it.de, plgSystemBit_vm_check_vatid
if ($task == 'checkvat')
{
$vatid = JRequest::getVar('vatid'); 
if (!empty($vatid))
{
       @header('Content-Type: text/html; charset=utf-8');
	   @header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	   @header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

	$app = JFactory::getApplication('site');
	$plugin = JPluginHelper::getPlugin('system', 'bit_vm_check_vatid');
	$pluginParams = new JRegistry();
	$pluginParams->loadString($plugin->params);
	
	$vies_url = $pluginParams->get('vies_url','http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl');
	$vat_to_check = $vatid;
	$error_msg = $pluginParams->get('error_msg_invalid_id','Invalid VAT number');
	$vies_down = $pluginParams->get('vies_down',1);
	$vies_down_error_msg = $pluginParams->get('error_msg_vies_down','EU validation service is currently not available for your country. Please try again later.');
	
	if(version_compare(JVERSION,'2.5.0','ge')) {
// Joomla! 2.5 code here
		$plugin_short_path = 'plugins/system/bit_vm_check_vatid/bitvatidchecker/';
	} elseif(version_compare(JVERSION,'1.7.0','ge')) {
	// Joomla! 1.7 code here
		$plugin_short_path = 'plugins/system/bit_vm_check_vatid/bitvatidchecker/';
	}
	elseif(version_compare(JVERSION,'1.6.0','ge')) {
	// Joomla! 1.6 code here
		$plugin_short_path = 'plugins/system/bit_vm_check_vatid/bitvatidchecker/';
	} else {
	// Joomla! 1.5 code here
					$plugin_short_path = 'plugins/system/bitvatidchecker/';
	}
	
			
	require_once ( JPATH_SITE.DS.$plugin_short_path.'classes/euvatcheck.class.php');
	$vatcheck = new VmEUVatCheck($vat_to_check, $vies_url, $error_msg, $vies_down, $vies_down_error_msg);
	echo $vatcheck->validvatid;
	
	$session = JFactory::getSession(); 
	$vatids = $session->get('opc_vat', array());
	
	if (!is_array($vatids))
	$vatids = unserialize($vatids); 
	
	$isvalid = (string)$vatcheck->validvatid;
	if ($isvalid === '1') $isvalid = true; 
	else $isvalid = false; 
	$vatid = preg_replace("/[^a-zA-Z0-9]/", "", $vatid);
	$vatid = strtoupper($vatid); 
	$vatids[$vatid] = (bool)$isvalid; 
	$s = serialize($vatids); 
	
	$vatids = $session->set('opc_vat', $s);
	
	$app->close(); 
	die(); 
	}
else
{
$country = (int)JRequest::getVar('country_id'); 
if (!empty($country))
{

	   @header('Content-Type: text/html; charset=utf-8');
	   @header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	   @header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	   
			$db =& JFactory::getDBO();
			$q = "SELECT country_2_code FROM #__virtuemart_countries WHERE virtuemart_country_id =". (int)$country;
			$db->setQuery($q);
			$db->query();
			$country_2_code = $db->loadResult();
			echo $country_2_code;
			$app = JFactory::getApplication('site');
			$app->close(); 
			die(); 
						
}

}
}


require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'controllers'.DS.'opc.php'); 
require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'overrides'.DS.'virtuemart.cart.view.html.php'); 
require_once(JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'virtuemart.php'); 
JRequest::setVar('option', 'com_onepage'); 
$task = JRequest::getVar('task', ''); 
