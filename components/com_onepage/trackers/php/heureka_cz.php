<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<script type="text/javascript">
var _hrq = _hrq || [];

_hrq.push(['setKey',               

'<?php echo $this->escapeSingle($this->params->heureka_key); ?>' 
]); 


_hrq.push(['setOrderId',

'<?php echo $this->order['details']['BT']->virtuemart_order_id; ?>'
]);
<?php foreach ($this->order['items'] as $order_item) { ?>

_hrq.push(['addProduct',           

'<?php echo $this->escapeSingle($order_item->order_item_name); ?>',                    
                                     
  	                           

'<?php echo number_format($order_item->product_final_price, 2, '.', ''); ?>',

'<?php echo number_format($order_item->product_quantity , 0, '.', ''); ?>'
]);
<?php } ?>

_hrq.push(['trackOrder']);
(function() {
var ho = document.createElement('script');
ho.type = 'text/javascript'; ho.async = true;
ho.src = ('https:' == document.location.protocol
? 'https://ssl' : 'http://www') +
'.heureka.cz/direct/js/cache/1-roi-async.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(ho, s);
})();
</script>
<?php
if (!empty($this->params->allow_visitor_data))
 {
$user = JFactory::getUser(); 
$email = $user->get('email'); 
$username = $user->get('username'); 


 $url = 'http://www.heureka.cz/direct/dotaznik/objednavka.php?id='.$this->escapeSingle($this->params->heureka_key);
 $url .= '&email='.urlencode($email); 
 foreach ($this->order['items'] as $key=>$order_item) { 
	$url .= '&product[]='.urlencode($order_item->order_item_name); 
 } 

 
 $url .= '&orderid='.$this->order['details']['BT']->virtuemart_order_id; 
//http://www.heureka.sk/direct/dotaznik/objednavka.php?id=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx&email=meno@email.sk&produkt[]=nokia%206210&produkt[]=nokia%206310i&orderid=123456
 
 ?>
 <iframe style="width:1px;height:1px;border: 0px transparent;" src="<?php echo $url; ?>"></iframe>
 <?php 
 }