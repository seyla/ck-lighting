<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<script type="text/javascript">
var _hrq = _hrq || [];
// nastaví váš veřejný klíč, kterým je identifikován váš e-shop (klíč je automaticky vložen
  // do kódu, který najdete ve své administraci)                 
_hrq.push(['setKey',               

'<?php echo $this->escapeSingle($this->params->heureka_key); ?>' // veřejný klíč vašeho e-shopu
]); 

// vytvoří objednávku a nastaví ji ID, které vygeneroval váš e-shop                
_hrq.push(['setOrderId',

'<?php echo $this->order['details']['BT']->virtuemart_order_id; ?>'                              // číselné ID objednávky
]);
<?php foreach ($this->order['items'] as $order_item) { ?>
// přidá jeden produkt k objednávce (metoda je volána tolikrát, kolik je v objednávce produktů)            
_hrq.push(['addProduct',           

"<?php echo $this->escapeDouble($order_item->order_item_name); ?>",                      // název produktu (musí se shodovat s tím, který dáváte 
                                     // do klasického feedu) a pokud obsahuje apostrof ' 
  	                           // musíte ho escapovat zpětným lomítkem tedy \'

'<?php echo number_format($order_item->product_final_price, 2, '.', ''); ?>',                         // cena produktu za jeden kus (musí být číslo bez Kč apod.)

'<?php echo number_format($order_item->product_quantity , 0, '.', ''); ?>'                                // počet kusů produktu (musí být číslo bez ks apod.)
]);
<?php } ?>
// odešle celou objednávku našemu serveru
_hrq.push(['trackOrder']);

(function() {
var ho = document.createElement('script');
ho.type = 'text/javascript'; ho.async = true;
ho.src = ('https:' == document.location.protocol
? 'https://ssl' : 'http://www') +
'.heureka.sk/direct/js/cache/2-roi-async.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(ho, s);
})();
</script>
