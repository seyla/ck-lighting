<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
$order_total = $this->order['details']['BT']->order_total;

?>
<script type="text/javascript">
  if ((typeof gaJsHost == 'undefined') || (typeof _gat == 'undefined'))
   {
  var gaJsHost = (("https:" == document.location.protocol ) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
   }
</script>
<script type="text/javascript">
try{
  var pageTracker = _gat._getTracker("<?php echo $this->params->google_analytics_id; ?>");
  pageTracker._trackPageview();
  pageTracker._addTrans(
      "<?php echo $this->order['details']['BT']->virtuemart_order_id; ?>",            // transaction ID - required
      "<?php echo $this->escapeDouble($this->vendor['company']); ?>",  // affiliation or store name
      "<?php echo number_format($order_total, 2, '.', ''); ?>",           // total - required
      "<?php echo number_format($this->order['details']['BT']->order_tax, 2, '.', ''); ?>",            // tax
      "<?php echo number_format($this->order['details']['BT']->order_shipment, 2, '.', ''); ?>",           // shipping
      "<?php echo $this->escapeDouble($this->order['details']['BT']->city); ?>",        // city
      "<?php echo $this->escapeDouble($this->order['details']['BT']->state_name); ?>",      // state or province
      "<?php echo $this->escapeDouble($this->order['details']['BT']->country_3_code); ?>"              // country
    );

<?php foreach ($this->order['items'] as $key=>$order_item) { ?>
   // add item might be called for every item in the shopping cart
   // where your ecommerce engine loops through each item in the cart and
   // prints out _addItem for each 
   pageTracker._addItem(
      "<?php echo $order_item->virtuemart_product_id; ?>",           // transaction ID - necessary to associate item with transaction
      "<?php echo $this->escapeDouble($order_item->order_item_sku); ?>",           // SKU/code - required
      "<?php echo $this->escapeDouble($order_item->order_item_name); ?>",        // product name
      "<?php echo $this->escapeDouble($order_item->category_name ); ?>",   // category or variation
      "<?php echo number_format($order_item->product_final_price, 2, '.', ''); ?>",          // unit price - required
      "<?php echo number_format($order_item->product_quantity , 0, '.', ''); ?>"               // quantity - required
   );
<?php } ?>
   pageTracker._trackTrans(); //submits transaction to the Analytics servers
} catch(err) {}
</script>