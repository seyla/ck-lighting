/*
* This is main JavaScript file to handle registration, shipping, payment and other functions of checkout
* 
* @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/
function clearChromeAutocomplete()
{
	// not possible, let's try: 
	if (navigator.userAgent.toLowerCase().indexOf('chrome') >= 0) 
	{
	af = document.getElementById('adminForm');
	if (af != null)
	{
	af.setAttribute('autocomplete', 'off'); 
    setTimeout(function () {
        	document.getElementById('adminForm').setAttribute('autocomplete', 'on'); 
    }, 1500);
	}
	}
}

/*
* This function is ran by AJAX to generate shipping methods and tax information
* 
*/
function op_runSS(elopc, onlyShipping, force, cmd)
{
	
 
 if ((typeof elopc != 'undefined') && (elopc != null && (elopc == 'init')))
  {
	clearChromeAutocomplete(); 
    // first run
	elopc = document.getElementById('virtuemart_country_id'); 
//	if ((el != null) && (el.value != ''))
//	changeStateList(el);
    ship_id = getInputIDShippingRate();
	   
	payment_id = getPaymentId();
					
	getTotals(ship_id, payment_id);
	
	
  }
  
 if (!(cmd != null))
 {
 
 delay_ship = '';
 
 if (force == null && (!op_autosubmit))
 if (typeof(elopc) != 'undefined')
 {
 if (op_delay && op_last_field)
 { 
  if (elopc != null)
  if (elopc.name != null)
  if (!(elopc.name == op_last1 || elopc.name == op_last2))
  {
    resetShipping(); 
    showcheckout(); 
 	delay_ship = '&delay_ship=delay_ship';   
 	
  }
  else
  {
   
  }
 }
 }
 if (typeof(elopc) == 'undefined' && (force == null) && (op_delay) && (!op_autosubmit))
 {
    resetShipping(); 
    delay_ship = '&delay_ship=delay_ship';   
 }
 // op_last_field = false
 // force = false
 // op_delay = true
 // if delay is on, but we don't use last field, we will not load shipping
 if (op_delay && (!op_last_field) && (force != true))
 {
    resetShipping(); 
    delay_ship = '&delay_ship=delay_ship';   
 }
 
 
 if (op_autosubmit)
 {
  if (document.adminForm != null)
  {
   document.adminForm.submit();
   return true;
  }
 }
 if (op_dontloadajax) 
  {
   showcheckout(); 
   op_hidePayments();
   runPay();
   return true;
  }
 var ui = document.getElementById('user_id');
 var user_id = 0;
 if (ui!=null)
 {
  user_id = ui.value;
 }

 //if ((op_noshipping == false) || (op_noshipping == null))
 
 }
	if ((typeof xmlhttp2 != 'undefined') && (xmlhttp2 != null))
	{
	 
	}
	else
	{
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
     xmlhttp2=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
	xmlhttp2=new ActiveXObject("Microsoft.XMLHTTP");
    }
	}
    if (xmlhttp2!=null)
    {
    
	
	// if shipping section 
    var country = '';
    var zip = '';
    var state = '';
    var address_1 = '';
    var address_2 = '';
    var onlyS = 'false';
    if (onlyShipping !=null) 
    if (onlyShipping == true)
    {
     onlyS = 'true';
    }
    else 
    {
     onlyS = 'false';
    }
	shipping_open = shippingOpen(); 
		
	// since 2.0.100: if ((op_logged_in != '1') || (shipping_open))
	
    addressq = op_getaddress();
    country = op_getSelectedCountry();
    country = op_escape(country);
    zip = op_getZip();
    zip = op_escape(zip);
    state = op_getSelectedState();
    state = op_escape(state);
	
	
	
	var ship_to_info_id = 0;
	
	// if we are logged in
	if (!(op_logged_in != '1'))
	{
	if (!shipping_open)
	{
	  d = document.getElementById('ship_to_info_id_bt'); 
	  if (d != null)
	  ship_to_info_id = d.value; 
	}
	//else
	{
	 var st = document.getElementsByName('ship_to_info_id');
	  
     if (st!=null)
     {
	    
	    if (st.type == 'select-one')
		 {
		   if ((st.options != null) && (st.selectedIndex != null))
		   ship_to_info_id = st.options[st.selectedIndex].value; 
		 }
		else
		if (st.type == 'radio')
        for (i=0;i<st.length;i++)
        {
         if (st[i].checked) 
         ship_to_info_id = op_escape(st[i].value);
        }
     }
	}
	}

	shipping_open = shipping_open.toString(); 
    coupon_code = getCouponCode();
	
    var sPayment = getValueOfSPaymentMethod();
	sPayment = op_escape(sPayment);
	var sShipping = "";
	if ((op_noshipping == false) || (op_noshipping == null))
    {
    sShipping = getVShippingRate();
    sShipping = op_escape(sShipping);
    }
	
	op_saved_shipping_local = op_saved_shipping;
    op_saved_shipping = getInputIDShippingRate();
	op_saved_shipping_escaped = op_escape(op_saved_shipping);
    
   
   
    var query = 'coupon_code='+coupon_code+delay_ship+'&shiptoopen='+shipping_open+'&stopen'+shipping_open;
	if (typeof opc_theme != 'undefined')
	if (opc_theme != null)
	query += '&opc_theme='+opc_theme; 
	
    //var url = op_securl+"?option=com_onepage&view=ajax&format=raw&tmpl=component&op_onlyd="+op_onlydownloadable;
	if (!(typeof virtuemart_currency_id != 'undefined')) virtuemart_currency_id = ''; 
	
	var url = op_securl+"?option=com_onepage&nosef=1&task=opc&view=opc&format=opchtml&tmpl=component&op_onlyd="+op_onlydownloadable+'&virtuemart_currency_id='+virtuemart_currency_id;
	
	var extraquery = buildExtra(); 
	//op_log(extraquery); 
    
	if ((op_noshipping == false) || (op_noshipping == null))
	{
         query += "&virtuemart_country_id="+country+"&zip="+zip+"&virtuemart_state_id="+state+"&weight="+op_weight+"&ship_to_info_id="+ship_to_info_id+"&payment_method_id="+sPayment+"&os="+onlyS+"&user_id="+user_id+'&zone_qty='+op_zone_qty+addressq;
         query2 = "&selectedshipping="+op_saved_shipping_escaped+"&shipping_rate_id="+sShipping+"&order_total="+op_order_total+"&tax_total="+op_tax_total;
	}
	else 
	{
	 // no shipping section
	 query += "&no_shipping=1&virtuemart_country_id="+country+"&zip="+zip+"&virtuemart_state_id="+state+"&order_total="+op_order_total+"&tax_total="+op_tax_total+"&weight="+op_weight+"&ship_to_info_id="+ship_to_info_id+"&payment_method_id="+sPayment+"&os="+onlyS+"&user_id="+user_id+'&zone_qty='+op_zone_qty+addressq;
	 query2 = ''; 
	}
	query += "&virtuemart_currency_id="+op_currency_id;
	
	if (cmd != null)
	query += "&cmd="+cmd;
	
	
	// dont do duplicit requests when updated from onblur or onchange due to compatiblity
	if (cmd != null)
	{
	
	  // if we have a runpay request, check if the shipping really changed
	  if (force != true)
	  if (op_lastq == query && (op_saved_shipping_local == op_saved_shipping)) return true;
	}
	else
	if (op_lastq == query && (force != true)) 
	{
	
	return true;
	}
	//console.log(cmd, query); 
	op_lastq = query;
	query += query2+extraquery; 
	
	//if (!op_isrunning)
	{
	
    xmlhttp2.open("POST", url, true);
    
    //Send the proper header information along with the request
    xmlhttp2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xmlhttp2.setRequestHeader("Content-length", query.length);
    //xmlhttp2.setRequestHeader("Connection", "close");
    xmlhttp2.onreadystatechange= op_get_SS_response ;
    showLoader(cmd);
	
	 op_isrunning = true; 
     xmlhttp2.send(query); 
    }
    
 }
 else
 {
  
 }
 
}

function retrySend()
{
}

function refreshShipping(elopc)
{
  resetShipping(); 
  op_runSS(null, null, true); 
  elopc.href = "#";
  return false;
}

function  resetShipping()
{
   d = document.getElementById('shipping_goes_here'); 
   
   if (d != null)
   {
    d.style.minHeight = d.style.height;
    //if (!op_last_field)
    d.innerHTML = '<input type="hidden" name="invalid_country" id="invalid_country" value="invalid_country" />'; 
    //else d.innerHTML = '';
   }
   
   
   d2 = document.getElementById('op_last_field_msg'); 
   if (d2 != null)
   {
    if (op_refresh_html != '')
    d2.innerHTML = ''; 
	//op_refresh_html = d.innerHTML; 
	
   }
   return false;
}

function setShippingHtml(html)
{
  if (op_shipping_div == null)
  {
   sdiv = null;
   sdiv = document.getElementById('ajaxshipping');
   sib = document.getElementById('shipping_inside_basket');
   sib2 = document.getElementById('shipping_goes_here'); 
   if ((typeof(sib) != 'undefined') && (sib != null))
   {
     sdiv = sib;
   }
   if ((typeof(sib2) != 'undefined') && (sib2 != null))
    sdiv = sib2; 
   
   op_shipping_div = sdiv;
  }
  
   if (callAfterResponse != null && callAfterResponse.length > 0)
   {
   for (var x=0; x<callAfterResponse.length; x++)
	   {
	     eval(callAfterResponse[x]);
	   }
   }
  
  op_shipping_div.style.minHeight = op_shipping_div.style.height;
  op_shipping_div.innerHTML = html;
}

function showLoader(cmd)
{

   if (callBeforeLoader != null && callBeforeLoader.length > 0)
   {
   for (var x=0; x<callBeforeLoader.length; x++)
	   {
	     ret = eval(callBeforeLoader[x]); 
		 if (ret == 2)
		 {
		 return; 
		 }
		 
	   }
   }
 if (!op_loader) return;
 if (cmd != null)
 {
   if (cmd == 'runpay')
   {
     pp = document.getElementById('payment_html'); 
	 if (pp != null)
	 {
	   pp.innerHTML = '<img src="'+op_loader_img+'" title="Loading..." alt="Loading..." />';
	 }
   }
   
 }
 else
 {
 if (op_delay) resetShipping(); 
 if (op_loader)
 {
   
   setShippingHtml('<img src="'+op_loader_img+'" title="Loading..." alt="Loading..." /><input type="hidden" name="invalid_country" id="invalid_country" value="invalid_country" />'); 
  
   
 }
 }
}

function getCouponCode()
{
 var x = document.getElementById('op_coupon_code'); 
 if (typeof(x) != 'undefined' && (x != null))
 {
   return op_escape(x.value);
 }
 return "";
}

function showcheckout()
{
     var op_div = document.getElementById("onepage_main_div");
     if ((op_div != null) && (op_min_pov_reached == true))
     {
      if (op_div.style != null)
       if (op_div.style.display == 'none')
       {
         //will show OPC if javascript and ajax test OK
        
        
        op_div.style.display = '';
       }
       
     }
       
        
     
    /*
    if (false)   
    if (op_div != null)
    if (op_div.style.display != 'none')
       {
        var ocl = getElementsByClassName(op_continue_link.toString());
        if (ocl.style != null)
        {
         ocl.style.display = "none";
        }
        else
        for (i=0;i<ocl.length;i++)
	     {
	       
	       if (ocl[i].style != null)
	       ocl[i].style.display = "none";
	     }
	    }
	*/
}

function setPaymentHtml(html)
{
  if (html.indexOf('force_show_payments')>=0)
   {
      d = document.getElementById('payment_top_wrapper'); 
	  if ((typeof d != 'undefined') && (d!=null))
	   d.style.display = 'block'; 
   }
  else
  if (html.indexOf('force_hide_payments')>=0)
   {
      d = document.getElementById('payment_top_wrapper'); 
	  if ((typeof d != 'undefined') && (d!=null))
	   d.style.display = 'none'; 
   }
  var d = document.getElementById('payment_html');
  if (d != null)
  d.innerHTML = html;
  return true;
}

/*
* This is response function of AJAX
* Response is HTML code to be used inside noshippingheremsg DIV
*/       
function op_get_SS_response()
{

   
   if ((typeof xmlhttp2 != 'undefined') && (xmlhttp2 != null))
   {
   if (opc_debug)
   op_log(xmlhttp2); 
   }
    runjavascript = new Array(); 

  if (xmlhttp2.readyState==4 && xmlhttp2.status==200)
    {
    // here is the response from request
    var resp = xmlhttp2.responseText;
    if (resp != null) 
    {
	 // lets clear notices, etc... 
	 //try
	 {
	part = resp.indexOf('{"cmd":'); 
	 if (part >=0) 
	 {
	 resp = resp.substr(part); 
	if ((JSON != null) && (typeof JSON.decode != 'undefined'))
	{
	try
	{
	  reta = JSON.decode(resp); 
	}
	catch (e)
	 {
	   op_log('Error in Json data', resp); 
	 }
	}
	if ((typeof reta == 'undefined') || (!(reta != null)))
	{
	  reta = eval("(" + resp + ")");
	}
	
    if (typeof reta.basket != 'undefined')
	{
		 showNewBasket(reta.basket);
	}
	

	
	if (reta.shipping != null)
	{
	
	shippinghtml = reta.shipping;
	}
	else
	{
	
	shippinghtml = resp;
	}
	
	if (typeof reta.couponcode != 'undefined')
	{
		 showCouponCode(reta.couponcode);
		
	}
	
	if (reta.username_exists != null)
	 {
	   username_check_return(reta.username_exists); 
	 }
	if (reta.email_exists != null)
	 {
	   email_check_return(reta.email_exists); 
	 }
	
	if ((reta.totals_html != null) && (reta.totals_html != ''))
	{
		//op_log(reta.totals_html); 
	  dx = document.getElementById('opc_totals_hash'); 
		if (dx != null)
		dx.innerHTML = reta.totals_html; 
	}
	
	if (typeof reta.msgs != 'undefined')
	if (reta.msgs != null)
	setMsgs(reta.msgs); 

	if (typeof reta.debug_msgs != 'undefined')
	if (reta.debug_msgs != null)
	printDebugInfo(reta.debug_msgs); 

	
	if (reta.payment != null)
	paymenthtml = reta.payment; 
	
	//else alert('error'); 
    
	if (reta.javascript != null)
	runjavascript = reta.javascript; 
	
	if (reta.klarna != null)
	 {
	    setKlarnaAddress(reta.klarna); 
	 }
	}
	else
	shippinghtml = resp; 
	}
	/*
	catch (e)
	{
	  
	  shippinghtml = resp; 
	  paymenthtml = null;
	  // we cannot deserialize here
	}
	*/
	
	
	
	d2 = document.getElementById('op_last_field_msg'); 
    if (d2 != null)
    {
     d2.innerHTML = ''; 
    }
	 
	 if (resp.indexOf('payment_inner_html')>0)
	 if ((typeof paymenthtml != 'undefined') && (paymenthtml != null))
	 setPaymentHtml(paymenthtml);
	 
	 if (shippinghtml != 'opc_do_not_update')
	 setShippingHtml(shippinghtml); 
	 showcheckout(); 
    }
    else
    {
     
    }
   
    if ((op_saved_shipping != null) && (op_saved_shipping != ""))
     { 
      var ss = document.getElementById(op_saved_shipping);  
	  if (ss != null)
	  {
	  // we use try and catch here because we don't know if what type of html element is the shipping
	  try
	  {
	   // for option
       ss.selected = true;
	   // for checkbox and radio
       ss.checked = true;
	   }
	  catch (e)
	   {
	    ;
	   }
	  }
     }
    
	op_resizeIframe();

    op_hidePayments();
    runPay();
    }
	else
	{
	  if (xmlhttp2.readyState==4 && ((xmlhttp2.status>500)))
	   {
	     // here is the response from request
    var resp = xmlhttp2.responseText;
	// changed in 2.0.227
    if (resp != null) 
    { 
	 if (opc_debug)
	 op_log(resp); 
	 resp='<input type="hidden" name="invalid_country" value="invalid_country" />'; 
	 resp += JERROR_AN_ERROR_HAS_OCCURRED+' <a href="#" onclick="return refreshShippingRates();" >'+COM_ONEPAGE_CLICK_HERE_TO_REFRESH_SHIPPING+'</a>'; 
	 
	 
	 setShippingHtml(resp); 

    }
	   }
	}// end response is ok
    //changeTextOnePage(op_textinclship, op_currency, op_ordertotal); 
   
    // run shipping and payment javascript
	
   
   if (callAfterRender != null && callAfterRender.length > 0)
   {
   for (var x=0; x<callAfterRender.length; x++)
	   {
	     eval(callAfterRender[x]);
	   }
   }

   if (runjavascript.length>0)
	 {
	   for (var s=0; s<runjavascript.length; s++)
	    {
		  try
		  {
		  eval(runjavascript[s]); 
		  }
		  catch (e)
		  {
		  
		  }
		}
	 }
   
   
    return;
}

function printDebugInfo(msgs)
{
 for (var i=0; i<msgs.length; i++)
  {
  
    op_log(msgs[i]); 
  }
}

function removeCoupon()
{
	op_runSS(null, false, true, 'removecoupon'); 
	return false; 
}

function showNewBasket(html)
{
	d = document.getElementById('opc_basket'); 
	if (d!=null)
		d.innerHTML = html; 
}

function showCouponCode(couponcode)
{
    
	// remove coupon html: 
	r = '<a href="#" onclick="javascript: return removeCoupon()" class="remove_coupon">X</a>';
	dt = document.getElementById('tt_order_discount_after_txt_basket_code'); 
	if (dt != null)
	{
		if (couponcode == '')
		{
			dt.innerHTML = ''; 
			var d2 = document.getElementById('tt_order_discount_after_basket'); 
			if (d2 != null)
			d2.style.display = 'none'; 
		}
		else
			dt.innerHTML = '('+couponcode+' '+r+')'; 
	}
	else
	{
	d1 = document.getElementById('tt_order_discount_after_txt_basket'); 
	newd = document.createElement('span');
	newd.id = 'tt_order_discount_after_txt_basket_code'; 
	if (couponcode == '')
	{
	newd.innerHTML = ''; 
			var d2 = document.getElementById('tt_order_discount_after_basket'); 
			if (d2 != null)
			d2.style.display = 'none'; 

	}
		else
	newd.innerHTML = '('+couponcode+' '+r+')'; 
	if (d1 != null)
	d1.appendChild(newd); 
	}
	
	//tt_order_discount_after_txt
	dt = document.getElementById('tt_order_discount_after_txt_code'); 
	if (dt != null)
	{
		if (couponcode == '')
		{
					var d2 = document.getElementById('tt_order_discount_after_basket'); 
			if (d2 != null)
			d2.style.display = 'none'; 

			dt.innerHTML = ''; 
	    }
		else
			dt.innerHTML = '('+couponcode+' '+r+')'; 
	}
	else
	{
	d1 = document.getElementById('tt_order_discount_after_txt'); 
	if (d1 != null)
	if (d1.innerHTML == '')
		d1.innerHTML = op_coupon_discount_txt; 
	newd = document.createElement('span');
	newd.id = 'tt_order_discount_after_txt_code'; 
	if (couponcode == '')
	{
				var d2 = document.getElementById('tt_order_discount_after_basket'); 
			if (d2 != null)
			d2.style.display = 'none'; 

	newd.innerHTML = ''; 
	}
		else
	newd.innerHTML = ' ('+couponcode+')'; 
	if (d1 != null)
	d1.appendChild(newd); 
	}
	
}

function refreshShippingRates()
{  
  op_runSS(null, false, true); 
  return false; 
 
}

function setMsgs(msgs)
{
  if (msgs.length > 0)
  {
  
  
  
  for(i=0; i<msgs.length; i++)
  {
   op_log('OPC Alert: '+msgs[i]);
   //if (!no_alerts) 
   opc_error(msgs[i]); 
  }
    if (typeof jQuery != 'undefined')
  if (jQuery != null)
  if (typeof jQuery.scrollTo != 'undefined')
  jQuery.scrollTo( '#opc_error_msgs', 800, {easing:'elasout'} );

  }
  else
  {
    
    d=document.getElementById('opc_error_msgs');
	if (d != null)
	 {
	 
	  d.innerHTML = '';
	  d.style.display = 'none'; 
	 }
  }
}

function opc_error(msg)
{
   d=document.getElementById('opc_error_msgs');
   if (d!=null)
   {
   if (d.innerHTML.toString().indexOf(msg)<0)
    {
	  d.innerHTML += msg+'<br />'; 
	}
  d.style.display = 'block'; 
  }
  
 }

function changeTextOnePage3(op_textinclship, op_currency, op_ordertotal)
{
// op_runSS(true);
 op_hidePayments();
 
 // disabled here 17.oct 2011
 // it should not be needed as it is fetched before ajax call
 // op_saved_shipping = getInputIDShippingRate();
 changeTextOnePage(op_textinclship, op_currency, op_ordertotal);    
}
  
  // returns value of selected payment method      
	function getValueOfSPaymentMethod()
	{
		  // get active shipping rate
	  var e = document.getElementsByName("virtuemart_paymentmethod_id");
	  
	  //var e = document.getElementsByName("payment_method_id");
	  
	  
	  var svalue = "";
	 
	  
	  if (e.type == 'select-one')
	  {
	   ind = e.selectedIndex;
	   if (ind<0) ind = 0;
	   value = e.options[ind].value;
	   return value;
	  }
	  
	  
	  if (e)
      if (e.checked)
	  {
	    svalue = e.value;

	  }
	  else
	  {

	  for (i=0;i<e.length;i++)
	  {
	   if (e[i].type == 'select-one')
	  {
	   if (e[i].options.length <= 0) return ""; 
	   ind = e[i].selectedIndex;
	   if (ind<0) ind = 0;
	   value = e[i].options[ind].value;
	   return value;
	  }
	  
	   if (e[i].checked==true)
	     svalue = e[i].value;
	  }
	  }
	    if (svalue) return svalue;
	    
	    // last resort for hidden and not empty values of payment methods:
	   for (i=0;i<e.length;i++)
	   {
	    if (e[i].value != '')
	  {
	    if (e[i].id != null && (e[i].id != 'payment_method_id_coupon'))
	    return e[i].value;
	  }
	    }
		
	    return "";
	
	    
	}
	
  // returns amount of payment discout withou tax
	function op_getPaymentDiscount()
	{
	
	 var id = getValueOfSPaymentMethod();
	 if ((id) && (id!=""))
	 {
	  if (typeof(pdisc) !== 'undefined')
	  if (pdisc[id]) 
	  { 
            if (typeof(op_payment_discount) !== 'undefined' ) op_payment_discount = pdisc[id];
	    return pdisc[id];
	  }
	 }
	 return 0;
	}

	// returns value of selected shipping method
	function getVShippingRate()
	{
		  // get active shipping rate
	
	  
	  var svalue = "";
	  {
	  e = document.getElementsByName("virtuemart_shipmentmethod_id");
	  if (e != null)
	  {
	  for (i=0;i<e.length;i++)
	  {
	   if (e[i].type == 'select-one')
	   {
		if (e[i].options.length<=0) return ""; 
	    index = e[i].selectedIndex;
	    if (index<0) index = 0;
	    return e[i].options[index].value;
	   }
	   else
	   if ((e[i].checked==true) && (e[i].style.display != 'none'))
	     {
	     return e[i].value;
		 }
	  }
	  }
	  }
	  
	    if (svalue) 
	    {
	     return svalue;
	    }
	    return "";
	    
	}
	// returns input id of selected shipping method
	function getInputIDShippingRate()
	{
	  // get active shipping rate
	  var e = document.getElementsByName("shipping_rate_id");
      	  
	  if (e != null && (e.length > 0))
	  {
	  var id = "";
	  for (i=0;i<e.length;i++)
	  {
	   
	  	if (e[i].type == 'select-one')
	   {
		if (e[i].options.length<=0) return ""; 
	    index = e[i].selectedIndex;
	    if (index<0) index = 0;
	    return e[i].options[index].id;
	   }
	   else
	   if ((e[i].checked==true) && (e[i].style.display != 'none'))
	   {
	     if (e[i].id != null)
	     return e[i].id;
	     else return e[i].value;
	   }
	   else
	   if (e[i].type=="hidden")
	   {
	    if ((e[i].value.indexOf('free_shipping')>=0) && ((typeof(e[i].id) != 'undefined') && (e[i].id.indexOf('_coupon')<0))) return e[i].id;
	    if ((e[i].value.indexOf('choose_shipping')>=0) && ((typeof(e[i].id) != 'undefined') && (e[i].id.indexOf('_coupon')<0))) 
	     {
	      return e[i].id;
	     }
	   }
	  }
	  
	  return "";
	   
	  }
	  else
	  {
	    e = document.getElementsByName("virtuemart_shipmentmethod_id"); 
		
		 var id = "";
	  for (i=0;i<e.length;i++)
	  {
	  	if (e[i].type == 'select-one')
	   {
		if (e[i].options.length <= 0) return ""; 
	    index = e[i].selectedIndex;
	    if (index<0) index = 0;
	    return e[i].options[index].id;
	   }
	   else
	   if ((e[i].checked==true) && (e[i].style.display != 'none'))
	   {
	   
	     if (e[i].id != null)
	     return e[i].id;
	     else return e[i].value;
	   }
	   else
	   if (e[i].type=="hidden")
	   {
	    if ((e[i].value.indexOf('free_shipping')>=0) && ((typeof(e[i].id) != 'undefined') && (e[i].id.indexOf('_coupon')<0))) return e[i].id;
	    if ((e[i].value.indexOf('choose_shipping')>=0) && ((typeof(e[i].id) != 'undefined') && (e[i].id.indexOf('_coupon')<0))) 
	     {
	      return e[i].id;
	     }
	   }
	  }
	  
	  return "";
	   
	  }
	    
	}
	// returns internal id of shipping method ... only for standard shipping 
	function getIDvShippingRate()
	{
	 var svalue = getVShippingRate();
	 if ((svalue) && (svalue!=""))
	  {
	    svalue = url_decode_op(svalue);
	    scostarr = svalue.split("|");  
	    
	    if (scostarr)
	    if (scostarr[4]) return scostarr[4];
	    
	  }
	  return "";
	}
	
  function formatCurrency(total)
  {
  
   if ((total == 0) || (isNaN(parseFloat(total)))) total = '0.00';
   var arr = op_vendor_style.split('|');
   
   if (arr.length > 6)
   {
     var sep = arr[3];
     var tsep = arr[4];
     var dec = arr[2];
     var stylep = arr[5];
     // 0 = '00Symb';
     // 1 = '00 Symb'
     // 2 = 'Symb00'
     // 3 = 'Symb 00';
     var stylen = arr[6];
     // 0 = (Symb00)
     // 1 = -Symb00
     // 2 = Symb-00
     // 3 = Symb00-
     // 4 = (00Symb)
     // 5 = -00Symb
     // 6 = 00-Symb
     // 7 = 00Symb-
     // 8 = -00 Symb
     // 9 = -Symb 00
     // 10 = 00 Symb-
     // 11 = Symb 00-
     // 12 = Symb -00
     // 13 = 00- Symb
     // 14 = (Symb 00)
     // 15 = (00 Symb)
     
	 // arr[8] = positive
	 // arr[9] = negative
	 
     // format the number:
     //total = parseFloat(total.toString()).toFixed(dec);
     //totalstr = '';
     //mTotal = total;
     
	 // ok, in vm2 we've got: 
	 // arr[8] = positive
	 // arr[9] = negative
	 if (arr[8] != null)
	 {
     stylepvm2 = arr[8]; 
	 stylenvm2 = arr[9]; 
	 }
	 else 
	 {
     stylepvm2 = null;
	 stylenvm2 = null; 
	 }
	 return FormatNum2Currency(total, sep, tsep, stylep, stylen, op_currency, dec, stylepvm2, stylenvm2);
     
   }
   else
   {
    var dec = 2;
    if ((op_no_decimals != null) && (op_no_decimals == true)) dec = 0;
    if ((op_curr_after != null) && (op_curr_after == true))
    {
     total = parseFloat(total.toString()).toFixed(dec)+' '+op_currency;
    }
    else
     total = op_currency+' '+parseFloat(total.toString()).toFixed(dec);
    return total; 
   }
   
    

  }
  function reverseString(str)
  {
    splittext = str.toString().split("");
    revertext = splittext.reverse();
    return revertext.join("");
  }
  
  function op_escape(str)
  {
   if ((typeof(str) != 'undefined') && (str != null))
   {
     x = str.split("&").join("%26");
     return x;
   }
   else 
   return "";
  }
  /*
	Author: Robert Hashemian
	http://www.hashemian.com/
	Modified by stAn www.rupostel.com - Feb 2011
	You can use this code in any manner so long as the author's
	name, Web address and this disclaimer is kept intact.
	********************************************************
	 // stylep:
     // 0 = '00Symb';
     // 1 = '00 Symb'
     // 2 = 'Symb00'
     // 3 = 'Symb 00';
     // stylen:
     // 0 = (Symb00)
     // 1 = -Symb00
     // 2 = Symb-00
     // 3 = Symb00-
     // 4 = (00Symb)
     // 5 = -00Symb
     // 6 = 00-Symb
     // 7 = 00Symb-
     // 8 = -00 Symb
     // 9 = -Symb 00
     // 10 = 00 Symb-
     // 11 = Symb 00-
     // 12 = Symb -00
     // 13 = 00- Symb
     // 14 = (Symb 00)
     // 15 = (00 Symb)
  */
  function FormatNum2Currency(num, decpoint, sep, stylep, stylen, curr, decnum, stylepvm2, stylenvm2) {
  // check for missing parameters and use defaults if so
  
  // vm2:
  //'1|€|2|,||3|8|8|{number} {symbol}|{sign}{number} {symbol}'
  var isPos = true;
  if (parseFloat(num)>=0) isPos = true;
  else isPos = false;
	
  num = Math.round(num*Math.pow(10,decnum))/Math.pow(10,decnum);
  if (isPos == false) num = num * (-1);
   num = num.toString();
   
   a = num.split('.');
   x = a[0];
   if (a.length > 1)
   y = a[1];
   else y = '00';
   var z = "";

  
  if ((typeof(x) != "undefined") && (x != null)) {
    // reverse the digits. regexp works from left to right.
    z = reverseString(x);
    // this caused a hang in certain occations:
    /* for (i=x.length-1;i>=0;i--)
      z += x.charAt(i);
     */
    // add seperators. but undo the trailing one, if there
    z = z.replace(/(\d{3})/g, "$1" + sep);
    if (z.slice(-sep.length) == sep)
      z = z.slice(0, -sep.length);
    //x = "";
    // reverse again to get back the number
    /*for (i=z.length-1;i>=0;i--)
      x += z.charAt(i);
    */
    x = reverseString(z);
    // add the fraction back in, if it was there
    if (decnum > 0)
    {
     if (typeof(y) != "undefined" && y.length > 0)
     {
       if (y.length > decnum) y = y.toString().substr(0, decnum);
       if (y.length < decnum)
       {
        var missing = decnum - y.length;
        for (var u=0; u<missing; u++)
        {
         y += '0';
        } 
       }
       x += decpoint + y;
     }
    }
  }
  
  if (isPos == true)
  {
    // 0 = '00Symb';
     // 1 = '00 Symb'
     // 2 = 'Symb00'
     // 3 = 'Symb 00';
	 if (stylepvm2 != null)
	 {
	   if (curr.length>0)
	   stylepvm2 = stylepvm2.split('{number}').join(x).split('{symbol}').join(curr);
	   else
	   stylepvm2 = stylepvm2.split('{number}').join(x); 
	   
	   if (stylepvm2.indexOf('sign') >=0)
	   stylepvm2 = stylepvm2.split('{sign}').join('+');
	   
	   x = stylepvm2; 
	 }
	 else
     switch(parseInt(stylep))
     {
      case 0: 
      	x = x+curr;
      	break;
      case 1:
      	x = x+' '+curr;
      	break;
      case 2:
      	x = curr+x;
      	break;
      default:
      	x = curr+' '+x;
     }
  }
  else
  {
   if (stylenvm2 != null)
	 {
	   if (curr.length>0)
	   stylenvm2 = stylenvm2.split('{number}').join(x).split('{symbol}').join(curr);
	   else
	   stylenvm2 = stylenvm2.split('{number}').join(x); 
	   
	   if (stylenvm2.indexOf('sign') >=0)
	   stylenvm2 = stylenvm2.split('{sign}').join('-');
	   
	   x = stylenvm2; 
	 }
   else
   switch (parseInt(stylen))
   {
     // 0 = (Symb00)
     // 1 = -Symb00
     // 2 = Symb-00
     // 3 = Symb00-
     // 4 = (00Symb)
     // 5 = -00Symb
     // 6 = 00-Symb
     // 7 = 00Symb-
     // 8 = -00 Symb
     // 9 = -Symb 00
     // 10 = 00 Symb-
     // 11 = Symb 00-
     // 12 = Symb -00
     // 13 = 00- Symb
     // 14 = (Symb 00)
     // 15 = (00 Symb)
     case 0:
     	x = '('+curr+x+')';
     	break;
     case 1:
     	x = '-'+curr+x;
     	break;
     case 2:
     	x = curr+'-'+x;
     	break;
     case 3:
     	x = curr+x+'-';
     	break;
     case 4:
     	x = '('+x+curr+')';
     	break;
     case 5:
     	x = '-'+x+curr;
     	break;
     case 6:
     	x = x+'-'+curr;
     	break;
     case 7:
     	x = x+curr+'-';
     	break;
     case 8:
     	x = '-'+x+' '+curr;
     	break;
     case 9:
     	x = '-'+curr+' '+x;
     	break;
     case 10:
      	x = x+' '+curr+'-';
      	break;
     case 11:
      	x = curr+x+' -';
      	break;
      case 12:
      	x = curr+' -'+x;
      	break;
      case 13:
      	x = x+'- '+curr;
      	break;
      case 14:
      	x = '('+curr+' '+x+')';
      	break;
      case 15:
      	x = '('+x+' '+curr+')';
      	break;
      default:
      	x = '-'+x+' '+curr;
      }
      	
  }
  
  return x;
}

  
  
  /*
  * This function disables payment methods for a selected shipping method
  * or implicitly disabled payments   
  * THIS FUNCTION MIGHT GET RENAMED TO: op_onShippingChanged
  */
  function op_hidePayments()
  {
   // check if shipping had changed:
   op_saved_shipping2 = getInputIDShippingRate();
   
   if ((typeof(op_saved_shipping) == 'undefined' || op_saved_shipping == null) || (op_saved_shipping != op_saved_shipping2) || (op_firstrun))
   {
    op_firstrun = false;
	
   // check if the feature is enabled
   // if (op_payment_disabling_disabled) return "";
   // event handler for AfterShippingSelect
   if (callAfterShippingSelect != null && callAfterShippingSelect.length > 0)
   {
   for (var x=0; x<callAfterShippingSelect.length; x++)
	   {
	     eval(callAfterShippingSelect[x]);
	   }
   }
   }
  }
  /* Old function, not used any more
  */
  function op_hidePayments_obsolete() 
  {
     if (op_payment_disabling_disabled) return "";
     var sid = getIDvShippingRate();
	   var toClick = null;    
	   var clickit = false;
        var pms = document.getElementsByName("payment_method_id");
        if (pms != null)
        if ((sid != "") && (payconf[sid] !=null))
        {
          for (var c=0; c<pms.length; c++)
          {
           if (pms[c].type == 'select-one')
           {
             ind = pms[c].selectedIndex;
             if (ind<0) ind = 0;
             valu = pms[c].options[ind].value;
           }
           else
            valu = pms[c].value;
          
           if (valu != null)
          {
            if (sid!="")
            if (payconf[sid].toString().indexOf("/"+pms[c].value+",")>=0)
            {
             var sel = getPaymentId();

             if ((sel != null) && (sel!=""))
             {
             
              var selP = document.getElementById("payment_method_id_"+sel);
              if (selP.checked == true)
              { 
               
                toClick = pms[c];

              }
             }
            }
	    // if is payment to hide, than disable it           
	    if (sid!="")        
            if ((payconf[sid].toString().indexOf(","+pms[c].value+",")>=0) || (payconf[sid].toString().indexOf(","+pms[c].value+"/")>=0))
            {
              pms[c].disabled = true;
              if (pms[c].checked==true) {
               // so we have a disabled payment checked
                clickit = true;
              }
            }
            else
            {
            // if it is not, than enable it
               pms[c].disabled = false;
             // check fore generally disabled paymments
             if (op_disabled_payments != null)
             if (op_disabled_payments.toString().indexOf(","+pms[c].value+",")>=0)
               pms[c].disabled = true;
            }

            
            
          }
          }
        }
        else
        {
         // no shipping is selected, so let's show all methods
            // if it is not, than enable it
             for (var ff=0; ff<pms.length; ff++)
             {
               pms[ff].disabled = false;
             // check fore generally disabled paymments
             if (op_disabled_payments != null)
             if (op_disabled_payments.toString().indexOf(","+pms[ff].value+",")>=0)
               pms[ff].disabled = true;
              }
         
        }
        
    if ((toClick != null) && (clickit==true))
    {


     toClick.checked = true;
     toClick.click();

    }
    else {  }     

   return "";
  }
   /*
   * This function fetches totals array from ajax data
   */
   function getTotals(shipping_id, payment_id)
   {
     
    if (shipping_id == "") shipping_id = 'shipment_id_0';
    if (payment_id == "") return "";


    
   /*
		echo '-- Checkout Debug--<br />
		
	Subtotal: '.$order_subtotal.'<br />
	Taxable: '.$order_taxable.'<br />
	Payment Discount: '.$payment_discount.'<br />
	Coupon Discount: '.$coupon_discount.'<br />
	Shipping: '.$order_shipping.'<br />
	Shipping Tax : '.$order_shipping_tax.'<br />
	Tax : '.$order_tax.'<br />
	------------------------<br />
	Order Total: '.$order_total.'<br />
	----------------------------<br />' 
		;
*/
    var x = document.getElementById(shipping_id+'_'+payment_id+'_subtotal');
    if (x == null) {
    
    
    return "";
    
    }
	
    
    var subtotal = x.value;

	d = document.getElementById('opc_coupon_code_returned'); 
	
	if (d != null)
	coupon_code_returned = d.value; 
	else coupon_code_returned = ''; 
	
    x = document.getElementById(shipping_id+'_'+payment_id+'_payment_discount');
    var payment_discount = x.value;

    x = document.getElementById(shipping_id+'_'+payment_id+'_coupon_discount');
    var coupon_discount = x.value;
	
	
	
	
    x = document.getElementById(shipping_id+'_'+payment_id+'_order_shipping');
    var order_shipping = x.value;
    
    x = document.getElementById(shipping_id+'_'+payment_id+'_order_shipping_tax');
    var order_shipping_tax = x.value;
    
    x = document.getElementById(shipping_id+'_'+payment_id+'_order_total');
    var order_total = x.value;
    
	x = document.getElementById(shipping_id+'_'+payment_id+'_coupon_discount2');
    var coupon_discount2 = x.value;
	
    x = document.getElementsByName(shipping_id+'_'+payment_id+'_tax');
	xall = document.getElementsByName(shipping_id+'_'+payment_id+'_tax_all');
	//op_log(xall[0].value); 
    xname = document.getElementsByName(shipping_id+'_'+payment_id+'_taxname');
	
    // check if we have shipping inside basket
      var sib = document.getElementById('shipping_inside_basket_cost');
      if ((sib != null))
      {
        if (op_show_prices_including_tax == '1')
        total_s = formatCurrency(parseFloat(order_shipping)+parseFloat(order_shipping_tax));
        else
        total_s = formatCurrency(parseFloat(order_shipping));
        
        sib.innerHTML = total_s;
      }
    
      
      if (true)
      {
         if (op_fix_payment_vat == true)
         {
          // tax rate calculaction
          if (isNaN(parseFloat(op_detected_tax_rate)) || parseFloat(op_detected_tax_rate)==0.00) 
          taxr = parseFloat(op_custom_tax_rate);
          else
          taxr = parseFloat(op_detected_tax_rate);
          
          //else taxr = parseFloat(op_custom_tax_rate);
          
          p_disc = (-1) * (1 + taxr) * parseFloat(payment_discount);
          
         }
         else
         {
        	p_disc = (-1) * parseFloat(payment_discount);
         }
        total_s = formatCurrency(parseFloat(p_disc));
        
        sib = document.getElementById('payment_inside_basket_cost');
        if (sib != null)
        sib.innerHTML = total_s;
      }


    
    op_tax_total = parseFloat(0.0);
    //tax_data = new Array(x.length);
	tax_data = new Array(1);
	tax_data[0] = "|0"; 
	
	tax_name = new Array(); 
	
     //opc update 2.0.108: disable for
	// opc update 2.0.127: if (x.length > 0) , reenabled for
	for (i=0; i<x.length; i++ )
    {
	  //i = 0; 
     //var y = x.value;
     //var arr = y.split("|");
     var arr = x[i].value.split("|");
     var tax = 0;
     if (arr.length == 2) tax = arr[1];
     else tax = x[i].value;
     
     tax_data[i] = x[i].value;
     if (!isNaN(parseFloat(tax)))
     op_tax_total += parseFloat(tax);
     
	 if (typeof xname[i] != 'undefined' && (xname[i] != null))
	 if (xname[i].value != null)
	 { 
	 tax_name[i] = xname[i].value; 
	 }
	 else
	 tax_name[i] = ''; 
	 
    }
	
	if (x.length == 0)
	 {
	    x2 = document.getElementById(shipping_id+'_'+payment_id+'_tax_all'); 
		if (x2 != null)
		 {
		   tax_data = new Array(1);
		   tax_data[0] = x2.value; 
		   tax_name[0] = op_shipping_tax_txt;
		   
		  
		   
		 }
		 else
		 tax_name[0] = op_shipping_tax_txt;
	 }
	
    var taxx;
	
	// init taxes first by hiding them
    for (i=x.length; i<=4; i++)
    {
     taxx = document.getElementById('tt_tax_total_'+i+'_div');
	  taxx2 = document.getElementById('tt_tax_total_'+i+'_div_basket');
     if (typeof taxx !='undefined' && (taxx != null))
     {
      taxx.style.display = 'none';
     }
	 if (typeof taxx2 !='undefined' && (taxx2 != null))
     {
      taxx2.style.display = 'none';
	 
     }
    }
    // formatting totals here:
	/*
    var t = document.getElementById('totalam');
	if (!(t != null)) {
	  insertHtml = '<div id="totalam"><div id="tt_order_subtotal_div"><span id="tt_order_subtotal_txt" class="bottom_totals_txt"></span><span id="tt_order_subtotal" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_order_payment_discount_before_div"><span id="tt_order_payment_discount_before_txt" class="bottom_totals_txt"></span><span class="bottom_totals" id="tt_order_payment_discount_before"></span><br class="op_clear"/></div><div id="tt_order_discount_before_div"><span id="tt_order_discount_before_txt" class="bottom_totals_txt"></span><span id="tt_order_discount_before" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_shipping_rate_div"><span id="tt_shipping_rate_txt" class="bottom_totals_txt"></span><span id="tt_shipping_rate" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_shipping_tax_div"><span id="tt_shipping_tax_txt" class="bottom_totals_txt"></span><span id="tt_shipping_tax" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_tax_total_0_div"><span id="tt_tax_total_0_txt" class="bottom_totals_txt"></span><span id="tt_tax_total_0" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_tax_total_1_div"><span id="tt_tax_total_1_txt" class="bottom_totals_txt"></span><span id="tt_tax_total_1" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_tax_total_2_div"><span id="tt_tax_total_2_txt" class="bottom_totals_txt"></span><span id="tt_tax_total_2" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_tax_total_3_div"><span id="tt_tax_total_3_txt" class="bottom_totals_txt"></span><span id="tt_tax_total_3" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_tax_total_4_div"><span id="tt_tax_total_4_txt" class="bottom_totals_txt"></span><span id="tt_tax_total_4" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_order_payment_discount_after_div"><span id="tt_order_payment_discount_after_txt" class="bottom_totals_txt"></span><span id="tt_order_payment_discount_after" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_order_discount_after_div"><span id="tt_order_discount_after_txt" class="bottom_totals_txt"></span><span id="tt_order_discount_after" class="bottom_totals"></span><br class="op_clear"/></div><div id="tt_total_div"><span id="tt_total_txt" class="bottom_totals_txt"></span><span id="tt_total" class="bottom_totals"></span><br class="op_clear"/></div></div>';    
	  f = document.getElementById('vmMainPageOPC'); 
	  dv = document.createElement("div");
	  dv.style.display = 'none'; 
	  dv.innerHTML = insertHtml; 
	  t = dv; 
	  f.appendChild(dv); 
	}
	*/
    var t1 = document.getElementById('tt_order_subtotal_txt'); 
	
    // for google ecommerce: op_total_total, op_tax_total, op_ship_total
    // with VAT
    op_total_total = order_total;
    // only VAT
    op_tax_total += parseFloat(order_shipping_tax);
    // without VAT
    op_ship_total = order_shipping;
    
    
    if (never_show_total == true)
    {
     t.style.display = 'none';
    } 

    
    
    
    if ((op_show_only_total != null) && (op_show_only_total == true))
    {
    	 stru = document.getElementById('tt_total_txt')
		 if (stru != null)
		 str = srtu.innerHTML;
		 else str = ''; 
    	 if (str == '')
		 {
         d1 = document.getElementById('tt_total_txt'); 
		 if (d1 != null)
		 d1.innerHTML = op_textinclship;
		 }
         if ((op_custom_tax_rate != null) && (op_add_tax != null) && (op_custom_tax_rate != '') && (op_add_tax == true))
         {
          d1 = document.getElementById('tt_total'); 
		  if (d1 != null)
		  d1.innerHTML = formatCurrency((1+parseFloat(op_custom_tax_rate))*parseFloat(order_total));
         }
         else
		 {
    	 d1 = document.getElementById('tt_total'); 
		 if (d1 != null)
		 d1.innerHTML = formatCurrency(order_total);
		 }
		 
	     d1 = document.getElementById('tt_order_payment_discount_before_div'); 
		 if (d1 != null)
		 d1.style.display = "none";
		 d1 = document.getElementById('tt_order_discount_before_div'); 
		 if (d1 != null) d1.style.display = "none";	
		 d1 = document.getElementById('tt_order_subtotal_div'); 
		 if (d1 != null) d1.style.display = 'none';
		 d1 = document.getElementById('tt_shipping_rate_div'); 
		 if (d1 != null) d1.style.display = 'none';
		 d1 = document.getElementById('tt_shipping_tax_div'); 
		 if (d1 != null) d1.style.display = 'none';
		 return true;
    }
    
   	  // add tax to payment discount
	  /*
	  if (false)
   	  if (op_fix_payment_vat == true)
   	  if ((op_no_taxes == true) || (op_no_taxes_show == true) || (op_show_andrea_view == true) || ((payment_discount_before == '1') && (op_show_prices_including_tax == '1')))
      {
          if (isNaN(parseFloat(op_detected_tax_rate)) || parseFloat(op_detected_tax_rate)==0.00) 
          taxr = parseFloat(op_custom_tax_rate);
          else
          taxr = parseFloat(op_detected_tax_rate);

          p_disc =  (1 + taxr) * parseFloat(payment_discount);
          payment_discount = parseFloat(p_disc);
      }
	  */
   
    
    var locp = 'after';
    if (payment_discount_before == '1')
    {
     locp = 'before';
    }
    else locp = 'after';
    
    
     if (payment_discount > 0)
     {
     
         stru = document.getElementById('tt_order_payment_discount_'+locp+'_txt'); 
		 if (stru!=null)
		 str = stru.innerHTML;
		 else str = ''; 
		 
    	 if (str == '' || str == op_payment_fee_txt)
		 {
        d1 = document.getElementById('tt_order_payment_discount_'+locp+'_txt'); 
	    if (d1 != null) d1.innerHTML = op_payment_discount_txt;
	    }
      d1 = document.getElementById('tt_order_payment_discount_'+locp); 
	  if (d1 != null) d1.innerHTML = formatCurrency((-1)*payment_discount);
      if (op_override_basket)
      {
       e1 = document.getElementById('tt_order_payment_discount_'+locp+'_basket');
       if (e1 != null)
       e1.innerHTML = formatCurrency((-1)*parseFloat(payment_discount));
       e2 = document.getElementById('tt_order_payment_discount_'+locp+'_txt_basket');
       if (e2 != null)
       e2.innerHTML = op_payment_discount_txt;
       if (!op_payment_inside_basket)
       {
       e3 = document.getElementById('tt_order_payment_discount_'+locp+'_div_basket');
       if (e3 != null)
       e3.style.display = "";
       }
      }
      d1 = document.getElementById('tt_order_payment_discount_'+locp+'_div'); 
	  if (d1 != null) d1.style.display = "block";
     }
     else
     if (payment_discount < 0)
     {
      stru = document.getElementById('tt_order_payment_discount_'+locp+'_txt');
	  if (stru!=null)
	  str = stru.innerHTML;
	  else str = ''; 
      if (str == '' || (str == op_payment_discount_txt))
	  {
      d1 = document.getElementById('tt_order_payment_discount_'+locp+'_txt'); 
	  if (d1 != null)
	  d1.innerHTML = op_payment_fee_txt;
	  }
      d1 = document.getElementById('tt_order_payment_discount_'+locp); 
	  if (d1 != null) d1.innerHTML = formatCurrency((-1)*parseFloat(payment_discount));
      d1 = document.getElementById('tt_order_payment_discount_'+locp+'_div'); 
	  if (d1!=null) d1.style.display = "block";
      if (op_override_basket)
      {
       d1 = document.getElementById('tt_order_payment_discount_'+locp+'_basket'); 
	   if (d1 != null) d1.innerHTML = formatCurrency((-1)*parseFloat(payment_discount));
       d1 = document.getElementById('tt_order_payment_discount_'+locp+'_txt_basket'); 
	   if (d1 != null) d1.innerHTML = op_payment_fee_txt;
       if (!op_payment_inside_basket)
	   {
       d1 = document.getElementById('tt_order_payment_discount_'+locp+'_div_basket'); 
	   if (d1 != null) d1.style.display = "";
	   }
      }
      
      
     }
     else 
     {
      stru = document.getElementById('tt_order_payment_discount_'+locp+'_txt'); 
	  if (stru != null) str = stru.innerHTML;
	  else str = ''; 
	  
      if (str == '')
	  {
      d1 = document.getElementById('tt_order_payment_discount_'+locp+'_txt'); 
	  if (d1 != null) d1.innerHTML = "";
	  }
      d1 = document.getElementById('tt_order_payment_discount_'+locp); 
	  if (d1 != null) d1.innerHTML = "";
      d1 = document.getElementById('tt_order_payment_discount_'+locp+'_div'); 
	  if (d1 != null) d1.style.display = "none";
      if (op_override_basket)
      {
       d1 = document.getElementById('tt_order_payment_discount_'+locp+'_basket'); 
	   if (d1 != null) d1.innerHTML = ""
       d1 = document.getElementById('tt_order_payment_discount_'+locp+'_txt_basket'); 
	   if (d1 != null) d1.innerHTML = "";
       d1 = document.getElementById('tt_order_payment_discount_'+locp+'_div_basket'); 
	   if (d1 != null) d1.style.display = "none";
      }
     
     }
    
	  
     //odl: if (Math.abs(coupon_discount) > 0)
	
	
	 locp = 'after'; 
	 //(coupon_code_returned != '') ||
	 if ( (Math.abs(coupon_discount) > 0))
     {
	  if (coupon_discount < 0) coupon_discount = parseFloat(coupon_discount) * (-1);
      stru = document.getElementById('tt_order_discount_'+locp+'_txt'); 
	  if (stru != null) str = stru.innerHTML;
	  else str = ''; 
      if (str == '')
	  {
      d1 = document.getElementById('tt_order_discount_'+locp+'_txt'); 
	  if (d1 != null) d1.innerHTML = op_coupon_discount_txt;
	  }
      d1 = document.getElementById('tt_order_discount_'+locp); 
	  if (d1!= null) d1.innerHTML = formatCurrency((-1)*parseFloat(coupon_discount));
      d1 = document.getElementById('tt_order_discount_'+locp+'_div'); 
	  if (d1 != null) d1.style.display = "block";
	  if (op_override_basket)
	   {
        d1 = document.getElementById('tt_order_discount_'+locp+'_basket'); 
		if (d1 != null) 
		{
		d1.innerHTML = formatCurrency((-1)*parseFloat(coupon_discount));
		d1.style.display = 'block'; 
		}
		//tt_order_discount_after_txt_basket
        d1 = document.getElementById('tt_order_discount_'+locp+'_div_basket'); 
		if (d1 != null) d1.style.visibility = 'visible';
        d1 = document.getElementById('tt_order_discount_'+locp+'_div_basket'); 
		if (d1 != null) 
		{
				d1.style.display = '';
				/*
		if (d1.nodeName.toLowerCase() != 'tr')
		d1.style.display = 'block';
		else d1.style.display = 'table-row'; 
		  */
		}
		//tt_order_discount_after_basket
		
	   }
     }
     else
     {
		 
      stru = document.getElementById('tt_order_discount_'+locp+'_txt'); 
	  if (stru != null) 
	  str = stru.innerHTML;
	  else str = ''; 
	  
      if (str == '')
	  {
      d1 = document.getElementById('tt_order_discount_'+locp+'_txt'); 
	  if (d1 != null) d1.innerHTML = "";
	  }
	  
      d1 = document.getElementById('tt_order_discount_'+locp+''); 
	  if (d1 != null) d1.innerHTML = "";
	  
      d1 = document.getElementById('tt_order_discount_'+locp+'_div'); 
	  if (d1 != null) d1.style.display = "none";
	   if (op_override_basket)
	   {
	    e3 = document.getElementById('tt_order_discount_'+locp+'_div_basket');
	    if (e3 != null)
		{
		
	    e3.style.display = "none";
		}
	   }
     }
	 
	 locp = 'before'; 
	 if (Math.abs(coupon_discount2) > 0)
     {
	  
	  if (coupon_discount2 < 0) coupon_discount2 = parseFloat(coupon_discount2) * (-1);
      
      d1 = document.getElementById('tt_order_discount_'+locp+'_txt'); 
	  if (d1 != null) d1.innerHTML = op_other_discount_txt;
	  
      d1 = document.getElementById('tt_order_discount_'+locp); 
	  if (d1 != null) d1.innerHTML = formatCurrency((-1)*parseFloat(coupon_discount2));
      d1 = document.getElementById('tt_order_discount_'+locp+'_div'); 
	  if (d1 != null) d1.style.display = "block";
	  
	  if (op_override_basket)
	   {
        d1 = document.getElementById('tt_order_discount_'+locp+'_basket'); 
		if (d1 != null) 
		{
		d1.innerHTML = formatCurrency((-1)*parseFloat(coupon_discount));
		d1.style.display = 'block'; 
		}
		//tt_order_discount_after_txt_basket
        d1 = document.getElementById('tt_order_discount_'+locp+'_div_basket'); 
		if (d1 != null) d1.style.visibility = 'visible';
        d1 = document.getElementById('tt_order_discount_'+locp+'_div_basket'); 
		if (d1 != null) 
		{
		if (d1 != null) 
		{
		d1.style.display = '';
		/*
		if (d1.nodeName.toLowerCase() != 'tr')
		d1.style.display = 'block';
		else d1.style.display = 'table-row'; 
		*/
		}
		//d1.style.display = 'block';
		}
		//tt_order_discount_after_basket
		d1 = document.getElementById('tt_order_discount_'+locp+'_basket'); 
	  if (d1 != null) d1.innerHTML = formatCurrency((-1)*parseFloat(coupon_discount2));
		
	   }
	  
     }
     else
     {
     
      
      d1 = document.getElementById('tt_order_discount_'+locp+'_txt'); 
	  if (d1 != null) d1.innerHTML = "";
      d1 = document.getElementById('tt_order_discount_'+locp+''); 
	  if (d1 != null) d1.innerHTML = "";
      d1 = document.getElementById('tt_order_discount_'+locp+'_div'); 
	  if (d1 != null) d1.style.display = "none";
	   if (op_override_basket)
	   {
	    e3 = document.getElementById('tt_order_discount_'+locp+'_div_basket');
	    if (e3 != null)
	    e3.style.display = "none";
	   }
     }
	 locp = 'after'; 
	
    if ((op_no_taxes != true) && (op_no_taxes_show != true))
    {
    stru = document.getElementById('tt_order_subtotal_txt'); 
	if (stru != null) str = stru.innerHTML;
	else str = ''; 
    if (str == '')
	{
     d1 = document.getElementById('tt_order_subtotal_txt'); 
	 if (d1 != null) d1.innerHTML = op_subtotal_txt;
    }
    if (op_show_andrea_view == true)
	{
    d1 = document.getElementById('tt_order_subtotal'); 
	if (d1 != null) d1.innerHTML = formatCurrency(parseFloat(subtotal)+parseFloat(op_basket_subtotal_items_tax_only));
	}
    else
	{
    d1 = document.getElementById('tt_order_subtotal'); 
	if (d1 != null) d1.innerHTML = formatCurrency(subtotal);
	}
    d1 = document.getElementById('tt_order_subtotal_div'); 
	if (d1 != null) d1.style.display = 'block';
    if (op_override_basket)
    {
     stru = document.getElementById('tt_order_subtotal_txt_basket'); 
	 if (stru != null) str = stru.innerHTML;
	 else str = ''; 
     if (str == '')
	 {
	 
     d1 = document.getElementById('tt_order_subtotal_txt_basket'); 
	 if (d1 != null) d1.innerHTML = op_subtotal_txt;
	 }
     if (op_show_andrea_view == true)
	 {
     d1 = document.getElementById('tt_order_subtotal_basket'); 
	 if (d1 != null) d1.innerHTML = formatCurrency(parseFloat(subtotal)+parseFloat(op_basket_subtotal_items_tax_only));
	 }
     else
	 {
     d1 = document.getElementById('tt_order_subtotal_basket'); 
	 if (d1 != null) d1.innerHTML = formatCurrency(subtotal);
	 }
     d1 = document.getElementById('tt_order_subtotal_div_basket'); 
	 if (d1 != null) d1.style.display = '';
     
    }
    }
    else
    {
     d1 = document.getElementById('tt_order_subtotal_div'); 
	 if (d1 != null) d1.style.display = 'none';
         if (op_override_basket)
    {
     d1 = document.getElementById('tt_order_subtotal_div_basket'); 
	 if (d1 != null) d1.style.display = 'none';
     
    }

    }
    
    if (op_noshipping == false)
    {
     stru = document.getElementById('tt_shipping_rate_txt'); 
	 
	 if (stru!=null) str = stru.innerHTML;
	 else str = ''; 
	 
     if (str == '')
	 {
	 
     d1 = document.getElementById('tt_shipping_rate_txt'); 
	 if (d1 != null) d1.innerHTML = op_shipping_txt;
     }
    
    if (isNotAShippingMethod()) 
     { 
     if (op_override_basket)
	 {
      d1 =  document.getElementById('tt_shipping_rate_basket'); 
	  if (d1 != null) d1.innerHTML = op_lang_select;
	 }
     d1 = document.getElementById('tt_shipping_rate'); 
	 if (d1 != null) d1.innerHTML = op_lang_select;
     
     
     }
    else
    if (op_no_taxes_show != true && op_show_andrea_view != true)
    {
     if (op_show_prices_including_tax == '1')
	 {
      d1 = document.getElementById('tt_shipping_rate'); 
	  if (d1 != null) 
	  {
	  		 var opcs = parseFloat(order_shipping)+parseFloat(order_shipping_tax); 
			 if ((opcs == 0) && (use_free_text))
			 d1.innerHTML = opc_free_text;
			 else
		     d1.innerHTML = formatCurrency(opcs);

	 // d1.innerHTML = formatCurrency(parseFloat(order_shipping)+parseFloat(order_shipping_tax));
	  }
	 }
     else
	 {
     d1 = document.getElementById('tt_shipping_rate'); 
	 if (d1 != null) 
	 {
	 	     var opcs = parseFloat(order_shipping); 
			 if ((opcs == 0) && (use_free_text))
			 d1.innerHTML = opc_free_text;
			 else
		     d1.innerHTML = formatCurrency(opcs);

	    
	 }
	 }
     if (op_override_basket)
     {
      if (isNotAShippingMethod()) 
	  {
	  d1 = document.getElementById('tt_shipping_rate_basket'); 
	  if (d1 != null) d1.innerHTML = op_lang_select;
	  }
      else
      {
       if (op_show_prices_including_tax == '1')
	   {
        d1 = document.getElementById('tt_shipping_rate_basket'); 
		if (d1 != null) 
		{
		     var opcs = parseFloat(order_shipping)+parseFloat(order_shipping_tax); 
			 if ((opcs == 0) && (use_free_text))
			 d1.innerHTML = opc_free_text;
			 else
		     d1.innerHTML = formatCurrency(opcs);
		}
	   }
       else
	   {
       d1 = document.getElementById('tt_shipping_rate_basket'); 
	   if (d1 != null) 
	     {
		     var opcs = parseFloat(order_shipping);
			 if ((opcs == 0) && (use_free_text))
			 d1.innerHTML = opc_free_text;
			 else
		     d1.innerHTML = formatCurrency(opcs);
		 
		 }
	   }
      }
     }
    }
    else
    {
     d1 = document.getElementById('tt_shipping_rate'); 
	 if (d1 != null) 
	 {
	         var opcs = parseFloat(order_shipping)+parseFloat(order_shipping_tax);
			 if ((opcs == 0) && (use_free_text))
			 d1.innerHTML = opc_free_text;
			 else
		     d1.innerHTML = formatCurrency(opcs);
	 
	 }
     if (op_override_basket)
     {
      d1 = document.getElementById('tt_shipping_rate_basket'); 
	  if (d1 != null) 
	  {
	         var opcs = parseFloat(order_shipping)+parseFloat(order_shipping_tax);
			 if ((opcs == 0) && (use_free_text))
			 d1.innerHTML = opc_free_text;
			 else
		     d1.innerHTML = formatCurrency(opcs);
	    
	  }
     }
    }
    d1 = document.getElementById('tt_shipping_rate_div'); 
	if (d1 != null) d1.style.display = 'block';
	
	if (op_override_basket)
	{
	 if (!op_shipping_inside_basket)
	 {
	  d1 = document.getElementById('tt_shipping_rate_div_basket'); 
	  if (d1 != null) d1.style.display = '';
	 }
	 else 
	 {
	 d1 = document.getElementById('tt_shipping_rate_div_basket'); 
	 if (d1 != null) d1.style.display = 'none';
	 }
	}
	
	  
	  
    
     
	
    if ((order_shipping_tax > 0) && (op_sum_tax != true) && (op_no_taxes != true) && (op_no_taxes_show != true) && (op_show_andrea_view!=true) && (op_show_prices_including_tax != '1'))
    {
    stru = document.getElementById('tt_shipping_tax_txt'); 
	if (stru!=null) str = stru.innerHTML;
	else str = ''; 
	
    if (str == '')
	{
	
    d1 = document.getElementById('tt_shipping_tax_txt'); 
	if (d1 != null) d1.innerHTML = op_shipping_tax_txt;
	}
    d1 = document.getElementById('tt_shipping_tax'); 
	if (d1 != null) d1.innerHTML = formatCurrency(order_shipping_tax);
	
    d1 = document.getElementById('tt_shipping_tax_div'); 
	if (d1 != null) d1.style.display = "block";
    }
    else
    {
     stru = document.getElementById('tt_shipping_tax_txt'); 
	 if (stru != null) str = stru.innerHTML;
	 else str = ''; 
	 
     if (str == '')
	 {
      d1 = document.getElementById('tt_shipping_tax_txt'); 
	  if (d1 != null) d1.innerHTML = "";
	 }
     d1 = document.getElementById('tt_shipping_tax'); 
	 if (d1 != null) d1.innerHTML = "";
	 
     d1 = document.getElementById('tt_shipping_tax_div'); 
	 if (d1 != null) d1.style.display = "none";
    }
    }
    else
    {
     d1 = document.getElementById('tt_shipping_rate_div'); 
	 if (d1 != null) d1.style.display = 'none';
     d1 = document.getElementById('tt_shipping_tax_div'); 
	 if (d1 != null) d1.style.display = "none";
    }
    
    if ((op_no_taxes != true) && (op_no_taxes_show != true) && (op_show_andrea_view!=true))
    {
    for (i = 0; i<tax_data.length; i++)
    {
     var tx = document.getElementById('tt_tax_total_'+i);
     var tx_txt = document.getElementById('tt_tax_total_'+i+'_txt');
     var txt_div = document.getElementById('tt_tax_total_'+i+'_div');
     if (tx != null)
     {
      rate_arr = tax_data[i].split('|');
	  
	  //console.log(tax_data); 
	  //console.log(rate_arr); 
      //if (rate_arr[0] == '')
      //tx_txt.innerHTML = op_tax_txt;
      //else
      {
      if (rate_arr[1]>0)
      {
	 
	   test1 = parseFloat(rate_arr[0])*100;
       test2 = Math.round(parseFloat(rate_arr[0])*100); 
	   if (test1!=test2)
	   test2 = Math.round(parseFloat(rate_arr[0])*1000)/10;
	   if (test2 != test1)
	   test2 = Math.round(parseFloat(rate_arr[0])*10000)/100;
	   
	   taxr = test2+'%';
	   
      	if (rate_arr[0] != '')
      	{
       
	   if ((tax_name[i] != null) && (tax_name[i] != ''))
	   tx_txt.innerHTML = tax_name[i]; 
	   else
       tx_txt.innerHTML = op_tax_txt+'('+taxr+')';
	   
       if (op_basket_override)
       {
	   tx_txt2 = document.getElementById('tt_tax_total_'+i+'_txt_basket');
	   
	   if (tx_txt2 != null)
	   {
	   if ((tax_name[i] != null) && (tax_name[i] != ''))
	    tx_txt2.innerHTML = tax_name[i]; 
	    else
        tx_txt2.innerHTML = op_tax_txt+'('+taxr+')';
       }
	   else
	   {
	     // if the template does not have the posisions for all of the tax rates, it won't be shown !
	   }
	   
	   
	   
	   }
       }
       else
       {
       tx_txt.innerHTML = op_tax_txt;
	   if (op_basket_override)
       {
        d1 = document.getElementById('tt_tax_total_'+i+'_txt_basket'); 
		if (d1 != null) d1.innerHTML = op_tax_txt;
       }

       }
	   
	    if (typeof tx != 'undefined')
        if (tx != null) 
       if ((tax_data.length == 1) && (op_sum_tax == true))
       {
        tx.innerHTML = formatCurrency(parseFloat(rate_arr[1])+parseFloat(order_shipping_tax));
       }
       else
	   {
	   
       tx.innerHTML = formatCurrency(rate_arr[1]);
	   }
	   	if (typeof txt_div != 'undefined')
        if (txt_div != null) 
       txt_div.style.display = 'block';
	   if (op_basket_override)
       {
        if ((tax_data.length == 1) && (op_sum_tax == true))
        {
          document.getElementById('tt_tax_total_'+i+'_basket').innerHTML = formatCurrency(parseFloat(rate_arr[1])+parseFloat(order_shipping_tax));
          document.getElementById('tt_tax_total_'+i+'_div_basket').style.display = '';
        }
        else
        {
        d1 = document.getElementById('tt_tax_total_'+i+'_basket'); 
		if (d1 != null) d1.innerHTML = formatCurrency(rate_arr[1]);
        d1 = document.getElementById('tt_tax_total_'+i+'_div_basket'); 
		if (d1 != null) d1.style.display = '';
        }
       }
       
      }
      else
      {
	   if (typeof tx_txt != 'undefined')
       if (tx_txt != null) 
       tx_txt.innerHTML = "";
	   if (typeof tx != 'undefined')
       if (tx != null) 
       tx.innerHTML = "";
	   if (typeof txt_div != 'undefined')
       if (txt_div != null) 
       txt_div.style.display = 'none';
       if (op_basket_override)
       {
        d1 = document.getElementById('tt_tax_total_'+i+'_div_basket'); 
		if (d1 != null) d1.style.display = 'none';
       }
      }
      }
     }
    }
    }
    stru = document.getElementById('tt_total_txt'); 
	if (stru != null) str = stru.innerHTML;
	else str = ''; 
    if (str == '')
	{
     d1 = document.getElementById('tt_total_txt'); 
	 if (d1 != null) d1.innerHTML = op_textinclship;
	}
    d1 = document.getElementById('tt_total'); 
	if (d1 != null) d1.innerHTML = formatCurrency(order_total);
    if (op_basket_override)
    {
     d1 = document.getElementById('tt_total_basket'); 
	 if (d1 != null) d1.innerHTML = formatCurrency(order_total);
    }
    return "";
   }
   
    function syncShippingAndPayment()
    {
    if (op_noshipping == false) 
    {
     val = getVShippingRate();

     if (op_shipping_inside_basket)
     {
      var d = document.getElementById('new_shipping');
      d.value = val;
     }
     var s = document.getElementById('shipping_rate_id_coupon');
     if (s != null)
	 {
	  s.value = val;
	 }
	 
    }
	 valp = getValueOfSPaymentMethod();
    
     if (op_payment_inside_basket)
     {
      var df = document.getElementById('new_payment');
      df.value = valp;
     }
	
	 dd = document.getElementById('paypalExpress_ecm');
	 if (dd != null && (typeof(dd) != 'undefined'))
	 if (valp == op_paypal_id)
	 {
		// last test:
		// direct payments use payment_method_id_(PAYPALID)
		// 
		if (op_paypal_direct == true)
		{
		xx = document.getElementById('payment_method_id_'+valp); 
		if (xx.checked != true)
		dd.value = '2';
		else 
		dd.value = '';
		}
		else
		{
		 dd.value = '2';
		}
		
	    
	    
	 }
	 else
	 {
	   dd.value = '';
	 }
	 
	 var p = document.getElementById('payment_method_id_coupon');
	 
	 if (p != null)
	 {
	  p.value = valp;
	 }
	      
     
    }
	
	/* changes text of Order total
	*   msg3 is the "Order total: "
	*   curr is currency symbol html encoded
	*   order_total is VM order total (for US tax system it is generated in shippnig methods)  	
  */ 
	function changeTextOnePage(msg3, curr, order_total) {
		 
	  syncShippingAndPayment();
	  
	
	  
	  /*
	  if (op_payment_inside_basket || op_shipping_inside_basket)
	  {
	   syncShippingAndPayment();
	  }
	  */
	  if ((never_show_total != null) && (never_show_total == true) && (!op_override_basket)) return true;
	  var op_ship_base = 0;
	  // new in version 2
	 
	  var ship_id = getInputIDShippingRate();
	  
	  sd = document.getElementById('saved_shipping_id'); 
	  if (sd != null)
	   sd.value = ship_id; 
	   
	  // this part will be moved to OPC extensions
	  /*
	  if (false)
	  {
	  jQuery(this).data("usps");
	  jQuery("#usps_name").val(usps.service) ;
	  jQuery("#usps_rate").val(usps.rate) ;
	  }
	  */
	  // end of OPC extensions
	  
	  var payment_id = getPaymentId();
					
	  return getTotals(ship_id, payment_id);
	  
	}
	
	function op_show_all_including(msg3, strtotal, curr, tax_base, tax, tax_rate, op_ship_base, payment_discount)
	{
	  var ship_info = '';
	  var payment_info = '';
	  var product_grand = '';
	  
	  if (op_always_show_all)
	  {
	   ship_info = '<span style="font-size: 100%">'+op_shipping_txt+': '+formatCurrency(op_ship_base)+"</span><br />";  
	   if ((payment_discount != null) && (payment_discount != '') && (payment_discount != 0))
	   payment_info = '<span style="font-size: 100%">Payment discount & fees: '+formatCurrency((-1)*parseFloat(payment_discount.toString()))+"</span><br />";  
	   //if (op_show_prices_including_tax=='1')
	   //var op_grand_subtotal2 = parseFloat(op_grand_subtotal - parseFloat(parseFloat(op_grand_subtotal) / (1+parseFloat(tax_rate))));
	   if (op_show_prices_including_tax=='1')
	   product_grand = '<span style="font-size: 100%">Product grand subtotal: '+formatCurrency(op_grand_subtotal/(1+tax_rate))+"</span><br />";  
	   else
	   product_grand = '<span style="font-size: 100%">Product grand subtotal: '+formatCurrency(op_grand_subtotal)+"</span><br />";  
	  }
	  
	  op_total_total = strtotal;
	  
	  
	  var tax_rate_perc = parseFloat(tax_rate)*100;
	  
	  if (Math.round(tax_rate_perc)==tax_rate_perc)
	  tax_rate_perc = tax_rate_perc.toFixed(0).toString();
	  else
	  tax_rate_perc = tax_rate_perc.toFixed(1);
	  
	  var cup_txt = '';
	  if (op_coupon_amount != null) 
	  if (op_coupon_amount != 0)
	  {
	    cup_txt = '<span style="font-size: 100%">'+op_coupon_discount_txt+': -'+formatCurrency(op_coupon_amount.toString())+"</span><br />";
	  }
	  var show_text = msg3+"<span style='font-size:200%;'>"+formatCurrency(op_total_total)+" </span>";
		
	  if (((tax > 0) && (tax_rate > 0)) && (op_dont_show_taxes != '1'))
	  {
	   
	  // tax_base = curr+formatCurrency(tax_base);
	 //  tax = tax);
	   //tax_rate = (parseFloat(tax_rate.toString()) * 100).toFixed(2);
	   show_text = product_grand+ship_info+payment_info+"<span style='font-size: 100%'>"+op_subtotal_txt+": "+formatCurrency(tax_base)+"</span><br /><span style='font-size: 100%;'>"+op_tax_txt+" ("+tax_rate_perc+"%): "+formatCurrency(tax)+"</span><br />"+cup_txt+show_text;
	  }
	  // future release
	  /*
	  var tt_order_subtotal = document.getElementById("tt_order_subtotal");
	  if (tt_order_subtotal == null)
	  */
	  d1 = document.getElementById("totalam"); 
	  if (d1 != null) d1.innerHTML = show_text;
	  // future release:
	  /*
	  if (false)
	  {
	   // tax section
	    tax_txt = document.getElementById('tt_tax_total_txt');
	    tax_div = document.getElementById('tt_tax_total');
	    if (tax_txt.innerHTML == '') tax_txt.innerHTML = op_tax_txt+" ("+tax_rate_perc+"%): ";
	    tax_div.innerHTML = curr+tax;
	   // end tax section
	   
	   // total total section
	    total_txt = document.getElementById('tt_total_txt');
	    total_div = document.getElementById('tt_total');
	    if (total_txt.innerHTML == '') total_txt.innerHTML = msg3;
	    total_div.innerHTML = curr+op_total_total;
	   // end of total total section
	   
	   // coupon discount 
	    if ((op_coupon_amount != null) && (parseFloat(op_coupon_amount) != 0))
	    {
	     if (payment_discount_before == '1')
	     {
	      //cup_txt = document.getElementById('
	     }
	     else
	     {
	     }
	    }
	    
	   // end of coupon discount visibility
	   
	    
	    
	  }
	  */
	}
	
	// public method for url decoding
	function url_decode_op (string) {
		return this._utf8_decode(unescape(string));
	}

	
	// private method for UTF-8 decoding
	function _utf8_decode (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
	
	 
	
	 /* This function alters visibility of shipping address
	 *
	 */
	 function showSA(chk, divid)
	 {
	   if (document.getElementById(divid))
	   document.getElementById(divid).style.display = chk.checked ? '' : 'none';
	  
	   if (chk.checked)
	   {
	   elopc = document.getElementById('shipto_virtuemart_country_id');
	   /*
	   jQuery('#opcform').('input,textarea,select,button').each(function(el){
			if (el.hasClass('opcrequired')) {
				el.attr('class', 'required');
			}
	   
	   });
	   */
	   }
	   else 
	   {
	   elopc = document.getElementById('virtuemart_country_id');
	   /*
	     jQuery('#opcform').('input,textarea,select,button').each(function(el){
			if (el.hasClass('required')) {
				el.attr('class', 'opcrequired');
			}
	   
	   });
	   */
	   }
	   
	   
	   
	   // if we have a new country in shipping fields, let's update it
	   if (elopc != null)
	   {
	   
	   op_runSS(elopc);
	   }
	   //validateCountryOp(false, false);
	 }
	 
	 
	 /*
	 * This function is triggered when clicked on payment methods when CC payments are there
	 */
	 function runPayCC(msg_info, msg_text, msg3, curr, order_total)
	 {
	  //try
	  {
	   if (typeof changeCreditCardList == 'function')
	    {
	      changeCreditCardList();
	    }
	  }
	  //catch (e) 
	  {
	   
	  }
	  runPay(msg_info, msg_text, msg3, curr, order_total);
	  return true;
	 }
	 // this function is used when using select box for payment methods
	 function runPaySelect(element)
	 {
	    ind = element.selectedIndex;
	    value = element.options[ind].value;
		//IE8 fix, hasExtra = element.options[ind].getAttribute('rel'); 
		hasExtra = getAttr(element.options[ind], 'rel'); 
		
		if (hasExtra != null)
		if (hasExtra != 0)
		{
			d = document.getElementById('extra_payment_'+hasExtra); 
			if (d != null)
			{
				d.style.display = 'block'; 
				op_last_payment_extra = d; 
			}
		}
		else
		{
			if (op_last_payment_extra != null)
				op_last_payment_extra.style.display = 'none'; 
		}
	    runPay(value, value, op_textinclship, op_currency, op_ordertotal);
	 }
	 /*
	 * This function is triggered when clicked on payment methods when CC payments are NOT there
	 */
	 function runPay(msg_info, msg_text, msg3, curr, order_total)
	 {
	  setOpcId(); 
	  if (typeof(msg_info) == 'undefined' || msg_info == null || msg_info == '')
	  {
	    var p = getValueOfSPaymentMethod();
	    msg_info = p;
	    msg_text = p;
	    msg3 = op_textinclship;
	    curr = op_currency;
	    order_total = op_ordertotal;
	  }
	  
	  if (typeof(pay_btn[msg_info])!='undefined' && pay_msg[msg_info]!=null) msg_info = pay_msg[msg_info];
	  else msg_info = pay_msg['default'];
	  
	  if (typeof(pay_btn[msg_text])!='undefined' && pay_btn[msg_text]!=null) msg_text = pay_btn[msg_text];
	  else msg_text = pay_btn['default'];
	 
	  dp = document.getElementById("payment_info"); 
	  if (dp != null)
	  dp.innerHTML = msg_info;
	  cbt = document.getElementById("confirmbtn");
	  if (cbt != null)
	  {
	    
	    if (cbt.tagName.toLowerCase() == 'input')
	    cbt.value = msg_text;
	    else cbt.innerHTML = msg_text;
	  }
	  //default_ship = getIDvShippingRate();
	  changeTextOnePage(msg3, curr, order_total);
	  for (var x=0; x<callAfterPaymentSelect.length; x++)
	   {
	     eval(callAfterPaymentSelect[x]);
	   }
	   
	  return true;
	 }
/*	 
	 function runPay()
	 {
	  var p = getValueOfSPaymentMethod();
	  runPay(p, p, op_textinclship, op_currency, op_ordertotal);
	 }
*/
	 // gets id of selected payment method
	 function getPaymentId()
	 {
	  return getValueOfSPaymentMethod();
	 }

// return address query as &address_1=xyz&address_2=yyy 
function op_getaddress()
{
 var ret = '';
 if (shippingOpen())
 {
  // different shipping address is activated
  
  {
   a1 = document.getElementById('shipto_address_1_field');
   if (a1 != null)
   {
     ret += '&address_1='+op_escape(a1.value);
   }
   a2 = document.getElementById('shipto_address_2_field'); 
   if (a2 != null)
   {
     ret += '&address_2='+op_escape(a2.value);
   }
  }
 }
 if (ret == '')
 {
   a1 = document.getElementById('address_1_field');
   if (a1 != null)
   {
     ret += '&address_1='+op_escape(a1.value);
   }
   a2 = document.getElementById('address_2_field'); 
   if (a2 != null)
   {
     ret += '&address_2='+op_escape(a2.value);
   }
  
 }
 
 return ret;
 
}

function buildExtra()
{
   var eq = ''; 
   isSh = shippingOpen(); 
    
	   for (var i=0; i<op_userfields.length; i++)
	    {
		  // filter only needed
		  //if (((op_userfields[i].indexOf('shipto_')!=0) && (!isSh))  || (op_userfields[i].indexOf('shipto_')==0) && (isSh))
		  
		  if ((((op_userfields[i].indexOf('shipto_')==0) && (isSh)) || (op_userfields[i].indexOf('shipto_')!=0)))
		   {
		     /*
		     if (typeof eval('document.adminForm.'+op_userfields[i]) != 'undefined')
			 {
			 elopc = eval('document.adminForm.'+op_userfields[i]);
			 }
			 */
			 //else
		     elopc = document.getElementsByName(op_userfields[i]); 
			  
			 if (elopc != null)
			 for (var j=0; j<elopc.length; j++)
			  {
			   switch (elopc[j].type)
			    {
				  case 'password':  break;
				  case 'text':
					eq += '&'+op_userfields[i]+'='+op_escape(elopc[j].value);
					break; 
				  case 'select-one': 
				   if ((typeof elopc[j].value != 'undefined') && ((elopc[j].value != null)))
				   eq += '&'+op_userfields[i]+'='+op_escape(elopc[j].value);
				   else
				    {
					   if ((typeof elopc[j].options != 'undefined') && (elopc[j].options != null) && (typeof elopc[j].selectedIndex != 'undefined') && (elopc[j].selectedIndex != null))
					    eq += '&'+op_userfields[i]+'='+op_escape(elopc[j].options[elopc[j].selectedIndex].value);
					     
					}
					break;
				  case 'radio': 
				   if ((elopc[j].checked == true) && (elopc[j].value != null))
				      eq += '&'+op_userfields[i]+'='+op_escape(elopc[j].value);
				   else
				   if (elopc[j].checked == true)
				    eq += '&'+op_userfields[i]+'=1';
				   break; 
				  case 'hidden': 
				    if ((typeof elopc[j].value != 'undefined') && (elopc[j].value != null))
				    eq += '&'+op_userfields[i]+'='+op_escape(elopc[j].value);
					break; 
				 
				  default: 
				    if ((typeof elopc[j].value != 'undefined') && (elopc[j].value != null))
				    eq += '&'+op_userfields[i]+'='+op_escape(elopc[j].value);
				    break; 
				}
			  }
			  elopc = document.getElementsByName(op_userfields[i]+'[]'); 
			  if (elopc != null)
			   for (var j=0; j<elopc.length; j++)
			    {
				  
				    if ((typeof elopc[j].value != 'undefined') && (elopc[j].value != null))
					if (((typeof elopc[j].checked != 'undefined') && (elopc[j].checked)) || 
					 (typeof elopc[j].selected != 'undefined') && (elopc[j].selected))
				    eq += '&'+op_userfields[i]+'[]='+op_escape(elopc[j].value);
				   
				}
		   }
		}
	//op_log(eq); 
	return eq; 
}

function op_getSelectedCountry()
{
	
	  var sel_country = "";
	  if (shippingOpen())
	  {
	   // different shipping address is activated
	    
	     var sa = document.getElementById("sa_yrtnuoc_field");
	     if (sa != null)
	     sel_country = sa.value;
	     else
		 {
		
		  sa = document.getElementById('shipto_virtuemart_country_id');
		  if (sa != null)
		  if ((typeof sa.options != 'undefined') && (sa.options != null))
		     sel_country = sa.options[sa.selectedIndex].value;
		  else
		  if ((sa != null) && (sa.value != null)) sel_country = sa.value;
		  
		  //sel_country = sa.value;
		 }
	  }

	  // we will get country from bill to
	  if (sel_country == "")
	  {
	    var ba = document.getElementById("country_field");
	    if (ba!=null)
	    sel_country = ba.value;
		else
		{
	     ba = document.getElementById('virtuemart_country_id');
		 if (ba != null)
		 {
		 if ((typeof ba.options != 'undefined') && (ba.options != null))
		  sel_country = ba.options[ba.selectedIndex].value;
		 else
		 if ((ba != null) && (ba.value != null)) sel_country = ba.value;
		 }
		}
		
	  }

	 return sel_country; 
	 
}
	 
function op_getSelectedState()
    {
     sel_state = '';
   	if (shippingOpen())
	{
	  
	     var sa = document.getElementById("shipto_virtuemart_state_id");
	     if (sa != null)
		 {
		 if (((typeof sa.options != 'undefined') && (sa.options != null)) && (sa.selectedIndex != null))
		 {
		 if (typeof sa.options[sa.selectedIndex] != 'undefined')
	     sel_state = sa.options[sa.selectedIndex].value;
		 }
		 else 
		  {
		    // maybe it's hidden
			if ((typeof sa.value != 'undefined') && (sa.value != null))
			 sel_state = sa.value; 
		  }
		 }
	    
    }
    if (sel_state == '')
    {
    var c2 = document.getElementById("virtuemart_state_id");
    if (c2!=null)
    {
	if ((typeof c2.options != 'undefined') && (c2.options != null))
	{
	if (typeof c2.options[c2.selectedIndex] != 'undefined')
	sel_state = c2.options[c2.selectedIndex].value;
	}
	else 
	  {
		 
		  
		    // maybe it's hidden
			if ((typeof c2.value != 'undefined') && (c2.value != null))
			 sel_state = c2.value; 
		     
	  }
    }
    }
 return sel_state;
}

// return true if the shipping fields are open
function shippingOpen()
{
    var sc = document.getElementById("sachone");
    if (sc != null)
	{
	  if ((typeof(sc.checked) != 'undefined' && sc.checked) || (typeof(sc.selected) != 'undefined' && sc.selected))
	  {
	    
		if (!shippingOpenStatus)
	    for (var i=0; i<shipping_obligatory_fields.length; i++)
		 {
		   d = document.getElementById('shipto_'+shipping_obligatory_fields[i]+'_field'); 
		   if ((typeof d != 'undefined') && (d != null))
		    {
			  d.setAttribute('required', 'required'); 
			  d.setAttribute('aria-required', 'true'); 
			  if (d.className.indexOf('opcrequired')>=0) d.className = d.className.split('opcrequired').join(''); 
			  if (d.className.indexOf('required')<0) d.className += ' required'; 
			  
			  
			}
			
		   
		 }
		 
		shippingOpenStatus = true; 
	    return true;
	  }
	}
	
	if (shippingOpenStatus)
	 {
	  for (var i=0; i<shipping_obligatory_fields.length; i++)
		 {
		   d = document.getElementById('shipto_'+shipping_obligatory_fields[i]+'_field'); 
		   if ((typeof d != 'undefined') && (d != null))
		    {
			  d.removeAttribute('required'); 
			  d.removeAttribute('aria-required'); 
			  d.removeAttribute('aria-invalid'); 
			  d.className = d.className.split('required').join(''); 
			  d.className = d.className.split('invalid').join(''); 
			  
			}
		  // 
		 }
	  //... 
	 }
	shippingOpenStatus = false; 
	return false;
	
}

function doublemail_checkMailTwo(el)
{
	email_check(el); 
	doublemail_checkMail(); 
}

function op_log(msg)
{
  if ((typeof console != 'undefined') && (console != null))
  if (typeof console.log != 'undefined')
  {
   
   console.log(msg); 
  }
}

function op_getZip()
{
    var sel_zip = '';
    
	if (shippingOpen())
     {
	    {
	     var sa = document.getElementById("shipto_zip_field");
	     if (sa)
	     sel_zip = sa.value;
	    }
	  }
    if (sel_zip == '')
    {
    var c2 = document.getElementById("zip_field");
    if (c2!=null)
    {
	sel_zip = c2.value;
    }
    }
 return sel_zip;
}
	 
	 
	 // aboo is whether to alert user
	 // runCh is boolean whether to change stateList
	 function validateCountryOp(runCh, aboo, elopc)
	 {
	  
	  if (runCh != false )
	  { 
	   if (typeof changeStateList == 'function')
	   {
	    if (typeof states != 'undefined')
	     {
	      var d = document.getElementById('state');
		  try
		  {
	      origl = d.options.length;
	      statetxt = d.options[d.selectedIndex].text;
	      index = d.selectedIndex;
	      } catch (e) {;}
	      changeStateList(elopc);
	      try
	      {
	      if (d.options.length == origl)
	      {
	       if (statetxt == d.options[index].text) d.selectedIndex = index;
	      }
	      } catch(e) {;}
	      
	     }
	   }
	   
	  }
	  
	  if (op_last_field)
	  if (typeof elopc != 'undefined' && elopc != null)
	  if (op_last1 == 'state' || op_last2 == 'sa_etats')
	  {
	   if (elopc.name == 'country')
	   {
	   d = document.getElementById('state');
	   if (d != null)
	   {
	    if (d.options.length == 1)
	    return op_runSS(elopc, null, true);
	   }
	   }
	   else
	   {
	    d = document.getElementById('sa_etats');
	    if (d != null)
	    {
	     if (d.options.length == 1)
	     return op_runSS(elopc, null, true);
	    }
	   
	   }
	  }
	  
	  op_runSS(elopc);
	 }
	  
// this function checks shipping for logged in users
function check_ship_and_pay()
{
 // not for OPC2 yet
 /*
 if (false)
 if (!validateCC())
 {
  alert(op_general_error); 
  return false;
 }
 */
 var agreed=document.getElementById('agreed_field');
 if (agreed != null)
 if (agreed.checked != null)
 if (agreed.checked != true) 
 {
  alert(agreedmsg);
  return false;
 }
 
 var invalid_c = document.getElementById('invalid_country');
 if (invalid_c != null)
 {
  alert (noshiptocmsg);
  return false;
 }
 
 if (isNotAShippingMethod()) 
 {
  alert(shipChangeCountry);
  return false;
 }
 
 
  trackGoogleOrder();

  // to prevend double clicking
  so = document.getElementById('confirmbtn_button'); 
  if (so != null)
  {
   so.disabled = true; 
   //alert('ok');
  }
  else
  {
  so = document.getElementById('confirmbtn');
  if (so != null)
  so.disabled = true;
  }
  // lets differ submitting here to let google adwords to load
   if (typeof(acode) != 'undefined')
   if (acode != null)
   if (acode == '1')
     {
         op_timeout = new Date();
         window.setTimeout('checkIframeLoading()', 0);
         return false;
     }

  document.adminForm.submit();
  return false;
}
// return true if problem
function isNotAShippingMethod()
{
 if (op_noshipping == true) return false;
 var sh = getVShippingRate();
 
 if (sh.toString().indexOf('choose_shipping')>=0)
 {
  return true;
 }
 return false;  
}

function getLabels()
{
		    var labels = document.getElementsByTagName('label');
			for (var i = 0; i < labels.length; i++) {
				if (labels[i].htmlFor != '') {
					var elem = document.getElementById(labels[i].htmlFor);
					if (elem != null && elem.id != null)
					{
						elem.label = labels[i];
						if (labels[i].className != null)
						 {
						   labels[i].className = labels[i].className.replace(/missing/gi, ""); 
						 }
					}
			}
			}
		return null;
}

function validateCC()
{
  var pid = getValueOfSPaymentMethod(); 
  getLabels();
  if (typeof op_cca != 'undefined' && (op_cca != null))
   {
     if (op_cca.indexOf('~'+pid+'~')>=0)
	  {
	    ret = true; 
	    d1 = document.getElementById('order_payment_name'); 
		 if (d1 != null && d1.value == "") 
		  {
		    ret = false;
		    dd1 = document.getElementById('label_order_payment_name'); 
			if (dd1 != null) dd1.style.color = 'red';
		  else
		  {
		    if (typeof d1.label != 'undefined' && (d1.label != null))
			{
			  d1.label.className += ' missing';
			}
		  }
			
		  }
		d2 = document.getElementById('order_payment_number'); 
		
		if (d2 != null)
		{
		r1 = op_isValidCardNumber(d2.value);
		r2 = true;
		dc = document.getElementsByName('creditcard_code'); 
		if (dc != null && dc.length == '1')
		{
		  if (typeof dc.options != 'undefined' && (dc.options != null) && (dc.options.length >= 1) && (typeof dc.selectedIndex != 'undefined') && (dc.selectedIndex != null))
		  cctype = dc.options[dc[0].selectedIndex].value; 
		  if (typeof cctype != 'undefined')
		  {
		   cctype = cctype.toUpperCase(); 
		   r2 = op_isCardTypeCorrect(d2.value, cctype);  
		  }
		}
		
		
		if ((d2.value == "") || (!r1) || (!r2)) 
		{		
			ret = false;
		    dd2 = document.getElementById('label_order_payment_number'); 
			if (dd2 != null) dd2.style.color = 'red';
		    else
		   {
		    if (typeof d2.label != 'undefined' && (d2.label != null))
			   d2.label.className += ' missing';
		   }
			
		}
		}
		
		d3 = document.getElementById('credit_card_code');
		if (d3 != null && d3.value == "") 
		{
			ret = false;
		    dd3 = document.getElementById('label_credit_card_code'); 
			if (dd3 != null) dd3.style.color = 'red';
			else
		    {
		    if (typeof d3.label != 'undefined' && (d3.label != null))
			  d3.label.className += ' missing';
		    }

		 
		}
		return ret;
	  }
   }
  return true; 
  
}

// opc1 double click prevention start

// failsafe function to unblock the button in case of any problems
function unblockButton()
{
  so = document.getElementById('confirmbtn_button'); 
  if (so != null)
  {
   so.disabled = false; 
   //alert('ok');
  }
  else
  {
   so = document.getElementById('confirmbtn');
   if (so != null)
   so.disabled = false;
  }
}
// will disable the submit button so it cannot be pressed twice
function startValidation()
{
   // IE8 and IE7 check: 
  if (window.attachEvent && !window.addEventListener) {
   //return true; 
  }
  else
  opcsubmittimer = setTimeout('unblockButton()', 10000);
  // if any of javascript processes take more than 10 seconds, the button will get unblocked
  // the delay can occur on google ecommerce tracking, OPC tracking or huge DOM, or maybe a long insert query
  //opcsubmittimer = window.setTimeout('unblockButton()', 10000);
  
  // to prevend double clicking, we are using both button and input
  so = document.getElementById('confirmbtn_button'); 
  if (so != null)
  {
   so.disabled = true; 
   //alert('ok');
  }
  else
  {
   so = document.getElementById('confirmbtn');
   if (so != null)
   so.disabled = true;
  }
  
  
}

// submit the form or unblock the button
function endValidation(retVal)
{
  if (!(window.attachEvent && !window.addEventListener)) 
  if (typeof opcsubmittimer != 'undefined')
  if (opcsubmittimer != null)
  clearTimeout(opcsubmittimer); 
  
  if (!retVal)
   {
    // unblock the submit button
  // to prevend double clicking, we are using both button and input
  // confirmbtn_button
  so = document.getElementById('confirmbtn_button'); 
  if (so != null)
  {
   so.disabled = false; 
   //alert('ok');
  }
  else
  {
   so = document.getElementById('confirmbtn');
   if (so != null)
   so.disabled = false;
  }
  // the form will not be sumbmitted
  return false; 
	
   }
  try
  {
  // submit the form by javascript
  document.adminForm.submit();
  return false; 
  }
  catch(e)
  {
   // submit the form by returning true
   return true; 
  }
  
}
// opc1 double click prevention end

function opc_checkUsername(wasValid)
{

	if (!opc_no_duplicit_username) return wasValid; 
	if (!getRegisterAccount()) return wasValid; 
	// there has not been the ajax check yet
	
   if (!last_username_check)
   {
	   if (getRegisterAccount())
	   {
		   
		   return false; 
	   }
	   else 
		   return wasValid; 
	   
	 
   }
   return wasValid; 

}

function opc_checkEmail(wasValid)
{
	if (op_logged_in_joomla) return wasValid; 
	if (!opc_no_duplicit_email) return wasValid; 
	//if (!getRegisterAccount()) return wasValid; 
	// there has not been the ajax check yet
	
	
   if (!last_email_check)
   {
	   
	   return false; 
	   if (getRegisterAccount())
		   return false; 
	   else 
		   return wasValid; 
	   
	 
   }
   return wasValid; 

}



function getRegisterAccount()
{
	// double check in case google has autofill
		var el2 = document.getElementById('register_account'); 
		if (el2 != null)
		{
		
		if (el2.type == 'hidden')
		{
		   if (el2.value == '1') return true; 
           else return false; 		   
		}

		
			 if (el2.checked == true) return true; 
			 else return false; 
		}
		else
		{
			// if we have not register account check if password exists
			d = document.getElementById('opc_password_field'); 
			if (d!=null)
			return true; 
			else
			return false; 
		}
	   el = document.getElementsByName('register_account'); 
	   if (el != null)
	   if (el.length > 0)
	   {
		   if (typeof el.checked != 'undefined')
		   {
			    if (el.checked) return true; 
				if (el.checked == false) return false; 
		
		   }
		    if (typeof el.type != 'undefined')
			{
		    if (el.type != null)
		   	if (el.type == 'hidden')
				{
					if (el.value != '1') return false; 
				}
			}
			else
			{
			if (el[0].type != null)
			{
		   	if (el[0].type == 'hidden')
				{
					if (el[0].value != '1') return false; 
					
				}
			
		   	if (el[0].type == 'checkbox')
				{
					if (el[0].value != '1') return false; 
					
				}
			
				
			}	
			if (el[0].checked != null)
			{
			if (el[0].checked) return true; 
			else return false; 
			}
				
			}
			
				
				
	   }
	   else
	   {
	   		// if we have not register account check if password exists
			d = document.getElementById('opc_password_field'); 
			if (d!=null)
			return true; 
			else
			return false; 

	   }
	   // by default register account
	   return true; 
	   
}
function getBusinessState()
{
	var is_b = false; 
	d = document.getElementById('opc_is_business'); 
	if (d != null)
	{
		if (d.value == 1)
				var is_b = true; 
	}
	
	if (!is_b)
	{
		if ((typeof business_fields != 'undefined') && (business_fields != null))
		for (var i=0; i<business_fields.length; i++)
		{
			d2 = document.getElementById(business_fields[i]+'_field'); 
			if (d2 != null)
			d2.className = d2.className.split('required').join('notequired'); 
		    
		
			d2 = document.getElementById('shipto_'+business_fields[i]+'_field'); 
			if (d2 != null)
			d2.className = d2.className.split('required').join('notequired'); 
		    
		}
	}
    else
	{
		if ((typeof business_fields != 'undefined') && (business_fields != null))
		for (var i=0; i<business_fields.length; i++)
		{
			d2 = document.getElementById(business_fields[i]+'_field'); 
			if (d2 != null)
			d2.className = d2.className.split('notequired').join('required'); 

			d2 = document.getElementById('shipto_'+business_fields[i]+'_field'); 
			if (d2 != null)
			d2.className = d2.className.split('notequired').join('required'); 

		}
	}

	
}

function processPlaceholders()
{
   // support of placeholders and browsers that do not support them
	   var invalid = false; 
	for (var i = 0; i < op_userfields.length; i++) {
       var d = document.getElementsByName(op_userfields[i]); 
	   if (d != null)
	   if (d.length > 0)
	   {
		   //IE8 fix: title = d[0].getAttribute('placeholder'); 
		   var title = getAttr(d[0], 'placeholder'); 
		   
		   if (title != null)
		   if (title != 0)
		   if (d[0].className.indexOf('required')>=0)
		   if (typeof d[0].value != 'undefined')
		   if (d[0].value == title)
		   {
			   invalid = true; 
			   d[0].className = d[0].className.split('invalid').join('').concat(' invalid ');
		   }
	   }
    
	}
	if (invalid) return true; 
	return false; 
}
function emailCheckReg(id)
{
var found = false; 
if (id != null)
{
var em = document.getElementById(id); 
found = true; 
}
else
{
  var dd = document.getElementById('guest_email'); 
  if (dd != null)
  if (dd.value != '')
  {
  var em = dd; 
  found = true; 
  }
}
if (!found)
var em = document.getElementById('email_field'); 
			if (em != null)
			 {
			  var emv = em.value; 
			  var pattern = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])";
				//var pattern =/^[a-zA-Z0-9._-]+(\+[a-zA-Z0-9._-]+)*@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
			  var regExp = new RegExp(pattern,"");
			  if (!regExp.test(emv))
			  {
				 em.className += ' invalid'; 
				 alert(op_email_error);
			     return false; 
			   }
			   else 
			    {
				  em.className = em.className.split('invalid').join(''); 
				  return true; 
				}
			 }
			 return true; 
}

function removePlaceholders()
{

	for (var i = 0; i < op_userfields.length; i++) {
       d = document.getElementsByName(op_userfields[i]); 
	   if (d != null)
	   if (d.length > 0)
	   {
		   //title = d[0].getAttribute('placeholder'); 
		   var title = getAttr(d[0], 'placeholder'); 
		   if (title != null)
		   if (title != 0)
		   if (typeof d[0].value != 'undefined')
		   if (d[0].value == title)
		   {
			   d[0].value=''; 
		   }
	   }
    
	}
}

function validateFormOnePage(wasValid)
{

   if (wasValid != null) 
   {;}
   else wasValid = true; 
   
   if (wasValid) startValidation();
   
   if (op_logged_in != '1')
   if (!opc_checkUsername(wasValid))
   {
	   alert(username_error); 
	   return endValidation(false);
   }
   
   var isGuest = false; 
   var dg = document.getElementById('guest_email'); 
   if (dg != null)
   if (dg.value != '')
   isGuest = true; 
   
   if (!isGuest)
   if (op_logged_in != '1')
   if (!opc_checkEmail(wasValid))
   {
	   alert(email_error); 
	   return endValidation(false);
   }
	
   getBusinessState(); 
	// registration validation
	// var elem = jQuery('#name_field');
	//	elem.attr('class', "required");
    d = document.getElementById('name_field'); 
	if (d != null)
	{
			if (getRegisterAccount())
			{
				d.className += ' required'; 
			}				
			else
			{
				d.className = d.className.split('required').join(''); 				
			}				
	}		
		
	d = document.getElementById('register_account');
	if (!isGuest)
	if (d != null && (typeof d != 'undefined'))
	 {
	    //if ((d.checked) || ((!(d.checked != null)) && d.value=='1'))
		if (getRegisterAccount())
		{
			
		 if (!op_usernameisemail)
		 {
		 // if register account checked, make sure username, pwd1 and pwd2 are required
		 var d2 = document.getElementById('username_field'); 
		 if (d2 != null)
		 {
         d2.className += " required";
		 }
		 }
		 
		 var d2 = document.getElementById('opc_password_field');
		if (d2 != null)
		 {
         d2.classnName += " required";
		 }
		 
		 d2 = document.getElementById('opc_password2_field'); 
		 if (d2 != null)
		  {
            d2.className += ' required'; 
            
		  }
		}
		else
		{
		  if (!op_usernameisemail)
		  {
		// unset required for username, pwd1 and pwd2
		 var d2 = document.getElementById('username_field'); 
		 if (d2 != null)
		 {
		  
		  
          d2.className = d2.className.split('required').join(''); 
		 }
		 }
		   d2 = document.getElementById('opc_password_field');
		if (d2 != null)
		 {
        
         
		 d2.className = d2.className.split('required').join(''); 
		 }
		 var d2 = document.getElementById('opc_password2_field'); 
		 if (d2 != null)
		  {
		    d2.className = d2.className.split('required').join(''); 
		  }
		}
	 }
	 else
	 {
	  {
	   var d2 = document.getElementById('username_field'); 
		 if (d2 != null)
		 {
		 d2.className += ' required'; 
         
		 }
		  var d2 = document.getElementById('password_field');
		if (d2 != null)
		 {
         d2.className += ' required'; 
		}
		 var d2 = document.getElementById('opc_password_field');
		if (d2 != null)
		 {
		   d2.className += ' required'; 
         
		 }
		 var d2 = document.getElementById('opc_password2_field'); 
		 if (d2 != null)
		  {
           
		   d2.className += ' required'; 
		  }
		}
	 }
	 
	  var test = document.createElement('input');
	 
   // before we proceed with validation we have to check placeholders: 
   if (!('placeholder' in test))
   {
	   var isInvalid = processPlaceholders(); 
	 
	if (isInvalid)
	 {
		 alert(op_general_error); 
		 return endValidation(false);
	 }
	 else
	 {
	   removePlaceholders(); 
	 }
   }
	 
	 // passwords dont' match error
	 if (!isGuest)
	 if (getRegisterAccount())
	 {
	 var p = document.getElementById('opc_password_field'); 
	 if ((typeof p != 'undefined') && (p!=null))
	  {
	    var p2 = document.getElementById('opc_password2_field'); 
		if (p2 != null)
		{
		if (p.value != p2.value)
		{
		 alert(op_pwderror); 
		 return endValidation(false);
		}
		}
	  }
	}
	 // op_pwderror
if (isNotAShippingMethod()) 
 {
  alert(shipChangeCountry);
  return endValidation(false);
 }
 var invalid_c = document.getElementById('invalid_country');
 if (invalid_c != null)
 {
  alert (noshiptocmsg);
  return endValidation(false);
 }

 var agreed=document.getElementById('agreed_field');
 if (agreed != null)
 if (agreed.checked != null)
 if (agreed.checked != true) 
 {
  alert(agreedmsg);
  return endValidation(false);
 }


   // calls mootools function
   
    //opc2.0.198
	//if ((op_logged_in != '1') || (shipping_obligatory_fields.length > 0))
	
	
	if (op_logged_in == '1')
	{
	if (shipping_obligatory_fields.length > 0)
	{
	  var dd = document.getElementsByName('ship_to_info_id'); 
	  
	  if (typeof dd.value != 'undefined')
	  if (dd.value != null)
	  if (dd.value == 'new')
	  {
	  wasValid = fastValidation('shipto_', shipping_obligatory_fields, wasValid); 

	  }
	  
	  if (dd.length > 0)
	  if (dd[0] != null)
	  if (typeof dd[0].options != 'undefined')
	  {
	    
		var myval = dd[0].options[dd[0].selectedIndex].value; 
		if (myval == 'new')
		wasValid = fastValidation('shipto_', shipping_obligatory_fields, wasValid); 
		else
		{
		    var dx = document.getElementById('ship_to_info_id_bt'); 
			var bt = ''; 
			if (dx != null)
			{
			  var bt = dx.value; 
			}
			if (myval != bt)
			{
			var d = document.getElementById('opc_st_changed_'+myval);
			if (d!=null)
			if (d.value != null)
			if (d.value == '1')
			 {
			   wasValid = fastValidation('shipto_', shipping_obligatory_fields, wasValid); 
			 }
			}
		}
		
		
	  
	
	  
	  	  
 
	  
	}
	}
	}
	else
	if (shippingOpen())
	{
	  wasValid = fastValidation('shipto_', shipping_obligatory_fields, wasValid); 
	}
	
	if ((op_logged_in != '1'))
	{
	wasValid = fastValidation('', op_userfields, wasValid); 
	if (opc_debug)
	{
	 op_log('fields valid: '); 
	 op_log(wasValid); 
	}
	}
	else
	{
			var dx = document.getElementById('ship_to_info_id_bt'); 
			var bt = ''; 
			if (dx != null)
			{
			  var bt = dx.value; 
			}
			var d = document.getElementById('opc_st_changed_'+bt);
			if (d!=null)
			if (d.value != null)
			if (d.value == '1')
			 {
			   wasValid = fastValidation('', op_userfields, wasValid); 
			 }
		
	}
	if (!wasValid)
	{
	  alert(op_general_error); 
	  return endValidation(false);
	}
 // we need to check email particularly
 if (!emailCheckReg())
 {
 			     return endValidation(false); 
 }
			
			 // need to check state also
			 var em = document.getElementById('virtuemart_state_id'); 
			 if (em != null)
			  {
			  if (em.className.indexOf('required')>=0)
			  {
			    if (em.options != null)
				var val = em.options[em.selectedIndex].value; 
				else
				if (em.value != null)
				var val = em.value
				else 
				var val = ''; 
				
				if ((val == '') || (val == 'none'))
				{
				// we need to check if an empty state value is valid
				
				// country:
				var elopc = document.getElementById('virtuemart_country_id'); 
				 if (elopc.options != null)
				var value = elopc.options[elopc.selectedIndex].value; 
				else
				if (elopc.value != null)
				var value = elopc.value; 

				var dtest = document.getElementById('state_for_'+value);
				 if (!(dtest != null)) 
						{
						  // validation is okay
						   em.className = em.className.split('invalid').join(''); 
						}
						else
						{
						   em.className += ' invalid'; 
						   alert(op_general_error);
						   return endValidation(false); 
						
						}
				}
				}
			  }
			  if (shippingOpen())
			   {
			       em = document.getElementById('shipto_virtuemart_state_id'); 
			 if (em != null)
			  {
			   if (em.className.indexOf('required')>=0)
			   {
			    if (em.options != null)
				var val = em.options[em.selectedIndex].value; 
				else
				if (em.value != null)
				var val = em.value
				else 
				var val = ''; 
				
				if ((val == '') || (val == 'none'))
				{
				// we need to check if an empty state value is valid
				
				// country:
				var elopc = document.getElementById('shipto_virtuemart_country_id'); 
				 if (elopc.options != null)
				var value = elopc.options[elopc.selectedIndex].value; 
				else
				if (elopc.value != null)
				var value = elopc.value; 

				var dtest = document.getElementById('state_for_'+value);
				 if (!(dtest != null)) 
						{
						  // validation is okay
						   em.className = em.className.split('invalid').join(''); 
						}
						else
						{
						   em.className += ' invalid'; 
						   alert(op_general_error);
						   return endValidation(false); 
						
						}
				}
			}
			  }
			   }
			 
 /*
 if (false)
 {
 // we will add this functionality later on
 if (!validateCC())
 {
  if (wasValid)
  alert(op_general_error); 
  return endValidation(false);
 }
 }
 */
 var valid2 = true;
 /*
 if (false)
 if (typeof submitregistration2 == 'function')
 {
   
   valid2 = submitregistration2();
   
   if (!valid2) return endValidation(false);
 }
 */
 // checks extensions functions
 if (callSubmitFunct != null)
 if (callSubmitFunct.length > 0)
 {
   for (var i = 0; i < callSubmitFunct.length; i++)
   {
     if (callSubmitFunct[i] != null)
     {
     
       if (typeof callSubmitFunct[i] == 'string' && 
        eval('typeof ' + callSubmitFunct[i]) == 'function') 
        {
          var valid3 = eval(callSubmitFunct[i]+'(true)'); 
          
          if (valid3 != null)
          if (!valid3) valid2 = false;
        }
     }
   }
 }
 
  //return false;
  
  if (valid2 != true) return endValidation(false);
  if (wasValid != true) return endValidation(false);
  
  //end
   trackGoogleOrder();

  // lets differ submitting here to let google adwords to load
   if (typeof(acode) != 'undefined')
   if (acode != null)
   if (acode == '1')
     {
         op_timeout = new Date();
         window.setTimeout('checkIframeLoading()', 0);
		 // we don't triggere endValidatation as it would unblock the button
		 // the endvalidation is triggered by checkIframeLoading()
         return false;
     }


   
   return endValidation(true); 

  
  
  
}
function toggleVis(obj)
{
 var elopc= document.getElementById(obj);
 if (elopc.style.display != 'none')
 {
  elopc.style.display = 'none';
 
 }
 else
 {
  elopc.style.display = '';
 }
}

function op_get_vat_respons()
{

  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    // here is the response from request
    var resp = xmlhttp.responseText;
    if (resp)
    {
    if (resp=='0')
    {
    document.getElementById(vat_input_id+"_div").className += " missing";
    document.getElementById('validvatid').innerHTML = " Invalid VAT!";
    op_vat_ok = 0;
    }
    else
    {
    if (resp=='1') {    
    op_vat_ok = 1;
    document.getElementById('validvatid').innerHTML = " VAT validated.";
    }
    
    document.getElementById(vat_input_id+"_div").className = "formLabel";
    }    

    return resp;
    }
    else return 2;
    }
    return 2;
}

// validation codes:
// 0 not validated
// 1 validate
// 2 some error (connection, wrong input, elements not found)
function op_check_vat()
{
if ((vat_input_id) && (vat_input_id!=''))
{
var vat_el = document.getElementById(vat_input_id+"_field");
var vatid = "";
if (vat_el)
{
  vatid = vat_el.value;
}
else return 2;
if (vatid=="") return 2;

if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
    xmlhttp.onreadystatechange= op_get_vat_respons ;
    xmlhttp.open("GET","/administrator/components/com_virtuemart/classes/onepage/index.php?vat="+vatid,true);
    xmlhttp.send(null);
    return 1;
}
return 2;
}

function op_replace_select(dest, src)
{
  destel = document.getElementById(dest);
  if (destel != null)
  {
  destel.options.length = 0;
  srcel = document.getElementById(src); 
  if (srcel != null)
  {
  for (var i=0; i<srcel.options.length; i++)
   {
     var oOption = document.createElement("OPTION");
     //o = new Option(srcel.options[i].value, srcel.options[i].text); 
	 oOption.value = srcel.options[i].value; 
	 oOption.text = srcel.options[i].text;
     destel.options.add(oOption);
   }
   }
   else
   {
     var oOption = document.createElement("OPTION");
     //o = new Option(srcel.options[i].value, srcel.options[i].text); 
	 oOption.value = ''; 
	 oOption.text = ' - ';
     destel.options.add(oOption);
    
   }
   }
}

function op_validateCountryOp2(b1 ,b2, elopc)
{
 changeStateList(elopc);
 
 validateCountryOp(false, b2, elopc);
 return "";
}

function changeStateList(elopc)
{
 
 var st = false;
 if (elopc.id != null)
 {
 if (elopc.id.toString().indexOf('shipto_')>-1)
 {
   st = true; 
 }
 }
 else return;
 
 if (elopc.selectedIndex != null)
 {
 }
 else 
 {
 //alert('err'); 
 return;
 }
 
 if (elopc.options != null)
 value = elopc.options[elopc.selectedIndex].value; 
 else
 if (elopc.value != null)
 value = elopc.value; 
 else
 return; 
 
 statefor = 'state_for_'+value; 
  
 
 if (!st)
 {
   
   st2 = document.getElementById('virtuemart_state_id'); 
   if (st2 != null)
    {
	  if (op_lastcountry != value)
	  op_replace_select('virtuemart_state_id', statefor); 
	  op_lastcountry = value; 
	  //st2.options = html;
	 // alert(st2.innerHTML);
	}
 }
 else
 {
  
   st3 = document.getElementById('shipto_virtuemart_state_id'); 
   if (st3 != null)
    {
	    if (op_lastcountryst != value)
		op_replace_select('shipto_virtuemart_state_id', statefor); 
		op_lastcountryst = value; 
	}
 
 }
 
 
 

 
}


function trackGoogleOrder()
{
  
 if (op_run_google == true)
 {
 var c1 = document.getElementById("city_field");
 var city = '';
 if (c1!=null) 
 {
  city = c1.value;
 }
 var c2 = document.getElementById("virtuemart_state_id");
 var state = '';
 if (c2!=null)
 {
  if (c2.selectedIndex != null)
  {
  var w = c2.selectedIndex;
  if (w != null) 
  if (w > -1)
  state = c2.options[w].text; 
  }
  else
  state = c2.value;
 }
 var c3 = document.getElementById("virtuemart_country_id");
 var country = '';
 if (c3 != null)
 {
  if (c3.selectedIndex != null)
  {
  var w = c3.selectedIndex;
  if (w != null) 
  if (w > -1)
  country = c3.options[w].text; 
  }
  else 
  {
  country = c3.value;
  }
 }
 if (state == ' - ') state = '';
 if (state == ' -= Select =- ') state = '';
 if (state == 'none') state = '';
 if (state == '-') state = '';
 // this function is not implemented
 if (!isNaN(parseFloat(op_tax_total)))
 op_tax_total = parseFloat(op_tax_total).toFixed(2);
 try
 {
 
 if (!((typeof pageTracker != 'undefined') && (pageTracker != null)))
 {
   if (typeof _gat != 'undefined')
   {
     pageTracker = _gat._getTrackerByName();
	 window.pageTracker = pageTracker; 
   }
 }
 
 if (typeof(window.pageTracker)=='object')
 {
 
 //alert(g_order_id+" "+op_vendor_name+" "+op_total_total+" "+op_tax_total+" "+op_ship_total+" "+city+" "+state+" "+country);
 if (opc_debug)
 {
   op_log(g_order_id, op_vendor_name, op_total_total, op_tax_total, op_ship_total, city, state, country); 
 }
 pageTracker._addTrans(g_order_id, op_vendor_name, op_total_total, op_tax_total, op_ship_total, city, state, country );
 var ps = document.getElementsByName("prod_id");
 if (ps!=null)
 {
   for (i = 0; i<ps.length; i++)
   {
        var pid = ps[i].value;
        var sku = document.getElementById("prodsku_"+pid);
	var name = document.getElementById("prodname_"+pid);
	var cat = document.getElementById("prodcat_"+pid);
	var qu = document.getElementById("prodq_"+pid);
	var pp = document.getElementById("produprice_"+pid);
	if ((sku!=null) && (name!=null) && (cat!=null) && (qu!=null) && (pp!=null))
	{
	if (opc_debug)
	{
	op_log(g_order_id, sku.value, name.value, cat.value, pp.value, qu.value); 
	}
//	alert (g_order_id+" "+sku.value+" "+name.value+" "+cat.value+" "+pp.value+" "+qu.value);
 	pageTracker._addItem(g_order_id, sku.value, name.value, cat.value, pp.value, qu.value);
 	}
   }
   pageTracker._trackTrans();
 }
 }
 
 else
 {
  // err: op_tax_total

   // pageTracker._addTrans(g_order_id, op_vendor_name, op_total_total, op_tax_total, op_ship_total, city, state, country );
   //alert(g_order_id+" "+op_vendor_name+" "+op_total_total+" "+op_tax_total+" "+op_ship_total+" "+city+" "+state+" "+country);
   //if ((typeof(ga)!='undefined') && (ga.async == true) && (typeof(_gaq)!='undefined') )
   if (window._gat && window._gat._getTracker)
   {
   if (opc_debug)
 {
   op_log(g_order_id, op_vendor_name, op_total_total, op_tax_total, op_ship_total, city, state, country); 
 }
   
	    _gaq.push(['_addTrans',
     g_order_id,           // order ID - required
     op_vendor_name,  // affiliation or store name
     op_total_total,          // total - required
     op_tax_total,           // tax
     op_ship_total,              // shipping
     city,       // city
     state,     // state or province
     country             // country
     ]);
     var ps = document.getElementsByName("prod_id");
 	if (ps!=null)
 	{
   		for (i = 0; i<ps.length; i++)
   		{
        var pid = ps[i].value;
        var sku = document.getElementById("prodsku_"+pid);
		var name = document.getElementById("prodname_"+pid);
		var cat = document.getElementById("prodcat_"+pid);
		var qu = document.getElementById("prodq_"+pid);
		var pp = document.getElementById("produprice_"+pid);

		if ((sku!=null) && (name!=null) && (cat!=null) && (qu!=null) && (pp!=null))
		{
if (opc_debug)
	{
	op_log(g_order_id, sku.value, name.value, cat.value, pp.value, qu.value); 
	}
 		
 		_gaq.push(['_addItem',
    	g_order_id,           // order ID - required
    	sku.value,           // SKU/code - required
    	name.value,        // product name
    	cat.value,   // category or variation
    	pp.value,          // unit price - required
    	qu.value               // quantity - required
  		]);

 		}
   		}
   
 	}
      _gaq.push(['_trackTrans']);
//	alert (g_order_id+" "+sku.value+" "+name.value+" "+cat.value+" "+pp.value+" "+qu.value);
   }
 
 }
 }
 catch (e)
 {
   
 }
 }
 // ok, lets track tracking code here
 //var td = document.getElementById('tracking_div');
 if (typeof(acode) != 'undefined')
 if (acode != null)
 if (acode == '1')
     {
 var tr_id = document.getElementById('tracking_div');
 if (typeof(tr_id) !== 'undefined' && tr_id != null)
 {
 	var html = '<iframe id="trackingIFrame" name="trackingFrame" src="'+op_securl+'?option=com_onepage&nosef=1&task=tracker&view=opc&format=opchtml&tmpl=component&controller=opc&amount='+op_total_total+'" height="50" width="400" frameborder="0"></iframe>';
        tr_id.innerHTML = html;
       
 }
  }
 return true;
}

function changeSemafor()
{
     //alert('semafor changed');
    op_semafor = true;
}

function checkIframeLoading() {
var date = new Date();
if (date - op_timeout > op_maxtimeout) op_semafor = true;
if (op_semafor == true) 
    {
        return endValidation(true);
    }
    window.setTimeout('checkIframeLoading()', 300);
    return true;
}
function formSubmit()
{

    document.adminForm.submit();
    
    return false;

}


/*
* This function is to overwrite submitting of any form which could have been inside main adminform
* syntax: extSubmit('option', 'com_virtuemart', 'task', 'addToCart'... etc
* first the name of hidden input box, second it's value
*/
function extSubmit()
{
 var arguments = extSubmit.arguments;
 var html = '';
 for (var i = 0; i < arguments.length; i = i + 2 )
 {
  if (i+1 < arguments.length)
  {
   document.adminForm.elements[arguments[i]].value = arguments[i+1];
   /*
   dd = document.getElementsByName(arguments[i]);
   if (dd.length > 0)
   {
	 for (var j = 0; j<dd.length; j++)
	 {
	  
	  //dd[j].value = arguments[i+1];
//	  alert(arguments[i+1]);
	 }     
   }
   else
     html += '<input type="hidden" name="'+arguments[i]+'" value="'+arguments[i+1]+'" />';
   */
  }
 }
 document.getElementById('totalam').innerHTML += html;
 //document.adminForm.submit();
 
}

// sets style.display to block for id
// and hide id2, id3, id4... etc... 
function op_unhide(id)
{
 var x = document.getElementById(id);
 if (x != null)
 {
   if (x.style != null) 
    if (x.style.display != null)
      x.style.display = 'block';
 }
 
 for( var i = 1; i < arguments.length; i++ ) {
		
 id2 = arguments[i];
 if (id2 != null)
 {
 x = document.getElementById(id2);
 if (x != null)
 {
   if (x.style != null) 
    if (x.style.display != null)
      x.style.display = 'none';
 }
 }


	}
 
  // if we use it in a href we don't want to click on it, just unhide stuff
  
 return false;
}
// will unhide the first two
function op_unhide2(id, id2)
{
 var x = document.getElementById(id);
 if (x != null)
 {
   if (x.style != null) 
    if (x.style.display != null)
      x.style.display = 'block';
 }
 var x = document.getElementById(id2);
 if (x != null)
 {
   if (x.style != null) 
    if (x.style.display != null)
      x.style.display = 'block';
 }
 
 for( var i = 2; i < arguments.length; i++ ) {
		
 id2 = arguments[i];
 if (id2 != null)
 {
 x = document.getElementById(id2);
 if (x != null)
 {
   if (x.style != null) 
    if (x.style.display != null)
      x.style.display = 'none';
 }
 }


	}
 
  // if we use it in a href we don't want to click on it, just unhide stuff
  
 return false;
}




/*
	Developed by Robert Nyman, http://www.robertnyman.com
	Code/licensing: http://code.google.com/p/getelementsbyclassname/
*/	
var getElementsByClassName = function (className, tag, elm){
	if (document.getElementsByClassName) {
		getElementsByClassName = function (className, tag, elm) {
			elm = elm || document;
			var elements = elm.getElementsByClassName(className),
				nodeName = (tag)? new RegExp("\\b" + tag + "\\b", "i") : null,
				returnElements = [],
				current;
			for(var i=0, il=elements.length; i<il; i+=1){
				current = elements[i];
				if(!nodeName || nodeName.test(current.nodeName)) {
					returnElements.push(current);
				}
			}
			return returnElements;
		};
	}
	else if (document.evaluate) {
		getElementsByClassName = function (className, tag, elm) {
			tag = tag || "*";
			elm = elm || document;
			var classes = className.split(" "),
				classesToCheck = "",
				xhtmlNamespace = "http://www.w3.org/1999/xhtml",
				namespaceResolver = (document.documentElement.namespaceURI === xhtmlNamespace)? xhtmlNamespace : null,
				returnElements = [],
				elements,
				node;
			for(var j=0, jl=classes.length; j<jl; j+=1){
				classesToCheck += "[contains(concat(' ', @class, ' '), ' " + classes[j] + " ')]";
			}
			try	{
				elements = document.evaluate(".//" + tag + classesToCheck, elm, namespaceResolver, 0, null);
			}
			catch (e) {
				elements = document.evaluate(".//" + tag + classesToCheck, elm, null, 0, null);
			}
			while ((node = elements.iterateNext())) {
				returnElements.push(node);
			}
			return returnElements;
		};
	}
	else {
		getElementsByClassName = function (className, tag, elm) {
			tag = tag || "*";
			elm = elm || document;
			var classes = className.split(" "),
				classesToCheck = [],
				elements = (tag === "*" && elm.all)? elm.all : elm.getElementsByTagName(tag),
				current,
				returnElements = [],
				match;
			for(var k=0, kl=classes.length; k<kl; k+=1){
				classesToCheck.push(new RegExp("(^|\\s)" + classes[k] + "(\\s|$)"));
			}
			for(var l=0, ll=elements.length; l<ll; l+=1){
				current = elements[l];
				match = false;
				for(var m=0, ml=classesToCheck.length; m<ml; m+=1){
					match = classesToCheck[m].test(current.className);
					if (!match) {
						break;
					}
				}
				if (match) {
					returnElements.push(current);
				}
			}
			return returnElements;
		};
	}
	return getElementsByClassName(className, tag, elm);
};


/**
* @version		$Id: joomla.javascript.js 10389 2008-06-03 11:27:38Z pasamio $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL
* Joomla! is Free Software
*/

/**
* Writes a dynamically generated list
* @param string The parameters to insert into the <select> tag
* @param array A javascript array of list options in the form [key,value,text]
* @param string The key to display for the initial state of the list
* @param string The original key that was selected
* @param string The original item value that was selected
*/
function op_writeDynaList( selectParams, source, key, orig_key, orig_val ) {
	//if (selectParams.indexOf('credit')>-1) alert('ok');
	var html = '\n	<select ' + selectParams + '>';
	var i = 0;
	for (x in source) {
		if (source[x][0] == key) {
			var selected = '';
			if ((orig_key == key && orig_val == source[x][1]) || (i == 0 && orig_key != key)) {
				selected = 'selected="selected"';
			}
			html += '\n		<option value="'+source[x][1]+'" '+selected+'>'+source[x][2]+'</option>';
		}
		i++;
	}
	html += '\n	</select>';
	
	document.writeln( html );
}

/**
* Changes a dynamically generated list
* @param string The name of the list to change
* @param array A javascript array of list options in the form [key,value,text]
* @param string The key to display
* @param string The original key that was selected
* @param string The original item value that was selected
*/
function op_changeDynaList( listname, source, key, orig_key, orig_val ) {
	var list = eval( 'document.adminForm.' + listname );

	// empty the list
	if (typeof list != 'undefined' && (list != null))
	{;} else {return;}
	if (typeof list.options != 'undefined' && (list.options != null))
	{;} else {return;}
	
	for (i in list.options.length) {
		list.options[i] = null;
	}
	i = 0;
	for (x in source) {
		if (source[x][0] == key) {
			opt = new Option();
			opt.value = source[x][1];
			opt.text = source[x][2];

			if ((orig_key == key && orig_val == opt.value) || i == 0) {
				opt.selected = true;
			}
			list.options[i++] = opt;
		}
	}
	list.length = i;
}

// copyright: http://evolt.org/node/24700
// Submitted by JohnLloydJones on May 5, 2002 - 09:55.

function op_isValidCardNumber (strNum)
{
   var nCheck = 0;
   var nDigit = 0;
   var bEven = false;
   
   for (n = strNum.length - 1; n >= 0; n--)
   {
      var cDigit = strNum.charAt (n);
      if (op_isDigit (cDigit))
      {
         var nDigit = parseInt(cDigit, 10);
         if (bEven)
         {
            if ((nDigit *= 2) > 9)
               nDigit -= 9;
         }
         nCheck += nDigit;
         bEven = ! bEven;
      }
      else if (cDigit != ' ' && cDigit != '.' && cDigit != '-')
      {
         return false;
      }
   }
   return (nCheck % 10) == 0;
}
function op_isDigit (c)
{
   var strAllowed = "1234567890";
   return (strAllowed.indexOf (c) != -1);
}
function op_isCardTypeCorrect (strNum, type)
{
   var nLen = 0;
   for (n = 0; n < strNum.length; n++)
   {
      if (op_isDigit (strNum.substring (n,n+1)))
         ++nLen;
   }
  
   if (type == 'VISA')
      return ((strNum.substring(0,1) == '4') && (nLen == 13 || nLen == 16));
   else if (type == 'AMEX')
      return ((strNum.substring(0,2) == '34' || strNum.substring(0,2) == '37') && (nLen == 15));
   else if (type == 'MC')
      return ((strNum.substring(0,2) == '51' || strNum.substring(0,2) == '52'
              || strNum.substring(0,2) == '53' || strNum.substring(0,2) == '54'
              || strNum.substring(0,2) == '55') && (nLen == 16));
   else if (type == 'DINERS')
      return ((strNum.substring(0,2) == '30' || strNum.substring(0,2) == '36'
				|| strNum.substring(0,2) == '38') && (nLen == 14));
   else if (type == 'DISCOVER')
      return ((strNum.substring(0,4) == '6011' ) && (nLen == 16));
   else if (type == 'JCB')
      return ((strNum.substring(0,4) == '3088' || strNum.substring(0,4) == '3096'
              || strNum.substring(0,4) == '3112' || strNum.substring(0,4) == '3158'
              || strNum.substring(0,4) == '3337' || strNum.substring(0,4) == '3528') && (nLen == 16));

   else
      return true;
	  
	  // stAn mod: this function checks for basic validation, but if type of card is not Visa, Amex or Master Card it still returns true
   
}

function inValidate(el)
{
  el.className = el.className+= ' invalid'; 
  if (opc_debug)
   {
     if (typeof el.name != 'undefined')
	 if (el.name != null)
     op_log(el.name); 
   }
}
function makedValidated(el)
{
  el.className = el.className.split('invalid').join(''); 
}

function checkEmpty(el)
{
  if (el.disabled) return true; 
  if (el.type == 'radio')
  {
    var col = document.getElementsByName(el.name); 
	for (var i = 0; i<col.length; i++)
	 {
	   if (typeof col[i].checked != 'undefined')
	   if (col[i].checked) return false; 
	   if (typeof col[i].checked != 'undefined')
	   if (col[i].selected) return false; 
	 }
	 return true; 
  }
  
  if (el.name.indexOf('virtuemart_state')>=0) return false; 
  
  if (el.type == 'checkbox')
  {
	 if (el.checked) return false; 
	 return true; 
  }

  
  if (typeof el.value != 'undefined')
  if (el.value != null)
  {
   if (el.value == '') return true; 
   placeholder = getAttr(el, 'placeholder');
   if (placeholder != null)
   if (el.value == placeholder) return true;
  }
  if (typeof el.options != 'undefined')
  if (typeof el.selectedIndex != 'undefined')
  {
  if (el.options.length <= 1) return false; 
  if (el.selectedIndex < 0) return true; 
  if (el.options[el.selectedIndex] == '') return true; 
  }
  return false; 
}

function fastValidation(type, fields, valid)
{
  
  if (!(fields != null)) fields = op_userfields; 
  if (type != null)
  {
        op_log('entering validation...'); 
       for (var i=0; i<fields.length; i++)
	    {
		  // filter only needed
		  //if (((op_userfields[i].indexOf('shipto_')!=0) && (!isSh))  || (op_userfields[i].indexOf('shipto_')==0) && (isSh))
		  
		  // special case, shiping fields are not validated by opc: 
		  if ((type == '') && (fields[i].indexOf('shipto_')>=0)) continue; 
		  
		  if ((fields[i].indexOf(type)==0) || (type == '')) var cF = fields[i]; 
		  else var cF = type+fields[i]; 
		  
		  
		 
		   
		   {
		      
		     var elopc = document.getElementsByName(cF); 
			  
			 if (elopc != null)
			 
			 for (var j=0; j<elopc.length; j++)
			  if (elopc[j].className.indexOf('required')>=0)
			  {
			  op_log('Validating: '+elopc[j].name); 
			   switch (elopc[j].type)
			    {
				  case 'password':  break;
				  case 'text':
					if (checkEmpty(elopc[j])) 
					{
					  inValidate(elopc[j]); 
					  valid = false;
					}
					else makedValidated(elopc[j]); 
					break; 
				  case 'select-one': 
				   
				   if (checkEmpty(elopc[j])) 
					{
					  inValidate(elopc[j]); 
					  valid = false;
					}
					else makedValidated(elopc[j]); 
					break; 
				   
				  case 'radio': 
				  if (checkEmpty(elopc[j])) 
					{
					  inValidate(elopc[j]); 
					  valid = false;
					}
					else makedValidated(elopc[j]); 
					break; 

				  case 'hidden': 
				    
					break; 
				 
				  default: 
				   if (checkEmpty(elopc[j])) 
					{
					  inValidate(elopc[j]); 
					  valid = false;
					}
					else makedValidated(elopc[j]); 
					break; 
				}
			  }
			  
			  var elopc = document.getElementsByName(op_userfields[i]+'[]'); 
			  
			  var localtest = false; 
			  var sum = 0; 
			  if (elopc != null)
			  if (elopc.length > 0)
			  {
			   for (var j=0; j<elopc.length; j++)
			    if (elopc[j].className.indexOf('required')>=0)
			    {
				  // at least one from array must be selected
				   if (!checkEmpty(elopc[j])) sum++;
				   
				}
				
				if (elopc != null)
			   for (var j=0; j<elopc.length; j++)
			   if (elopc[j].className.indexOf('required')>=0)
			    if (sum == 0)
				{
				  var divd = document.getElementById(cF+'_div'); 
				  if (divd != null)
				  inValidate(divd); 
			      inValidate(elopc[j]); 
				  valid = false;
				}
				else 
				{
				  var divd = document.getElementById(cF+'_div'); 
				  if (divd != null)
				  makedValidated(divd); 

				  makedValidated(elopc[j]); 
				}
			  }
			  
		   }
		}
  }
  return valid; 
}


function op_openlink(el)
{
  if (el.className.indexOf('modal')>=0) return false; 
  window.open(el.href,'','scrollbars=yes,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
  return false;
}

function op_resizeIframe()
{


if ((typeof parent != 'undefined') && (parent != null))
{
if (typeof parent.resizeIframe != 'undefined')
{
 parent.resizeIframe(document.body.scrollHeight);
}
}
}

function updateProduct(el)
{
  
  //ie8 fix: rel = el.getAttribute('rel');
  rel = getAttr(el, 'rel'); 
  
  if (rel != null)
  if (rel != 0)
  {
  arr = rel.split('|'); 
  cart_id = arr[0]; 
  hash = arr[1]; 
  d = document.getElementById('quantity_for_'+hash); 
  if (d!=null)
  {
	  quantity = d.value; 
  } 
  
  }
  cart_id = op_escape(cart_id); 
  cmd = 'update_product&cart_virtuemart_product_id='+cart_id+'&quantity='+quantity; 
  op_runSS(this, false, true, cmd);
  
  return false; 
}

function setCouponAjax(el)
{
	d = document.getElementById('coupon_code'); 
	if (d != null)
	{
		code = op_escape(d.value); 
		op_runSS(this, false, true, 'process_coupon&new_coupon='+code); 		
	}
	return false; 
}

function deleteProduct(el)
{
  
  //ie8 fix, rel = el.getAttribute('rel');
  rel = getAttr(el, 'rel'); 
  if (rel != null)
  if (rel != 0)
  {
  arr = rel.split('|'); 
  cart_id = arr[0]; 
  hash = arr[1]; 
  
  
  }
  cart_id = op_escape(cart_id); 
  cmd = 'delete_product&cart_virtuemart_product_id='+cart_id; 
  op_runSS(this, false, true, cmd);
  
  return false; 
}

(function($){
	var undefined,
	methods = {
		list: function(options) {
			
		},
		update: function() {
		},
		addToList: function() {
			
		}
	};

	$.fn.vm2frontOPC = function( method ) {
 
	};
})(jQuery)



function op_login()
{
 
 
 
 //alert(op_com_user_task+' '+op_com_user_action+' '+op_com_user); 
 //return false;
 if (document.adminForm.username != null)
 {
 document.adminForm.username.value = document.adminForm.username_login.value;
 uname = document.adminForm.username_login.value;
 }
 else
 {
    var usern = document.createElement('input');
    usern.setAttribute('type', 'hidden');
    usern.setAttribute('name', 'username');
    usern.setAttribute('value', document.getElementById('username_login').value);
	uname = document.getElementById('username_login').value; 
    document.adminForm.appendChild(usern);
 }
 
 pwde = document.getElementById('passwd_login'); 
 if (pwde != null)
 pwd = pwde.value; 
 else
 {
   if (typeof document.adminForm.password != 'undefined')
     {
	   pwd = document.adminForm.password.value; 
	 }
	else
	if (typeof document.adminForm.passwd != 'undefined')
	 {
	   pwd = document.adminForm.passwd.value; 
	 }
	 else 
	 pwd = ''; 
 }
 if ((pwd.split(' ').join() == '') || (uname.split(' ').join()==''))
  {
    alert(op_general_error); 
	return false; 
  }  
 document.getElementById('opc_option').value = op_com_user;
 //document.adminForm.task.value = op_com_user_task;
 document.getElementById('opc_task').value = op_com_user_task;
 
 document.adminForm.action = op_com_user_action;
 document.adminForm.controller.value = 'user'; 
 document.adminForm.view.value = ''; 
 
 document.adminForm.submit();
 return true;
}

function submitenter(el, e)
{
 var charCode;
    
    if(e && e.which){
        charCode = e.which;
    }else if(window.event){
        e = window.event;
        charCode = e.keyCode;
    }


if (charCode == 13)
   {
   op_login();
   return false;
   }
else
   return true;
}



function op_showEditST2()
{
  // edit_address_list_st_section
  // edit_address_st_section
  d1 = document.getElementById('edit_address_list_st_section'); 
  if (d1 != null)
  d1.style.display = 'none'; 
  
  d2 = document.getElementById('edit_address_st_section'); 
  if (d2 != null)
  d2.style.display = 'block'; 
  
  return false; 
}

function op_showEditST(id)
{
   d = document.getElementById('opc_st_'+id); 
   if (d != null)
   d.style.display = 'none'; 
   
   d = document.getElementById('opc_stedit_'+id); 
   if (d != null)
   d.style.display = 'block'; 
   
   d = document.getElementById('opc_st_changed_'+id);
   if (d!=null)
   if (d.value != null)
   d.value = '1'; 
   
   return false; 
   
}

function changeST(el)
{
  if (el.options != null)
  if (el.selectedIndex != null)
   {
      user_info_id = el.options[el.selectedIndex].value; 
	  d = document.getElementById('hidden_st_'+user_info_id); 
	  
	  bt = document.getElementById('ship_to_info_id_bt'); 
	  sa = document.getElementById('sachone');
	  if (bt != null)
	  if (bt.value == user_info_id)
	   {
	     // the selected ship to is bt address
		 
		 sa.value = ''; 
		 sa.setAttribute('checked', false); 
		 eval('sa.checked=false'); 
		
		 
	   }
	   else
	    {
		  sa.value = 'adresaina'; 
		  sa.setAttribute('checked', true); 
		  eval('sa.checked=true'); 
		}
	   
	  
	  
	  if (d != null)
	   {
	     d2 = document.getElementById('edit_address_list_st_section'); 
		 html = d.innerHTML; 
		 html = html.split('REPLACE'+user_info_id+'REPLACE').join(''); 
		 d2.innerHTML = html;
		 
	   }
   }
   op_runSS(el); 
}

function send_special_cmd(el, cmd)
{
  // op_runSS(el, onlyShipping, force, cmd)
  op_runSS(el, false, true, cmd); 
  return false; 
}

function refreshPayment()
{
  op_runSS(null, false, true, 'runpay');
}

function setKlarnaAddress(address)
{
  
  /*
  return array(
            'email'           => $this->getEmail(),
            'telno'           => $this->getTelno(),
            'cellno'          => $this->getCellno(),
            'fname'           => $this->getFirstName(),
            'lname'           => $this->getLastName(),
            'company'         => $this->getCompanyName(),
            'careof'          => $this->getCareof(),
            'street'          => $this->getStreet(),
            'house_number'    => $this->getHouseNumber(),
            'house_extension' => $this->getHouseExt(),
            'zip'             => $this->getZipCode(),
            'city'            => $this->getCity(),
            'country'         => $this->getCountry(),
        );
 */
  if (address != null)
    { ;; } else return;
	
   
  d = document.getElementById('email_field'); 
  if (d != null)
  if (address.email != null)
  if (address.email != '')
  //if (d.value == '')
  d.value = address.email; 

  d = document.getElementById('phone_1_field'); 
  if (d != null)
  if (address.telno != null)
  if (address.telno != '')
  //if (d.value == '')
  d.value = address.telno; 

  d = document.getElementById('first_name_field'); 
  if (d != null)
  if (address.fname != null)
  if (address.fname != '')
  //if (d.value == '')
  d.value = address.fname; 

  d = document.getElementById('company_name_field'); 
  if (d != null)
  if (address.company != null)
  if (address.company != '')
  //if (d.value == '')
  d.value = address.company; 

  d = document.getElementById('last_name_field'); 
  if (d != null)
  if (address.lname != null)
  if (address.lname != '')
  //if (d.value == '')
  d.value = address.lname; 
  
  d = document.getElementById('zip_field'); 
  if (d != null)
  if (address.zip != null)
  if (address.zip != '')
  //if (d.value == '')
  d.value = address.zip; 
  
   d = document.getElementById('city_field'); 
  if (d != null)
  if (address.city != null)
  if (address.city != '')
  //if (d.value == '')
  d.value = address.city; 
  
  d = document.getElementById('address_1_field'); 
  if (d != null)
  if (address.street != null)
  if (address.street != '')
  //if (d.value == '')
  {
   d.value = address.street; 
   if (address.house_number != null)
   if (address.house_number != '')
   d.value += ' '+address.house_number;
   
   if (address.house_extension != null)
   if (address.house_extension != '')
   d.value += ' '+address.house_extension;
  
  }
  
  
  
 
}
// this function is used when you need to get rid of a javascript within opc's themes and you are using $html = str_replace('op_runSS', 'op_doNothing', $html);
// returns false so there is no form submission or action 
// use op_doNothing2 to allow return action such as link redirect or similar
function op_doNothing()
{
  return false; 
}

function op_doNothing2()
{
  return true; 
}

function showFields( show, fields ) {
		   	if( fields ) {
		   		for (i=0; i<fields.length;i++) {
		   			if( show ) {
		   				d = document.getElementById( fields[i] + '_div' );
						if (d != null)
						d.style.display = '';
		   				d = document.getElementById( fields[i] + '_input' );
						if (d != null)
						d.style.display = '';
		   			} else {
		   				d = document.getElementById( fields[i] + '_div' );
						if (d != null)
						d.style.display = 'none';
		   				d = document.getElementById( fields[i] + '_input' );
						if (d!=null)
						d.style.display = 'none';
		   			}
		   		}
		   	}
		   }
		   
function getPaymentElement()
{
		  // get active shipping rate
	  var e = document.getElementsByName("virtuemart_paymentmethod_id");
	  if (!(e != null)) return; 
	  
	  //var e = document.getElementsByName("payment_method_id");
	  
	  
	  var svalue = "";
	 
	  
	  if (e.type == 'select-one')
	  {
	   ind = e.selectedIndex;
	   if (ind<0) ind = 0;
	   value = e.options[ind].value;
	   return e.options[ind];
	  }
	  
	  
	  if (e)
      if (e.checked)
	  {
	    return e; 
	    svalue = e.value;

	  }
	  else
	  {

	  for (i=0;i<e.length;i++)
	  {
	   if (e[i].type == 'select-one')
	  {
	  if (e[i].options.length <= 0) return ""; 
	   ind = e[i].selectedIndex;
	   if (ind<0) ind = 0;
	   value = e[i].options[ind].value;
	   return e[i].options[ind];
	  }
	  
	   if (e[i].checked==true)
	     return e[i];
	  }
	  }
	    
	    
	    // last resort for hidden and not empty values of payment methods:
	   for (i=0;i<e.length;i++)
	   {
	    if (e[i].value != '')
	  {
	    if (e[i].id != null && (e[i].id != 'payment_method_id_coupon'))
	    return e[i];
	  }
	    }
		
	    return null

}		   
function setOpcId()
{
  el = getPaymentElement(); 
  if (!(el!=null)) return; 
  
  d = document.getElementById('opc_payment_method_id'); 
  //ie8 fix: atr = el.getAttribute('id'); 
  atr = getAttr(el, 'id'); 
  if ((atr != null) && (atr != "") && (atr != 0))
    {
	   
	   if (d!=null)
	   d.value = atr; 
	   return;
	}
  if (d!=null)
  d.value = ''; 
  return;

}
function username_check_return(exists)
{
  op_user_name_checked = true; 
  d = document.getElementById('username_already_exists'); 
  if (d != null)
  if (exists)
   {
	 if (opc_no_duplicit_username) last_username_check  = false; 
     d.style.display = 'block'; 
   }
  else
   {
	if (opc_no_duplicit_username) last_username_check  = true; 
    d.style.display = 'none'; 
   }
}
function email_check_return(exists)
{
  op_email_checked = true; 
  d = document.getElementById('email_already_exists'); 
  if (d != null)
   if (exists)
   {
	 if (opc_no_duplicit_email) last_email_check  = false; 
     d.style.display = 'block'; 
   }
  else
   {
	if (opc_no_duplicit_email) last_email_check  = true; 
    d.style.display = 'none'; 
   }

}


function username_check(el)
{
  //username_already_exists
  op_runSS(el, false, true, 'checkusername');
  return true; 
}

function email_check(el)
{
  //email_already_exists
  op_runSS(el, false, true, 'checkemail');
  return true; 
}

//var opcFuncs = {
    function getAttr(ele, attr) {
	   
        var result = (ele.getAttribute && ele.getAttribute(attr)) || null;
        if( !result ) {
            var attrs = ele.attributes;
            var length = attrs.length;
            for(var i = 0; i < length; i++)
                if(attrs[i].nodeName === attr)
                    result = attrs[i].nodeValue;
        }
        return result;
    }
//};




/* support for async loading */
if (typeof jQuery != 'undefined' && (jQuery != null))
			{
			 jQuery(document).ready(function() {

			 if (typeof op_runSS == 'undefined') return;
			  op_runSS('init'); 
 		  		 });
			}
			else
			 {
			   if ((typeof window != 'undefined') && (typeof window.addEvent != 'undefined'))
			   {
			   window.addEvent('domready', function() {
			   
			      op_runSS('init'); 
			    });
			   }
			   else
			   {
			     if(window.addEventListener){ // Mozilla, Netscape, Firefox
			window.addEventListener("load", function(){ op_runSS('init', false, true, null ); }, false);
			 } else { // IE
			window.attachEvent("onload", function(){ op_runSS('init', false, true, null); });
			 }
			   }
			 }