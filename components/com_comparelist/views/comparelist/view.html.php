<?php
defined('_JEXEC') or die ('Restricted access');
jimport('joomla.application.component.view');

class ComparelistViewComparelist extends JView
{
	function display($tpl = null) 
	{

		$app = JFactory::getApplication();
		$pathway = $app->getPathway();
		$pathway->addItem(JText::_('COM_COMPARE_COMPARE_PRODUCT'),JRoute::_('index.php?option=com_comparelist&view=comparelist'));
		
		parent::display($tpl);
	}
}
