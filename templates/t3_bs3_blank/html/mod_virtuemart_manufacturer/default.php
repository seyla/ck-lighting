<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$col= 1 ;
$current_manufacturer_id = JRequest::getInt('virtuemart_manufacturer_id',0);
?>
<div class="vmgroup<?php echo $params->get( 'moduleclass_sfx' ) ?>">

<?php if ($headerText) : ?>
	<div class="vmheader"><?php echo $headerText ?></div>
<?php endif;
if ($display_style =="div") { ?>
	<div class="vmmanufacturer<?php echo $params->get('moduleclass_sfx'); ?>">
	<?php foreach ($manufacturers as $manufacturer) {
		$link = JROUTE::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $manufacturer->virtuemart_manufacturer_id);
		if ($manufacturer->virtuemart_manufacturer_id==$current_manufacturer_id) {
			$active_class = ' class="active"';
		} else {
			$active_class = '';
		}
		?>
		<div style="float:left;"<?php echo $active_class; ?>>
			<a href="<?php echo $link; ?>">
		<?php
		if ($manufacturer->images && ($show == 'image' or $show == 'all' )) { ?>
			<?php echo $manufacturer->images[0]->displayMediaThumb('',false);?>
		<?php
		}
		if ($show == 'text' or $show == 'all' ) { ?>
		 <div><?php echo $manufacturer->mf_name; ?></div>
		<?php
		} ?>
			</a>
		</div>
		<?php
		if ($col == $manufacturers_per_row) {
			echo "</div><div style='clear:both;'>";
			$col= 1 ;
		} else {
			$col++;
		}
	} ?>
	</div>
	<br style='clear:both;' />

<?php
} else {
	$last = count($manufacturers)-1;
?>
<div class="list_carousel_brand responsive">
<ul id="brand_slider" class="vmmanufacturer<?php echo $params->get('moduleclass_sfx'); ?>">
<?php
foreach ($manufacturers as $manufacturer) {
	$link = JROUTE::_('index.php?option=com_virtuemart&view=category&virtuemart_manufacturer_id=' . $manufacturer->virtuemart_manufacturer_id);
	if ($manufacturer->virtuemart_manufacturer_id==$current_manufacturer_id) {
		$active_class = ' class="active"';
	} else {
		$active_class = '';
	}
	?>
	<li<?php echo $active_class; ?>><a href="<?php echo $link; ?>">
		<?php
		if ($manufacturer->images && ($show == 'image' or $show == 'all' )) { 
		 $images = $manufacturer->images;
			$main_image_title = $images[0]->file_title;
			$main_image_alt = $images[0]->file_meta;
			if (!empty($images[0]->file_url_thumb)){
				$main_image_url1 = JURI::root().''.$images[0]->file_url_thumb;
			}else {
				$main_image_url1 = JURI::root().'/images/stories/virtuemart/noimage.gif';
				}
			
			$image = '<img data-original="'.$main_image_url1 .'"  src="modules/mod_virtuemart_manufacturer/js/images/preloader.gif"  title="'.$main_image_title.'"   alt="'.$main_image_alt. '" class="lazy browseProductImage featuredProductImageFirst"/>';
		?>
			<?php echo $image;?>
		<?php
		}
		if ($show == 'text' or $show == 'all' ) { ?>
		 <div><?php echo $manufacturer->mf_name; ?></div>
		<?php
		}
		?>
		</a>
	</li>
	<?php
	if ($col == $manufacturers_per_row && $manufacturers_per_row && $last) {
		echo '</li>';
		$col= 1 ;
	} else {
		$col++;
	}
	$last--;
} ?>
</ul>
<div class="clearfix"></div>
</div>
<?php }
	if ($footerText) : ?>
	<div class="vmfooter<?php echo $params->get( 'moduleclass_sfx' ) ?>">
		 <?php echo $footerText ?>
	</div>
<?php endif; ?>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
if( (jQuery("#t3-mainbody .row .t3-sidebar-left").hasClass("t3-sidebar")) || (jQuery("#t3-mainbody .row .t3-sidebar-right").hasClass("t3-sidebar")) ) {
		jQuery("#brand_slider").owlCarousel({
		items : 5,
		autoPlay : 7000,
		 itemsDesktop : [1000,4], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0
		itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
		stopOnHover : true,
		lazyLoad : false,
		navigation : true,
		 navigationText: [
			"<i class='fa fa-angle-left'></i>",
			"<i class='fa fa-angle-right'></i>"
		]
		}); 
		jQuery("#brand_slider img.lazy").show().lazyload({
			effect : "fadeIn",
			event : "sporty"
		});
	jQuery(window).bind("load", function() {
    var timeout = setTimeout(function() {
        jQuery("#brand_slider img.lazy").trigger("sporty")
    }, 5000);
	});
}else {
			jQuery("#brand_slider").owlCarousel({
		items : 6,
		autoPlay : 7000,
		 itemsDesktop : [1000,4], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0
		itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
		stopOnHover : true,
		lazyLoad : false,
		navigation : true,
		 navigationText: [
			"<i class='fa fa-angle-left'></i>",
			"<i class='fa fa-angle-right'></i>"
		]
		}); 
		jQuery("#brand_slider img.lazy").show().lazyload({
			effect : "fadeIn",
			event : "sporty"
		});
	jQuery(window).bind("load", function() {
    var timeout = setTimeout(function() {
        jQuery("#brand_slider img.lazy").trigger("sporty")
    }, 5000);
	});

	}
	});
    
    </script>
