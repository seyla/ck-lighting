<?php  
if(is_file(JPATH_BASE.DS."components/com_compare/assets/js.php")){
require_once(JPATH_BASE.DS."components/com_compare/assets/js.php");
}?>
<?php // no direct access
error_reporting('E_ALL');
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

	$pwidth= ' width100';
	$float="center";
	if (isset($product->step_order_level))
						$step=$product->step_order_level;
					else
						$step=1;
					if($step==0)
						$step=1;
					$alert=JText::sprintf ('COM_VIRTUEMART_WRONG_AMOUNT_ADDED', $step);
		  $discont = $product->prices[discountAmount];
  $discont = abs($discont);
  		   $show_price = $currency->createPriceDiv('salesPrice', '', $product->prices,true);


	?>
<li class="items">
                       <div class="prod-row">
                       <div class="product-box hover hover back_w  spacer item <?php if ($discont>0) { echo 'disc';} ?> ">
                       <div class="left-img">
                            <div class="browseImage ">
                             <div class="lbl-box">
                             <div class="offafter"></div>
                                        <div class="offbefore"></div>
                                    <div class="limited"><?php echo JText::_('DR_LIMITED_OFFER');?></div>
                                    </div>
                                  <div class="img-wrapper">
                                   <?php
                                            $image = $product->images[0]->displayMediaFull('class="browseProductImage featuredProductImageFirst" id="Img_to_Js_'.$product->virtuemart_product_id.'" border="0"',false) ;
											if(!empty($product->images[1])){
											 $image2 = $product->images[1]->displayMediaFull('class="browseProductImage featuredProductImageSecond"  border="0"',false) ;
											} else {$image2= $product->images[0]->displayMediaFull('class="browseProductImage featuredProductImageSecond"  border="0"',false) ;}
                                            echo JHTML::_('link', JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$product->virtuemart_product_id.'&virtuemart_category_id='.$product->virtuemart_category_id),$image);
                                    ?>
                                    </div>
                                    
                            
                            
                            </div>  
                            </div>      
                            <div class="slide-hover">
                                <div class="wrapper">
                                            <div class="Title">
                                            <?php echo JHTML::link(JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$product->virtuemart_product_id.'&virtuemart_category_id='.$product->virtuemart_category_id), shopFunctionsF::limitStringByWord($product->product_name,'40','...'), array('title' => $product->product_name)); ?>
                                            </div>
                                            <div class="clear"></div>
                                         <?php
										  $showRating = $ratingModel->showRating($product->virtuemart_product_id);
										 if ($showRating=='true'){
                                        $rating = $ratingModel->getRatingByProduct($product->virtuemart_product_id);
                                        if( !empty($rating)) {
                                            $r = $rating->rating;
                                        } else {
                                            $r = 0;
                                        }
                                        $maxrating = VmConfig::get('vm_maximum_rating_scale',5);
                                                        $ratingwidth = ( $r * 100 ) / $maxrating;//I don't use round as percetntage with works perfect, as for me
                                            ?>
                                          <?php  if( !empty($rating)) {  ?>                        
                                                <span class="vote">
                                                    <span title="" class="vmicon ratingbox" style="display:inline-block;">
                                                        <span class="stars-orange" style="width:<?php echo $ratingwidth;?>%">
                                                        </span>
                                                    </span>
                                                    <span class="rating-title"><?php echo JText::_('COM_VIRTUEMART_RATING').' '.round($rating->rating, 2) . '/'. $maxrating; ?></span>
                                                </span>
                                           <?php } else { ?>
                                              <span class="vote">
                                                    <span title="" class="vmicon ratingbox" style="display:inline-block;">
                                                        <span class="stars-orange" style="width:<?php echo $ratingwidth;?>%">
                                                        </span>
                                                    </span>
                                                    <span class="rating-title"><?php echo JText::_('COM_VIRTUEMART_RATING').' '.JText::_('COM_VIRTUEMART_UNRATED') ?></span>
                                               </span>
                                            <?php } } ?>
                                 
                                <div class="clear"></div>
                                <?php // Product Short Description
									if(!empty($product->product_s_desc)) { ?>
									<div class="desc1"><?php echo shopFunctionsF::limitStringByWord($product->product_s_desc, 100, '...') ?></div>
								<?php } ?>
                                </div>
                                <div class="time-box">
                                <div class="indent">
                               
                                 <?php if ((!empty($product->prices['salesPrice'])) && !$product->images[0]->file_is_downloadable) { ?>
                                    <div class="price">
                                     <div class="product-price" id="productPrice<?php echo $product->virtuemart_product_id ?>">
                                    <?php
                                        if ($product->prices['salesPrice']>0) { ?>
                                    <span class="price-sale">
                                    <span class="text"><?php echo JText::_('DR_SPECIAL_DEAL_PRICE'); ?>:</span>
									<?php echo '<span class="sales">' . $currency->createPriceDiv('salesPrice','',$product->prices,true) . '</span>';?>
                                    </span>
                                    <?php } ?>
                                    <?php
                                        if ($product->prices['basePriceWithTax']>0) { ?>
                                    <span class="price-old">
                                    <span class="text"><?php echo JText::_('DR_OLD_PRICE'); ?>:</span>
									<?php echo '<span class="WithoutTax">' . $currency->createPriceDiv('basePriceWithTax','',$product->prices,true) . '</span>';?>
                                    </span>
                                    <?php } ?>
                                    <?php
                                       if ($discont>0)  { ?>
                                    <span class="price_save">
                                     <span class="text"><?php echo JText::_('DR_YOU_ARE_SAVING'); ?>:</span>
									<?php echo '<span class="discount">' . $currency->createPriceDiv('discountAmount','',$product->prices,true) . '</span>'; ?>
                                    </span>
                                    <?php } ?>
                                    <div class="clear" ></div>
                                    </div>
                                    </div>
                                <?php } ?> 
                                <?php
								// sale timer starts 
								include("timer.php");
								//sale timer ends
								?>
                                </div>
                               <div class="bzSaleTimerDesc"><?php echo JText::_('DR_HURRY'); ?><div>&nbsp;&nbsp;<?php echo $product->product_in_stock ?>&nbsp;<?php echo JText::_('DR_HURRY_ITEMS'); ?></div></div>
                                <div class="bzSaleTimerDesc2"><div><?php echo $product->product_ordered ?></div>&nbsp;&nbsp;<?php echo JText::_('DR_BOOKED'); ?> </div>
                                <div class="clear" ></div>
                                </div>
                                    <div class="wrapper-slide">
                                  <?php  if (!empty($show_price)){

                                   if ((!VmConfig::get('use_as_catalog', 0) and !empty($product->prices['salesPrice'])) && !$product->images[0]->file_is_downloadable) { ?>

									<div class="addtocart-area2">
										<?php 
							$stockhandle = VmConfig::get ('stockhandle', 'none');
if (($stockhandle == 'disableit' or $stockhandle == 'disableadd') and (($product->product_in_stock - $product->product_ordered) < 1) || (($product->product_in_stock - $product->product_ordered) < $product->min_order_level ))  {
			 ?>
											<a class="addtocart-button hasTooltip" title="<?php echo JText::_('COM_VIRTUEMART_CART_NOTIFY') ?>" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=productdetails&layout=notify&virtuemart_product_id='.$product->virtuemart_product_id); ?>"><?php echo JText::_('COM_VIRTUEMART_CART_NOTIFY') ?><span></span></a>
										<?php } else { ?>
										<form method="post" class="product" action="index.php" id="addtocartproduct<?php echo $product->virtuemart_product_id ?>">
                                         <input name="quantity" type="hidden" value="<?php echo $step ?>" />
										<div class="addtocart-bar2">
                                        <script type="text/javascript">
													function check(obj) {
													// use the modulus operator '%' to see if there is a remainder
													remainder=obj.value % <?php echo $step?>;
													quantity=obj.value;
													if (remainder  != 0) {
														alert('<?php echo $alert?>!');
														obj.value = quantity-remainder;
														return false;
														}
													return true;
													}
											</script> 
											<?php // Display the quantity box 
											if ((!empty($product->customsChilds)) || (!empty($product->customfieldsCart))) { ?>
											<span class="attributes"><b>*</b> Product has attributes</span>
                                            <div class="addtocart_button2">
											<?php echo JHTML::link($product->link, JText::_('DR_VIRTUEMART_SELECT_OPTION').'<span>&nbsp;</span>', array('title' =>JText::_('DR_VIRTUEMART_SELECT_OPTION'),'class' => 'addtocart-button hasTooltip')); ?>
                                      	  </div>
										
										<?php } else { ?>
										<span class="quantity-box">
											<input type="text" class="quantity-input js-recalculate" name="quantity[]" onblur="check(this);" value="<?php if (isset($product->step_order_level) && (int)$product->step_order_level > 0) {
			echo $product->step_order_level;
		} else if(!empty($product->min_order_level)){
			echo $product->min_order_level;
		}else {
			echo '1';
		} ?>"/>
										</span>
										<span class="quantity-controls">
											<input type="button" class="quantity-controls quantity-plus" />
											<input type="button" class="quantity-controls quantity-minus" />
										</span>
										<?php // Add the button
											$button_lbl = JText::_('COM_VIRTUEMART_CART_ADD_TO');
											$button_cls = 'addtocart-button cart-click hasTooltip'; //$button_cls = 'addtocart_button';
										?>
											<?php // Display the add to cart button ?>
											<div class="clear"></div>
											<span class="addtocart_button2">
												<button type="submit" value="<?php echo $button_lbl ?>" title="<?php echo JText::_('COM_VIRTUEMART_CART_ADD_TO');?>" class="<?php echo $button_cls ?>"><?php echo $button_lbl ?><span>&nbsp;</span></button>
											</span>
                                            
										<input type="hidden" class="pname" value="<?php echo $product->product_name ?>"/>
										<input type="hidden" name="option" value="com_virtuemart" />
										<input type="hidden" name="view" value="cart" />
										<noscript><input type="hidden" name="task" value="add" /></noscript>
                                        <input type="hidden" class="item_id" name="virtuemart_product_id[]" value="<?php echo $product->virtuemart_product_id ?>"/>
										<input type="hidden" name="virtuemart_category_id[]" value="<?php echo $product->virtuemart_category_id ?>" />	
										<?php }?>
									</div>
									</form>
									<?php } ?>
                                    </div>
							<?php } } ?>
                              <?php if(is_file(JPATH_BASE.DS."components/com_wishlists/template/wishlists.tpl.php")){  ?>
                             <div class="wishlist list_wishlists<?php echo $product->virtuemart_product_id;?>">
                                <?php require(JPATH_BASE.DS."components/com_wishlists/template/wishlists.tpl.php"); ?>
                             </div>
                           <?php } ?>       
                             <?php if(is_file(JPATH_BASE.DS."components/com_comparelist/template/comparelist.tpl.php")){  ?>
                             <div class="jClever compare_cat list_compare<?php echo $product->virtuemart_product_id;?>">
                                <?php require(JPATH_BASE.DS."components/com_comparelist/template/comparelist.tpl.php"); ?>
                             </div>
                           <?php } ?> 
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            </div>
                           <div class="clear"></div>                    
					</li>