<?php
/**
* @version		$Id: mod_virtuemart_countdown.php 2011-06-04 14:10:00Cecil Gupta $
* @package		Joomla
* @copyright	Copyright (C) 2011 Cecil Gupta. All rights reserved
* @license		GNU/GPL
*/
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

$document =& JFactory::getDocument();
$document->addStyleSheet("modules/mod_virtuemart_countdown/css/bzTimer.css");
	JHTML::script(JURI::base().'modules/mod_virtuemart_countdown/css/iview.pack.js');
	if(!function_exists("get_time_difference")) {
	function get_time_difference( $start, $end ){
		$uts['start'] = strtotime( $start );
		$uts['end'] = strtotime( $end );
		if( $uts['start']!==-1 && $uts['end']!==-1 ){
			if( $uts['end'] >= $uts['start'] ){
				$diff = $uts['end'] - $uts['start'];
				if( $days=intval((floor($diff/86400))) )$diff = $diff % 86400;
				if( $hours=intval((floor($diff/3600))) )$diff = $diff % 3600;
				if( $minutes=intval((floor($diff/60))) )$diff = $diff % 60;
				$diff = intval( $diff );
				return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
			}
		}else{
			trigger_error( "Invalid date/time data detected", E_USER_WARNING );
			return( false );
		};
		//return(false);
	}
};

	//$theme = (string)$params->get( 'theme', 1 );
	if (isset($_GET['view'])) {$opt_content = $_GET['view'];} else {$opt_content="no_content";}
	$timer_format = $params->get( 'time_format', "hours" );
	$modulesuffix = $params->get('moduleclass_sfx');
	$sale_text = $params->get( 'sale_text', "" );
	$product_id = $params->get( 'product_id', 22 );
	$show_price = (bool)$params->get( 'show_price', 1 );
	$show_addtocart = (bool)$params->get( 'show_addtocart', 1 );
	$debug_mode = (bool)$params->get( 'debug_mode', 0 );
	$component_suffix="";
	
if ($opt_content !== 'productdetails') {
$productArray = preg_split("/[\s,]+/", $product_id);

?>

<div id="slideShow" class="list">
<ul id="slider2">
<?php 

foreach($productArray as $product_index=>$product_ids){
	//print_r ($product_ids);

		$mainframe = Jfactory::getApplication();

		/* Load  VM function */
		if (!class_exists( 'mod_virtuemart_countdown' )) require('helper.php');
		$ratingModel = VmModel::getModel('ratings');
		$productModel = VmModel::getModel('Product');
		$product = $productModel->getProduct($product_ids);
		
		
		$productModel->addImages($product);

		$currency = CurrencyDisplay::getInstance();
		vmJsApi::jQuery();
		if ($show_addtocart) {
			vmJsApi::jPrice();
			vmJsApi::cssSite();
		}
				/* load the template */
		$db =& JFactory::getDBO();
		$query = "SELECT publish_down FROM #__virtuemart_calcs WHERE virtuemart_calc_id = {$product->product_discount_id}";
		$db->setQuery($query);
		$end_date1 = $db->loadResult();
		$query = "SELECT product_price_publish_down FROM #__virtuemart_product_prices WHERE virtuemart_product_id = ".$product_ids;
		$db->setQuery($query);
		$end_date2 = $db->loadResult();
		if(($end_date2 != "0000-00-00 00:00:00") && !is_null($end_date2)){
			$end_date1 = $end_date2;
		}
		$end_date = strtotime($end_date1);
		$end_date = date("m/d/y H:i:s",$end_date);
		//print_r($end_date);
		$today1 = date("m/d/Y H:i:s");
		$remain = get_time_difference($today1, $end_date);


		$product_in_stock = $product->product_in_stock;
		if(!empty($product)) {
			if($product_in_stock){
					if(is_array($remain)){
						require(JModuleHelper::getLayoutPath('mod_virtuemart_countdown'));
					}
			}
		}
}
?>
</ul>
<div class="clear" ></div>
</div>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
			$('#slideShow .hasTooltip').tooltip();
				$("#slideShow #slider2").responsiveSlides({
				  auto: true,             // Boolean: Animate automatically, true or false
				  speed: 800,            // Integer: Speed of the transition, in milliseconds
				  timeout: 12000,          // Integer: Time between slide transitions, in milliseconds
				  pager: true,           // Boolean: Show pager, true or false
				  nav: true,             // Boolean: Show navigation, true or false
				  random: false,          // Boolean: Randomize the order of the slides, true or false
				  pause: true,           // Boolean: Pause on hover, true or false
				  pauseControls: true,    // Boolean: Pause when hovering controls, true or false
				  maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
				  navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
				  manualControls: "",     // Selector: Declare custom pager navigation
				  namespace: "space",   // String: Change the default namespace used
				  before: function(){},   // Function: Before callback
				  after: function(){}     // Function: After callback
				});
				
			});
			jQuery(function(){
				jQuery('#slideShow').addClass('loader');
				jQuery(window).load(function() {
					jQuery('#slideShow').removeClass('loader'); // remove the loader when window gets loaded.
					jQuery('#slideShow.list ul').show();
				});
			});

		</script>
<?php } ?>