<?php

defined('_JEXEC') or die('Restricted access');
require_once JPATH_SITE.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'cart.php';


class CartHelper {
	function __construct() {
		if (!class_exists( 'VmConfig' ))
			require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
		
		if(!class_exists('calculationHelper'))
			require(dirname(__FILE__).DS.'calculationh.php');

		$this->cart = VirtueMartCart::getCart(false);
		JFactory::getLanguage()->load('com_virtuemart');
	}

	public function getPrices() {
		$price	= $this->cart->getCartPrices();
		if(!$price) return null;

		require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';
		$currency_display	= CurrencyDisplay::getInstance();

		foreach($price as $id=>$value) {
			if(!is_array($value)) {
				continue;
			}
			$price_data["products"][$id]["subtotal_tax_amount"]	= $this->getPriceString($price[$id]["subtotal_tax_amount"], $currency_display);
			$price_data["products"][$id]["subtotal_discount"]	= $this->getPriceString($price[$id]["subtotal_discount"], $currency_display);
			if (VmConfig::get('checkout_show_origprice',1)
					&& !empty($this->cart->pricesUnformatted[$id]['basePriceWithTax'])
					&& $this->cart->pricesUnformatted[$id]['basePriceWithTax'] != $this->cart->pricesUnformatted[$id]['salesPrice'] ) {

				$price_data["products"][$id]["subtotal_with_tax"] = '<span class="line-through">'.$currency_display->createPriceDiv('basePriceWithTax','', $this->cart->pricesUnformatted[$id],true,false,$this->cart->products[$id]->quantity).'</span><br />';
			}
			$price_data["products"][$id]["subtotal_with_tax"].= $currency_display->createPriceDiv('salesPrice','', $this->cart->pricesUnformatted[$id],false,false,$this->cart->products[$id]->quantity);
		}

		$price_data["taxAmount"]			= $this->getPriceString($this->cart->pricesUnformatted["taxAmount"], $currency_display);
		$price_data["discountAmount"]		= $this->getPriceString($this->cart->pricesUnformatted["discountAmount"], $currency_display);
		$price_data["salesPrice"]			= $this->getPriceString($this->cart->pricesUnformatted["salesPrice"], $currency_display);
		$price_data["shipmentTax"]			= $this->getPriceString($this->cart->pricesUnformatted["shipmentTax"], $currency_display);
		$price_data["salesPriceShipment"]	= $this->getPriceString($this->cart->pricesUnformatted["salesPriceShipment"], $currency_display);
		$price_data["paymentTax"]			= $this->getPriceString($this->cart->pricesUnformatted["paymentTax"], $currency_display);
		$price_data["salesPricePayment"]	= $this->getPriceString($this->cart->pricesUnformatted["salesPricePayment"], $currency_display);
		$price_data["billTaxAmount"]		= $this->getPriceString($this->cart->pricesUnformatted["billTaxAmount"], $currency_display);
		$price_data["billDiscountAmount"]	= $this->getPriceString($this->cart->pricesUnformatted["billDiscountAmount"], $currency_display);
		$price_data["billTotal"]			= $this->getPriceString($this->cart->pricesUnformatted["billTotal"], $currency_display);

		return $price_data;
	}

	public function lSelectShipment() {
		$found_shipment_method			= false;
		$shipment_not_found_text 		= JText::_('COM_VIRTUEMART_CART_NO_SHIPPING_METHOD_PUBLIC');
		$this->shipment_not_found_text	= $shipment_not_found_text;

		$shipments_shipment_rates=array();
		if (!$this->checkShipmentMethodsConfigured()) {
			$this->shipments_shipment_rates=$shipments_shipment_rates;
			$this->found_shipment_method=$found_shipment_method;
			return;
		}
		$selectedShipment = (empty($this->cart->virtuemart_shipmentmethod_id) ? 0 : $this->cart->virtuemart_shipmentmethod_id);

		$shipments_shipment_rates = array();
		if (!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');
		JPluginHelper::importPlugin('vmshipment');
		$dispatcher = JDispatcher::getInstance();
		$this->cart->getCartPrices();
		if(empty($this->cart->BT["zip"])) {
		    $this->cart->BT=array();
		    $this->cart->BT["zip"]=" ";
		}
		if(is_array($this->cart->ST) && empty($this->cart->ST["zip"])) {
		    $this->cart->ST["zip"]=" ";
		}
		$returnValues = $dispatcher->trigger('plgVmDisplayListFEShipment', array( $this->cart, $selectedShipment, &$shipments_shipment_rates));
		// if no shipment rate defined
		$found_shipment_method = false;
		
		foreach ($returnValues as $returnValue) {
			if($returnValue){
				$found_shipment_method = true;
				break;
			}
		}
		$shipment_not_found_text = JText::_('COM_VIRTUEMART_CART_NO_SHIPPING_METHOD_PUBLIC');

		$shipments=array();
		foreach($shipments_shipment_rates as $items) {
			if(is_array($items)) {
				foreach($items as $item) {
					$shipments[]=$item;
				}
			} else {
				$shipments[]=$items;
			}
		}
		
		//$checked_item = JRequest::getVar('virtuemart_shipmentmethod_id', 0, 'COOKIE');
		foreach ($shipments as $k=>&$item) {
			if($k == $checked_item) {
				$item = str_replace('<input type="radio"', '<input type="radio" checked="checked" ', $item);
			} else {
				continue;
			}
		}
		
		//var_dump(JRequest::getVar('virtuemart_shipmentmethod_id', false, 'COOKIE'));
		
		$this->shipment_not_found_text=$shipment_not_found_text;
		$this->shipments_shipment_rates=$shipments;
		$this->found_shipment_method=$found_shipment_method;
		return;
	}

	public function lSelectPayment() {
		$found_payment_method=false;
		$payment_not_found_text='';
		$payments_payment_rates=array();
		if (!$this->checkPaymentMethodsConfigured()) {
			$this->paymentplugins_payments=$payments_payment_rates;
			$this->found_payment_method=$found_payment_method;
		}

		$selectedPayment = empty($this->cart->virtuemart_paymentmethod_id) ? 0 : $this->cart->virtuemart_paymentmethod_id;

		$paymentplugins_payments = array();
		if(!class_exists('vmPSPlugin'))
			require_once(JPATH_VM_PLUGINS.DS.'vmpsplugin.php');

		JPluginHelper::importPlugin('vmpayment');
		$dispatcher = JDispatcher::getInstance();
		$this->cart->getCartPrices();
		$returnValues = $dispatcher->trigger('plgVmDisplayListFEPayment', array($this->cart, $selectedPayment, &$paymentplugins_payments));
		// if no payment defined
		$found_payment_method = false;
		foreach ($returnValues as $returnValue) {
			if($returnValue){
				$found_payment_method = true;
				break;
			}
		}

		if (!$found_payment_method) {
			$link=''; // todo
			$payment_not_found_text = JText::sprintf('COM_VIRTUEMART_CART_NO_PAYMENT_METHOD_PUBLIC', '<a href="'.$link.'">'.$link.'</a>');
		}

		$payments=array();
		foreach($paymentplugins_payments as $items) {
			if(is_array($items)) {
				foreach($items as $item) {
					$payments[]=$item;
				}
			} else {
				$payments[]=$items;
			}
		}
		$this->payment_not_found_text  = $payment_not_found_text;
		$this->paymentplugins_payments = $payments;
		$this->found_payment_method    = $found_payment_method;
	}

	private function checkPaymentMethodsConfigured() {

		//For the selection of the payment method we need the total amount to pay.
		$paymentModel = VmModel::getModel('Paymentmethod');
		$payments = $paymentModel->getPayments(true, false);
		if (empty($payments)) {

			$text = '';
			if (!class_exists('Permissions'))
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'permissions.php');
			if (Permissions::getInstance()->check("admin,storeadmin")) {
				$uri = JFactory::getURI();
				$link = $uri->root() . 'administrator/index.php?option=com_virtuemart&view=paymentmethod';
				$text = JText::sprintf('COM_VIRTUEMART_NO_PAYMENT_METHODS_CONFIGURED_LINK', '<a href="' . $link . '">' . $link . '</a>');
			}

			vmInfo('COM_VIRTUEMART_NO_PAYMENT_METHODS_CONFIGURED', $text);

			$tmp = 0;
			$this->found_payment_method = $tmp;

			return false;
		}
		return true;
	}

	private function checkShipmentMethodsConfigured() {

		//For the selection of the shipment method we need the total amount to pay.
		$shipmentModel = VmModel::getModel('Shipmentmethod');
		$shipments = $shipmentModel->getShipments();
		if (empty($shipments)) {

			$text = '';
			if (!class_exists('Permissions'))
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'permissions.php');
			if (Permissions::getInstance()->check("admin,storeadmin")) {
				$uri = JFactory::getURI();
				$link = $uri->root() . 'administrator/index.php?option=com_virtuemart&view=shipmentmethod';
				$text = JText::sprintf('COM_VIRTUEMART_NO_SHIPPING_METHODS_CONFIGURED_LINK', '<a href="' . $link . '">' . $link . '</a>');
			}

			vmInfo('COM_VIRTUEMART_NO_SHIPPING_METHODS_CONFIGURED', $text);

			$tmp = 0;
			$this->found_shipment_method=$tmp;

			return false;
		}
		return true;
	}

	function setPayment() {
		if(!class_exists('vmPSPlugin'))
			require_once(JPATH_VM_PLUGINS.DS.'vmpsplugin.php');

		JPluginHelper::importPlugin('vmpayment');
		$this->cart->setPaymentMethod(JRequest::getInt('virtuemart_paymentmethod_id'));

		$msg		= "";
		$dispatcher = JDispatcher::getInstance();
		$rets 		= $dispatcher->trigger('plgVmOnSelectCheckPayment',array($this->cart,&$msg));

		if(JRequest::getCmd('api_ajax')!='set_checkout') {
			$db=JFactory::getDBO();
			$query=$db->getQuery(true);
			$query->select('payment_element');
			$query->from('#__virtuemart_paymentmethods');
			$query->where('virtuemart_paymentmethod_id='.JRequest::getInt('virtuemart_paymentmethod_id'));
			$db->setQuery($query);
			$method=$db->loadResult();

			if($method=='authorizenet') {
				$this->cart->setCartIntoSession();
				return true;
			}
		}

		foreach($rets as $ret) {
			if($ret===false) {
				$msgs=JFactory::getApplication()->getMessageQueue();
				$messages=array();
				foreach($msgs as $msg) {
					$messages[]=str_replace("<br/>","\n",$msg["message"]);
				}

				return $messages;
			}
		}
		$this->cart->setCartIntoSession();
		return true;
	}

	function setShipment() {
		if(!class_exists('vmPSPlugin'))
			require_once(JPATH_VM_PLUGINS.DS.'vmpsplugin.php');

		JPluginHelper::importPlugin('vmshipment');
		$this->cart->setShipment(JRequest::getInt('virtuemart_shipmentmethod_id'));

		$dispatcher = JDispatcher::getInstance();
		$rets = $dispatcher->trigger('plgVmOnSelectCheckShipment',array(&$this->cart));
		foreach($rets as $ret) {
			if($ret===false) {
				$msgs=JFactory::getApplication()->getMessageQueue();
				$messages=array();
				foreach($msgs as $msg) {
					$messages[]=str_replace("<br/>","\n",$msg["message"]);
				}
				return $messages;
			}
		}
		$this->cart->setCartIntoSession();
		return true;
	}

	function setAddress() {
		$post		= JRequest::get('post');
		if(JRequest::getInt('STsameAsBT')=='1') {
			$this->cart->STsameAsBT=1;
			$this->cart->ST=0;
			$this->cart->setCartIntoSession();
		} else {
			$this->cart->STsameAsBT=0;
			if(!strlen($post['shipto_address_type_name'])) {
				$post['shipto_address_type_name']='ST';
			}
		}

		if($post['tosAccepted']) {
			$post['agreed']=1;
		} else {
			$post['agreed']=0;
		}
		if($this->cart->STsameAsBT==1) {
			$this->cart->saveAddressInCart($post,'BT');
		} else {
			$this->cart->saveAddressInCart($post,'ST');
			$this->cart->saveAddressInCart($post,'BT');
		}
	}

	function register() {

		
		//JRequest::setVar(JUtility::getToken(),1);
		$post		= JRequest::get('post');
		$user		= VmModel::getModel('user');
		$user->_id	= 0;
		$ret=$user->store($post,false);
		$lang 	= JFactory::getLanguage();
		
		//load plugin and theme language files
		$lang->load('com_virtuemart_shoppers'); // fixed in VM 2.0.22+
		$lang->load('plg_system_onestepcheckout',JPATH_ADMINISTRATOR);
		$lang->load('plg_system_onestepcheckout_theme',JPATH_ADMINISTRATOR); // added in ver 3.0.x
		
		if(!isset($ret["success"]) || $ret["success"]==false || $ret==false) {
			
			// Store addresses
			$post['address_type'] = 'BT';
			$user->store($post);
			$post['address_type'] = 'ST';
			$user->storeAddress($post);
			
			$messages=array();
			foreach(JFactory::getApplication()->getMessageQueue() as $message) {
				$messages[]=$message["message"];
			}
			return array('error'=>1,'message'=>implode(" ",$messages));
		}

		$messages=array();
		foreach(JFactory::getApplication()->getMessageQueue() as $message) {
			$messages[]=$message["message"];
		}
		return $ret;
	}

	function updateProduct() {
		$this->cart->updateProductCart(JRequest::getString('id'));
	}

	function removeProduct() {
		$this->cart->removeProductCart(JRequest::getString('id'));
	}

	function setCoupon() {
		if(!class_exists('VirtueMartCart'))
			require(JPATH_VM_SITE.DS.'helpers'.DS.'cart.php');
		$cart = $this->cart;

		if (!class_exists('CouponHelper')) {
			require(JPATH_VM_SITE . DS . 'helpers' . DS . 'coupon.php');
		}
		
		$prices = $this->getCartPrices();
		$coupon_code = JRequest::getString('coupon');
		$msg = CouponHelper::ValidateCouponCode($coupon_code , $prices['salesPrice']);
		if($msg=='') {
			$cart->couponCode = $coupon_code;
			$cart->setCartIntoSession();
			$msg = JText::_('COM_VIRTUEMART_CART_COUPON_VALID');
			$success = 1;
		} else {
			$cart->couponCode = '';
			$cart->setCartIntoSession();
			$success = 0;
		}
		unset($cart);
		$cart = VirtueMartCart::getCart(false);
		$prices = $this->getCartPrices();
		
		require_once
			JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'currencydisplay.php';

		$currency_display	=	CurrencyDisplay::getInstance();
		$price_data			=	array();

		$price_data["couponTax"]			= $this->getPriceString($prices["couponTax"], $currency_display);
		$price_data["salesPriceCoupon"]		= $this->getPriceString($prices["salesPriceCoupon"], $currency_display);
		$price_data["billTaxAmount"]		= $this->getPriceString($prices["billTaxAmount"], $currency_display);
		$price_data["billDiscountAmount"]	= $this->getPriceString($prices["billDiscountAmount"], $currency_display);
		$price_data["billTotal"]			= $this->getPriceString($prices["billTotal"], $currency_display);
		return $price_data;
	}

	private function getPriceString($value,$currencyDisplay){
		return !empty($value)?$currencyDisplay->priceDisplay($value):"";
	}

	function assignValues() {
		$new	= false;
		//New Address is filled here with the data of the cart (we are in the cart)
		if (!class_exists('VirtueMartCart'))
			require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
		$cart = VirtueMartCart::getCart();
		$userModel = VmModel::getModel('user');
		$user = $userModel->getCurrentUser();
		$address_count = 0;
		$address_arr= array();
		foreach ($user->userInfo as $address) {
			if ($address->address_type == 'BT') {
				$cart->saveAddressInCart((array) $address, $address->address_type,true);
			}
			if ($address->address_type == 'ST') {
				//$cart->saveAddressInCart((array) $address, $address->address_type,true);
				$address_arr[] = (array)$address;
				$address_count++;
			}
		}
		$ST_lastAddress = $address_arr[0];
		
		if($cart->ST) foreach ($cart->ST as $key=> &$fld ) {
			if(isset($ST_lastAddress[$key])) {
				$fld = $ST_lastAddress[$key];
			}
		}

		$cart->prepareAddressDataInCart("BT",$new);
		$this->BTaddress=$cart->BTaddress;
		
		$cart->prepareAddressDataInCart("ST",$new);
		$this->STaddress=$cart->STaddress;
		
		$this->lSelectShipment();
		$this->lSelectPayment();
	}
	function renderCountryList ($countryId = 0, $multiple = FALSE, $_attrib = array(), $_prefix = '', $required = 0) {
	
		$countryModel = VmModel::getModel ('country');
		$countries = $countryModel->getCountries (TRUE, TRUE, FALSE);
		
		$attrs = array();
		$name = 'country_name';
		$id = 'virtuemart_country_id';
		$idA = $_prefix . 'virtuemart_country_id';
		$attrs['class'] = 'virtuemart_country_id';
		$attrs['class'] = 'vm-chzn-select';
		// Load helpers and  languages files
		if (!class_exists( 'VmConfig' )) require(JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'config.php');
		VmConfig::loadConfig();
		VmConfig::loadJLang('com_virtuemart_countries');
		vmJsApi::chosenDropDowns();
	
		$sorted_countries = array();
		$lang = JFactory::getLanguage();
		$prefix="COM_VIRTUEMART_COUNTRY_";
		foreach ($countries as  $country) {
			$country_string = $lang->hasKey($prefix.$country->country_3_code) ?   JText::_($prefix.$country->country_3_code)  : $country->country_name;
			$sorted_countries[$country->virtuemart_country_id] = $country_string;
		}
	
		asort($sorted_countries);
	
		$countries_list=array();
		$options = array();
		$options[] = '<option value="" data-iso="" selected="selected">'.JText::_ ('COM_VIRTUEMART_LIST_EMPTY_OPTION').'</option>';
		$i=0;
		
		foreach ($sorted_countries as  $key=>$value) {
			$countries_list[$i] = new stdClass();
			$countries_list[$i]->$id = $key;
			$countries_list[$i]->$name = $value;
			foreach($countries as  $k=>$country ) {
				if($key == $country->virtuemart_country_id) {
					$countries_list[$i]->country_2_code = $country->country_2_code;
				}
			}
			$options[] = '<option value="'.$key.'" data-iso="'.$countries_list[$i]->country_2_code.'"'.($countryId == $countries_list[$i]->$id ?' selected="selected"':'').'>'.$value.'</option>';
			$i++;
		}
		
	
		$ret =  '<select id="'.$id.'" name="'.$id.'" class="vm-chzn-select required">'.implode('', $options).'</select>';
 		//echo($ret);
		return $ret;
	}
	public function emptyCart(){
		$this->cart->emptyCart();
		exit('done');
	}
	public function getCartPrices($checkAutomaticSelected=true) {
	
		if(!class_exists('calculationHelper')) require(dirname(__FILE__).DS.'calculationh.php');
		$calculator = calculationHelper::getInstance();
	
		$this->cart->pricesUnformatted = $calculator->getCheckoutPrices($this->cart, $checkAutomaticSelected);
	
		return $this->cart->pricesUnformatted;
	}
	public function pre_checkout() {
		//Use false to prevent valid boolean to get deleted
		$post = JRequest::get("GET");
		if(JRequest::getInt('STsameAsBT')=='1') {
			$this->cart->STsameAsBT=1;
			$this->cart->ST=0;
			$this->cart->setCartIntoSession();
		} else {
			$this->cart->STsameAsBT=0;
			if(!strlen($post['shipto_address_type_name'])) {
				$post['shipto_address_type_name']='ST';
			}
		}
		
		if($post['tosAccepted']) {
			$post['agreed']=1;
		} else {
			$post['agreed']=0;
		}
		if($this->cart->STsameAsBT==1) {
			$this->cart->saveAddressInCart($post,'BT');
		} else {
			$this->cart->saveAddressInCart($post,'ST');
			$this->cart->saveAddressInCart($post,'BT');
		}
		JFile::write(JPATH_ROOT.DS.'post.html', print_r($post,1));
		return $this->cart->STsameAsBT;
	}
	
}
?>
