<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$userFields = array('agreed','name','username','password','password2');

$shortFields = array('shipto_address_type_name','shipto_email','shipto_first_name','shipto_last_name','shipto_company','shipto_city','shipto_zip','shipto_virtuemart_country_id','shipto_virtuemart_state_id');
$noRenderTypes = array('delimiter');
$noRenderTitle = array('shipto_address_2');
?>
<input id="STsameAsBT" type="checkbox"/><label id="STsameAsBTLeb"  for="STsameAsBT"><?php echo JText::_('COM_VIRTUEMART_USER_FORM_ST_SAME_AS_BT');?></label>
<div class="shipping_info" id="div_shipto">
	<div class="pane round-box">
		<div class="pane-inner" id="table_shipto">
			<ul class="adminform user-details no-border" id="table_shippingto" >
			<?php foreach($this->helper->STaddress["fields"] as $_field):?>
			<?php
				if(in_array($_field['name'],$userFields) || in_array($_field['type'], $noRenderTypes)) {
					continue;
				}
			?>
				<li class="<?php echo in_array($_field['name'], $shortFields)?'short':'long';?>">
					<div class="field-wrapper">
					<?php if(!in_array($_field['name'], $noRenderTitle)):?>
					<label class="<?php echo $_field['name']?>" for="<?php echo $_field['name']?>_field">
						<?php echo $_field['title']. ($_field['required'] ? '<em>*</em>' : '')?>
					</label>
					<br>
					<?php endif;?>
					<?php
					if($_field['name']=='shipto_zip') {
				    	$_field['formcode']=str_replace('input','input onchange="nx.checkout.update_form();"',$_field['formcode']);
				    } else if($_field['name']=='shipto_virtuemart_country_id') {
				    	$_field['formcode']=str_replace('<select','<select onchange="nx.checkout.update_form();"',$_field['formcode']);
				    	$_field['formcode']=str_replace('class="virtuemart_country_id','class="shipto_virtuemart_country_id',$_field['formcode']);
				    } else if($_field['name']=='shipto_virtuemart_state_id') {
				    	$_field['formcode']=str_replace('id="virtuemart_state_id"','id="shipto_virtuemart_state_id"',$_field['formcode']);
				    	$_field['formcode']=str_replace('<select','<select onchange="nx.checkout.update_form();"',$_field['formcode']);
				    }
					echo $_field['formcode']."\n";
					?>
				</li>

			<?php endforeach;?>
			</ul>

		</div>
	</div>
</div>