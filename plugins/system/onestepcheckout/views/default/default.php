<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$document = &JFactory::getDocument();
//load common vars
$app 	= JFactory::getApplication('site');
$doc 	= JFactory::getDocument();
$user 	= JFactory::getUser();
$input	= $app->input;
$lang 	= JFactory::getLanguage();

//load plugin and theme language files
$lang->load('com_virtuemart_shoppers'); // fixed in VM 2.0.22+
$lang->load('plg_system_onestepcheckout',JPATH_ADMINISTRATOR);
$lang->load('plg_system_onestepcheckout_theme',JPATH_ADMINISTRATOR); // added in ver 3.0.x

//load js translate text
JText::script('SYSTEM_ONESTEPCHECKOUT_TOS_CONFIRM');
JText::script('SYSTEM_ONESTEPCHECKOUT_SHIPMENT_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_PAYMENT_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_COUNTRY_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_SHIPTO_COUNTRY_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_SHIPTO_STATE_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_FORM_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_COUPON_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_DATA_INVALID');
JText::script('COM_VIRTUEMART_LIST_EMPTY_OPTION');
JText::script('SYSTEM_ONESTEPCHECKOUT_INFO_INVALID');

// process checkout form
if(!$this->cart->products) { //empty cart
	$shopfront_link = JROUTE::_('index.php?option=com_virtuemart&view=virtuemart');
?>

<div class="cart-view" id="cart-view-3cols" data-layout="">
	<div class="cart-title">
	    <h3 class="module-title check-marg"><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_CART_TITLE_EMPTY');?></h3>
	</div>
	<div class="cart-empty">
        <?php echo JText::sprintf('SYSTEM_ONESTEPCHECKOUT_CART_TITLE_EMPTY_DESC',$shopfront_link);?>
    </div>
</div>
<?php
} else { // has products

// load plugin params
$plugin			= JPluginHelper::getPlugin('system','onestepcheckout');
$params			= new JRegistry($plugin->params);
$color_skin  = $params->get('preset_colors','blue');

//load helpers
if(!class_exists('CartHelper'))
	require_once JPATH_SITE.DS.'plugins/system/onestepcheckout/helpers/cart.php';
$this->helper	= new CartHelper();
$this->helper->assignValues();

// load javascripts
// Add JavaScript Frameworks
JHtml::_('jquery.framework');
JHtml::_('behavior.formvalidation');
vmJsApi::jPrice();
//JHtmlJquery::libs(array('cookies.2.2.0','onepagecheckout-1cols'));
 $document->addScript(JURI::base().'plugins/system/onestepcheckout/assets/js/jquery.cookies.2.2.0.min.js');
  $document->addScript(JURI::base().'plugins/system/onestepcheckout/assets/js/jquery.onepagecheckout-1cols.js');
//load css theme
JHTML::stylesheet('facebox.css', 'components/com_virtuemart/assets/css/', false);
//JHTML::stylesheet('style-'.$color_skin.'.css', 'plugins/system/onestepcheckout/assets/themes/default/css/', false);
$document->addStyleDeclaration('#facebox .content {display: block !important; height: 480px !important; overflow: auto; width: 560px !important; }');
$document->addStyleDeclaration('#system-message-container {display: none;}');
// set js vars
$keys = array(
			'show_tax' 						=> VmConfig::get('show_tax'),
			'agree_to_tos_onorder' 			=> VmConfig::get('agree_to_tos_onorder'),
			'oncheckout_show_legal_info' 	=> VmConfig::get('oncheckout_show_legal_info'),
			'use_as_catalog' 				=> VmConfig::get('use_as_catalog'),
			'checkout_show_origprice' 		=> VmConfig::get('checkout_show_origprice'),
			'coupons_enable' 				=> VmConfig::get('coupons_enable'),
			'oncheckout_show_register' 		=> VmConfig::get('oncheckout_show_register'),
			'loader_image_link'				=> 'plugins/system/onestepcheckout/assets/images/loader.gif',
			'set_coupon_link'				=> 'index.php?api_controller=checkout&api_ajax=set_coupon',
			'add_countries_link'			=> 'index.php?option=com_virtuemart&view=state&format=json&virtuemart_country_id=',
			'token'							=> JUtility::getToken(),
			'show_delivery_time'			=> $params->get('show_delivery_time'),
			'geoip'							=> $params->get('geoip')
		);
$config = json_encode($keys);

?>
<script type="text/javascript">
var vmconfig = <?php echo !empty($config) ? $config:'{}';?>;
</script>
<?php
$use_geoip = $params->get('geoip');
if($use_geoip){?>
	<script src="//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js" type="text/javascript"></script>
<?php }?>
<?php if($params->get('show_ajax_loading',1)):?>
<style>
#general-ajax-load {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #CCCCCC;
    display: none;
    padding: 20px;
    position: fixed;
    top: 0;
    left: 0;
}
</style>
<div id="general-ajax-load">
	Loading
</div>
<?php endif;?>
<!-- begin : checkout page  -->
<div id="cart-view-default" data-layout="700px">
<div class="cart-view">
        <h3 class="module-title"><span><span><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_TITLE'); ?></span></span></h3>
  <div class="login-box">
    
		<div class="width50 floatleft right">
			<?php // Continue Shopping Button
			if ($this->continue_link_html != '') {?>
                <div class="back-to-category right-link">
                    <a style="display:inline-block;" href="<?php echo $this->continue_link ?>" class="button_back button reset2"><i class="fa fa-reply"></i><?php echo JText::sprintf('DR_CONT_SHOP') ?></a>
                </div>
			<?php } ?>
		</div>
	<div class="clear"></div>

	<?php echo shopFunctionsF::getLoginForm($this->cart,false);
	if ($this->checkout_task) $taskRoute = '&task='.$this->checkout_task;
	else $taskRoute ='';
	?>
 </div>
 </div>
 <div class="billing-box">
<form method="post" id="checkoutForm" name="checkoutForm" action="<?php echo JRoute::_( 'index.php?option=com_virtuemart&view=cart'.$taskRoute,$this->useXHTML,$this->useSSL ); ?>">
	<?php
	echo $this->loadTemplate('pricelist');
	?>
	<div class="module-4eck cart-view bill">
	<?php // Leave A Comment Field ?>
	<div class="customer-comment marginbottom15">
		<span class="comment"><?php echo JText::_('COM_VIRTUEMART_COMMENT_CART'); ?></span><br />
		<textarea class="customer-comment" name="customer_comment" cols="50" rows="4"><?php echo $this->cart->customer_comment; ?></textarea>
	</div>
    <table
		class="cart-summary bot-total"
		cellspacing="0"
		cellpadding="0"
		border="0"
		width="100%">

	<tr class="shipment-pane">
    			<td colspan="3" align="right"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL_SHIPMENT') ?>: </td>
				<?php
                if ( VmConfig::get('show_tax')) { ?>
				<td colspan="3" align="right"><?php echo "<span  class='priceColor2' id='shipment_tax'>".$this->currencyDisplay->createPriceDiv('shipmentTax','', $this->cart->pricesUnformatted['shipmentTax'],false)."</span>"; ?> </td>
                <?php } ?>
				<td colspan="3"></td>
				<td colspan="3" align="right" id="shipment"><?php echo $this->currencyDisplay->createPriceDiv('salesPriceShipment','', $this->cart->pricesUnformatted['salesPriceShipment'],false); ?> </td>
		</tr>

		<tr class="payment-pane">
				<td  colspan="3" align="right"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL_PAYMENT') ?>: </td>
                <?php if ( VmConfig::get('show_tax')) { ?>
				<td  colspan="3" align="right"><?php echo "<span  class='priceColor2' id='payment_tax'>".$this->currencyDisplay->createPriceDiv('paymentTax','', $this->cart->pricesUnformatted['paymentTax'],false)."</span>"; ?> </td>
                <?php } ?>
				<td colspan="3" align="right"><?php //echo "<span  class='priceColor2'>".$this->cart->prices['paymentDiscount']."</span>"; ?></td>
				<td colspan="3" align="right" id="payment"><?php  echo $this->currencyDisplay->createPriceDiv('salesPricePayment','', $this->cart->pricesUnformatted['salesPricePayment'],false); ?> </td>
			</tr>
		  <tr class="sectiontableentry2">
			<td class="bold" colspan="3" align="right"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL') ?>: </td>

                        <?php if ( VmConfig::get('show_tax')) { ?>
			<td class="red-bold" colspan="3" align="right"> <?php echo "<span  class='priceColor2' id='total_tax'>".$this->currencyDisplay->createPriceDiv('billTaxAmount','', $this->cart->pricesUnformatted['billTaxAmount'],false)."</span>" ?> </td>
                        <?php } ?>
			<td class="red-bold" colspan="3" align="right"> <?php echo "<span  class='priceColor2' id='total_amount'>".$this->currencyDisplay->createPriceDiv('billDiscountAmount','', $this->cart->pricesUnformatted['billDiscountAmount'],false)."</span>" ?> </td>
			<td class="red-bold" colspan="3" align="right"><strong id="bill_total"><?php echo $this->currencyDisplay->createPriceDiv('billTotal','', $this->cart->pricesUnformatted['billTotal'],false) ?></strong></td>
		  </tr>
		    <?php
		    if ( $this->totalInPaymentCurrency) {
			?>

		       <tr class="sectiontableentry2">
					    <td colspan="3" align="right"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL_PAYMENT') ?>: </td>

					    <?php if ( VmConfig::get('show_tax')) { ?>
					    <td colspan="3" align="right">  </td>
					    <?php } ?>
					    <td colspan="3" align="right">  </td>
					    <td colspan="3" align="right"><strong><?php echo $this->currencyDisplay->createPriceDiv('totalInPaymentCurrency','', $this->totalInPaymentCurrency,false); ?></strong></td>
				      </tr>
				      <?php
		    }
		    ?>

	</table>
	<?php // Leave A Comment Field END ?>

		<?php // Continue and Checkout Button ?>
		<div class="checkout-button-top">

			<?php // Terms Of Service Checkbox
			if (!class_exists('VirtueMartModelUserfields')){
				require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'userfields.php');
			}
			$userFieldsModel = VmModel::getModel('userfields');
			if($userFieldsModel->getIfRequired('agreed')){
			    ?>
			    <label for ="tosAccepted">
			    <?php
				if(!class_exists('VmHtml'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'html.php');
				echo VmHtml::checkbox('tosAccepted',$this->cart->tosAccepted,1,0,'class="terms-of-service"');

				if(VmConfig::get('oncheckout_show_legal_info',1)){
				?>
				<div class="terms-of-service">
					<span class="terms-of-service" rel="facebox"><span class="vmicon vm2-termsofservice-icon"></span><?php echo JText::_('COM_VIRTUEMART_CART_TOS_READ_AND_ACCEPTED'); ?><span class="vm2-modallink"></span></span>
					<div id="full-tos" style="display: none">
						<h2><?php echo JText::_('COM_VIRTUEMART_CART_TOS'); ?></h2>
						<?php echo $this->cart->vendor->vendor_terms_of_service;?>

					</div>
				</div>
				<?php } ?>
			    </label></br>
		    <?php
			}

			//echo $this->checkout_link_html;
			if (!VmConfig::get('use_as_catalog')) {
				echo '<a class="vm-button-correct" href="javascript:void(0);"><span>' . JText::_('COM_VIRTUEMART_ORDER_CONFIRM_MNU') . '</span></a>';
			}
			$text = JText::_('COM_VIRTUEMART_ORDER_CONFIRM_MNU');
			?>
		</div>
        </div>
		<?php // Continue and Checkout Button END ?>

		<input type='hidden' name='task' value='confirm'/>
		<input type='hidden' name='option' value='com_virtuemart'/>
		<input type='hidden' name='view' value='cart'/>

</form>
</div>
</div>
<!-- end : checkout page  -->
<?php }?>