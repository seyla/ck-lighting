<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$userFields = array('agreed','name','username','password','password2');

$shortFields = array('email','first_name','last_name','company','city','zip','virtuemart_country_id','virtuemart_state_id','name','username');
$noRenderTypes = array('delimiter');
$noRenderTitle = array('address_2');

?>
<div class="billing_info" id="div_billto">
	<div class="pane round-box">
		<div class="pane-inner">

			<ul class="adminform user-details no-border" id="table_billto">
			<?php foreach($this->helper->BTaddress["fields"] as $_field):?>
			<?php
				if(in_array($_field['name'],$userFields) || in_array($_field['type'], $noRenderTypes)) {
					continue;
				}
			?>
				<li class="<?php echo in_array($_field['name'], $shortFields)?'short':'long';?>">
					<div class="field-wrapper">
					<?php if(!in_array($_field['name'], $noRenderTitle)):?>
					<label class="<?php echo $_field['name']?>" for="<?php echo $_field['name']?>_field">
						<?php echo $_field['title']. ($_field['required'] ? '<em>*</em>' : '')?>
					</label>
					<?php endif;?>
					<?php
					if($_field['name']=='zip') {
						$_field['formcode']=str_replace('input','input onchange="nx.checkout.update_form();"',$_field['formcode']);
					} else if($_field['name']=='virtuemart_country_id') {
						$_field['formcode'] = $this->helper->renderCountryList();
						$_field['formcode']= str_replace('<select','<select onchange="nx.checkout.update_form();"',$_field['formcode']);
					} else if($_field['name']=='virtuemart_state_id') {
						$_field['formcode']=str_replace('<select','<select onchange="nx.checkout.update_form();"',$_field['formcode']);
					}
					echo $_field['formcode']."\n";
					?>
					</div>
				</li>

			<?php endforeach;?>
			</ul>
			<ul id="user-actions-trigger">
				<?php if(!JFactory::getUser()->id):?>
				<li>
					<input id="register" type="checkbox"/><label id="registerLeb" for="register"><?php echo JText::_('COM_VIRTUEMART_REGISTER'); ?></label>
					<ul id="user-register-fields">
					<?php foreach($this->helper->BTaddress["fields"] as $_field):?>
					<?php
						if(!in_array($_field['name'],$userFields) || in_array($_field['type'], $noRenderTypes)) {
							continue;
						}
					?>
						<li class="<?php echo in_array($_field['name'], $shortFields)?'short':'long';?>">
							<div class="field-wrapper <?php echo $_field['name']?>">
								<?php if(!in_array($_field['name'], $noRenderTitle)):?>
								<label class="<?php echo $_field['name']?>" for="<?php echo $_field['name']?>_field">
								<?php echo $_field['title']?> <em>*</em>
								</label>
								<?php if($_field['name'] != 'agreed'):?>
								<br>
								<?php endif;?>
								<?php endif;?>
								<?php
								echo $_field['formcode']."\n";
								?>
							</div>
						</li>
					<?php endforeach;?>
					</ul>
				</li>
				<?php endif;?>
			</ul>

		</div>
	</div>
</div>