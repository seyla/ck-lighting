<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<div class="payment-pane">
	<div class="pane round-box">
		<h3 class="title">
			<span class="icon icon-four"></span>
			<?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_PAYMENT_METHOD')?>
		</h3>
		<div class="pane-inner">
		<?php
		echo JText::_('COM_VIRTUEMART_CART_SELECTPAYMENT');
		if(!empty($this->layoutName) && $this->layoutName=='default') {
			echo "<fieldset id='payments'>";
				$c = 1;
				foreach($this->helper->paymentplugins_payments as $payments) {
					echo str_replace('type="radio"','type="radio"',$payments)."<br />";
				}
			echo "</fieldset>";
		} else {
			JText::_('COM_VIRTUEMART_CART_PAYMENT');
		}
		?>
		</div>
	</div>
</div>
