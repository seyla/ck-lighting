<?php defined('_JEXEC') or die('Restricted access');
// Check to ensure this file is included in Joomla!
$plugin=JPluginHelper::getPlugin('system','onestepcheckout');
$params=new JRegistry($plugin->params);
$cols = 3;
?>
<fieldset id="cart-contents" class="round-box">
<h3 class="title"><span class="icon icon-checked"></span><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_ORDER_REVIEWS')?></h3>
	<table
		class="cart-summary no-border"
		cellspacing="0"
		cellpadding="0"
		border="0"
		width="100%">
		<tr>
			<th align="left" class="th-name"><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_NAME') ?></th>
			<th
				align="left"
				width="50px" class="th-price"><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_PRICE') ?></th>
			<th
				align="left"
				width="110px" class="th-quanlity"><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_QUANTITY') ?></th>

            <?php if ( VmConfig::get('show_tax')) { ?>
                <th align=""left"" width="60px" class="th-tax"><?php  echo "<span  class='priceColor2'>".JText::_('COM_VIRTUEMART_CART_SUBTOTAL_TAX_AMOUNT') ?></th>
			<?php } ?>
            <th align="left" width="60px" class="th-discount"><?php echo "<span  class='priceColor2'>".JText::_('COM_VIRTUEMART_CART_SUBTOTAL_DISCOUNT_AMOUNT') ?></th>
			<th align="left" width="60px" class="th-total th-last"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL') ?></th>
		</tr>

		<?php
		$i=1;
		$c = 1;
		foreach( $this->cart->products as $pkey =>$prow ) {
			?>
			<tr valign="top"
				class="product-detail-row <?php echo $c==count($this->cart->products)? "product-detail-last-row ":""?>sectiontableentry<?php echo $i ?>"
				id="product_row_<?php echo $pkey; ?>">
				<td align="left" >
					<?php if ( $prow->virtuemart_media_id) {  ?>
						<span class="cart-images">
						 <?php
						 if(!empty($prow->image)){
							echo $prow->image->displayMediaThumb('',false);
						}else {
							$quantity = 1;
							$product_model = VmModel::getModel('product');
							$product_model->addImages($prow);
							if($prow->images[0]) {
								echo $prow->images[0]->displayMediaThumb('',false);
							}
						}
						 ?>
						</span>
					<?php } ?>
					<?php echo JHTML::link($prow->url, $prow->product_name,array("class"=>"product-name"));?>
					<br>
					<?php if(!empty($prow->product_sku)):?>
					<span class="product-sku"><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_SKU').": "; echo $prow->product_sku; ?></span>
					<?php endif;?>
					<?php echo$prow->customfields; ?>

				</td>
				<td align="left" class="base-price">
				<?php
					echo $this->currencyDisplay->createPriceDiv('basePrice','', $this->cart->pricesUnformatted[$pkey],false);
					?>
				</td>
				<td align="left" class="product-quanlity" >
				<input type="text"
						title="<?php echo  JText::_('COM_VIRTUEMART_CART_UPDATE') ?>"
						class="inputbox quanlity-inputbox"
						size="3"
						maxlength="4"
						value="<?php echo $prow->quantity ?>" id='quantity_<?php echo $pkey; ?>'/>

				<input type="button"
						class="icon-update vm2-add_quantity_cart"
						name="update"
						title="<?php echo  JText::_('COM_VIRTUEMART_CART_UPDATE') ?>"
						align="middle"
						data-pid ="<?php echo $pkey; ?>"
						/>

				<a class="icon-remove vm2-remove_from_cart"
					name="remove"
					title="<?php echo JText::_('COM_VIRTUEMART_CART_DELETE') ?>"
					align="middle"
					href="javascript:void(0)"
					data-pid ="<?php echo $pkey; ?>"
					> </a>

				</td>

				<?php if ( VmConfig::get('show_tax')) { ?>
				<td align="left">
				<?php echo "<span  class='priceColor2' id='subtotal_tax_amount_".$pkey."'>".$this->currencyDisplay->createPriceDiv('taxAmount','', $this->cart->pricesUnformatted[$pkey],false,false,$prow->quantity)."</span>" ?></td>
                <?php } ?>

				<td align="left" class="product-discount">
					<?php echo "<span  class='priceColor2' id='subtotal_discount_".$pkey."'>".$this->currencyDisplay->createPriceDiv('discountAmount','', $this->cart->pricesUnformatted[$pkey],false,false,$prow->quantity)."</span>" ?></td>

				<td class="sub-total td-last" colspan="0" align="right" id="subtotal_with_tax_<?php echo $pkey; ?>">
				<?php
				if (VmConfig::get('checkout_show_origprice',1)
						&& !empty($this->cart->pricesUnformatted[$pkey]['basePriceWithTax'])
						&& $this->cart->pricesUnformatted[$pkey]['basePriceWithTax'] != $this->cart->pricesUnformatted[$pkey]['salesPrice'] ) {
	              echo '<span class="line-through">'.$this->currencyDisplay->createPriceDiv('basePriceWithTax','', $this->cart->pricesUnformatted[$pkey],true,false,$prow->quantity) .'</span><br />' ;
    		    }
				echo $this->currencyDisplay->createPriceDiv('salesPrice','', $this->cart->pricesUnformatted[$pkey],false,false,$prow->quantity);
				?>
				</td>
			</tr>
		<?php
			$i = 1 ? 2 : 1;
			$c++;
		} ?>
		<!--Begin of SubTotal, Tax, Shipment, Coupon Discount and Total listing -->
          <?php if ( VmConfig::get('show_tax')) { $colspan=2; } else { $colspan=1; } ?>

		  <tr class="price-result-1">
			<td colspan="3" align="right">
				<?php echo JText::_('COM_VIRTUEMART_ORDER_PRINT_PRODUCT_PRICES_TOTAL'); ?></td>

            <?php if ( VmConfig::get('show_tax')) { ?>
			<td align="left">
				<?php echo "<span  class='priceColor2' id='tax_amount'>".$this->currencyDisplay->createPriceDiv('taxAmount','', $this->cart->pricesUnformatted,false)."</span>" ?></td>
            <?php } ?>

			<td align="left">
				<?php echo "<span  class='priceColor2' id='discount_amount'>".$this->currencyDisplay->createPriceDiv('discountAmount','', $this->cart->pricesUnformatted,false)."</span>" ?></td>
			<td align="right" id="sales_price">
				<?php echo $this->currencyDisplay->createPriceDiv('salesPrice','', $this->cart->pricesUnformatted,false) ?></td>

		  </tr>

		<?php
		foreach($this->cart->cartData['DBTaxRulesBill'] as $rule){ ?>
			<tr class="sectiontableentry<?php $i ?>">
				<td colspan="4" align="right"><?php echo $rule['calc_name'] ?> </td>

                                   <?php if ( VmConfig::get('show_tax')) { ?>
				<td align="right"> </td>
                                <?php } ?>
				<td align="right"> <?php echo $this->currencyDisplay->createPriceDiv($rule['virtuemart_calc_id'].'Diff','', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'].'Diff'],false);  ?></td>
				<td align="right"><?php echo $this->currencyDisplay->createPriceDiv($rule['virtuemart_calc_id'].'Diff','', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'].'Diff'],false);   ?> </td>
			</tr>
			<?php
			if($i) $i=1; else $i=0;
		} ?>

		<?php

		foreach($this->cart->cartData['taxRulesBill'] as $rule){ ?>
			<tr class="sectiontableentry<?php $i ?>">
				<td colspan="4" align="right"><?php echo $rule['calc_name'] ?> </td>
				<?php if ( VmConfig::get('show_tax')) { ?>
				<td align="right"><?php echo $this->currencyDisplay->createPriceDiv($rule['virtuemart_calc_id'].'Diff','', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'].'Diff'],false); ?> </td>
				 <?php } ?>
				<td align="right"><?php    ?> </td>
				<td align="right"><?php echo $this->currencyDisplay->createPriceDiv($rule['virtuemart_calc_id'].'Diff','', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'].'Diff'],false);   ?> </td>
			</tr>
			<?php
			if($i) $i=1; else $i=0;
		}

		foreach($this->cart->cartData['DATaxRulesBill'] as $rule){ ?>
			<tr class="sectiontableentry<?php $i ?>">
				<td colspan="4" align="right"><?php echo   $rule['calc_name'] ?> </td>

                                     <?php if ( VmConfig::get('show_tax')) { ?>
				<td align="right"> </td>

                                <?php } ?>
				<td align="right"><?php  echo $this->currencyDisplay->createPriceDiv($rule['virtuemart_calc_id'].'Diff','', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'].'Diff'],false);   ?>  </td>
				<td align="right"><?php echo $this->currencyDisplay->createPriceDiv($rule['virtuemart_calc_id'].'Diff','', $this->cart->pricesUnformatted[$rule['virtuemart_calc_id'].'Diff'],false);   ?> </td>
			</tr>
			<?php
			if($i) $i=1; else $i=0;
		} ?>



		  <tr class="sectiontableentry2">
			<td colspan="3" align="right"><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_SHIPPING_COST') ?>: </td>

			<td colspan="3" align="right"><?php echo "<span  class='priceColor2' id='shipment'>".$this->currencyDisplay->createPriceDiv('salesPriceShipment','', $this->cart->pricesUnformatted['shipmentTax'],false)."</span>"; ?></td>
		  </tr>

		  <tr class="sectiontableentry2" id="cart_total_row">
			<td colspan="3" align="right"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL') ?>: </td>

                        <?php if ( VmConfig::get('show_tax')) { ?>
			<td align="right"> <?php echo "<span  class='priceColor2' id='total_tax'>".$this->currencyDisplay->createPriceDiv('billTaxAmount','', $this->cart->pricesUnformatted['billTaxAmount'],false)."</span>" ?> </td>
                        <?php } ?>
			<td align="right"> <?php echo "<span  class='priceColor2' id='total_amount'>".$this->currencyDisplay->createPriceDiv('billDiscountAmount','', $this->cart->pricesUnformatted['billDiscountAmount'],false)."</span>" ?> </td>
			<td align="right"><strong id="bill_total"><?php echo $this->currencyDisplay->createPriceDiv('billTotal','', $this->cart->pricesUnformatted['billTotal'],false) ?></strong></td>
		  </tr>
		    <?php
		    if ( $this->totalInPaymentCurrency) {
			?>

		       <tr class="sectiontableentry2" id="InPaymentCurrency" style="display: none;">
					    <td colspan="3" align="right"><?php echo JText::_('COM_VIRTUEMART_CART_TOTAL_PAYMENT') ?>: </td>

					    <?php if ( VmConfig::get('show_tax')) { ?>
					    <td align="right">  </td>
					    <?php } ?>
					    <td align="right">  </td>
					    <td align="right"><strong><?php echo $this->currencyDisplay->createPriceDiv('totalInPaymentCurrency','', $this->totalInPaymentCurrency,false); ?></strong></td>
				      </tr>
				      <?php
		    }
		    ?>
			<?php
		if (VmConfig::get('coupons_enable')) {
		?>
			<tr class="coupon-pane">
				<td colspan="3" align="left" class="border-radius-lb">
				    <?php if(!empty($this->layoutName) && $this->layoutName=='default') {
					    echo $this->loadTemplate('coupon');
				    }
				?>
				 <?php
					echo "<span id='coupon_code_txt'>".@$this->cart->cartData['couponCode']."</span>";
					echo @$this->cart->cartData['couponDescr'] ? (' (' . $this->cart->cartData['couponDescr'] . ')' ): '';
				 ?>
				</td>
					<?php if ( VmConfig::get('show_tax')) { ?>
					<td align="right" id="coupon_tax">
						<?php echo $this->currencyDisplay->createPriceDiv('couponTax','', @$this->cart->pricesUnformatted['couponTax'],false); ?> </td>
					 <?php } ?>
					<td align="right">&nbsp;</td>
					<td align="right" id="coupon_price" class="border-radius-rb">
						<?php echo $this->currencyDisplay->createPriceDiv('salesPriceCoupon','', @$this->cart->pricesUnformatted['salesPriceCoupon'],false); ?> </td>
			</tr>
		<?php } ?>

	</table>
</fieldset>

