<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<div class="delivery-time-pane">
	<div class="pane round-box">
		<h3 class="title">
			<span class="icon icon-five"></span>
			<?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_DELIVERY_TIME')?>
		</h3>
		<div class="pane-inner">
		<ul>
			<li><input type="radio" name="delivery_time_type" checked="checked"><label>As Soon As Possible</label></li>
			<!-- 
			<li><input type="radio" name="delivery_time_type"><label>Select Preferred Time</label></li>
			 -->
		</ul>
		</div>
	</div>
</div>