<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<div class="reset">
	<form id="user-registration"
		action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>"
		method="post"
		class="form-validate">
		<p><?php echo JText::_('COM_USERS_REMIND_DEFAULT_LABEL')?></p>
			<fieldset>
			<label id="jform_email-lbl" for="jform_email" class="hasTip required" title=""><?php echo JText::_('COM_USERS_FIELD_REMIND_EMAIL_LABEL')?><em class="star">&nbsp;*</em></label>
			<input required="required" name="jform[email]" class="validate-email required" id="jform_email" value="" size="30" type="email">
			<div style="display:none" id="advice-required-entry-login-email" class="validation-advice">This is a required field.</div>
			</fieldset>
		<div class="clr"></div>
		<div class="gray-line"></div>
		<div class="reset-action">
			<em>* <?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_REQUIRED_FIELDS')?></em>
			<button class="btn-back"><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_BACK'); ?></button>
			<button type="submit" class="validate"><?php echo JText::_('JSUBMIT'); ?></button>
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>
