<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

$document = &JFactory::getDocument();
//load common vars
$app 	= JFactory::getApplication('site');
$doc 	= JFactory::getDocument();
$user 	= JFactory::getUser();
$input	= $app->input;
$lang 	= JFactory::getLanguage();

//load plugin and theme language files
VmConfig::loadJLang('com_virtuemart_shoppers',TRUE);
$lang->load('plg_system_onestepcheckout',JPATH_ADMINISTRATOR);
$lang->load('plg_system_onestepcheckout_theme',JPATH_ADMINISTRATOR); // added in ver 3.0.x

//load js translate text
JText::script('SYSTEM_ONESTEPCHECKOUT_TOS_CONFIRM');
JText::script('SYSTEM_ONESTEPCHECKOUT_SHIPMENT_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_PAYMENT_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_COUNTRY_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_SHIPTO_COUNTRY_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_SHIPTO_STATE_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_FORM_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_COUPON_INVALID');
JText::script('SYSTEM_ONESTEPCHECKOUT_DATA_INVALID');
JText::script('COM_VIRTUEMART_LIST_EMPTY_OPTION');
JText::script('SYSTEM_ONESTEPCHECKOUT_INFO_INVALID');

?>
<?php if(!$this->cart->products):?>
<?php
$shopfront_link = JROUTE::_('index.php?option=com_virtuemart&view=virtuemart');
?>
<div class="cart-view" id="cart-view-3cols" data-layout="">
	<div class="cart-title">
	    <h1><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_CART_TITLE_EMPTY');?></h1>
	</div>
	<div class="cart-empty">
        <?php echo JText::sprintf('SYSTEM_ONESTEPCHECKOUT_CART_TITLE_EMPTY_DESC',$shopfront_link);?>
    </div>
</div>

<?php else:?>
<?php
JHtml::_('behavior.formvalidation');
$document = &JFactory::getDocument();
$plugin			= JPluginHelper::getPlugin('system','onestepcheckout');
$params			= new JRegistry($plugin->params);
$color_skin  = $params->get('preset_colors','blue');

if (!class_exists( 'VmConfig' ))
	require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');

if(!class_exists('CartHelper'))
	require_once JPATH_SITE.DS.'plugins/system/onestepcheckout/helpers/cart.php';

vmJsApi::jPrice();

JHtml::_('jquery.framework');
JHtmlJquery::ui(array('all'));
JHtmlJquery::libs(array('cookies.2.2.0','onepagecheckout-3cols'));

vmJsApi::css('jquery-ui', 'plugins/system/onestepcheckout/assets/themes/3cols/ui/onepagecheckout-'.$color_skin.'-theme', '', false);

JHTML::stylesheet('style.css', 'plugins/system/onestepcheckout/assets/themes/3cols/css/', false);
JHTML::stylesheet('style-'.$color_skin.'.css', 'plugins/system/onestepcheckout/assets/themes/3cols/css/', false);

$document->addStyleDeclaration('#system-message-container {display: none;}');
?>
<?php
$use_geoip = $params->get('geoip');
if($use_geoip){?>
	<script src="//j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js" type="text/javascript"></script>
<?php }?>
<?php
$this->helper	= new CartHelper();
$this->helper->assignValues();

$keys = array(
		'show_tax' 						=> VmConfig::get('show_tax'),
		'agree_to_tos_onorder' 			=> VmConfig::get('agree_to_tos_onorder'),
		'oncheckout_show_legal_info' 	=> VmConfig::get('oncheckout_show_legal_info'),
		'use_as_catalog' 				=> VmConfig::get('use_as_catalog'),
		'checkout_show_origprice' 		=> VmConfig::get('checkout_show_origprice'),
		'coupons_enable' 				=> VmConfig::get('coupons_enable'),
		'oncheckout_show_register' 		=> VmConfig::get('oncheckout_show_register'),
		'loader_image_link'				=> 'plugins/system/onestepcheckout/assets/images/loader.gif',
		'set_coupon_link'				=> 'index.php?api_controller=checkout&api_ajax=set_coupon',
		'add_countries_link'			=> 'index.php?option=com_virtuemart&view=state&format=json&virtuemart_country_id=',
		'token'							=> JUtility::getToken(),
		'show_delivery_time'			=> $params->get('show_delivery_time'),
		'geoip'							=> $params->get('geoip')
);
$config = json_encode($keys);
?>
<script type="text/javascript">
var vmconfig = <?php echo !empty($config) ? $config:'{}';?>;
</script>
<?php if($params->get('show_ajax_loading',1)):?>
<style>
#general-ajax-load {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #CCCCCC;
    display: none;
    padding: 20px;
    position: fixed;
    top: 0;
    left: 0;
}
</style>
<div id="general-ajax-load">
	Loading
</div>
<?php endif;?>
<div class="cart-view" id="cart-view-3cols" data-layout="" data-user-guest="<?php echo JFactory::getUser()->guest;?>">
	<div class="cart-view-top">
		<div class="width50 floatleft">
			<h1><?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_TITLE'); ?></h1>
		</div>
		<div class="width50 floatleft right">
			<?php // Continue Shopping Button
			if ($this->continue_link_html != '') {
				echo $this->continue_link_html;
			} ?>
		</div>
	<div class="clear"></div>
	</div>
	<div id="login-pane">
		<?php if(JFactory::getUser()->id):?>
			<div class="user-login-action">
			<?php echo shopFunctionsF::getLoginForm($this->cart,false);?>
			</div>
		<?php endif;?>
		<p>Please fill in the fields below to complete your order.</p>
		<?php if(!JFactory::getUser()->id):?>
		<a href="javascript:void(0)" id="login-modal-trigger">Already registered? Click here to login.</a>
		<?php else:?>
		<?php endif;?>

	<div id="login-remind-modal" style="display: none;">
	<?php
		$lang = JFactory::getLanguage();
		$lang->load('com_users');
	?>
	<?php echo $this->loadTemplate('remind_password')?>
	</div>
	<div id="login-reset-modal" style="display: none;">
	<?php echo $this->loadTemplate('reset_password')?>
	</div>
	</div><!-- #login-pane -->
	<div id="login-modal" style="display: none;">
		<?php echo $this->loadTemplate('login_form')?>
	</div><!-- #login-modal -->

	<form method="post"
		id="checkoutForm"
		name="checkoutForm"
		action="<?php echo JRoute::_( 'index.php?option=com_virtuemart&view=cart'.$taskRoute,$this->useXHTML,$this->useSSL ); ?>">

		<div id="left-pane">
			<div id="billing_info">
				<?php echo $this->loadTemplate('billing_info');?>
			</div><!-- billing_info -->

			<div id="shipping_info">
				<?php echo $this->loadTemplate('"shipping_info"');?>
			</div><!-- shipping_info -->

		</div><!-- left-pane -->

		<div id="right-pane">
			<div id="right-pane-top">
				<div id="shipping_method">
					<?php echo $this->loadTemplate('shipping_method');?>
				</div><!-- shipping_method -->

				<div id="payment_method">
					<?php echo $this->loadTemplate('payment_method');?>
				</div><!-- payment_method -->
				<?php if($params->get('show_delivery_time',1)):?>
				<div id="delivery_time">
					<?php echo $this->loadTemplate('delivery_time');?>
				</div><!-- delivery_time -->
				<?php endif;?>
			</div><!--  right-pane-top -->
			<div class="clearfix"></div>
			<div id="alert-message" style="display: none"></div>
			<div class="clearfix"></div>
			<div id="right-pane-midle">
				<?php echo $this->loadTemplate('pricelist'); ?>
			</div><!--  right-pane-midle -->
			
			<div id="right-pane-bottom">
				<?php // Leave A Comment Field ?>
				<div class="customer-comment">
					<span class="comment"><?php echo JText::_('COM_VIRTUEMART_COMMENT_CART'); ?></span>
					<textarea class="customer-comment" name="customer_comment" cols="50" rows="4"><?php echo $this->cart->customer_comment; ?></textarea>
				</div>
				<?php // Leave A Comment Field END ?>
				<?php if($params->get('show_empty_cart_button')):?>
				<a id="empty_cart" href="javascript:void(0)" style="float:left;clear:both">Empty Cart</a>
				<?php endif;?>
				<?php // Continue and Checkout Button ?>
				<div class="checkout-button-top">

					<?php // Terms Of Service Checkbox
					if (!class_exists('VirtueMartModelUserfields')){
						require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'userfields.php');
					}
					$userFieldsModel = VmModel::getModel('userfields');
					if($userFieldsModel->getIfRequired('agreed')){
						$show_TOS = VmConfig::get('oncheckout_show_legal_info',1);
						$css_class = $show_TOS==1?'':'style="display:none"';
					    ?>
					    <label for ="tosAccepted">
					    <?php
						if(!class_exists('VmHtml'))require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'html.php');
						echo VmHtml::checkbox('tosAccepted',$this->cart->tosAccepted,1,0,'class="terms-of-service" '.$css_class);

						if($show_TOS){
						?>
						<div class="terms-of-service">
							<span class="terms-of-service" rel="facebox">
								<?php echo JText::_('COM_VIRTUEMART_CART_TOS_READ_AND_ACCEPTED'); ?>
								<span class="vm2-modallink"></span>
							</span>
							<div id="full-tos" style="display: none">
								<h2><?php echo JText::_('COM_VIRTUEMART_CART_TOS'); ?></h2>
								<?php echo $this->cart->vendor->vendor_terms_of_service;?>

							</div>
						</div>
						<?php } ?>
					    </label>
				    <?php
					}

					//echo $this->checkout_link_html;
					if (!VmConfig::get('use_as_catalog')) {
						echo '<a class="vm-button-correct" href="javascript:void(0);"><span>' . JText::_('COM_VIRTUEMART_ORDER_CONFIRM_MNU') . '</span></a>';
					}
					$text = JText::_('COM_VIRTUEMART_ORDER_CONFIRM_MNU');
					?>
				</div>
		<?php // Continue and Checkout Button END ?>
			</div><!-- right-pane-bottom -->

		</div><!-- right-pane -->

		<input type='hidden' name='task' value='confirm'/>
		<input type='hidden' name='option' value='com_virtuemart'/>
		<input type='hidden' name='view' value='cart'/>
	</form>

	<div class="cart-view-bottom">
	</div><!-- cart-view-bottom -->
</div>
<?php endif;?>

