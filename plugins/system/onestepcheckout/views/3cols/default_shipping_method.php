<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
/* var_dump($this->helper->shipments_shipment_rates);die; */
?>
<div class="shipment-pane">
	<div class="pane round-box">
		<h3 class="title">
			<span class="icon icon-three"></span>
			<?php echo JText::_('SYSTEM_ONESTEPCHECKOUT_SHIPPING_METHOD')?>
		</h3>
		<div class="pane-inner">
		<?php
		echo JText::_('COM_VIRTUEMART_CART_SELECTSHIPMENT');
		if(!empty($this->layoutName) && $this->layoutName=='default') {
			echo "<fieldset id='shipments'>";
			foreach($this->helper->shipments_shipment_rates as $rates) {
				echo str_replace("input",'input',$rates)."<br />";
			}
			echo "</fieldset>";
		} else {
			JText::_('COM_VIRTUEMART_CART_SHIPPING');
		}

		?>
		</div>
	</div>
</div>