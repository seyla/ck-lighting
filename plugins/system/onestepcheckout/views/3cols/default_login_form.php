<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

if(!class_exists('shopFunctionsF'))
	require(JPATH_VM_SITE.DS.'helpers'.DS.'shopfunctionsf.php');

$comUserOption = shopfunctionsF::getComUserOption();
if (empty($this->url)){
	$uri = JFactory::getURI();
	$url = $uri->toString(array('path', 'query', 'fragment'));
} else{
	$url = $this->url;
}

$user = JFactory::getUser();

if ($user->id == 0  ) {
	JHtml::_('behavior.formvalidation');
	JHTML::_ ( 'behavior.modal' );
}
?>
<form name="com-login"
	method="post"
	action="<?php echo JRoute::_('index.php', $this->useXHTML, $this->useSSL); ?>"
	id="com-form-login">
	<fieldset class="userdata">
		<p><?php echo JText::_('COM_VIRTUEMART_ORDER_CONNECT_FORM'); ?></p>
		<p id="com-form-login-username">
			<label for="com-login-username"><?php echo JText::_('COM_VIRTUEMART_USERNAME'); ?> <em>*</em></label>
			<input type="text"
				name="username"
				class="inputbox"
				size="18"
				alt="<?php echo JText::_('COM_VIRTUEMART_USERNAME'); ?>"
				value=""
			/>
			<div style="display:none" id="advice-required-entry-login-username" class="validation-advice">This is a required field.</div>
		</p>

		<p id="com-form-login-password">
			<label for="modlgn-passwd"><?php echo JText::_('COM_VIRTUEMART_PASSWORD'); ?> <em>*</em></label>
			<input
				id="modlgn-passwd"
				type="password"
				name="password"
				class="inputbox"
				size="18" alt="<?php echo JText::_('COM_VIRTUEMART_PASSWORD'); ?>"
				value=""
			/>
			<div style="display:none" id="advice-required-entry-login-password" class="validation-advice">This is a required field.</div>
		</p>

		<p id="com-form-login-remember">
            <?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
            <label for="remember"><?php echo $remember_me = JVM_VERSION===1? JText::_('Remember me') : JText::_('JGLOBAL_REMEMBER_ME') ?></label>
            <input type="checkbox" id="remember" name="remember" class="inputbox" value="yes" alt="Remember Me" />
            <?php endif; ?>
		</p>
	</fieldset>
	<div class="clr"></div>
	<div class="gray-line"></div>
    <div class="login-links">
       <a id="remind-password-trigger" href="<?php echo JRoute::_('index.php?option='.$comUserOption.'&view=remind'); ?>">
       <?php echo JText::_('COM_VIRTUEMART_ORDER_FORGOT_YOUR_USERNAME'); ?></a>
       <br>
       <a id="reset-password-trigger" href="<?php echo JRoute::_('index.php?option='.$comUserOption.'&view=reset'); ?>">
       <?php echo JText::_('COM_VIRTUEMART_ORDER_FORGOT_YOUR_PASSWORD'); ?></a>
    </div>
    <div class="login-action">
    	<em>* Required Fields</em>
		<input type="submit" name="Submit" class="default" value="<?php echo JText::_('COM_VIRTUEMART_LOGIN') ?>" />
     </div>
	<div class="clr"></div>

	<input type="hidden" value="user.login" name="task">
    <input type="hidden" name="option" value="<?php echo $comUserOption ?>" />
    <input type="hidden" name="return" value="<?php echo base64_encode($url) ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>
