<?php
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemOneStepCheckout extends JPlugin {
	function __construct($config,$params) {
		parent::__construct($config,$params);
	}

	public function onAfterDispatch()
	{
		// exit if in backend
		if(JFactory::getApplication()->isAdmin()) {
			return;
		}
		// exit if not in cart of virtuemart
		if((JRequest::getCmd('view')=='cart') or (JRequest::getCmd('view')=='pluginresponse')) {

			//load controller
			if(!class_exists('VirtueMartControllerCart'))
				require_once(JPATH_VM_SITE.DS.'controllers'.DS.'cart.php');
	
			$app 		= JFactory::getApplication();
			$doc 		= JFactory::getDocument();
			$theme		= $this->params->get('theme','default');
			$template	= $app->getTemplate();
			//remove old content
			$doc->setBuffer('','component');
	
			$_class 	= 'VirtueMartControllerCart';
			$_viewPath 	= JPATH_ROOT.DS.'plugins/system/onestepcheckout/views/'.$theme;
			$_templateViewPath 	= JPATH_ROOT.DS.'templates/'.$template.'/html/onestepcheckout/views/'.$theme;
	
			$controller = new $_class();
	
			$view = $controller->getView('cart', 'html');
			$view->addTemplatePath($_viewPath);
			$view->addTemplatePath($_templateViewPath);
	
			$lang = JFactory::getLanguage();
			$extension = 'com_virtuemart';
			$lang->load($extension);
			
			//get view content
			ob_start();
			$view->display();
			$content = ob_get_contents();
			ob_end_clean();
	
			//put new content into document
			$doc->setBuffer($content,'component');
		}
	}

	function onAfterRoute() {
		//exit if in backend
		if(JFactory::getApplication()->isAdmin()) {
			return;
		}
		if(JRequest::getCmd('api_controller')=='checkout') {
			define('JPATH_COMPONENT',JPATH_SITE.DS.'components'.DS.'com_virtuemart');
			if(!class_exists('CartHelper'))
				require_once JPATH_SITE.DS.'plugins/system/onestepcheckout/helpers/cart.php';
			//load helper
			$cartHelper = new CartHelper();
			switch(JRequest::getCmd('api_ajax')) {
				//process coupon
				case 'set_coupon':
					$json_result		= $cartHelper->setCoupon();
					echo json_encode($json_result);
					break;
				//process form
				case 'update_form':
					if(JRequest::getInt('update_address',1)==1) {
						$cartHelper->setAddress();
					}
					$json_result=$cartHelper->setPayment();
					if(is_array($json_result)) {
						echo json_encode(array('error'=>1,'message'=>implode($json_result)));
						break;
					}
					$json_result=$cartHelper->setShipment();
					if(is_array($json_result)) {
						echo json_encode(array('error'=>1,'message'=>implode($json_result)));
						break;
					}
					$cartHelper->lSelectShipment();
					$cartHelper->lSelectPayment();
					$data				= array();
					$data["shipments"]	= $cartHelper->shipments_shipment_rates;
					$data["payments"]	= $cartHelper->paymentplugins_payments;
					$data["price"]		= $cartHelper->getPrices();
					echo json_encode($data);
					break;
				//process update product
				case 'update_product':
					$cartHelper->setAddress();
					$cartHelper->updateProduct();
					$cartHelper->lSelectShipment();
					$cartHelper->lSelectPayment();
					$data				= array();
					$data["shipments"]	= $cartHelper->shipments_shipment_rates;
					$data["payments"]	= $cartHelper->paymentplugins_payments;
					$data["price"]		= $cartHelper->getPrices();
					echo json_encode($data);
					break;
				//process remove product
				case 'remove_product':
					$cartHelper->setAddress();
					$cartHelper->removeProduct();
					$cartHelper->lSelectShipment();
					$cartHelper->lSelectPayment();
					$data				= array();
					$data["shipments"]	= $cartHelper->shipments_shipment_rates;
					$data["payments"]	= $cartHelper->paymentplugins_payments;
					$data["price"]		= $cartHelper->getPrices();
					echo json_encode($data);
					break;

				//process user register
				case 'register':
					$json_result=$cartHelper->register();
					echo json_encode($json_result);
					break;
				// checkout ???
				case 'empty_cart':
					$cartHelper->emptyCart();
					break;

				case 'set_checkout':
					$data			= array();
					$data['status'] = 'success';
					$data['stsameasbt'] = $cartHelper->pre_checkout();
					echo json_encode($data);
					break;
			}
			JFactory::getApplication()->close();
		}
	}
}
?>
