<?php
/**
 * @version		$Id: sef.php 21097 2011-04-07 15:38:03Z dextercowley $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

/*
if(version_compare(JVERSION,'3.0.0','ge')) {
  if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR); 
  JLoader::register('JDate', JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'overrides'.DIRECTORY_SEPARATOR.'joomla3'.DIRECTORY_SEPARATOR.'date.php'); 
 // require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'overrides'.DIRECTORY_SEPARATOR.'joomla3'.DIRECTORY_SEPARATOR.'date.php'); 
// Joomla! 1.7 code here
}
*/

/**
 * Joomla! SEF Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	System.sef
 */
//require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'language.php'); 
class plgSystemOpc extends JPlugin
{
    public function onAfterRoute() {
	
	if (!file_exists(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'plugin.php')) return;
	require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'plugin.php'); 
	
	if (!OPCplugin::checkLoad()) return; 
	
	OPCplugin::loadShoppergroups();
	  
	if (!OPCplugin::isOPCcheckoutEnabled()) return;
	
	OPCplugin::getCache(); 

	  if (OPCplugin::alterActivation()) return; 
	  if (OPCplugin::alterRegistration()) return; 
	  
	  
	  
	  OPCplugin::enableSilentRegistration(); 
	  if (!OPCplugin::checkOPCtask()) return; 
	  OPCplugin::keyCaptchaSupport(); 
	  
	 
	  if (!OPCplugin::loadOPCcartView()) return; 
	  OPCplugin::fixVMbugVirtuemartUser(); 
	  OPCplugin::fixVMbugNewShippingAddress(); 
	  
	  OPCplugin::setItemid(); 
	  OPCplugin::loadOpcForLoggedUser(); 
	  OPCplugin::updateJoomlaCredentials(); 
	  OPCplugin::updateAmericanTax(); 

	
	}
	
	
	
	
	
	/**
	 * Converting the site URL to fit to the HTTP request
	 */
	public function onAfterRender()
	{


		
	  if (!file_exists(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'plugin.php')) return;
	  //if ($format != 'html') return;

		$app = JFactory::getApplication();

		if ($app->getName() != 'site') {
			return true;
		}
		$format = JRequest::getVar('format', 'html'); 
		if (!file_exists(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'language.php')) return;
		
		$task = JRequest::getVar('task', ''); 
		$view = JRequest::getCMD('view'); 
		
		 if(('com_virtuemart' == JRequest::getCMD('option') && !$app->isAdmin()) && (('cart'==$view) || ($view=='pluginresponse'))) 
		 {
			 $buffer = JResponse::getBody();
		if ($task != 'checkout')
		{
		//Replace src links
		$base	= JURI::base(true).'/';
		
		 //orig opc: 
		 $buffer = str_replace('$(".virtuemart_country_id").vm2', '// $(".virtuemart_country_id").vm2', $buffer); 
		 //$buffer = str_replace('$(".shipto_virtuemart_country_id").vm2', '// $(".virtuemart_country_id").vm2', $buffer); 
		 //$buffer = str_replace('$(".virtuemart_country_id").vm2front("list",{dest : "#virtuemart_state_id",ids : ""});', '', $buffer); 
		 //$buffer = str_replace('$("select.virtuemart_country_id").vm2front("list",{dest : "#virtuemart_state_id",ids : ""});', '', $buffer); 
		 $buffer = str_replace('$("select.virtuemart_country_id").vm2', '// $("select.virtuemart_country_id").vm2', $buffer); 
		 $buffer = str_replace('$("select.shipto_virtuemart_country_id").vm2', '// $("select.shipto_virtuemart_country_id").vm2', $buffer); 
		 //$("select.virtuemart_country_id")
		 $buffer = str_replace('$(".virtuemart_country_id").vm2front', '// $(".virtuemart_country_id").vm2front', $buffer); 
		 $buffer = str_replace('$("#virtuemart_country_id").vm2front', '// $("#virtuemart_country_id").vm2front', $buffer);
		 $buffer = str_replace('$("#shipto_virtuemart_country_id").vm2front', '// $("#shipto_virtuemart_country_id").vm2front', $buffer);
		 $buffer = str_replace('jQuery(\'#zip_field, #shipto_zip_field\')', '// jQuery(\'#zip_field, #shipto_zip_field\')', $buffer); 
		 //$buffer = str_replace('$(".vm-chzn-select").chosen', '// $(".vm-chzn-select").chosen', $buffer);
		 $buffer = str_replace('/plugins/vmpayment/klarna/klarna/assets/js/klarna_general.js', '/components/com_onepage/overrides/payment/klarna/klarna_general.js', $buffer); 
		 
		 $inside = JRequest::getVar('insideiframe', ''); 
		 if (!empty($inside))
		  {
		    $buffer = str_replace('<body', '<body onload="javascript: return parent.resizeIframe(document.body.scrollHeight);"', $buffer); 
		  }
		 //$buffer = str_replace('$(".virtuemart_country_id").vm2front("list",{dest : "#virtuemart_state_id",ids : ""});', '', $buffer); 
		$buffer = str_replace('jQuery("input").click', 'jQuery(null).click', $buffer);
		$buffer = str_replace('#paymentForm', '#adminForm', $buffer);
		
		/*
		if (isset(VmPlugin::$ccount))
	    $buffer .= '<script type="text/javascript">console.log("positive cache: '.VmPlugin::$ccount.'")</script>';
		*/
		
		 }
		    if (class_exists('plgSystemBit_vm_change_shoppergroup'))
			{
				$js_text = "<script type=\"text/javascript\">location.reload()</script>";
				//$js_text = "location.reload()";
				$c = 0; 
				$buffer = str_replace($js_text, '', $buffer, $c); 
				
				 
				
			}
			
			JResponse::setBody($buffer);
		
		
		}

		
		
		return true;
	}
	// we will disable the 
	public function plgVmInterpreteMathOp2($calc, $rule, $price, $revert)
	{
		return false; 
	}
	
	public function plgVmonSelectedCalculatePriceShipment(VirtueMartCart $cart, &$cart_prices, &$cart_prices_name) {
	  
	  if (!empty($cart->virtuemart_shipmentmethod_id))
	  if ($cart->virtuemart_shipmentmethod_id < 0)
	  if (class_exists('OPCcache'))
	  if (!empty(OPCcache::$cachedResult['currentshipping']))
	  {
		return OPCcache::getStoredCalculation($cart, $cart_prices, $cart_prices_name); 
	  }
	}
	// triggered from: \administrator\components\com_virtuemart\models\orders.php
	public function plgVmOnUserOrder(&$_orderData)
	{
		// fix vm2.0.22 bug
		if (empty($_orderData->order_payment) && (empty($_orderData->order_payment_tax)))
		{
			
		 if (!class_exists('VirtueMartCart'))
		 require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');

			
					$cart = VirtueMartCart::getCart();
					$prices = $cart->getCartPrices();
					if (!empty($prices['salesPricePayment']))
					{
						$_orderData->order_payment = (float)$prices['salesPricePayment'];
					}
			
		}
	}
	function plgVmOnUserStore(&$data)
	{
	  
	  //if ((empty($data['username'])) && (!empty($data['email']))) $data['username'] = $data['email']; 
	}
	public function plgVmRemoveCoupon($_code, $_force)
	{
	   if (!file_exists(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'plugin.php')) return;
	   if (empty($_force))
	    {
		   include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
		   if (!empty($do_not_allow_gift_deletion)) return true; 
		}
		return null; 
	}
	public function plgVmOnUpdateOrderPayment(&$data,$old_order_status)
	{
	  if (!file_exists(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'plugin.php')) return;
	  require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'plugin.php'); 
	  OPCplugin::checkGiftCoupon($data, $old_order_status);  
	  
	  
	}
	
}
