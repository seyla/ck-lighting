<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class  plgSystemJQuery extends JPlugin
{
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	public function onAfterRoute()
	{
		if (JFactory::getApplication()->isAdmin())
		{
			return true;
		}
		if($this->params->get("load_mootools",1)){
			/* The following line loads the MooTools JavaScript Library */
			JHtml::_('behavior.framework', true);
		}
		
		JHtml::_('jquery.framework');
		
		if($this->params->get("load_jquery_ui",1)){
			/* The following line loads the jQuery UI JavaScript Library */
			JHtmlJquery::ui(array('all'));
		}
	}
}
