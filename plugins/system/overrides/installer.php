<?php
defined ('_JEXEC') or die('Restricted access');


defined ('DS') or define('DS', DIRECTORY_SEPARATOR);

@ini_set ('memory_limit', '32M');
@ini_set ('max_execution_time', '120');


	class plgSystemOverridesInstallerScript {

		public function preflight () {

		}

		public function install () {
			$name = 'plg_system_overrides';
			$published = 1;
				
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('extension_id');
			$query->from('#__extensions');
			$query->where('name='.$db->quote($name));
			$db->setQuery($query);
			$r = $db->loadResult();
			if ($r)
			{
				$query = $db->getQuery(true);
				$query->update($db->quoteName('#__extensions'))
						->set($db->quoteName('enabled').' = '.$db->quote($published))
						->where($db->quoteName('name').' = '.$db->quote($name));
				$db->setQuery($query);
				$db->query();
			}
		}

		public function discover_install () {
		}

		public function postflight () {
			$this->addonsInstall();
		}

		public function addonsInstall () {

		}

		private function recurse_copy ($src, $dst) {

			$dir = opendir ($src);
			$this->createIndexFolder ($dst);

			if (is_resource ($dir)) {
				while (FALSE !== ($file = readdir ($dir))) {
					if (($file != '.') && ($file != '..')) {
						if (is_dir ($src . DS . $file)) {
							$this->recurse_copy ($src . DS . $file, $dst . DS . $file);
						} else {
							if (JFile::exists ($dst . DS . $file)) {
								if (!JFile::delete ($dst . DS . $file)) {
									$app = JFactory::getApplication ();
									$app->enqueueMessage ('Couldnt delete ' . $dst . DS . $file);
								}
							}
							if (!JFile::move ($src . DS . $file, $dst . DS . $file)) {
								$app = JFactory::getApplication ();
								$app->enqueueMessage ('Couldnt move ' . $src . DS . $file . ' to ' . $dst . DS . $file);
							}
						}
					}
				}
				closedir ($dir);
				if (is_dir ($src)) {
					//JFolder::delete ($src);
				}
			} else {
				$app = JFactory::getApplication ();
				$app->enqueueMessage ('Couldnt read dir ' . $dir . ' source ' . $src);
			}

		}


		public function uninstall () {

			return TRUE;
		}

		/**
		 * creates a folder with empty html file
		 *
		 * @author Max Milbers
		 *
		 */
		public function createIndexFolder ($path) {

			if (JFolder::create ($path)) {
				if (!JFile::exists ($path . DS . 'index.html') && $path != JPATH_ROOT) {
					JFile::copy (JPATH_ROOT . DS . 'components' . DS . 'index.html', $path . DS . 'index.html');
				}
				return TRUE;
			}
			return FALSE;
		}

		public function createJUIFolder ($path=""){
			if(empty($path)){
				$path = JPATH_SITE . DS .'media' . DS .'jui';
			}
			$app = JFactory::getApplication ();

			if(!JFolder::exists($path)) {
				if(!$this->createIndexFolder($path)) {
					$app->enqueueMessage ('Couldnt create dir ' . $path );
				} else {
					$app->enqueueMessage ('Created dir ' . $path );
				}
			}

		}

	}//class


// pure php no tag
