<?php
/**
 * @version		opctracking.php 
 * @copyright	Copyright (C) 2005 - 2013 RuposTel.com
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgSystemOpctracking extends JPlugin
{
    public function onAfterRoute() {
	  JHTML::script('opcping.js', 'components/com_onepage/assets/js/', false);
	}
	
	static $delay; 
	public function plgVmConfirmedOrder($cart, $data)
	{
	  if (!self::check()) return; 
	  if (defined('OPCTRACKINGORDERCREATED')) return; 
	  else define('OPCTRACKINGORDERCREATED', 1); 
	  
	  if (!is_object($data))
	  if (isset($data['details']['BT']))
	  {
	   //self::$delay = true; 
	   $order = $data['details']['BT']; 
	   self::orderCreated($order, 'P');  
	  }
	}
	
	public function plgVmOnUpdateOrderPayment(&$data,$old_order_status)
	{
	  if (!self::check()) return; 
	  if (defined('OPCTRACKINGORDERCREATED')) return; 
	  else define('OPCTRACKINGORDERCREATED', 1); 
	  
	  self::orderCreated($data, $old_order_status);  
	  
	  
	}
	//$returnValues = $dispatcher->trigger('plgVmOnCheckoutAdvertise', array( $this->cart, &$checkoutAdvertise));
	
	public function plgVmOnCheckoutAdvertise($cart, &$html)
	{
	
	  // we will create hash only when cart view calls checkoutAdvertise
	  $this->registerCart(); 
	}
	
	private function orderCreated(&$data, $old_order_status)
	{
	  
	  $hash2 = uniqid('opc', true); 
	  $hashn = JApplication::getHash('opctracking'); 
	  $hash = JRequest::getVar($hashn, $hash2, 'COOKIE'); 
      if ($hash2 == $hash) 
	  OPCtrackingHelper::setCookie($hash); 
	  
	    
		OPCtrackingHelper::orderCreated($hash, $data, $old_order_status); 
	
	    
	}
	
	public function plgVmOnUpdateOrderShipment(&$data,$old_order_status)
	{
	  if (defined('OPCTRACKINGORDERCREATED')) return; 
	  else define('OPCTRACKINGORDERCREATED', 1); 
	  
	  if (!self::check()) return; 
	  self::orderCreated($data, $old_order_status);  
	    
	   
		
	}
	
	private function check()
	{
	  	$app = JFactory::getApplication();
		if ($app->getName() != 'site') {
			return false;
		}
		if (!file_exists(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opctracking.php')) return false;
		
		$format = JRequest::getVar('format', 'html'); 
		if ($format != 'html') return false;

		$doc = JFactory::getDocument(); 
		$class = strtoupper(get_class($doc)); 
		if ($class != 'JDOCUMENTHTML') return false; 
		
		if(version_compare(JVERSION,'3.0.0','ge')) 
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'compatibilityj3.php'); 
		else
	    require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'compatibilityj2.php'); 

		
		require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opctracking.php'); 
		
		return true; 

	}
	
	public function registerCart()
	{
	   if (!self::check()) return; 
	   $hash2 = uniqid('opc', true); 
	   $hashn = JApplication::getHash('opctracking'); 
	   $hash = JRequest::getVar($hashn, $hash2, 'COOKIE'); 
	   if ($hash2 == $hash)
	   {
	   // create new cookie if not set
	   OPCtrackingHelper::setCookie($hash); 
	   }
	   
	   OPCtrackingHelper::registerCart($hash); 
	}
	
	public function onAfterRender()
	{
	   //if (!empty(self::$delay)) return; 
	   if (!self::check()) return; 
	   
	   
	   if (!file_exists(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opctracking.php')) return false;
	   require_once(JPATH_SITE.DS.'components'.DS.'com_onepage'.DS.'helpers'.DS.'opctracking.php'); 
	   
	   //if (!class_exists('OPCtracking')) return; 
	   
	   
	   
	   if (!OPCtrackingHelper::checkStatus()) return; 
	   
	   if (empty(OPCtrackingHelper::$html)) return; 
	   $html = OPCtrackingHelper::$html; //OPCtrackingHelper::getHTML(); 
	   
	   $buffer = JResponse::getBody();
	   $bodyp = stripos($buffer, '</body'); 
	   $buffer = substr($buffer, 0, $bodyp).$html.substr($buffer, $bodyp); 
	   
	   
	   JResponse::setBody($buffer);
	   
	}

		
}
