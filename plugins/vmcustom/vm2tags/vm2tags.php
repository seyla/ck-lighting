<?php
/**
 * plg_vmcustom_vm2tags
 * @copyright Copyright (C) Adrien Roussel www.nordmograph.com
 * @license http://www.nordmograph.com/en/licence.html GNU GENERAL PUBLIC LICENSE Version 3
 */
 error_reporting('E_ALL');
defined('_JEXEC') or 	die( 'Direct Access to ' . basename( __FILE__ ) . ' is not allowed.' ) ;
if (!class_exists('vmCustomPlugin')) require(JPATH_VM_PLUGINS . DS . 'vmcustomplugin.php');
class plgVmCustomVm2tags extends vmCustomPlugin {
	function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
		$this->_tablepkey = 'id';
		$this->tableFields = array_keys($this->getTableSQLFields());
		$varsToPush = array(
			'maxtags'=>array('','int')
		);
		$this->setConfigParameterable('custom_params',$varsToPush);
	}
	protected function getVmPluginCreateTableSQL() {
		return $this->createTableSQL('Product Tags Table');
	}	
	function getTableSQLFields() {
		$SQLfields = array(
	    'id' => 'int(11) unsigned NOT NULL AUTO_INCREMENT',
	    'virtuemart_product_id' => 'int(11) UNSIGNED DEFAULT NULL',
	    'virtuemart_custom_id' => 'int(11) UNSIGNED DEFAULT NULL'
		);
		return $SQLfields;
	}
	function plgVmOnDisplayProductVariantFE($field,&$row,&$group) {
		return '';
	}
	function plgVmOnProductEdit($field, $product_id, &$row,&$retValue) {
		if ($field->custom_element != $this->_name) return '';
		// $this->tableFields = array ( 'id', 'virtuemart_custom_id', 'custom_specification_default1', 'custom_specification_default2' );
		$this->parseCustomParams($field);
		$this->getPluginProductDataCustom($field, $product_id);
		$doc				=& JFactory::getDocument();
		$juri 				= JURI::base();
		
		//$doc->addScript(JURI::root(true).'/plugins/vmcustom/vm2tags/js/jquery.tagsinput.min.js');
		//$doc->addScript('https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js');
		//$doc->addStyleSheet(JURI::root(true).'/plugins/vmcustom/vm2tags/js/jquery.tagsinput.css');
		
		
		//$doc->addScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js');
		//$doc->addStyleSheet('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css');
		//$tag_script=" $(function() { 	$('#custom_param['.$row.'][product_tags]').tagsInput({width:'auto'}); 	});";
	//	$doc->addScriptDeclaration($tag_script);
		
		
		$html ='';
		//if ($field->custom_value != $this->_pname) return '';
		//$plgParam = $this->getVmCustomParams($field->virtuemart_custom_id);
		//if (empty($param)) {
		//	$param['sound_url']= $group->sound_url');
		//}
		if (empty($field->product_tags) )			$product_tags = '';
		else $product_tags = $field->product_tags;
		
		$html .= JText::_('VMCUSTOM_VM2TAGS_SEPARATEDTAGS');
		$html .=' <input type="text" value="'.$product_tags.'" size="80" name="custom_param['.$row.'][product_tags]" id="custom_param['.$row.'][product_tags]" class="tags" /> ';
		
		
		
		
		
		
		

		$retValue .= $html  ;
		$row++;
		return true  ;
	}
	/**
	 * @ idx plugin index
	 * @see components/com_virtuemart/helpers/vmCustomPlugin::onDisplayProductFE()
	 * @author Patrick Kohl
	 * eg. name="customPlugin['.$idx.'][comment] save the comment in the cart & order
	 */
	function plgVmOnDisplayProductFE( $product, &$idx,&$group){
		$this->parseCustomParams($group);
		$this->getPluginProductDataCustom($group, $product->virtuemart_product_id);
		
		
	//	if ($field->custom_value != $this->_pname) return '';
		//$plgParam = $this->getVmCustomParams($field->virtuemart_custom_id);
		
		$juri 				= JURI::base();
		$db 				=& JFactory::getDBO();
		$doc				=& JFactory::getDocument();
		$doc->addStylesheet($juri.'components/com_vm2tags/assets/css/style.css');
		$html = '';
		$cparams 					=& JComponentHelper::getParams('com_vm2tags');
		$mode	 					= $cparams->getValue('mode','1');
		$vm2tags_itemid 					= $cparams->getValue('vm2tags_itemid');
		if($vm2tags_itemid=='')
			$vm2tags_itemid = JRequest::getVar('Itemid');

		$maxtags		= $group->maxtags;
		
		$q=" SELECT `custom_param` 
		FROM `#__virtuemart_product_customfields` 
		WHERE `custom_value`='vm2tags' 
		AND `virtuemart_product_id`='".$product->virtuemart_product_id."'  
		AND `virtuemart_customfield_id` =".$group->virtuemart_customfield_id." 
		ORDER BY `virtuemart_customfield_id` ASC";
		$db->setQuery($q);
		$product_tags = $db->loadResult();
		
		
		$obj = json_decode($product_tags);
		$sep_tags = $obj->{'product_tags'};
		$sep_tags = explode(',',$sep_tags);
		$product_tags = '';
			
		foreach($sep_tags as $sep_tag){
			
			$sep_tag = strtolower($sep_tag);
			//$sep_tag = str_replace(' ','',$sep_tag);
			if($mode==1)
				$tag_url = JRoute::_('index.php?option=com_vm2tags&view=productslist&tag='.$sep_tag.'&Itemid='.$vm2tags_itemid  );
			else
				$tag_url = JRoute::_('index.php?searchword='.$sep_tag.'&ordering=newest&searchphrase=exact&option=com_search&Itemid='.$vm2tags_itemid  );
			if($sep_tag!='')
				$product_tags .= '<a class="vm_tag" href="'.$tag_url.'">'.$sep_tag.'</a> ; ';
			
		}
		
	
		$html .= '<div class="product_tags">';
		$html .= $product_tags ;
		$html .= '</div>';

		if($product_tags !=''){
			$group->display .= $html;
			return true;
		}
    }

	function plgVmOnStoreProduct($data,$plugin_param){
		$this->tableFields = array ( 'id', 'virtuemart_product_id', 'virtuemart_custom_id' );

		return $this->OnStoreProduct($data,$plugin_param);
	}
	/**
	 * We must reimplement this triggers for joomla 1.7
	 * vmplugin triggers note by Max Milbers
	 */
	protected function plgVmOnStoreInstallPluginTable($psType) {
		return $this->onStoreInstallPluginTable($psType);
	}

	function plgVmSetOnTablePluginParamsCustom($name, $id, &$table){
		return $this->setOnTablePluginParams($name, $id, $table);
	}

	function plgVmDeclarePluginParamsCustom($psType,$name,$id, &$data){
		return $this->declarePluginParams($psType, $name, $id, $data);
	}

	/**
	 * Custom triggers note by Max Milbers
	 */
	function plgVmOnDisplayEdit($virtuemart_custom_id,&$customPlugin){
		return $this->onDisplayEditBECustom($virtuemart_custom_id,$customPlugin);
	}

}

// No closing tag
	