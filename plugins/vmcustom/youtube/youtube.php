<?php
/*------------------------------------------------------------------------
# Youtube Video - Virtuemart CustomField Plugin
# ------------------------------------------------------------------------
# author    Team HexSys
# copyright Copyright (C) 2013 wwww.hexsystechnologies.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.hexsystechnologies.com
# Technical Support:  Forum - http://www.hexsystechnologies.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die('Restricted access');

if (!class_exists('vmCustomPlugin')) require(JPATH_VM_PLUGINS . DS . 'vmcustomplugin.php');

class plgVmCustomYoutube extends vmCustomPlugin {


	// instance of class
// 	public static $_this = false;

	function __construct(& $subject, $config) {
// 		if(self::$_this) return self::$_this;
		parent::__construct($subject, $config);
		
		$this->_tablepkey = 'id';
		$this->tableFields = array_keys($this->getTableSQLFields());
		$varsToPush = array( 'u_url'=>array('','string'),
							 'u_width'=>array('400px','string'),
							 'u_height'=>array('250px','string'),
							 'u_fscreen'=>array('1','int'));

		$this->setConfigParameterable('custom_params',$varsToPush);

	}

	// get product param for this plugin on edit
	function plgVmOnProductEdit($field, $product_id, &$row,&$retValue) {
		if ($field->custom_element != $this->_name) return '';
		// $html .='<input type="text" value="'.$field->custom_size.'" size="10" name="custom_param['.$row.'][custom_size]">';
		$this->parseCustomParams($field);

		$html ='
				<table class="admintable">
					'.VmHTML::row('input','U_URL','custom_param['.$row.'][u_url]',$field->u_url).
					VmHTML::row('input','VMCUSTOM_WIDTH','custom_param['.$row.'][u_width]',$field->u_width).
					VmHTML::row('input','VMCUSTOM_HEIGHT','custom_param['.$row.'][u_height]',$field->u_height).
					VmHTML::row('radioList','VMCUSTOM_FULLSCREEN','custom_param['.$row.'][u_fcreen]',$field->u_fscreen, array(1=>JText::_('JYES'), 0=> JText::_('JNO'))).
				'</table>';
		$retValue .= $html;
		$row++;
		return true ;
	}

	function plgVmOnDisplayProductFE( $product, &$idx,&$group) {
		
		// default return if it's not this plugin
		if ($group->custom_element != $this->_name) return '';

		$this->_tableChecked = true;
		$this->getCustomParams($group);
		//$this->getPluginCustomData($group, $product->virtuemart_product_id);

		// Here the plugin values
		//$html =JTEXT::_($group->custom_title) ;

		$group->display .=  $this->renderByLayout('default',array($this->params,&$idx,&$group ) );

		return true;
		
	}


}

// No closing tag