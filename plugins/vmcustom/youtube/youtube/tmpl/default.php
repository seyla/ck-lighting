<?php
/*------------------------------------------------------------------------
# Youtube Video - Virtuemart CustomField Plugin
# ------------------------------------------------------------------------
# author    Team HexSys
# copyright Copyright (C) 2013 wwww.hexsystechnologies.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.hexsystechnologies.com
# Technical Support:  Forum - http://www.hexsystechnologies.com/support-forum
-----------------------------------------------------------------------*/
// No direct access
defined('_JEXEC') or die('Restricted access');

?>

<iframe width="<?php echo $this->params->u_width; ?>" height="<?php echo $this->params->u_height; ?>" src="<?php echo $this->params->u_url; ?>" frameborder="0" <?php if($this->params->u_fscreen) echo 'allowfullscreen'; ?>></iframe>