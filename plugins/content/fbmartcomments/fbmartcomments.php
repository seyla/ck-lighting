<?php
 /**
 * @package VirtueMart Plugins
 * @author ITechnoDev, LLC
 * @copyright (C) 2014 - ITechnoDev, LLC
 * @license GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 **/

// No direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php');

class plgContentfbmartcomments extends JPlugin
{
	public function onContentPrepare($context, &$row, &$params, $page = 0)
	{	
		if($context == 'com_virtuemart.productdetails')
		{ 	
 
			$doc	 	=&	JFactory::getDocument();
			$config 	=& 	JFactory::getConfig();
			
			
			$uri		= 	JURI::getInstance();
			$root		= 	$uri->toString( array('scheme', 'host', 'port'));
			//$url	 	= 	$root . $_SERVER['REQUEST_URI'];
			
			$lang		=	&JFactory::getLanguage();
			$lang_tag	=	$lang->getTag();
			$lang_tag	=	str_replace("-","_",$lang_tag);
		
			
			
			$vmConfig 		= VmConfig::loadConfig();
			$productModel 	= VmModel::getModel('Product');
			$product 		= $productModel->product;
			$productModel->addImages($product);
			$product 		= $productModel->product;
			
			$baseurl = JURI::base();
			$imageObj  = $product->images[0];
			$imagePath = $baseurl.$imageObj->file_url;
			$prodName  = $product->product_name;
			
			$url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id,true,-1);
 
			
			$width 			= 	 $this->params->get('width');
			$num_posts 		= 	 $this->params->get('num_posts');
			$app_id 		= 	 $this->params->get('app_id','');
			$colorscheme 	=    $this->params->get('colorscheme');
			$mobile		    = 	 $this->params->get('mobile');
			
			$mail_to		=	 $this->params->get('mail_to');
			$mail_subject	=	 $this->params->get('mail_subject','New Facebook Comment');
			$og_url			=	 $this->params->get('og_url');

			
			$enable_email   = 	 $this->params->get('enable_email',0);
			
			if ($mobile=='0') $mobile='';
			if ($mobile=='1') $mobile='false';
			if ($mobile=='2') $mobile='true';
				
			if ($app_id!="") 
			{
				$doc->addCustomTag('<meta property="fb:app_id" content="'.$app_id.'"/>');
			}

			if ($og_url=="1")
			{	
				$doc->addCustomTag('<meta property="og:url" content="'.$url.'"/>');
				$doc->addCustomTag('<meta property="og:site_name" content="'.$config->getValue('config.sitename').'"/>');
				$doc->addCustomTag('<meta property="og:title" content="'.$prodName.'"/>');
				$doc->addCustomTag('<meta property="og:locale" content="'.$lang_tag.'"/>');
				$doc->addCustomTag('<meta property="og:type"  content="article"/>');
				$doc->addCustomTag('<meta property="og:image" content="'.$imagePath.'"/>');
			}
			
			
			
 
			 $sendmailphp	=	JURI::base();
		     $sendmailphp  .=	"plugins".DS."content".DS."fbmartcomments".DS."sendmail.php";
			 
			 $src = "";
			 
			if ($mail_to != "" && $enable_email==1)
			{
					$mail_body 	  = "A new comment has been posted";
					$sendmailphp .= "?body=".urlencode($mail_body);
					$sendmailphp .= "&to=".urlencode($mail_to)."&subject=".urlencode($mail_subject)."&url=".urlencode($url);
					
					
					$src =	"<div id=\"fb-root\"></div>\n";
					$src.=  "<script>\n" ;
					
					$src.=	"function sendmail(cid) {\n";
					$src.=	"var xmlhttp;\n";
					$src.=	"var ps = '".$sendmailphp."&cid='+cid;\n";
					$src.=	"if (window.XMLHttpRequest) {xmlhttp=new XMLHttpRequest();} else {xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');}\n";
					$src.=	"xmlhttp.open('GET',ps,true);\n";
					$src.=	"xmlhttp.send();\n";
					$src.=	"};\n";
					
					$src.= " window.fbAsyncInit = function() {\n" ;
					$src.=	"FB.init({\n" ;
					
					if ($app_id!="")
					{
						$src.=	  "appId      : '$app_id',\n"  ;
					}
					
					
					
					$src.=	  "status     : true, \n" ;
					$src.=	  "xfbml      : true   \n" ;
					$src.=	"});\n" ;
					$src.=	"FB.Event.subscribe('comment.create', function (response) {sendmail(response.commentID);});\n";
					$src.=  "};\n" ;
					$src.=  "(function(d, s, id){\n";
					$src.=	"var js, fjs = d.getElementsByTagName(s)[0];\n";
					$src.=		"if (d.getElementById(id)) {return;}\n";
					$src.=		"js = d.createElement(s); js.id = id;\n";
					$src.=		 "js.src = \"//connect.facebook.net/$lang_tag/all.js\";\n";
					$src.=		"fjs.parentNode.insertBefore(js, fjs);\n";
					$src.=	 "}(document, 'script', 'facebook-jssdk'));\n";
					$src.=	"</script>\n";
			}
			else 
			{
						$src = "<div id=\"fb-root\"></div>\n";
						$src.= "<script>\n";
						$src.= "(function(d, s, id) {\n";
						$src.=   "var js, fjs = d.getElementsByTagName(s)[0];\n";
						$src.=   "if (d.getElementById(id)) return;\n";
						$src.=   "js = d.createElement(s); js.id = id;\n";
						  
						$src.=   "js.src =   \"//connect.facebook.net/$lang_tag/all.js#xfbml=1\" ;" ;  
						  
						$src.=   "fjs.parentNode.insertBefore(js, fjs);\n";
						$src.= "}(document, 'script', 'facebook-jssdk'));\n";
						$src.= "</script>\n";	
			}
 

			$src .= "\n<div class=\"fb-comments\" data-href=\"$url\" data-width=\"$width\" data-mobile=\"$mobile\"  data-colorscheme=\"$colorscheme\" data-num-posts=\"$num_posts\"></div>" ;
	
	
					$row->text .= $src;
		}	
	}
}
?>
